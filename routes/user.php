<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('user')->user();

    //dd($users);

    $posts = \App\Post::orderBy('post_id', 'desc')->paginate(3);
//    $posts->withPath('coach/home');
    return view('user.home',compact('posts'));
})->name('home');
Route::get('/home', 'StudentController@index')->name('home');
