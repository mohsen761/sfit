<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/bodybuilding/admin/coachCheckout','AdminApiController@coachCheckout');
Route::get('/bodybuilding/admin/coaches','AdminApiController@coaches');
Route::get('/bodybuilding/admin/coachInformation','AdminApiController@coachInformation');
Route::get('/bodybuilding/admin/delete_file','AdminApiController@delete_file');
Route::get('/bodybuilding/admin/get_post_comments','AdminApiController@get_post_comments');
Route::get('/bodybuilding/admin/getCheckouts','AdminApiController@getCheckouts');
Route::get('/bodybuilding/admin/getPosts','AdminApiController@getPosts');
Route::get('/bodybuilding/admin/getSubscriptionInformation','AdminApiController@getSubscriptionInformation');
Route::get('/bodybuilding/admin/getUserInformation','AdminApiController@getUserInformation');
Route::get('/bodybuilding/admin/getUsersInformation','AdminApiController@getUsersInformation');
Route::get('/bodybuilding/admin/gyms','AdminApiController@gyms');
Route::get('/bodybuilding/admin/keys','AdminApiController@keys');
Route::get('/bodybuilding/admin/order','AdminApiController@order');
Route::post('/bodybuilding/admin/set_like_comment','AdminApiController@set_like_comment');
Route::post('/bodybuilding/admin/setCoachAccess','AdminApiController@setCoachAccess');
Route::post('/bodybuilding/admin/setCoachVisibility','AdminApiController@setCoachVisibility');
Route::post('/bodybuilding/admin/setGymVisibility','AdminApiController@setGymVisibility');
Route::post('/bodybuilding/admin/uploadPost','AdminApiController@uploadPost');


Route::get('/bodybuilding/coach/gym/getMyGymImages','CoachGymApiController@getMyGymImages');
Route::get('/bodybuilding/coach/gym/getMyGymInformation','CoachGymApiController@getMyGymInformation');
Route::get('/bodybuilding/coach/gym/my_gyms','CoachGymApiController@my_gyms');
Route::post('/bodybuilding/coach/gym/send_gym_time_and_day_with_array','CoachGymApiController@send_gym_time_and_day_with_array');
Route::post('/bodybuilding/coach/gym/sendGymInformation','CoachGymApiController@sendGymInformation');
Route::post('/bodybuilding/coach/gym/sendGymTimeAndDay','CoachGymApiController@sendGymTimeAndDay');
Route::post('/bodybuilding/coach/gym/setGymLocation','CoachGymApiController@setGymLocation');


Route::get('/bodybuilding/coach/program/deleteExercise','CoachProgramApiController@deleteExercise');
Route::get('/bodybuilding/coach/program/deleteProgram','CoachProgramApiController@deleteProgram');
Route::get('/bodybuilding/coach/program/deleteSupplement','CoachProgramApiController@deleteSupplement');
Route::get('/bodybuilding/coach/program/exercise','CoachProgramApiController@exercise');
Route::get('/bodybuilding/coach/program/food','CoachProgramApiController@food');
Route::get('/bodybuilding/coach/program/getMyProgramDate','CoachProgramApiController@getMyProgramDate');
Route::get('/bodybuilding/coach/program/getProgramDate','CoachProgramApiController@getProgramDate');
Route::get('/bodybuilding/coach/program/getProgramDetails','CoachProgramApiController@getProgramDetails');
Route::post('/bodybuilding/coach/program/insert_program','CoachProgramApiController@insert_program');
Route::post('/bodybuilding/coach/program/send_program','CoachProgramApiController@send_program');
Route::post('/bodybuilding/coach/program/sendExercise','CoachProgramApiController@sendExercise');
Route::post('/bodybuilding/coach/program/sendFood','CoachProgramApiController@sendFood');
Route::post('/bodybuilding/coach/program/sendSupplement','CoachProgramApiController@sendSupplement');
Route::post('/bodybuilding/coach/program/setProgramState','CoachProgramApiController@setProgramState');
Route::post('/bodybuilding/coach/program/setProgramType','CoachProgramApiController@setProgramType');
Route::get('/bodybuilding/coach/program/supplement','CoachProgramApiController@supplement');


Route::get('/bodybuilding/gym/delete_gym','GymApiController@delete_gym');
Route::get('/bodybuilding/gym/getGymDaysAndTimes','GymApiController@getGymDaysAndTimes');
Route::get('/bodybuilding/gym/getGymImages','GymApiController@getGymImages');
Route::get('/bodybuilding/gym/gymInformation','GymApiController@gymInformation');
Route::get('/bodybuilding/gym/gyms','GymApiController@gyms');
Route::get('/bodybuilding/gym/my_gyms','GymApiController@my_gyms');
Route::post('/bodybuilding/gym/send_gym_time_and_day_with_array','GymApiController@send_gym_time_and_day_with_array');
Route::post('/bodybuilding/gym/sendGymInformation','GymApiController@sendGymInformation');
Route::post('/bodybuilding/gym/sendGymTimeAndDay','GymApiController@sendGymTimeAndDay');
Route::post('/bodybuilding/gym/setGymLocation','GymApiController@setGymLocation');
Route::post('/bodybuilding/gym/uploadGymImages','GymApiController@uploadGymImages');
Route::get('/bodybuilding/gym/workouts/find_movement','GymApiController@find_movement');
Route::get('/bodybuilding/gym/workouts/get_workouts','GymApiController@get_workouts');
Route::post('/bodybuilding/gym/workouts/send_movement','GymApiController@send_movement');
Route::post('/bodybuilding/gym/music/listOfMusic','GymApiController@listOfMusic');

Route::get('/test','CoachApiController@test');
Route::post('/bodybuilding/coach/loginCoaches','CoachApiController@loginCoaches');
Route::post('/bodybuilding/coach/coachEvidence','CoachApiController@coachEvidence');
Route::get('/bodybuilding/coach/findCoachImageProfile','CoachApiController@findCoachImageProfile');
Route::get('/bodybuilding/coach/forceUpdate','CoachApiController@forceUpdate');
Route::get('/bodybuilding/coach/getBalance','CoachApiController@getBalance');
Route::get('/bodybuilding/coach/getCoachImages','CoachApiController@getCoachImages');
Route::get('/bodybuilding/coach/getCoachInformation','CoachApiController@getCoachInformation');
Route::get('/bodybuilding/coach/getCoachStory','CoachApiController@getCoachStory');
Route::get('/bodybuilding/coach/getMyGymImages','CoachApiController@getMyGymImages');
Route::get('/bodybuilding/coach/getMyGymInformation','CoachApiController@getMyGymInformation');
Route::get('/bodybuilding/coach/getPrices','CoachApiController@getPrices');
Route::get('/bodybuilding/coach/getStudentFigureImages','CoachApiController@getStudentFigureImages');
Route::get('/bodybuilding/coach/getStudentFigureImagesDate','CoachApiController@getStudentFigureImagesDate');
Route::get('/bodybuilding/coach/getSubscriptionDate','CoachApiController@getSubscriptionDate');
Route::get('/bodybuilding/coach/getUserDeviceId','CoachApiController@getUserDeviceId');
Route::get('/bodybuilding/coach/getUserInformation','CoachApiController@getUserInformation');
Route::get('/bodybuilding/coach/getUsersInformation','CoachApiController@getUsersInformation');
Route::post('/bodybuilding/coach/requestCheckout','CoachApiController@requestCheckout');
Route::get('/bodybuilding/coach/RequestsForCoach','CoachApiController@RequestsForCoach');
Route::post('/bodybuilding/coach/send_program','CoachApiController@send_program');
Route::post('/bodybuilding/coach/sendGymInformation','CoachApiController@sendGymInformation');
Route::post('/bodybuilding/coach/sendGymTimeAndDay','CoachApiController@sendGymTimeAndDay');
Route::post('/bodybuilding/coach/set_coach_story','CoachApiController@setCoachStory');
Route::post('/bodybuilding/coach/setGymLocation','CoachApiController@setGymLocation');
Route::post('/bodybuilding/coach/setOrderHadSeen','CoachApiController@setOrderHadSeen');
Route::post('/bodybuilding/coach/setPrices','CoachApiController@setPrices');
Route::post('/bodybuilding/coach/setSubscriptionInformation','CoachApiController@setSubscriptionInformation');
Route::post('/bodybuilding/coach/signUpCoaches','CoachApiController@signUpCoaches');
Route::post('/bodybuilding/coach/updateCoachInformation','CoachApiController@updateCoachInformation');
Route::post('/bodybuilding/coach/uploadCoachImageProfile','CoachApiController@uploadCoachImageProfile');
Route::post('/bodybuilding/coach/uploadCoachImages','CoachApiController@uploadCoachImages');
Route::post('/bodybuilding/coach/uploadGymImages','CoachApiController@uploadGymImages');



Route::post('/bodybuilding/student/check_user_have_order','StudentApiController@check_user_have_order');
Route::get('/bodybuilding/student/check_user_order_image','StudentApiController@check_user_order_image');
Route::post('/bodybuilding/student/chooseCoach','StudentApiController@chooseCoach');
Route::get('/bodybuilding/student/coaches','StudentApiController@coaches');
Route::get('/bodybuilding/student/coachesInformation','StudentApiController@coachesInformation');
Route::get('/bodybuilding/student/coachImages','StudentApiController@coachImages');
Route::post('/bodybuilding/student/confirm','StudentApiController@confirm');
Route::post('/bodybuilding/student/figure_exercise_images','StudentApiController@figure_exercise_images');
Route::get('/bodybuilding/student/findMyCoachImageProfile','StudentApiController@findMyCoachImageProfile');
Route::get('/bodybuilding/student/findUserImageProfile','StudentApiController@findUserImageProfile');
Route::get('/bodybuilding/student/forceUpdate','StudentApiController@forceUpdate');
Route::post('/bodybuilding/student/forget_password','StudentApiController@forget_password');
Route::get('/bodybuilding/student/get_items_count','StudentApiController@get_items_count');
Route::get('/bodybuilding/student/get_nutrition','StudentApiController@get_nutrition');
Route::get('/bodybuilding/student/getCoachDeviceId','StudentApiController@getCoachDeviceId');
Route::get('/bodybuilding/student/getPrices','StudentApiController@getPrices');
Route::get('/bodybuilding/student/getUserInformation','StudentApiController@getUserInformation');
Route::post('/bodybuilding/student/loginUsers','StudentApiController@loginUsers');
Route::get('/bodybuilding/student/order','StudentApiController@order');
Route::post('/bodybuilding/student/pay','StudentApiController@pay');
Route::post('/bodybuilding/student/set_coach_score','StudentApiController@set_coach_score');
Route::post('/bodybuilding/student/setOrderHadSeen','StudentApiController@setOrderHadSeen');
Route::post('/bodybuilding/student/signUpUsers','StudentApiController@signUpUsers');
Route::post('/bodybuilding/student/submit_student_order','StudentApiController@submit_student_order');
Route::post('/bodybuilding/student/submit_student_order_with_balance','StudentApiController@submit_student_order_with_balance');
Route::get('/bodybuilding/student/top_students','StudentApiController@top_students');
Route::post('/bodybuilding/student/update_password','StudentApiController@update_password');
Route::post('/bodybuilding/student/updateUserInformation','StudentApiController@updateUserInformation');
Route::post('/bodybuilding/student/updateUserScore','StudentApiController@updateUserScore');
Route::post('/bodybuilding/student/uploadUserImageProfile','StudentApiController@uploadUserImageProfile');

Route::get('/bodybuilding/student/program/deleteExercise','StudentProgramApiController@deleteExercise');
Route::get('/bodybuilding/student/program/deleteProgram','StudentProgramApiController@deleteProgram');
Route::get('/bodybuilding/student/program/deleteSupplement','StudentProgramApiController@deleteSupplement');
Route::get('/bodybuilding/student/program/exercise','StudentProgramApiController@exercise');
Route::get('/bodybuilding/student/program/food','StudentProgramApiController@food');
Route::get('/bodybuilding/student/program/get_free_program_id','StudentProgramApiController@get_free_program_id');
Route::get('/bodybuilding/student/program/getMyProgramDate','StudentProgramApiController@getMyProgramDate');
Route::get('/bodybuilding/student/program/getProgramDate','StudentProgramApiController@getProgramDate');
Route::get('/bodybuilding/student/program/getProgramDetails','StudentProgramApiController@getProgramDetails');
Route::get('/bodybuilding/student/program/getProgramId','StudentProgramApiController@getProgramId');
Route::get('/bodybuilding/student/program/getProgramImages','StudentProgramApiController@getProgramImages');
Route::post('/bodybuilding/student/program/insert_program','StudentProgramApiController@insert_program');
Route::post('/bodybuilding/student/program/send_exercise_with_array','StudentProgramApiController@send_exercise_with_array');
Route::post('/bodybuilding/student/program/sendExercise','StudentProgramApiController@sendExercise');
Route::post('/bodybuilding/student/program/sendFood','StudentProgramApiController@sendFood');
Route::post('/bodybuilding/student/program/sendSupplement','StudentProgramApiController@sendSupplement');
Route::get('/bodybuilding/student/program/supplement','StudentProgramApiController@supplement');
Route::get('/bodybuilding/student/program/swipe_exercise','StudentProgramApiController@swipe_exercise');
Route::post('/bodybuilding/student/program/update_exercise','StudentProgramApiController@update_exercise');
Route::post('/bodybuilding/student/program/update_supplement','StudentProgramApiController@update_supplement');



///test
Route::get('/swapexercises','CoachApiController@swapexercises');
//swapexercises