<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('coach')->user();

    //dd($users);
$posts = \App\Post::orderBy('post_id', 'desc')->paginate(3);
//    $posts->withPath('coach/home');
    return view('coach.test',compact('posts'));
})->name('home');
Route::get('/home', 'CoachController@index')->name('home');

