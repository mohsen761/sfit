<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//Route::group(['prefix' =>'coach'],function (){
//    Route::get('/login' ,'AuthCoach\LoginController@showLoginForm')->name('coach.login');
//    Route::post('/login' ,'AuthCoach\LoginController@login')->name('coach.login.submit');
//    Route::get('/', 'CoachController@index')->name('coach.home')->middleware('coach');
//    Route::get('/register','AuthCoach\RegisterController@showRegistrationForm')->name('coach.register');
//    Route::post('/register','AuthCoach\RegisterController@register')->name('coach.submit');
//    Route::get('logout','AuthCoach\LoginController@logout')->name('coach.logout');
//    Route::post('password/email','AuthCoach\ForgotPasswordController@sendResetLinkEmail')->name('coach.password.email');
//    Route::get('password/reset','AuthCoach\ForgotPasswordController@showLinkRequestForm')->name('coach.password.request');
//    Route::post('password/reset','AuthCoach\ResetPasswordController@reset');
//    Route::get('password/reset/{token}','AuthCoach\ResetPasswordController@showResetForm')->name('coach.password.reset');
//});





//Route::get('/home', 'HomeController@index');
Route::group(['prefix' => 'coach'], function () {
  Route::get('/login', 'CoachAuth\LoginController@showLoginForm')->name('coach.login');
  Route::post('/login', 'CoachAuth\LoginController@login');
  Route::get('/logout', 'CoachAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'CoachAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'CoachAuth\RegisterController@register');

  Route::post('/password/email', 'CoachAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'CoachAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'CoachAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'CoachAuth\ResetPasswordController@showResetForm');


  Route::get('/myusers', 'CoachController@myusers');
  Route::get('/myusers/{user}/food/{y}/{m}/{d}','CoachController@myusersdetailfood');
  Route::get('/myusers/{user}/exercise/{y}/{m}/{d}','CoachController@myusersdetailexercise');
  Route::get('/myusers/{user}/supplement/{y}/{m}/{d}','CoachController@myusersdetailsupplement');
  Route::get('/myusers/{user}', 'CoachController@myusersdetail');
  Route::get('/myorders', 'CoachController@myorders');
  Route::get('/mygym', 'GymController@mygym');
  Route::get('/mygym/{gym}/edit', 'GymController@edit');
  Route::post('/mygym/{id}/update', 'GymController@update');
  Route::post('/mygym/store', 'GymController@store');
  Route::get('/myorders/{order}/{type}/{ordertype}','CoachController@ordersend');
  Route::post('/myorders/{order}/{type}/{ordertype}','CoachController@postordersend');
  Route::post('/myorders/{order}/sendprogram','CoachController@sendprogram');
  Route::get('/myorders/{order}', 'CoachController@order');


  Route::get('/myorders/exercise/edit/{exercise}/edit', 'CoachController@editexercise');
  Route::get('/myorders/exercise/delete/{exercise}/delete', 'CoachController@deleteexercise');
  Route::post('/myorders/exercise/update/{exercise}/update', 'CoachController@updateexercise');

  Route::get('/myorders/food/edit/{food}/edit', 'CoachController@editfood');
  Route::get('/myorders/food/delete/{food}/delete', 'CoachController@deletefood');
  Route::post('/myorders/food/update/{food}/update', 'CoachController@updatefood');
Route::get('/myorders/supplement/edit/{supplement}/edit', 'CoachController@editsupplement');
  Route::get('/myorders/supplement/delete/{supplement}/delete', 'CoachController@deletesupplement');
  Route::post('/myorders/supplement/update/{supplement}/update', 'CoachController@updatesupplement');

Route::get('/myorders/program/delete/{program}/delete','CoachController@deleteprogram');
Route::get('/myorders/program/edit/{program}/edit','CoachController@editprogram');
Route::post('/myorders/program/update/{program}/update','CoachController@updateprogram');


  Route::get('/myorders/exercise/{order}', 'CoachController@exercise');
  Route::get('/myorders/food/{order}', 'CoachController@food');
  Route::get('/myorders/supplement/{order}', 'CoachController@supplement');

  Route::get('/post/{id}','CoachController@post');
  Route::get('/myprof','CoachController@myprof');
  Route::get('/getmoney','CoachController@getmoney');
  Route::get('/registergetmoney','CoachController@registergetmoney');
  Route::get('/editprice','CoachController@editprice');
  Route::post('/changeprice','CoachController@changeprice');
  Route::get('/myprof/{id}/edit','CoachController@edit');
  Route::post('/myprof/{id}/update', 'CoachController@update');
  Route::get('/test' , 'CoachController@test');
  Route::get('/changepassword','CoachController@changepassword');
  Route::post('/changepassword','CoachController@postchangepassword');


    Route::post('image-upload', 'ImageUploadController@imageUploadProgram')->name('image.upload.program');
    Route::get('image-upload', 'ImageUploadController@imageUpload')->name('image.upload');
    Route::get('/images/{type}/{order}/{id}/{image}/delete','ImageUploadController@imagedelete');


    Route::post('/images/coachimageprofile','ImageUploadController@uploadcoachprofileimage')->name('image.upload.profile');
    Route::get('/images/coachimageprofile','ImageUploadController@deletecoachprofileimage')->name('image.delete.profile');
    Route::post('/images/coachimagegym','ImageUploadController@uploadgymimage')->name('image.upload.gym');
    Route::post('/images/coachimagedoc','ImageUploadController@uploaddocimage')->name('image.upload.doc');
    Route::post('/images/coachimage','ImageUploadController@uploadcoachimage')->name('image.upload.coachimage');

    Route::post('/autocomplete/fetch', 'ImageUploadController@fetch')->name('autocomplete.fetch');

    Route::get('/figure/fetch','ImageUploadController@figureimage')->name('figure.fetch');
    Route::post('/swapexercises', 'CoachController@swapexercises')->name('swapexercises');
    Route::get('/delete/{coach}/{id}','CoachController@deleteimage');


    Route::get('/change' ,'CoachController@change');

});


Route::group(['prefix' => 'user'], function () {
  Route::get('/login', 'UserAuth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'UserAuth\LoginController@login');
  Route::get('/logout', 'UserAuth\LoginController@logout')->name('logout');

  Route::get('/register', 'UserAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'UserAuth\RegisterController@register');

  Route::post('/password/email', 'UserAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'UserAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'UserAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'UserAuth\ResetPasswordController@showResetForm');
  Route::get('/post/{id}','StudentController@post');
    Route::post('/changepassword','StudentController@postchangepassword');
    Route::get('/myprof','StudentController@myprof');
    Route::get('/myprof/{id}/edit','StudentController@edit');
    Route::post('/myprof/{id}/update', 'StudentController@update');
    Route::get('/changepassword','StudentController@changepassword');
    Route::get('/selectcoach','StudentController@selectcoach');
    Route::get('/coachdetail/{id}','StudentController@coachdetail');
    Route::get('/coachdetail/select/{id}','StudentController@selectthiscoach');
    Route::get('/neworder','StudentController@neworder');
    Route::post('/user/imageupload/f1','ImageUploadController@imageuploadf')->name('image.upload.f1');
    Route::post('/user/imageupload/f2','ImageUploadController@imageuploadf')->name('image.upload.f2');
    Route::post('/user/imageupload/f3','ImageUploadController@imageuploadf')->name('image.upload.f3');
    Route::post('/user/imageupload/f4','ImageUploadController@imageuploadf')->name('image.upload.f4');
    Route::post('/user/imageupload/f5','ImageUploadController@imageuploadf')->name('image.upload.f5');
    Route::post('/user/imageupload/f6','ImageUploadController@imageuploadf')->name('image.upload.f6');
    Route::post('/user/imageupload/f7','ImageUploadController@imageuploadf')->name('image.upload.f7');
    Route::post('/images/userimageprofile','ImageUploadController@uploaduserprofileimage')->name('upload.user.profile');
    Route::get('/images/userimageprofile','ImageUploadController@deleteuserprofileimage')->name('user.delete.profile');

    Route::post('/setgoal', 'StudentController@setgoal');
    Route::get('/myorders', 'StudentController@myorders');
    Route::get('/myprograms', 'StudentController@myprograms');
    Route::get('/freeprograms', 'StudentController@freeprograms');
    Route::get('/freeprograms/{type}', 'StudentController@freeprogramstype');
    Route::get('/freeprograms/{type}/{id}', 'StudentController@freeprogramsshow');
    Route::get('/movements', 'StudentController@movements');
    Route::get('/movements/{id}', 'StudentController@showmovement');
    Route::get('/exercise/{id}', 'StudentController@exercise');
    Route::get('/food/{id}', 'StudentController@food');
    Route::get('/supplement/{id}', 'StudentController@supplement');

    Route::get('/pay/res/', 'StudentController@success')->name('success');
    Route::get('/pay/failed/','StudentController@failed')->name('failed');


});

Route::get('/home','GuestController@posts');
Route::get('/post/{id}','GuestController@showpost');
Route::get('/freeprograms', 'GuestController@freeprograms');
Route::get('/freeprograms/{type}', 'GuestController@freeprogramstype');
Route::get('/freeprograms/{type}/{id}', 'GuestController@freeprogramsshow');
Route::get('/movements', 'GuestController@movements');
Route::get('/movements/{id}', 'GuestController@showmovement');
Route::get('/coaches', 'GuestController@coaches');
Route::get('/coachdetail/{id}', 'GuestController@coachdetail');
Route::get('/gyms', 'GuestController@gyms');
Route::get('/gyms/{id}', 'GuestController@showgym');
//Auth::routes();

//Route::get('/pay/res/', '')->name('success');
//Route::get('/pay/failed/', function (){
//    dd('sik');
//})->name('failed');
//Route::get('/home', 'HomeController@index');