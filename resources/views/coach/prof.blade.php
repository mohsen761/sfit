@extends('coach.test')

@section('content')
    <title> پروفایل من</title>

    <!-- Main content -->
    <article class="main-content" role="main" style="text-align: right;">

        <div class="container col">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
                <div class="container" >
                    {{--<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">--}}
                    {{--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>--}}
                    {{--<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--}}
                    <style>
                        /***
                        User Profile Sidebar by @keenthemes
                        A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
                        Licensed under MIT
                        ***/
                        /*.mySlides {display:none;}*/
                        body {
                            background: #F1F3FA;
                        }

                        /* Profile container */
                        .profile {
                            margin: 20px 0;
                        }

                        /* Profile sidebar */
                        .profile-sidebar {
                            padding: 20px 0 10px 0;
                            background: #fffacc;
                        }

                        .profile-userpic img {
                            float: none;
                            margin: 0 auto;
                            width: 50%;
                            height: 50%;
                            -webkit-border-radius: 50% !important;
                            -moz-border-radius: 50% !important;
                            border-radius: 50% !important;
                        }

                        .profile-usertitle {
                            text-align: center;
                            margin-top: 20px;
                        }

                        .profile-usertitle-name {
                            color: #5a7391;
                            font-size: 16px;
                            font-weight: 600;
                            margin-bottom: 7px;
                        }

                        .profile-usertitle-job {
                            text-transform: uppercase;
                            color: #5b9bd1;
                            font-size: 12px;
                            font-weight: 600;
                            margin-bottom: 15px;
                        }

                        .profile-userbuttons {
                            text-align: center;
                            margin-top: 10px;
                        }

                        .profile-userbuttons .btn {
                            text-transform: uppercase;
                            font-size: 11px;
                            font-weight: 600;
                            padding: 6px 15px;
                            margin-right: 5px;
                        }

                        .profile-userbuttons .btn:last-child {
                            margin-right: 0px;
                        }

                        .profile-usermenu {
                            margin-top: 30px;
                        }

                        .profile-usermenu ul li {
                            border-bottom: 1px solid #f0f4f7;
                        }

                        .profile-usermenu ul li:last-child {
                            border-bottom: none;
                        }

                        .profile-usermenu ul li a {
                            color: #93a3b5;
                            font-size: 14px;
                            font-weight: 400;
                        }

                        .profile-usermenu ul li a i {
                            margin-right: 8px;
                            font-size: 14px;
                        }

                        .profile-usermenu ul li a:hover {
                            background-color: #fafcfd;
                            color: #5b9bd1;
                        }

                        .profile-usermenu ul li.active {
                            border-bottom: none;
                        }

                        .profile-usermenu ul li.active a {
                            color: #5b9bd1;
                            background-color: #f6f9fb;
                            border-left: 2px solid #5b9bd1;
                            margin-left: -2px;
                        }

                        /* Profile Content */
                        .profile-content {
                            padding: 20px;
                            background: #fff;
                            min-height: 460px;
                        }
                    </style>
                    <div class=" profile" style="max-width: 800px">

                        <div class="col-md-10" style=" box-shadow: 10px 10px 10px #888888;;background-color: #a0a0a0">
                            <a href="/coach/home" class="btn btn-primary"> بازگشت به عقب</a>
                            <div class="profile-sidebar" style=";width: 100%;">
                                <!-- SIDEBAR USERPIC -->
                                <div class="w3-content w3-display-container">
                                    <img id="currentPhoto" style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $user->coach_id }}/0.png" onerror="this.onerror=null; this.src='/images/profile.png'" alt="">
                                    <img id="currentPhoto" style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $user->coach_id }}/1.png" onerror="this.onerror=null; this.src='/images/profile.png'" alt="">
                                    <img id="currentPhoto" style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $user->coach_id }}/2.png" onerror="this.onerror=null; this.src='/images/profile.png'" alt="">
                                            {{--<img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $user->coach_id }}/1.png" alt="">--}}
                                            {{--<img style="width: 100%;" class="mySlides"  src="/bodybuilding/coach/images/coachesImages/{{ $user->coach_id }}/0.png" alt="">--}}
                                            {{--<img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $user->coach_id }}/2.png" alt="">--}}
                                    <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10094;</button>
                                    <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10095;</button>



                                </div>
                                <script>
                                    var slideIndex = 1;
                                    showDivs(slideIndex);

                                    function plusDivs(n) {
                                        showDivs(slideIndex += n);
                                    }

                                    function showDivs(n) {
                                        var i;
                                        var x = document.getElementsByClassName("mySlides");
                                        if (n > x.length) {slideIndex = 1}
                                        if (n < 1) {slideIndex = x.length}
                                        for (i = 0; i < x.length; i++) {
                                            x[i].style.display = "none";
                                        }
                                        x[slideIndex-1].style.display = "block";
                                    }
                                </script>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle">
                                    <div class="profile-usertitle-name">
                                        {{ $user->coach_name  .'  '.  $user->coach_family }}
                                    </div>
                                    <div class="profile-usertitle-job">
                                        مربی
                                    </div>
                                    <hr>
                                    <p><img style="width: 30px" src="/images/honors.png" alt="">  افتخارات: </p>
                                    <p >  {{ $user->coach_honors }}  </p> <hr>
                                    <p><img style="width: 30px" src="/images/age.jpg" alt="">  سن: </p>
                                    <p>  {{ $user->coach_age }}  </p> <hr>
                                    <p><img style="width: 30px" src="/images/state.jpg" alt="">  استان: </p>
                                    <p>  {{ $user->coach_state }}  </p> <hr>
                                    <p><img style="width: 30px" src="/images/credit.png" alt="">  شماره کارت: </p>
                                    <p>  {{ $user->coach_card_number }}  </p> <hr>
                                    <p><img style="width: 30px" src="/images/credit.png" alt="">  شماره شبا: </p>
                                    <p>  {{ $user->coach_shaba_number }}  </p> <hr>
                                    <p><img style="width: 30px" src="/images/phone.png" alt="">  موبایل: </p>
                                    <p>  {{ $user->coach_mobile }}  </p> <hr>
                                    <p><img style="width: 30px" src="/images/email.png" alt="">  ایمیل: </p>
                                    <p>  {{ $user->email }}  </p> <hr>
                                    <p><img style="width: 30px" src="/images/instagram.png" alt="">  اینستاگرام: </p>
                                    <p>  {{ $user->coach_instagram }}  </p> <hr>
                                </div>
                                <a class="btn btn-lg btn-secondary" role="button" href="/coach/myprof/{{ $user->coach_id }}/edit"> ویرایش اطلاعات من </a>
                            </div>
                        </div>
                    </div>
                </div>

                    </div>  <br> <br>


        {{--{{ $orders->links() }}--}}



    </article>
    <!-- END Main content -->




@endsection

