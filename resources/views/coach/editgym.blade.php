@extends('coach.test')

@section('content')
    <title>  ویرایش باشگاه من</title>
    <!-- Main content -->
    <article class="col-xl-12 main-content" role="main" style="text-align: right;margin-top: 5%">
        <a href="/coach/mygym" class="btn btn-primary"> بازگشت به عقب</a>
        <div class="container " style="" >
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
                @if(session('success'))
                <div style="width: fit-content; float: right" class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

                <div class="col-xl-10" style=";background-color: #fffacc;box-shadow: 10px 10px 10px #888888;border-width: 2px;border-style: groove;border-color: #3f9ae5;padding: 4%">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <div class="container">
                            <div class="panel panel-primary" style="padding: 6%">

                                <div class="panel-body mt-5" >
                                    @if ($message = Session::get('gym'))
                                        <div class="alert alert-success alert-block" style="text-align: right">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>
                                        </div>
                                        <img style="width: 200px;height: auto" src="{{ Session::get('gymimage') }}">
                                    @endif
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>خطا!</strong> داده های خودرا چک کنید
                                            <ul>
                                                @foreach ($errors->all() as $error)

                                                    <li>{{ $error }}</li>

                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.gym') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class=" panel-heading"><h5>ازینجا عکس باشگاه خود را آپلود کنید</h5></div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="file" name="image" class="form-control">
                                                <input type="hidden" name="gymid" value="{{  $gymdetail->gym_id }}" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-outline-success">آپلود عکس باشگاه</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    <form style="width: 100%"  action="/coach/mygym/{{  $gymdetail->gym_id }}/update" method="post">
                        <style>
                            .form-group{
                                width: 100%;
                                direction: rtl;
                                font-size: 15px;
                            }
                            .form-control{
                                font-size: 15px;
                            }
                        </style>
                        {{ csrf_field() }}
                        <div class="form-group float-label-control">
                            <label for="">نام باشگاه </label>
                            <input value="{{$gymdetail->gym_name }}" name="gym_name" type="text" class="form-control" placeholder="نام باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">استان باشگاه </label>
                            <input value="{{$gymdetail->gym_state }}" name="gym_state" type="text" class="form-control" placeholder="استان باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">شهریه باشگاه </label>
                            <input value="{{ $gymdetail->gym_monthly_fee }}" name="gym_monthly_fee" type="text" class="form-control" placeholder="شهریه باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">نوع باشگاه </label>
                            <select name="gym_type" id="">
                                <option value="gentleman" @if($gymdetail->gym_type == 'gentleman') selected @endif>مردانه</option>
                                <option value="ladies" @if($gymdetail->gym_type == 'ladies') selected @endif>زنانه</option>
                                <option value="ladies_and_gentleman" @if($gymdetail->gym_type == 'ladies_and_gentleman') selected @endif>مردانه و زنانه</option>
                            </select>

                        </div>
                        <div class="form-group float-label-control">
                            <label for="">شهر باشگاه </label>
                            <input value="{{ $gymdetail->gym_city }}" name="gym_city" type="text" class="form-control" placeholder="شهر باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">آدرس باشگاه </label>
                            <input value="{{ $gymdetail->gym_address }}" name="gym_address" type="text" class="form-control" placeholder="آدرس باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">امکانات باشگاه </label>
                            <input value="{{ $gymdetail->gym_services }}" name="gym_services" type="text" class="form-control" placeholder="امکانات باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">تعداد ورزشکاران باشگاه </label>
                            <input value="{{ $gymdetail->gym_students_number }}" name="gym_students_number" type="text" class="form-control" placeholder="تعداد ورزشکاران باشگاه">
                        </div>
                        {{--<div class="form-group float-label-control">--}}
                            {{--<label for="">طول باشگاه </label>--}}
                            {{--<input value="{{ $gymdetail->longitude }}" name="longitude" type="text" class="form-control" placeholder="طول باشگاه">--}}
                        {{--</div>--}}
                        {{--<div class="form-group float-label-control">--}}
                            {{--<label for="">عرض باشگاه </label>--}}
                            {{--<input value="{{ $gymdetail->latitude }}" name="latitude" type="text" class="form-control" placeholder="عرض باشگاه">--}}
                        {{--</div>--}}
                            <button  class="btn btn-outline-success btn-lg mr-5 mb-5 mt-5" style="alignment: center" type="submit" value="اعمال تغییرات" >اعمال تغییرات</button>

                    </form>
                </div>
                <br/>



            {{--{{ $orders->links() }}--}}

        </div>

    </article>
    <!-- END Main content -->


@endsection
