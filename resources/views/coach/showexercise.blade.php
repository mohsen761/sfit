@extends('coach.test')

@section('content')

<title>برنامه های تمرینی</title>
<a href="/coach/myorders" class="btn btn-primary" style="padding-top: 1.5%;width: fit-content"> بازگشت به عقب</a>

<style>

    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        border: 1px solid #ccc;
        border-top: none;
    }
</style>
<div class="tab" style="text-align: right;float: right">
    <button class="tablinks" onclick="openCity(event, 'seven')">روز هفتم</button>
    <button class="tablinks" onclick="openCity(event, 'six')">روز ششم</button>
    <button class="tablinks" onclick="openCity(event, 'five')">روز پنجم</button>
    <button class="tablinks" onclick="openCity(event, 'four')">روز چهارم</button>
    <button class="tablinks" onclick="openCity(event, 'three')">روز سوم</button>
     <button class="tablinks" onclick="openCity(event, 'two')">روز دوم</button>
    <button class="tablinks" onclick="openCity(event, 'one')">روز اول</button>
</div>
<br>
<br>

<style>
    .table{
        box-shadow: 10px 10px 10px #888888;
    }
</style>
@if(session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
@if(session('success'))
    <div style="width: fit-content; float: right" class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@foreach($programs as $program)
@if($program_state =='not_sent')
    <form action="/coach/swapexercises" method="post" style="text-align: right">
        @csrf
        <lable>میخاهم حرکت شماره ی </lable> <input id="first" type="text" name="first" style="width: 10%" >
        <lable>با حرکت شماره ی </lable> <input id="second" type="text" name="second" style="width: 10%">
        عوض شود.
        <input type="hidden" id="temp" name="true" value="true">
        <button type="submit" class="btn btn-outline-success btn-lg"> عوض کردن</button>
    </form>
@endif
@endforeach

<script>
    $(document).on('click', 'tr', function(){
let id =$(this).attr("id");
        if ($(this).attr("id") != undefined) {
            if ($('#first').val() == "") {
                $('#first').val(id);
            }else {
                $('#second').val(id);
            }
        }

    });

</script>
<div id="one" class="tabcontent" style="width: 90%" >
    @foreach($programs as $program)
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td>شماره</td>
                <td> عنوان  :  <h5>{{ $program->title }}</h5> </td>

                {{--<td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>--}}

                <td> طول  : <h5>{{ $program->period }}روز</h5>  </td>

                <td>  تاریخ  : <h5>{{ str_replace("...","",str_limit($program->created_at,10)) }}</h5></td>

                <td>  نوع  : <h5>برنامه تمرینی</h5></td>

                @if($program_state =='not_sent')
                    <td>   <h5>عملیات</h5></td>
                @endif
            </tr>
            <tr>
                <td></td>
                <td scope="col"><h6>حرکت:</h6></td>
                {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
                <td scope="col"><h6> ست:</h6></td>
                <td scope="col"><h6> تکرار: </h6></td>
                <td scope="col"><h6> سیستم تمرینی :</h6></td>
                {{--@if($program_state =='not_sent')--}}
                {{--<td scope="col">   <h5></h5></td>--}}
                {{--@endif--}}
                <td>   @if($program_state =='not_sent')
                        {{--<a class="btn btn-outline-warning" href="/coach/myorders/program/edit/{{ $program->program_id }}/edit"> ویرایش برنامه</a>--}}
                        <a class="btn btn-outline-danger" href="/coach/myorders/program/delete/{{ $program->program_id }}/delete"> حذف برنامه</a></td>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($exercises as $item)

                @foreach($item as $exercise)

                        @if($exercise->days =='first')
                        <tr id="{{ $exercise->exercise_id}}" >
                            <td>{{ $exercise->exercise_id}}</td>
                            <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                            {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                            <td> <h6>{{ $exercise->sets }} ست</h6></td>
                            <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                            @if($exercise->exercise_systems !='')
                                <td>  <h6>به صورت :{{ $exercise->exercise_systems }} </h6></td>@else <td></td>@endif

                            @if($program_state =='not_sent')
                                <td><a class="btn btn-outline-warning" href="/coach/myorders/exercise/edit/{{ $exercise->exercise_id }}/edit"> ویرایش</a>
                                    <a class="btn btn-outline-danger" href="/coach/myorders/exercise/delete/{{ $exercise->exercise_id }}/delete"> حذف</a></td>
                            @endif
                    </tr>
                        @endif

                @endforeach

            @endforeach
            </tbody>

        </table>
    @endforeach
</div>

<script>
    function openCity(evt, cityName) {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>

<div id="two" class="tabcontent" style="width: 90%">
@foreach($programs as $program)
        <table class=" table table-striped  table-dark " style="text-align: right;margin: 2%;">

        <thead>
        <tr style="">
            {{--<td>  ردیف </td>--}}
            <td>شماره</td>
            <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

            {{--<td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>--}}

            <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

            <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->created_at,10)) }}</h5></td>

            <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            @if($program_state =='not_sent')
                <td>   <h5>عملیات</h5></td>
                @endif
        </tr>
        <tr>
            <td></td>
            <td scope="col"><h6>حرکت:</h6></td>
            {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
            <td scope="col"><h6> ست:</h6></td>
            <td scope="col"><h6> تکرار: </h6></td>
            <td scope="col"><h6> سیستم تمرینی :</h6></td>
            {{--@if($program_state =='not_sent')--}}
                {{--<td scope="col">   <h5></h5></td>--}}
            {{--@endif--}}
            <td>   @if($program_state =='not_sent')
                {{--<a class="btn btn-outline-warning" href="/coach/myorders/program/edit/{{ $program->program_id }}/edit"> ویرایش برنامه</a>--}}
                <a class="btn btn-outline-danger" href="/coach/myorders/program/delete/{{ $program->program_id }}/delete"> حذف برنامه</a></td>
            @endif
        </tr>
        </thead>
            <tbody>
        @foreach($exercises as $item)

            @foreach($item as $exercise)

                @if($exercise->days =='second')
                    <tr id="{{ $exercise->exercise_id}}">
                        <td>{{ $exercise->exercise_id}}</td>
                            <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                            {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                            <td> <h6>{{ $exercise->sets }} ست</h6></td>
                            <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                            @if($exercise->exercise_systems !='')
                                <td>  <h6>به صورت :{{ $exercise->exercise_systems }} </h6></td>@else <td></td>@endif

                            @if($program_state =='not_sent')
                                <td><a class="btn btn-outline-warning" href="/coach/myorders/exercise/edit/{{ $exercise->exercise_id }}/edit"> ویرایش</a>
                                    <a class="btn btn-outline-danger" href="/coach/myorders/exercise/delete/{{ $exercise->exercise_id }}/delete"> حذف</a></td>
                           @endif
                    </tr>
                @endif

            @endforeach

        @endforeach
        </tbody>

        </table>
    @endforeach
    </div>
<div id="three" class="tabcontent" style="width: 90%">
@foreach($programs as $program)
        <table class=" table table-striped  table-dark " style="text-align: right;margin: 2%;">

        <thead>
        <tr style="">
            <td>شماره</td>
            <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

            {{--<td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>--}}

            <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

            <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->created_at,10)) }}</h5></td>

            <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            @if($program_state =='not_sent')
                <td>   <h5>عملیات</h5></td>
                @endif
        </tr>
        <tr>
            <td></td>
            <td scope="col"><h6>حرکت:</h6></td>
            {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
            <td scope="col"><h6> ست:</h6></td>
            <td scope="col"><h6> تکرار: </h6></td>
            <td scope="col"><h6> سیستم تمرینی :</h6></td>
            {{--@if($program_state =='not_sent')--}}
                {{--<td scope="col">   <h5></h5></td>--}}
            {{--@endif--}}
            <td>   @if($program_state =='not_sent')
                {{--<a class="btn btn-outline-warning" href="/coach/myorders/program/edit/{{ $program->program_id }}/edit"> ویرایش برنامه</a>--}}
                <a class="btn btn-outline-danger" href="/coach/myorders/program/delete/{{ $program->program_id }}/delete"> حذف برنامه</a></td>
            @endif
        </tr>
        </thead>
            <tbody>
        @foreach($exercises as $item)

            @foreach($item as $exercise)

                @if($exercise->days =='third')
                    <tr id="{{ $exercise->exercise_id}}">
                        <td>{{ $exercise->exercise_id}}</td>
                            <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                            {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                            <td> <h6>{{ $exercise->sets }} ست</h6></td>
                            <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                            @if($exercise->exercise_systems !='')
                                <td>  <h6>به صورت :{{ $exercise->exercise_systems }} </h6></td>@else <td></td>@endif

                            @if($program_state =='not_sent')
                                <td><a class="btn btn-outline-warning" href="/coach/myorders/exercise/edit/{{ $exercise->exercise_id }}/edit"> ویرایش</a>
                                    <a class="btn btn-outline-danger" href="/coach/myorders/exercise/delete/{{ $exercise->exercise_id }}/delete"> حذف</a></td>
                            @endif
                    </tr>
                @endif

            @endforeach

        @endforeach
        </tbody>

        </table>
    @endforeach
    </div>
<div id="four" class="tabcontent" style="width: 90%">
@foreach($programs as $program)
        <table class=" table table-striped  table-dark " style="text-align: right;margin: 2%;">

        <thead>
        <tr style="">
            <td>شماره</td>
            <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

            {{--<td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>--}}

            <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

            <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->created_at,10)) }}</h5></td>

            <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            @if($program_state =='not_sent')
                <td>   <h5>عملیات</h5></td>
                @endif
        </tr>
        <tr>
            <td></td>
            <td scope="col"><h6>حرکت:</h6></td>
            {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
            <td scope="col"><h6> ست:</h6></td>
            <td scope="col"><h6> تکرار: </h6></td>
            <td scope="col"><h6> سیستم تمرینی :</h6></td>
            {{--@if($program_state =='not_sent')--}}
                {{--<td scope="col">   <h5></h5></td>--}}
            {{--@endif--}}
            <td>   @if($program_state =='not_sent')
                {{--<a class="btn btn-outline-warning" href="/coach/myorders/program/edit/{{ $program->program_id }}/edit"> ویرایش برنامه</a>--}}
                <a class="btn btn-outline-danger" href="/coach/myorders/program/delete/{{ $program->program_id }}/delete"> حذف برنامه</a></td>
            @endif
        </tr>
        </thead>
            <tbody>
        @foreach($exercises as $item)

            @foreach($item as $exercise)

                @if($exercise->days =='fourth')
                    <tr id="{{ $exercise->exercise_id}}">
                        <td>{{ $exercise->exercise_id}}</td>
                            <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                            {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                            <td> <h6>{{ $exercise->sets }} ست</h6></td>
                            <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                            @if($exercise->exercise_systems !='')
                                <td>  <h6>به صورت :{{ $exercise->exercise_systems }} </h6></td>@else <td></td>@endif

                            @if($program_state =='not_sent')
                                <td><a class="btn btn-outline-warning" href="/coach/myorders/exercise/edit/{{ $exercise->exercise_id }}/edit"> ویرایش</a>
                                    <a class="btn btn-outline-danger" href="/coach/myorders/exercise/delete/{{ $exercise->exercise_id }}/delete"> حذف</a></td>
                            @endif </tr>
                @endif

            @endforeach

        @endforeach
        </tbody>

        </table>
    @endforeach
    </div>
<div id="five" class="tabcontent" style="width: 90%">
@foreach($programs as $program)
        <table class=" table table-striped  table-dark " style="text-align: right;margin: 2%;">

        <thead>
        <tr style="">
            <td>شماره</td>
            <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

            {{--<td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>--}}

            <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

            <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->created_at,10)) }}</h5></td>

            <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            @if($program_state =='not_sent')
                <td>   <h5>عملیات</h5></td>
                @endif
        </tr>
        <tr>
            <td></td>
            <td scope="col"><h6>حرکت:</h6></td>
            {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
            <td scope="col"><h6> ست:</h6></td>
            <td scope="col"><h6> تکرار: </h6></td>
            <td scope="col"><h6> سیستم تمرینی :</h6></td>
            {{--@if($program_state =='not_sent')--}}
                {{--<td scope="col">   <h5></h5></td>--}}
            {{--@endif--}}
            <td>   @if($program_state =='not_sent')
                {{--<a class="btn btn-outline-warning" href="/coach/myorders/program/edit/{{ $program->program_id }}/edit"> ویرایش برنامه</a>--}}
                <a class="btn btn-outline-danger" href="/coach/myorders/program/delete/{{ $program->program_id }}/delete"> حذف برنامه</a></td>
            @endif
        </tr>
        </thead>
            <tbody>
        @foreach($exercises as $item)

            @foreach($item as $exercise)

                @if($exercise->days =='fifth')
                    <tr id="{{ $exercise->exercise_id}}">
                        <td>{{ $exercise->exercise_id}}</td>
                            <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                            {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                            <td> <h6>{{ $exercise->sets }} ست</h6></td>
                            <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                            @if($exercise->exercise_systems !='')
                                <td>  <h6>به صورت :{{ $exercise->exercise_systems }} </h6></td>@else <td></td>@endif

                            @if($program_state =='not_sent')
                                <td><a class="btn btn-outline-warning" href="/coach/myorders/exercise/edit/{{ $exercise->exercise_id }}/edit"> ویرایش</a>
                                    <a class="btn btn-outline-danger" href="/coach/myorders/exercise/delete/{{ $exercise->exercise_id }}/delete"> حذف</a></td>
                            @endif
                    </tr>
                @endif

            @endforeach

        @endforeach
        </tbody>

        </table>
    @endforeach
    </div>
<div id="six" class="tabcontent" style="width: 90%">
@foreach($programs as $program)
        <table class=" table table-striped  table-dark " style="text-align: right;margin: 2%;">

        <thead>
        <tr style="">
            <td>شماره</td>
            <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

            {{--<td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>--}}

            <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

            <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->created_at,10)) }}</h5></td>

            <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            @if($program_state =='not_sent')
                <td>   <h5>عملیات</h5></td>
                @endif
        </tr>
        <tr>
            <td></td>
            <td scope="col"><h6>حرکت:</h6></td>
            {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
            <td scope="col"><h6> ست:</h6></td>
            <td scope="col"><h6> تکرار: </h6></td>
            <td scope="col"><h6> سیستم تمرینی :</h6></td>
            {{--@if($program_state =='not_sent')--}}
                {{--<td scope="col">   <h5></h5></td>--}}
            {{--@endif--}}
            <td>   @if($program_state =='not_sent')
                {{--<a class="btn btn-outline-warning" href="/coach/myorders/program/edit/{{ $program->program_id }}/edit"> ویرایش برنامه</a>--}}
                <a class="btn btn-outline-danger" href="/coach/myorders/program/delete/{{ $program->program_id }}/delete"> حذف برنامه</a></td>
            @endif
        </tr>
        </thead>
            <tbody>
        @foreach($exercises as $item)

            @foreach($item as $exercise)

                @if($exercise->days =='sixth')
                    <tr id="{{ $exercise->exercise_id}}">
                        <td>{{ $exercise->exercise_id}}</td>
                            <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                            {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                            <td> <h6>{{ $exercise->sets }} ست</h6></td>
                            <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                            @if($exercise->exercise_systems !='')
                                <td>  <h6>به صورت :{{ $exercise->exercise_systems }} </h6></td>@else <td></td>@endif

                            @if($program_state =='not_sent')
                                <td><a class="btn btn-outline-warning" href="/coach/myorders/exercise/edit/{{ $exercise->exercise_id }}/edit"> ویرایش</a>
                                    <a class="btn btn-outline-danger" href="/coach/myorders/exercise/delete/{{ $exercise->exercise_id }}/delete"> حذف</a></td>
                            @endif
                    </tr>@endif

            @endforeach

        @endforeach
        </tbody>

        </table>
    @endforeach
    </div>
<div id="seven" class="tabcontent" style="width: 90%">
@foreach($programs as $program)
        <table class=" table table-striped  table-dark " style="text-align: right;margin: 2%;">

        <thead>
        <tr style="">
<td>شماره</td>
            <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

            {{--<td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>--}}

            <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

            <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->created_at,10)) }}</h5></td>

            <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            @if($program_state =='not_sent')
                <td>   <h5>عملیات</h5></td>
                @endif
        </tr>
        <tr>
            <td></td>
            <td scope="col"><h6>حرکت:</h6></td>
            {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
            <td scope="col"><h6> ست:</h6></td>
            <td scope="col"><h6> تکرار: </h6></td>
            <td scope="col"><h6> سیستم تمرینی :</h6></td>
            {{--@if($program_state =='not_sent')--}}
                {{--<td scope="col">   <h5></h5></td>--}}
            {{--@endif--}}
            <td>   @if($program_state =='not_sent')
                {{--<a class="btn btn-outline-warning" href="/coach/myorders/program/edit/{{ $program->program_id }}/edit"> ویرایش برنامه</a>--}}
                <a class="btn btn-outline-danger" href="/coach/myorders/program/delete/{{ $program->program_id }}/delete"> حذف برنامه</a></td>
            @endif
        </tr>
        </thead>
            <tbody>
        @foreach($exercises as $item)

            @foreach($item as $exercise)

                @if($exercise->days =='seventh')
                    <tr id="{{ $exercise->exercise_id}}">
                        <td>{{ $exercise->exercise_id}}</td>
                            <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                            {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                            <td> <h6>{{ $exercise->sets }} ست</h6></td>
                            <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                            @if($exercise->exercise_systems !='')
                                <td>  <h6>به صورت :{{ $exercise->exercise_systems }} </h6></td>@else <td></td>@endif

                            @if($program_state =='not_sent')
                                <td><a class="btn btn-outline-warning" href="/coach/myorders/exercise/edit/{{ $exercise->exercise_id }}/edit"> ویرایش</a>
                                    <a class="btn btn-outline-danger" href="/coach/myorders/exercise/delete/{{ $exercise->exercise_id }}/delete"> حذف</a></td>
                            @endif
                    </tr> @endif

            @endforeach

        @endforeach
        </tbody>

        </table>
    @endforeach
    </div>

@endsection