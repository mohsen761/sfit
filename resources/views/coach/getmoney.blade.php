@extends('coach.test')


@section('content')
    <title>تسویه حساب</title>


    <div class="col-xl-4 ml-5 center mt-5" style="padding: 2%;background-color: #fffacc;box-shadow: 10px 10px 10px #888888;float: right">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
            @if(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <h4> مبلغ قابل دریافت :</h4>
        <h4> {{ $value }} تومان</h4>

        <a href="/coach/registergetmoney" role="button" class="mt-5 btn btn-lg btn-outline-success">درخواست تسویه حساب</a>
    </div>
    @endsection