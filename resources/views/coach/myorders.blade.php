@extends('coach.test')

@section('content')
    <title> درخواست های شاگردان</title>
    <!-- Main content -->
    <article class="col-md-12 " role="main" style="padding: 1%;margin:1%;max-width:800px;text-align: right;">

        <div class="container ">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
                @if(count($orders) ==0 )
                    <div style="width: fit-content" class="alert alert-danger">
                    شما هنوز درخواستی ندارید!
                     </div>
                @endif
            @foreach ($orders as $order)
                <div class="ml-5" style="margin-right: -5%;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;border-width: 2px;border-style: groove;border-color: #3f9ae5;padding: 30px">
                    <div class="row ">
                        <a class="" href="/coach/myusers/{{ $order->user_id }}">  مشاهده مشخصات شاگرد {{$order->firstname }}{{$order->lastname }} </a>
                    </div>
                    <a  class="mt-2 btn btn-lg btn-outline-success p-4" href="/coach/myorders/{{ $order->order_id }}">
                <article class="post ">
                    <div class="post-content  ">

                        <div class="row">  {{$order->firstname }} {{$order->lastname .'  '}}برای شما درخواست
                            @if($order->exercise =='yes') <button >  برنامه تمرینی</button> _ @endif
                            @if($order->food =='yes') <button> برنامه غذایی</button>  _ @endif
                            @if($order->supplement =='yes') <button> </h1> برنامه مکملی</button> @endif
                            فرستاده است
                        </div>
                        <div class="row">
                            <p>  {{ $order->price }} تومان  </p>
                        </div>
                        <div class="row">
                            {{--<p>تاریخ: {{ str_replace("_","/", $order->order_date) }}</p>--}}
                            <p>تاریخ: {{  $order->order_date }} </p>
                        </div>


                    </div>
                </article>
                    </a>
                    <div class="row mt-2">
                        @if($order->program_state == 'sent') <p>برنامه باموفقیت ارسال شده است</p> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt="">
                        @else
                            <p>برنامه ای هنوز ارسال نشده است</p>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt="">
                        @endif
                    </div>
                    <div class="row">
                        @if($order->payment_state == 'paid') <p>برنامه باموفقیت تسویه حساب شده است</p> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt="">
                        <br>
                        @else
                            <p>برنامه تسویه حساب نشده است</p>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt="">
                            <br>
                        @endif
                    </div>
                </div>
                <br/>
            @endforeach


                {{--@if(count($orders) !=0 )  {{ $orders->links() }} @endif--}}

        </div>

    </article>
    <!-- END Main content -->


    <!-- Scripts -->
    {{--<script src="http://labs.infyom.com/laravelgenerator/public/assets/js/theDocs.all.min.js"></script>--}}
    {{--</body>--}}
    {{--</html>--}}

@endsection

