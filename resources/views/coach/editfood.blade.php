@extends('coach.test')

@section('content')

    <title>  ویرایش برنامه غذایی</title>
    <!-- Main content -->
    <article class="col-xl-12 main-content" role="main" style=";text-align: right;margin-top: 5%">
        <a href="/coach/myorders/" class="btn btn-primary"> بازگشت به عقب</a>
        <div class="container " >
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if(session('success'))
                <div style="width: fit-content; float: right" class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <div class="col-xl-6" style="background-color: #fffacc;box-shadow: 10px 10px 10px #888888;border-width: 2px;border-style: groove;border-color: #3f9ae5;padding: 4%">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form style="width: 100%"  action="/coach/myorders/food/update/{{  $food->food_id }}/update" method="post">
                    <style>
                        .form-group{
                            width: 100%;
                            direction: rtl;
                            font-size: 15px;
                        }
                        .form-control{
                            font-size: 15px;
                        }
                    </style>
                    {{ csrf_field() }}

                    <div class="form-group float-label-control">
                        <label for="">وعده غذایی </label>
                        <select name="meal" id="" >
                                <option value="breakfast" @if($food->meal=='breakfast') selected @endif> صبحانه</option>
                                <option value="snack" @if($food->meal=='snack') selected @endif> ناشتا</option>
                                <option value="lunch" @if($food->meal=='lunch') selected @endif> نهار</option>
                                <option value="before_exercise" @if($food->meal=='before_exercise') selected @endif> قبل از تمرین</option>
                                <option value="after_exercise" @if($food->meal=='after_exercise') selected @endif> بعد از تمرین</option>
                                <option value="dinner" @if($food->meal=='dinner') selected @endif> شام </option>
                                <option value="before_sleep" @if($food->meal=='before_sleep') selected @endif> قبل از خواب</option>
                        </select>
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">پیشنهاد اول </label>
                        <input value="{{ $food->offer1 }}" name="offer1" type="text" class="form-control" placeholder="پیشنهاد اول">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">پیشنهاد دوم </label>
                        <input value="{{ $food->offer2 }}" name="offer2" type="text" class="form-control" placeholder="پیشنهاد دوم">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">پیشنهاد سوم </label>
                        <input value="{{ $food->offer3 }}" name="offer3" type="text" class="form-control" placeholder="پیشنهاد سوم">
                    </div>

                    <button  type="submit" value="اعمال تغییرات" >اعمال تغییرات</button>

                </form>
            </div>
            <br/>



            {{--{{ $orders->links() }}--}}

        </div>

    </article>



@endsection