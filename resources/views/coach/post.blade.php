@extends('coach.test')

@section('content')


    <!-- Main content -->
    <article class="col-md-9 col-sm-9 main-content" role="main"style=";text-align: right;margin-top: 5%">
        <a href="/coach/home" class="btn btn-primary"> بازگشت به عقب</a>
        <div class="container col">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @foreach($post as $item)
                    <title> {{ $item->post_title }}</title>

                        <div class="col" style="padding: 2%;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;;text-align: right ;direction: rtl;">
                            <div style="font-size: 200%">{{ $item->post_title }} <br></div>
                            <br>
                            <div>
                                <img class="rounded img-fluid" src="/bodybuilding/admin/images/post/{{ $item->post_id }}.png" alt="">
                            </div>
                            <br>
                            <div>{{ $item->post_description }}</div>
                            {{--<div>{{ str_limit($post->post_description, $limit = 200, $end = '...')}}</div>--}}
                        </div>  <br> <br>

               @endforeach



        </div>

        {{--{{ $orders->links() }}--}}



    </article>
    <!-- END Main content -->


    <!-- Scripts -->
    <script src="http://labs.infyom.com/laravelgenerator/public/assets/js/theDocs.all.min.js"></script>
    </body>
    </html>

@endsection
@section('sidebar')

<link rel="stylesheet" href="/css/style.css">
<script src="/js/sidebar.js"></script>

<div class="wrapper" style="margin-left: 950px ;position: fixed;float: right;height: 100%">
<div id="content">
<button type="button" id="sidebarCollapse" class="navbar-btn">
<span></span>
<span></span>
<span></span>
</button>
</div>
<!-- Sidebar -->
<nav id="sidebar">
<div class="sidebar-header">
<h3>sfit</h3>
</div>

<ul class="list-unstyled components" >
<li>
<a class="" href="/coach/home" >خانه</a>
</li>
<li><a class=""
href="/coach/myorders" >درخواست ها</a></li>
<li>
<a class="active"
href="#">شاگردان من </a>
<ul>
<li>
<a class="active"
href="/coach/myusers">لیست شاگردان</a>
</li>

</ul>
</li>
<li>
<a
href="#">باشگاه ها</a>
<ul>
<li>
<a class=""
href="/coach/mygym">باشگاه من</a>
</li>
</ul>
</li>
<li>
<a class=""
href="#">درخواست تسویه حساب</a>
</li>
<li>
<a class="" href="#">ثبت یا ویرایش قیمت ها</a>

</li>
<li>
<a class="" href="http://labs.infyom.com/laravelgenerator/docs/5.8/releases">راهنما</a>
</li>
<li>
<a class="" href="http://labs.infyom.com/laravelgenerator/docs/routes-explorer">درباره ی ما</a>
</li>
<li>
<a class="" href="/coach/logout">خروج از حساب کاربری</a>
</li>

</ul>
</nav>


</div>


@endsection
