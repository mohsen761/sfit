@extends('coach.test')

@section('content')

    <title>برنامه های غذایی</title>
    <a href="/coach/myorders" class="btn btn-primary" style="width: fit-content"> بازگشت به عقب</a>
    @foreach($programs as $program)
        <table class=" table table-striped table-responsive-sm table-bordered table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>

                <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

                <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->program_date,10)) }}</h5></td>


                @if($program_state =='not_sent')
                    <td>   <h5>عملیات</h5></td>
                @endif
            </tr>
            <tr>
                <td scope="col"><h6>نام مکمل:</h6></td>
                <td scope="col"><h6>زمان مصرف:</h6></td>
                <td scope="col"><h6>مقدار:</h6></td>
                <td scope="col"><h6>روز مصرف:</h6></td>

                {{--@if($program_state =='not_sent')--}}
                {{--<td scope="col">   <h5></h5></td>--}}
                {{--@endif--}}
                <td>
                    {{--<a class="btn btn-outline-warning" href="/coach/myorders/program/edit/{{ $program->program_id }}/edit"> ویرایش برنامه</a>--}}
                    @if($program_state =='not_sent')
                        <a class="btn btn-outline-danger" href="/coach/myorders/program/delete/{{ $program->program_id }}/delete"> حذف برنامه</a></td>
                    @endif

            </tr>
            </thead>
            <tbody>
            @foreach($supplements as $item)

                @foreach($item as $supplement)

                    @if($program->program_id == $supplement->program_id)
                        <tr>
                            <td scope="row">  <h6>{{ $supplement->supplement_name }}</h6></td>
                            <td> <h6>{{ $supplement->time_to_eat }} </h6></td>
                            <td>   <h6>{{ $supplement->dosage }} </h6></td>
                            <td>   <h6>
                                    @if($supplement->day_to_eat =='training_day')روز تمرین
                                        @elseif($supplement->day_to_eat =='rest_day')روز استراحت
                                        @elseif($supplement->day_to_eat =='training_rest_day')روز استراحت و تمرین
                                        @else
                                    {{ $supplement->day_to_eat }}
                                    @endif
                                </h6></td>

                            @if($program_state =='not_sent')
                                <td><a class="btn btn-outline-warning" href="/coach/myorders/supplement/edit/{{ $supplement->supplement_id }}/edit"> ویرایش</a>
                                    <a class="btn btn-outline-danger" href="/coach/myorders/supplement/delete/{{ $supplement->supplement_id }}/delete"> حذف</a></td>
                            @endif
                        </tr>
                    @endif

                @endforeach

            @endforeach


            </tbody>

        </table>
    @endforeach



@endsection