@extends('coach.test')

@section('content')
    <title>  ویرایش اطلاعات من</title>
    <!-- Main content -->
        <a href="/coach/myprof" class="btn btn-primary"  > بازگشت به عقب</a>
        <div class="container ml-5" style="text-align: right;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;">
            <div class="panel panel-primary" style="padding: 6%">

                <div class="panel-body mt-5" >
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block" style="text-align: right">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        <img style="width: 200px;height: auto" src="{{ Session::get('image') }}">
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            <ul>
                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.profile') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class=" panel-heading"><h5>ازینجا عکس پروفایل خود را آپلود کنید</h5></div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" name="image" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-outline-success">آپلود عکس پروفایل</button>
                            </div>
                        </div>
                    </form>
                        @if ($message = Session::get('profile'))
                            <div class="alert alert-success alert-block" style="text-align: right">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                            <img style="width: 200px;height: auto" src="{{ Session::get('profileimage') }}">
                        @endif
                </div>
            </div>
        </div>
        <div class="container mt-5 ml-5" style="text-align:right;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;">
            <div class="panel panel-primary" style="padding: 6%">

                <div class="panel-body" >

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>خطا!</strong> داده های خودرا چک کنید
                            <ul>
                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.doc') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class=" panel-heading"><h5>ازینجا عکس مدارک خود را آپلود کنید.(حداکثر سه عکس.درهر بار یک عکس را آپلود کنید.)</h5></div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" name="image" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-outline-success">آپلود عکس مدارک</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <div class="container mt-5 ml-5" style="text-align:right;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;">
            <div class="panel panel-primary" style="padding: 6%">

                <div class="panel-body" >

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>خطا!</strong> داده های خودرا چک کنید
                            <ul>
                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.coachimage') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class=" panel-heading"><h5>ازینجا عکس هایی از خود را آپلود کنید.(حداکثر سه عکس.درهر بار یک عکس را آپلود کنید.)</h5></div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" name="image" class="form-control" required>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-outline-success">آپلود عکس </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>



        <div class="container " style="text-align: right">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if(session('doc'))
                <div style="width: fit-content; float: right" class="alert alert-success">
                    {{ session('doc') }}
                </div>
            @endif
                @if(session('coachimage'))
                <div style="width: fit-content; float: right" class="alert alert-success">
                    {{ session('coachimage') }}
                </div>
            @endif

            <div class="mt-5 ml-5"  style="background-color: #fffacc; box-shadow: 10px 10px 10px #888888;padding: 2%;border-width: 2px;border-style: groove;border-color: #3f9ae5;">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <form action="">
                    <div class="" >

                        <div class="col-md-12">
                            <div class="profile-userpic">
                                <img class="img-thumbnail " onerror="this.onerror=null; this.src='/images/profile.png'" style="width:70%;height:auto;" src="
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/coach/images/coachesImages/'. $Coach->id .'/0.png'))--}}
                                        /bodybuilding/coach/images/coachesImages/{{ $Coach->coach_id }}/0.png " >
                                <a href="/coach/delete/{{ $Coach->coach_id }}/0" >
                                    <button type="submit" class="close" data-dismiss="modal">&times;</button>حذف
                                </a>
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="profile-userpic">
                                <img class="img-thumbnail " onerror="this.onerror=null; this.src='/images/profile.png'" style="width:70%;height:auto;" src="
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/coach/images/coachesImages/'. $Coach->id .'/1.png'))--}}
                                        /bodybuilding/coach/images/coachesImages/{{ $Coach->coach_id }}/1.png " >
                                <a href="/coach/delete/{{ $Coach->coach_id }}/1" >
                                    <button type="submit" class="close" data-dismiss="modal">&times;</button>حذف
                                </a>
                                {{--@else--}}
                                {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}

                                {{--<img width="150" src="http://sfit.ir/bodybuilding/coach/images/coachesImages/{{ $Coach->id }}/1.png" alt="">--}}

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="profile-userpic">
                                <img class="img-thumbnail " style="width:70%;height:auto;" onerror="this.onerror=null; this.src='/images/profile.png'" src="
                                {{--@if(@GetImageSize('http://images/coachesImages/'. $Coach->id .'/2.png'))--}}
                                        /bodybuilding/coach/images/coachesImages/{{ $Coach->coach_id }}/2.png " >
                                <a href="/coach/delete/{{ $Coach->coach_id }}/2" >
                                    <button type="submit" class="close" data-dismiss="modal">&times;</button>حذف
                                </a>
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="profile-userpic">


                                {{--@if(@GetImageSize('localhost:8000/images/coachesImagesProfile/'.$Coach->id.'.png'))--}}
                                    <img class="img-thumbnail " onerror="this.onerror=null; this.src='/images/profile.png'" style="width:70%;height:auto;" src="/bodybuilding/coach/images/coachesImagesProfile/{{ $Coach->coach_id }}.png " >
                                <a href="{{ route('image.delete.profile') }}" >
                                    <button type="submit" class="close" data-dismiss="modal">&times;</button>حذف
                                </a>
                                {{--@endif--}}
                            </div>
                        </div>
                    </div>
                    </form>
                    <hr>
                    <br>
                <form action="/coach/myprof/{{  $Coach->coach_id }}/update" method="post" style="width: 100%">
                    <style>
                        .form-group{
                            width: 500px;
                            direction: rtl;
                            font-size: 15px;
                        }
                        .form-control{
                            font-size: 15px;
                            width: 100%;
                        }
                    </style>
                    {{ csrf_field() }}

                    <div style="width: 80%;" class="mt-5 col-md-10 col-sm-6  form-group float-label-control">
                        <label for="">نام  </label>
                        <input value="{{$Coach->coach_name }}" name="coach_name" type="text" class="form-control" placeholder="نام " required>
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6  form-group float-label-control">
                        <label for="">نام خانوادگی </label>
                        <input value="{{$Coach->coach_family }}" name="coach_family" type="text" class="form-control" placeholder="نام خانوادگی" required>
                    </div>
                    <div style="width: 80%"  class="col-md-12 col-sm-6 form-group float-label-control">
                        <label for="">افتخارات مربی </label>
                        <textarea  required rows="10" cols="30"  name="coach_honors" type="text" class="form-control" placeholder="افتخارات مربی"> {{ $Coach->coach_honors }}</textarea>
                    </div>

                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <label for="">سن </label>
                        <input required value="{{ $Coach->coach_age }}" name="coach_age" type="text" class="form-control" placeholder="سن">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <label for="">استان  </label>
                        <input required value="{{ $Coach->coach_state }}" name="coach_state" type="text" class="form-control" placeholder="استان ">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <label for="">شماره کارت </label>
                        <input  value="{{ $Coach->coach_card_number }}" maxlength="16" name="coach_card_number" type="text" class="form-control" placeholder="شماره کارت">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <label for=""> شماره شبا</label>
                        <input  value="{{ $Coach->coach_shaba_number }}" maxlength="24" name="coach_shaba_number" type="text" class="form-control" placeholder="شماره شبا">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <label for="">شماره موبایل </label>
                        <input required value="{{ $Coach->coach_mobile }}" name="coach_mobile" type="text" class="form-control" placeholder="شماره موبایل">
                    </div>
                    {{--<div class="form-group float-label-control">--}}
                        {{--<label for="">ایمیل </label>--}}
                        {{--<input value="{{ $coach->email }}" name="email" type="text" class="form-control" placeholder="ایمیل">--}}
                    {{--</div>--}}
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <label for="">اینستاگرام </label>
                        <input  value="{{ $Coach->coach_instagram }}" name="coach_instagram" type="text" class="form-control" placeholder="اینستاگرام">
                    </div>
                    <button class="btn btn-outline-success btn-lg mr-5 mb-5 mt-5" style="alignment: center"  type="submit" value="اعمال تغییرات" >اعمال تغییرات</button>

                </form>
            </div>
            <br/>



            {{--{{ $orders->links() }}--}}

        </div>

    </article>
    <!-- END Main content -->


    <!-- Scripts -->
    {{--<script src="http://labs.infyom.com/laravelgenerator/public/assets/js/theDocs.all.min.js"></script>--}}
    {{--</body>--}}
    {{--</html>--}}

@endsection
