@extends('coach.test')


@section('content')
    <title>ثبت یا تغییر قیمت </title>


    <div class="col-xl-8 ml-5 center mt-5" style="background-color: #fffacc;box-shadow: 10px 10px 10px #888888;;padding: 7%;float: right;
  border-right-style: solid;
  border-bottom-style: dotted;
  border-left-style: solid;">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
            <div class="row" >
            <div style="float:right" class="mb-5 ">
                <h4 style="float: right">قیمت برنامه تمرینی: </h4>
                <h4>@if(isset($coachPrices->exercise_price)){{ $coachPrices->exercise_price }}   تومان@else نامشخص @endif</h4>
                <hr>
                <h4 style="float: right">قیمت برنامه غذایی:</h4>
                <h4>@if(isset($coachPrices->food_price)){{ $coachPrices->food_price }}    تومان@else نامشخص @endif</h4>
                <hr>
                <h4 style="float: right">قیمت برنامه مکملی:</h4>
                <h4>@if(isset($coachPrices->supplement_price)){{ $coachPrices->supplement_price }}    تومان@else نامشخص @endif
                </h4>
                <hr>
                <h4 style="float: right">تخفیف کلی :</h4> <br>
                <h4>@if(isset($coachPrices->discount)){{ $coachPrices->discount }}    درصد@else نامشخص @endif</h4>
            </div>

            <div class="col-lg-12"  >
                <h3 style="float: right" >تغییر یا افزودن قیمت </h3>
                <br>
                <hr>
                <form action="/coach/changeprice" method="post" style="float: right;text-align: right">
                    {{  csrf_field() }}
                    <div class="form-group float-label-control">
                        <label for="">قیمت برنامه تمرینی </label>
                        <input required value="@if(isset($coachPrices->exercise_price)){{ $coachPrices->exercise_price }} @endif" name="exercise_price" type="text" class="form-control" placeholder="قیمت برنامه تمرینی">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">قیمت برنامه غذایی </label>
                        <input required value="@if(isset($coachPrices->food_price)){{ $coachPrices->food_price }} @endif" name="food_price" type="text" class="form-control" placeholder="قیمت برنامه غذایی">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">قیمت برنامه مکملی </label>
                        <input required value="@if(isset($coachPrices->supplement_price)){{ $coachPrices->supplement_price }} @endif" name="supplement_price" type="text" class="form-control" placeholder="قیمت برنامه مکملی">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">تخفیف کلی </label>
                        <input required value="@if(isset($coachPrices->discount)){{ $coachPrices->discount }} @endif" name="discount" type="number" class="form-control" placeholder="تخفیف کلی">
                    </div>
                    <button class="btn btn-outline-success" type="submit" value="اعمال تغییرات" >اعمال تغییرات</button>
                </form>
            </div>
            </div>

    </div>
@endsection