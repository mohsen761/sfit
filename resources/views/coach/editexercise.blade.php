@extends('coach.test')

@section('content')

    <title>  ویرایش تمرین</title>
    <!-- Main content -->
    <article class="col-xl-12 main-content" role="main" style=";text-align: right;margin-top: 5%">
        <a href="/coach/myorders/" class="btn btn-primary"> بازگشت به عقب</a>
        @if(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if(session('success'))
            <div style="width: fit-content; float: right" class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="container " >


            <div class="col-xl-6" style="background-color: #fffacc;box-shadow: 10px 10px 10px #888888;border-width: 2px;border-style: groove;border-color: #3f9ae5;padding: 4%">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form style="width: 100%"  action="/coach/myorders/exercise/update/{{  $exercise->exercise_id }}/update" method="post">
                    <style>
                        .form-group{
                            width: 100%;
                            direction: rtl;
                            font-size: 15px;
                        }
                        .form-control{
                            font-size: 15px;
                        }
                    </style>
                    {{ csrf_field() }}
                    <div class="form-group float-label-control">
                        <label for="">حرکت </label>
                        <input value="{{$exercise->type_of_movement }}" name="type_of_movement" type="text" class="form-control" placeholder="حرکت">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">روز هفته </label>
                        <select name="days" id="" >
                            <option value="first" @if($exercise->days =='first') selected @endif>روز اول</option>
                            <option value="second" @if($exercise->days =='second') selected @endif>روز دوم</option>
                            <option value="third" @if($exercise->days =='third') selected @endif>روز سوم</option>
                            <option value="fourth" @if($exercise->days =='fourth') selected @endif>روز چهارم</option>
                            <option value="fifth" @if($exercise->days =='fifth') selected @endif>روز پنجم</option>
                            <option value="sixth" @if($exercise->days =='sixth') selected @endif>روز ششم</option>
                            <option value="seventh" @if($exercise->days =='seventh') selected @endif>روز هفتم</option>
                        </select>
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">ست </label>
                        <input value="{{ $exercise->sets }}" name="sets" type="text" class="form-control" placeholder="ست">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">تکرار هرست </label>
                        <input value="{{ $exercise->repetitions }}" name="repetitions" type="text" class="form-control" placeholder="تکرار هرست">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">سیستم تمرینی </label>
                        <input value="{{ $exercise->exercise_systems }}" name="exercise_systems" type="text" class="form-control" placeholder="سیستم تمرینی">
                    </div>
                    <button  type="submit" value="اعمال تغییرات" >اعمال تغییرات</button>

                </form>
            </div>
            <br/>



            {{--{{ $orders->links() }}--}}

        </div>

    </article>



    @endsection