@extends('coach.test')

@section('content')
    <title> درخواست های شاگردان</title>
    <!-- Main content -->
    <article class="col-md-8 main-content" role="main"  style="background-color: #fffacc; box-shadow: 10px 10px 10px #888888;;text-align: right;">

        <div class="container col">
            <a href="/coach/myorders" class="btn btn-primary" style="width: fit-content"> بازگشت به عقب</a>
                <div style="margin-top:10%;border-width: 2px;border-style: groove;border-color: #3f9ae5;width: 100%;padding: 4%">
                @if($order->program_state == 'not_sent')
                    <a role="button" class="btn" href="/coach/myorders/{{ $order->order_id }}">
                        <article class="post ">
                            <header>
                                <div class="row">
                                    <h2>
                                        <p>{{$order->firstname }}{{$order->lastname }}</p>
                                    </h2>
                                </div>
                            </header>
                            <div class="post-content ">
                                <div class="row">
                                    @if($order->exercise =='yes') <h1><p>برنامه تمرینی</p> </h1>/ @endif
                                    @if($order->food =='yes') <h1><p>برنامه غذایی</p>  </h1>/ @endif
                                    @if($order->supplement =='yes') <h1><p> </h1>برنامه مکملی</p>/ @endif
                                </div>
                                <div class="row">
                                    <p>  {{ $order->price }} تومان  </p>
                                </div>
                                <div class="row">
                                    <p>تاریخ: {{ str_replace("_","/", $order->order_date) }}</p>
                                </div>
                                <div class="row">
                                    @if($order->program_state == 'sent') <p>برنامه باموفقیت ارسال شده است</p> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt="">
                                    @else
                                        <p>برنامه ای هنوز ارسال نشده است</p>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt="">
                                    @endif
                                </div>
                                <div class="row" style="margin-top: 2%">
                                    @if($order->payment_state == 'paid') <p>برنامه باموفقیت تسویه حساب شده است</p> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt="">
                                    @else
                                        <p>برنامه هنوز تسویه حساب نشده است</p>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt="">
                                    @endif
                                </div>
                            </div>
                        </article>
                    </a>
                </div>
                <br/>
            <div class="col-xl-12">
                <button type="button" name="add" id="addfigure" class="btn btn-success">مشاهده عکس های فیگوری</button>
                <div id="dynamic_field" style=""></div>
                {{--modal--}}
                <div class="modal fade" id="modal_detail">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"> عکس های فیگوری</h4>
                                <button id="close" type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div  class="modal-body">
                                <div id="figure-modal" ></div>
                            </div>
                            <div class="modal-footer">
                                <a role="button" id="close1" href="#" data-dismiss="modal" class="btn btn-danger">بستن</a>
                            </div>
                        </div>
                    </div>
                </div>
                {{--modal--}}

                <input name="user_id" type="hidden" value="{{ $order->user_id }}">
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#close').click(function () {
                            $('#figure-modal').html('');
                        });
                        $('#close1').click(function () {
                            $('#figure-modal').html('');
                        });
                        $('#addfigure').click(function () {
                            let userid = $('input[name="user_id"]').val();
                            $.ajax({
                                url:"{{ route('figure.fetch') }}",
                                method:"GET",
                                data:{id:userid},
                                success:function(data){
                                    $('#dynamic_field').fadeIn();
                                    $('#dynamic_field').html(data);
                                }
                            });
                        });

                    });

                </script>
                <script >
                    $(document).on('click', 'li', function(){
                        $('#dynamic_field').fadeOut();
                        let id_beli = $(this).text();
                        let userid = $('input[name="user_id"]').val();
                        let res1 = id_beli.slice(32, 42);
                        let res2 = res1.replace("/", "_");
                        let res = res2.replace("/", "_");
                        // let orderdate = $('a[id="modalfigure"]').val();
                       $('#figure-modal').append('<div >'+"جفت بازو از جلو"+' <img onerror="this.onerror=null; this.src=\'/images/noimage.png\'"style="width: 70%;" src="/bodybuilding/student/images/joft_bazu_az_jolo/'+userid+'/'+res+'.png" alt=""></div>');
                       $('#figure-modal').append('<div class="mt-5" ">'+"جفت بازو از پشت"+' <img onerror="this.onerror=null; this.src=\'/images/noimage.png\'" style="width: 70%;" src="/bodybuilding/student/images/joft_bazu_az_posht/'+userid+'/'+res+'.png" alt=""></div>');
                       $('#figure-modal').append('<div class="mt-5">'+"پشت بازو از بغل"+' <img onerror="this.onerror=null; this.src=\'/images/noimage.png\'"style="width: 70%;" src="/bodybuilding/student/images/posht_bazu_az_baghal/'+userid+'/'+res+'.png" alt=""></div>');
                       $('#figure-modal').append('<div class="mt-5">'+"شکم و پاها از جلو"+' <img onerror="this.onerror=null; this.src=\'/images/noimage.png\'"style="width: 70%;" src="/bodybuilding/student/images/shekam_va_paha_az_jolo/'+userid+'/'+res+'.png" alt=""></div>');
                       $('#figure-modal').append('<div class="mt-5">'+"زیربغل از جلو"+' <img onerror="this.onerror=null; this.src=\'/images/noimage.png\'"style="width: 70%;" src="/bodybuilding/student/images/zir_baghal_az_jolo/'+userid+'/'+res+'.png" alt=""></div>');
                       $('#figure-modal').append('<div class="mt-5">'+"زیربغل از پشت"+' <img onerror="this.onerror=null; this.src=\'/images/noimage.png\'"style="width: 70%;" src="/bodybuilding/student/images/zir_baghal_az_posht/'+userid+'/'+res+'.png" alt=""></div>');
                       $('#figure-modal').append('<div class="mt-5">'+"قفسه سینه"+' <img onerror="this.onerror=null; this.src=\'/images/noimage.png\'"style="width: 70%;" src="/bodybuilding/student/images/ghafase_sine/'+userid+'/'+res+'.png" alt=""></div>');
                       $('#figure-modal').append('<div class="mt-5">'+"نیم رخ"+' <img onerror="this.onerror=null; this.src=\'/images/noimage.png\'"style="width: 70%;" src="/bodybuilding/student/images/nimrokh/'+userid+'/'+res+'.png" alt=""></div>');

                    });
                </script>
                {{--$figure1 = "http://sfit.ir/bodybuilding/student/images/joft_bazu_az_jolo/$order->user_id/$figureImageDate.png";--}}
                {{--$figure2 = "http://sfit.ir/bodybuilding/student/images/joft_bazu_az_posht/$order->user_id/$figureImageDate.png";--}}
                {{--$figure3 = "http://sfit.ir/bodybuilding/student/images/posht_bazu_az_baghal/$order->user_id/$figureImageDate.png";--}}
                {{--$figure4 = "http://sfit.ir/bodybuilding/student/images/shekam_va_paha_az_jolo/$order->user_id/$figureImageDate.png";--}}
                {{--$figure5 = "http://sfit.ir/bodybuilding/student/images/zir_baghal_az_jolo/$order->user_id/$figureImageDate.png";--}}
                {{--$figure6 = "http://sfit.ir/bodybuilding/student/images/zir_baghal_az_posht/$order->user_id/$figureImageDate.png";--}}
                {{--$figure7 = "http://sfit.ir/bodybuilding/student/images/ghafase_sine/$order->user_id/$figureImageDate.png";--}}
                {{--$figure8 = "http://sfit.ir/bodybuilding/student/images/nimrokh/$order->user_id/$figureImageDate.png",--}}
            </div>
                <div class="col-lg-12 mt-5" style=""  >

                    @if($order->exercise =='yes')
                       <div class="col-lg-12" style=";width: 100% ;border-color: #3f9ae5 ;border-radius: 2px;border-width: 2px;border-style: groove;padding: 4%">
                           <p> ارسال برنامه تمرینی</p>
                        {{--@if(isset($program) && $program->program_type == 'exercise')--}}
                               <a  href="/coach/myorders/exercise/{{$order->order_id}}">ویرایش برنامه های متنی گذشته</a>
                           <hr>
                        {{--@else--}}
                           <a  href="/coach/myorders/{{ $order->order_id }}/exercise/checkbox">ارسال آسان(جدید)</a>
                           <hr>
                               <a  href="/coach/myorders/{{ $order->order_id }}/exercise/image">ارسال عکسی</a>
                               <hr>
                               <a  href="/coach/myorders/{{ $order->order_id }}/exercise/text">ارسال متنی</a>
                               <hr>
                        {{--@endif--}}
                         </div>
                    @endif
                    @if($order->food =='yes')
                            <div class="col-lg-12" style="margin-top: 2%;width: 100% ;border-color: #3f9ae5 ;border-radius: 2px;border-width: 2px;border-style: groove;padding: 4%">
                                <p> ارسال برنامه غذایی</p>
                                {{--@if(isset($program) && $program->program_type == 'food')--}}
                                    <a href="/coach/myorders/food/{{$order->order_id}}">ویرایش برنامه های متنی گذشته</a>
                                <hr>
                                {{--@else--}}
                                <a href="/coach/myorders/{{ $order->order_id }}/food/image">ارسال به صورت عکس</a>
                                <hr>
                                <a href="/coach/myorders/{{ $order->order_id }}/food/text">ارسال به صورت متن</a>
                                    {{--@endif--}}
                            </div>
                        @endif
                        @if($order->supplement =='yes')
                            <div class="col-lg-12" style="margin-top: 2%;width: 100% ;border-color: #3f9ae5 ;border-radius: 2px;border-width: 2px;border-style: groove;padding: 4%">
                                <p> ارسال برنامه مکملی</p>
                                {{--@if(isset($program) && $program->program_type == 'supplement')--}}
                                    <a href="/coach/myorders/supplement/{{$order->order_id}}">ویرایش برنامه های متنی گذشته</a>
                                <hr>
                                {{--@else--}}
                                <a href="/coach/myorders/{{ $order->order_id }}/supplement/image">ارسال به صورت عکس</a>
                                <hr>
                                <a href="/coach/myorders/{{ $order->order_id }}/supplement/text">ارسال به صورت متن</a>
                                    {{--@endif--}}
                            </div>
                        @endif

            </div>
            <div style="margin-top: 2%;border-color: #3f9ae5 ;border-radius: 2px;border-width: 2px;border-style: groove;padding: 4%">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <form action="/coach/myorders/{{ $order->order_id }}/sendprogram" method="post">
                    {{ csrf_field() }}
                <h5>با زدن این دکمه شما فرستادن تمام برنامه های شاگرد خود را تایید کرده و دیگر نمیتوانید آن را تغییر دهید.</h5>
                <hr>
                <button type="submit" class="btn btn-lg btn-outline-success">فرستادن نهایی برنامه ها</button>
                </form>
            </div>
            @else
                @if($order->exercise =='yes')
                <div >
                    <a role="button" class="btn btn-lg btn-outline-success" href="/coach/myorders/exercise/{{ $order->order_id }}"> دیدن برنامه/برنامه های تمرینی فرستاده شده </a>
                </div>
                @endif

                    @if($order->food =='yes')
                <div>
                    <a role="button" class="btn btn-lg btn-outline-success" href="/coach/myorders/food/{{ $order->order_id }}"> دیدن برنامه/برنامه های غذایی فرستاده شده </a>
                </div>
                @endif

                    @if($order->supplement =='yes')
                <div>
                    <a role="button" class="btn btn-lg btn-outline-success" href="/coach/myorders/supplement/{{ $order->order_id }}"> دیدن برنامه/برنامه های مکملی فرستاده شده </a
                </div>
                @endif
            @endif
            {{--عکس های فیگوری--}}

            {{--<div class="row mt-4">--}}
                {{--عکس های فیگوری --}}{{--}}--}}
                {{--$figure1 = "http://sfit.ir/bodybuilding/student/images/joft_bazu_az_jolo/$order->user_id/$figureImageDate.png";--}}
                {{--$figure2 = "http://sfit.ir/bodybuilding/student/images/joft_bazu_az_posht/$order->user_id/$figureImageDate.png";--}}
                {{--$figure3 = "http://sfit.ir/bodybuilding/student/images/posht_bazu_az_baghal/$order->user_id/$figureImageDate.png";--}}
                {{--$figure4 = "http://sfit.ir/bodybuilding/student/images/shekam_va_paha_az_jolo/$order->user_id/$figureImageDate.png";--}}
                {{--$figure5 = "http://sfit.ir/bodybuilding/student/images/zir_baghal_az_jolo/$order->user_id/$figureImageDate.png";--}}
                {{--$figure6 = "http://sfit.ir/bodybuilding/student/images/zir_baghal_az_posht/$order->user_id/$figureImageDate.png";--}}
                {{--$figure7 = "http://sfit.ir/bodybuilding/student/images/ghafase_sine/$order->user_id/$figureImageDate.png";--}}
                {{--$figure8 = "http://sfit.ir/bodybuilding/student/images/nimrokh/$order->user_id/$figureImageDate.png",--}}
                {{--}}--}}
                {{--<a href="http://sfit.ir/bodybuilding/student/images/joft_bazu_az_jolo/{{$order->user_id }}/{{$order->order_date}}.png"> click</a>--}}
                {{--<div class="post-content">--}}
                    {{--<div class="post-graphical-image mr-3" style="" >--}}
                        {{--جفت بازو از جلو--}}
                            {{--<img class="img-thumbnail " style="width:70%;height:auto;" src="--}}
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/student/images/joft_bazu_az_jolo/'.$order->user_id.'/'.$order->order_date.'.png'))--}}
                                    {{--http://sfit.ir/bodybuilding/student/images/joft_bazu_az_jolo/{{$order->user_id }}/{{$order->order_date}}.png--}}
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                                    {{--" >--}}

                    {{--</div>--}}
                    {{--<div class="post-graphical-image mr-3" style="" >--}}
                        {{--جفت بازو از پشت--}}
                            {{--<img class="img-thumbnail " style="width:70%;height:auto;" src="--}}
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/student/images/joft_bazu_az_posht/'.$order->user_id.'/'.$order->order_date.'.png'))--}}
                                    {{--http://sfit.ir/bodybuilding/student/images/joft_bazu_az_posht/{{$order->user_id }}/{{$order->order_date}}.png--}}
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                                    {{--" >--}}

                    {{--</div>--}}
                    {{--<div class="post-graphical-image mr-3" style="" >--}}
                        {{--پشت بازو از بغل--}}
                            {{--<img class="img-thumbnail " style="width:70%;height:auto;" src="--}}
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/student/images/posht_bazu_az_baghal/'.$order->user_id.'/'.$order->order_date.'.png'))--}}
                                    {{--http://sfit.ir/bodybuilding/student/images/posht_bazu_az_baghal/{{$order->user_id }}/{{$order->order_date}}.png--}}
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                                    {{--" >--}}

                    {{--</div>--}}
                    {{--<div class="post-graphical-image mr-3" style="" >--}}
                        {{--شکم و پاها از جلو--}}
                            {{--<img class="img-thumbnail " style="width:70%;height:auto;" src="--}}
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/student/images/shekam_va_paha_az_jolo/'.$order->user_id.'/'.$order->order_date.'.png'))--}}
                                    {{--http://sfit.ir/bodybuilding/student/images/shekam_va_paha_az_jolo/{{$order->user_id }}/{{$order->order_date}}.png--}}
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                                    {{--" >--}}

                    {{--</div>--}}
                    {{--<div class="post-graphical-image mr-3" style="" >--}}
                        {{--زیربغل از جلو--}}
                            {{--<img class="img-thumbnail " style="width:70%;height:auto;" src="--}}
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/student/images/zir_baghal_az_jolo/'.$order->user_id.'/'.$order->order_date.'.png'))--}}
                                    {{--http://sfit.ir/bodybuilding/student/images/zir_baghal_az_jolo/{{$order->user_id }}/{{$order->order_date}}.png--}}
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                                    {{--" >--}}

                    {{--</div>--}}
                    {{--<div class="post-graphical-image mr-3" style="" >--}}
                        {{--زیربغل از پشت--}}
                            {{--<img class="img-thumbnail " style="width:70%;height:auto;" src="--}}
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/student/images/zir_baghal_az_posht/'.$order->user_id.'/'.$order->order_date.'.png'))--}}
                                    {{--http://sfit.ir/bodybuilding/student/images/zir_baghal_az_posht/{{$order->user_id }}/{{$order->order_date}}.png--}}
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                                    {{--" >--}}

                    {{--</div>--}}
                    {{--<div class="post-graphical-image mr-3" style="" >--}}
                        {{--قفسه سینه--}}
                            {{--<img class="img-thumbnail " style="width:70%;height:auto;" src="--}}
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/student/images/ghafase_sine/'.$order->user_id.'/'.$order->order_date.'.png'))--}}
                                    {{--http://sfit.ir/bodybuilding/student/images/ghafase_sine/{{$order->user_id }}/{{$order->order_date}}.png--}}
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                                    {{--" >--}}

                    {{--</div>--}}
                    {{--<div class="post-graphical-image mr-3" style="" >--}}
                       {{--نیمرخ--}}
                            {{--<img class="img-thumbnail " style="width:70%;height:auto;" src="--}}
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/student/images/nimrokh/'.$order->user_id.'/'.$order->order_date.'.png'))--}}
                                    {{--http://sfit.ir/bodybuilding/student/images/nimrokh/{{$order->user_id }}/{{$order->order_date}}.png--}}
                                {{--@else--}}
                                    {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                                    {{--" >--}}

                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--عکس های فیگوری--}}
        </div>

    </article>
    <!-- END Main content -->


    <!-- Scripts -->
    {{--<script src="http://labs.infyom.com/laravelgenerator/public/assets/js/theDocs.all.min.js"></script>--}}
    {{--</body>--}}
    {{--</html>--}}

@endsection

