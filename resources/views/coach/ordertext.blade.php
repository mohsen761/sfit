@extends('coach.test')

@section('content')
    <title>ارسال به صورت متن</title>
    <div class="row">
        <div class="col-lg-10" style=" box-shadow: 10px 10px 10px #888888;padding: 5%;margin-left: -100%;width: 100% ;">
            @if(session('success'))
                <div style="float: right;text-align: right;" class="alert alert-success alert-dark ">
                    {{ session('success') }}
                </div><br>
            @endif
            {{--exercise--}}
                @if($type =='exercise')
                  <div class="container">
                <h4 align="center">ارسال برنامه به صورت متن-برای تفکیک برنامه روزانه میتوانید انها را مشخص کنید و علامت اضافه را بزنید</h4>
                <hr>
                <div class="form-group">
                    <form method="post" action="/coach/myorders/{{ $order->order_id }}/{{ $type }}/text" name="add_name" id="add_name">
                                {{ csrf_field() }}
                          @if(isset($program_exercise))
                            <input type="text" name="title" value="{{ $program_exercise->title }}" class="form-control name_list" required="" />
                            <input type="number" name="period" value="{{ $program_exercise->period }}" class="form-control name_list" required="" />
                            <input type="text" name="description" value="{{ $program_exercise->description }}" class="form-control name_list" required="" />
                            <input type="hidden" name="program_type" value="{{ $type }}" />
                            <input type="hidden" name="program_exercise_update" value="{{ $program_exercise->program_id }}" />
                            <hr>
                            @else
                            <input type="text" name="title" placeholder=" عنوان برنامه را وارد کنید" class="form-control name_list" required="" />
                            <input type="number" name="period" placeholder=" مدت زمان برنامه را وارد کنید" class="form-control name_list" required="" />
                            <input type="text" name="description" placeholder="توضیحات برنامه را وارد کنید" class="form-control name_list" required="" />
                        <input type="hidden" name="program_type" value="{{ $type }}" />
                            <input type="hidden" name="program_exercise_update" value="no" />
                        <hr>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dynamic_field">
                                {{--<tr>--}}
                                    {{--<td><input id="movement_" type="text" name="name[]" placeholder="حرکت " class="form-control name_list" required="" />--}}
                                        {{--<div id="movementList" style="float: right;text-align: right">--}}
                                        {{--</div>--}}
                                    {{--</td>--}}
                                    {{--<td><input  style="width: 50px" type="text" name="set[]" placeholder="ست" class="form-control name_list" required="" /></td>--}}
                                    {{--<td><input  style="width: 70px;margin-right: -100px" type="text" name="repetitions[]" placeholder="تکرار" class="form-control name_list" required="" /></td>--}}
                                {{--</tr>--}}
                                <tr>
                                    {{--<td><select name="day[]" id="" style="width: 100%">--}}
                                            {{--<option value="first"> روز اول</option>--}}
                                            {{--<option value="second"> روز دوم</option>--}}
                                            {{--<option value="third"> روز سوم</option>--}}
                                            {{--<option value="fourth"> روز چهارم</option>--}}
                                            {{--<option value="fifth"> روز پنجم</option>--}}
                                            {{--<option value="sixth"> روز ششم</option>--}}
                                            {{--<option value="seventh"> روز هفتم</option>--}}
                                        {{--</select></td>--}}
                                    {{--<td><input   type="text" name="explain[]" placeholder="توضیحات" class="form-control name_list"  /></td>--}}
                                    {{--<td><input  style="width: 150px" type="text" name="exercise_systems[]" placeholder="سیستم تمرینی" class="form-control name_list"  /></td>--}}
                                    <td></td>
                                </tr>
                            </table>
                            <button type="button" name="add" id="add" class="btn btn-success">افزودن ردیف برنامه</button>
                            <input type="submit" name="submit" id="submit" class="btn btn-info" value="ارسال برنامه" />
                        </div>


                    </form>
                </div>
            </div>
                    {{--<div class="container box">--}}
                       {{--<br />--}}

                        {{--<div class="form-group">--}}
                            {{--<input type="text" name="movement_name" id="movement_name" class="form-control input-lg" placeholder="نام حرکت را بنویسید" />--}}
                            {{--<div id="movementList" style="float: right;text-align: right">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--{{ csrf_field() }}--}}
                    {{--</div>--}}

                    {{--<script>--}}
                        {{--$(document).ready(function(){--}}

                            {{--$('#movement_').keyup(function(){--}}
                                {{--var query = $(this).val();--}}
                                {{--if(query != '')--}}
                                {{--{--}}
                                    {{--var _token = $('input[name="_token"]').val();--}}
                                    {{--$.ajax({--}}
                                        {{--url:"{{ route('autocomplete.fetch') }}",--}}
                                        {{--method:"POST",--}}
                                        {{--data:{query:query, _token:_token},--}}
                                        {{--success:function(data){--}}
                                            {{--$('#movementList').fadeIn();--}}
                                            {{--$('#movementList').html(data);--}}
                                        {{--}--}}
                                    {{--});--}}
                                {{--}--}}
                            {{--});--}}

                            {{--$(document).on('click', 'li', function(){--}}
                                {{--$('#movement_').val($(this).text());--}}
                                {{--$('#movementList').fadeOut();--}}
                            {{--});--}}

                        {{--});--}}
                    {{--</script>--}}
                @endif
                {{--exercise--}}

            {{--food--}}
        @if($type =='food')
                    <div class="container">
                        <h4 align="center">ارسال برنامه به صورت متن-برای تفکیک برنامه روزانه میتوانید انها را مشخص کنید و علامت اضافه را بزنید</h4>
                        <hr>
                        <div class="form-group">
                            <form method="post" action="/coach/myorders/{{ $order->order_id }}/{{ $type }}/text" name="add_name" id="add_name">
                                {{ csrf_field() }}
                                @if(isset($program_food))
                                    <input type="text" name="title" value="{{ $program_food->title }}" class="form-control name_list" required="" />
                                    <input type="text" name="period" value="{{ $program_food->period }}" class="form-control name_list" required="" />
                                    <input type="text" name="description" value="{{ $program_food->description }}" class="form-control name_list" required="" />
                                    <input type="hidden" name="program_type" value="{{ $type }}" />
                                    <input type="hidden" name="program_food_update" value="{{ $program_food->program_id }}" />
                                    <hr>
                                @else
                                    <input type="text" name="title" placeholder="عنوان برنامه" class="form-control name_list" required="" />
                                    <input type="text" name="period" placeholder="مدت زمان برنامه" class="form-control name_list" required="" />
                                    <input type="text" name="description" placeholder="توضیحات برنامه" class="form-control name_list" required="" />
                                    <input type="hidden" name="program_type" value="{{ $type }}" />
                                    <input type="hidden" name="program_food_update" value="no" />
                                    <hr>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dynamic_field">
                                        {{--<tr>--}}
                                            {{--<td><select name="meal[]" id="" style="width: 100%">--}}
                                                    {{--<option value="breakfast"> صبحانه</option>--}}
                                                    {{--<option value="snack"> ناشتا</option>--}}
                                                    {{--<option value="lunch"> نهار</option>--}}
                                                    {{--<option value="before_exercise"> قبل از تمرین</option>--}}
                                                    {{--<option value="after_exercise"> بعد از تمرین</option>--}}
                                                    {{--<option value="dinner"> شام </option>--}}
                                                    {{--<option value="before_sleep"> قبل از خواب</option>--}}
                                                {{--</select></td>--}}

                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td><input  style="" type="text" name="offer1[]" placeholder="پیشنهاد اول" class="form-control name_list" required="" /></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td><input  style="" type="text" name="offer2[]" placeholder="پیشنهاد دوم" class="form-control name_list" /></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td><input  style="" type="text" name="offer3[]" placeholder="پیشنهاد سوم" class="form-control name_list"  /></td>--}}
                                            {{--<td></td>--}}
                                        {{--</tr>--}}
                                    </table><button type="button" name="add" id="addfood" class="btn btn-success">افزودن ردیف برنامه</button>
                                    <input type="submit" name="submit" id="submit" class="btn btn-info" value="ارسال برنامه" />
                                </div>


                            </form>
                        </div>
                    </div>
             @endif
            {{--food--}}


            {{--supplemet--}}
                @if($type =='supplement')
                    <div class="container">
                        <h4 align="center">ارسال برنامه به صورت متن-برای تفکیک برنامه روزانه میتوانید انها را مشخص کنید و علامت اضافه را بزنید</h4>
                        <hr>
                        <div class="form-group">
                            <form method="post" action="/coach/myorders/{{ $order->order_id }}/{{ $type }}/text" name="add_name" id="add_name">
                                {{ csrf_field() }}
                                @if(isset($program_supplement))
                                    <input type="text" name="title" value="{{ $program_supplement->title }}" class="form-control name_list" required="" />
                                    <input type="text" name="period" value="{{ $program_supplement->period }}" class="form-control name_list" required="" />
                                    <input type="text" name="description" value="{{ $program_supplement->description }}" class="form-control name_list" required="" />
                                    <input type="hidden" name="program_type" value="{{ $type }}" />
                                    <input type="hidden" name="program_supplement_update" value="{{ $program_supplement->program_id }}" />
                                    <hr>
                                @else
                                    <input type="text" name="title" placeholder="عنوان برنامه" class="form-control name_list" required="" />
                                    <input type="text" name="period" placeholder="مدت زمان برنامه" class="form-control name_list" required="" />
                                    <input type="text" name="description" placeholder="توضیحات برنامه" class="form-control name_list" required="" />
                                    <input type="hidden" name="program_type" value="{{ $type }}" />
                                    <input type="hidden" name="program_supplement_update" value="no" />
                                    <hr>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dynamic_field">
                                        {{--<tr>--}}
                                            {{--<td><input  style="" type="text" name="supplement_name[]" placeholder="نام مکمل" class="form-control name_list" required="" /></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td><input  style="" type="text" name="time_to_eat[]" placeholder="زمان مصرف" class="form-control name_list" required="" /></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<td><input  style="" type="text" name="dosage[]" placeholder="مقدار مصرف" class="form-control name_list" /></td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}

                                            {{--<td><select name="day_to_eat[]" id="">--}}
                                                    {{--<option value="training_day"> روز تمرین؟</option>--}}
                                                    {{--<option value="rest_day"> روز استراحت؟</option>--}}
                                                {{--</select></td>--}}
                                            {{--<td></td>--}}
                                        {{--</tr>--}}
                                    </table><button type="button" name="add" id="addsupplement" class="btn btn-success">افزودن ردیف برنامه</button>
                                    <input type="submit" name="submit" id="submit" class="btn btn-info" value="ارسال برنامه" />
                                </div>


                            </form>
                        </div>
                    </div>
                @endif

            {{--supplemet--}}
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            let postURL = "/addmore.php";
            let i=1;
            let j=1;
            let k=1;

            $('#add').click(function(){
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'" class="dynamic-added"><td>' +
                    '<input style="width: 200%;" id="movement_name'+i+'" type="text" name="name[]" placeholder="نام حرکت" class="form-control name_list" required="" /><div id="movementList'+i+'" style="float: right;text-align: right">\n' +
                    '                                        </div></td>\n' +
                    ''+
                    '</tr>' +
                    '<tr id="row'+i+'"><td> <input style="width: 70px" type="text" name="set[]" placeholder="ست" class="form-control name_list"  /></td> <td></td> <td><input  style="width: 70px;margin-right: -100px" type="text" name="repetitions[]" placeholder="تکرار" class="form-control name_list"  /></td> </tr>' +
                    '' +
                    '<tr id="row'+i+'">\n' +
                    '                                    <td><select name="day[]" id="" style="width: 100%;margin-top: 2px;height:35px;">\n' +
                    '                                            <option value="first"> روز اول</option>\n' +
                    '                                            <option value="second"> روز دوم</option>\n' +
                    '                                            <option value="third"> روز سوم</option>\n' +
                    '                                            <option value="fourth"> روز چهارم</option>\n' +
                    '                                            <option value="fifth"> روز پنجم</option>\n' +
                    '                                            <option value="sixth"> روز ششم</option>\n' +
                    '                                            <option value="seventh"> روز هفتم</option>\n' +
                    '                                        </select></td>\n' +
                    '                                    <td><input  style="width: 150px" type="text" name="exercise_systems[]" placeholder="سیستم تمرینی" class="form-control name_list"  /></td>\n' +
                    '                                    <td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">حذف </button></td>\n' +
                    '                                <hr></tr>');
                $('#movement_name'+i+'').keyup(function(){
                    let query = $(this).val();
                    if(query != '')
                    {
                        var _token = $('input[name="_token"]').val();
                        $.ajax({
                            url:"{{ route('autocomplete.fetch') }}",
                            method:"POST",
                            data:{query:query, _token:_token},
                            success:function(data){
                                $('#movementList'+i+'').fadeIn();
                                $('#movementList'+i+'').html(data);
                            }
                        });
                    }
                });

                $(document).on('click', 'li', function(){
                    $('#movement_name'+i+'').val($(this).text());
                    $('#movementList'+i+'').fadeOut();
                });
            });
            $('#addfood').click(function(){
                j++;
                $('#dynamic_field').append('<tr id="row'+j+'" class="dynamic-added">' +
                   ' <td><select name="meal[]" id="" style="width: 100%">'+
                   ' <option value="breakfast"> صبحانه</option>'+
                   ' <option value="snack"> ناشتا</option>'+
                   ' <option value="lunch"> نهار</option>'+
                   ' <option value="before_exercise"> قبل از تمرین</option>'+
                      '<option value="after_exercise"> بعد از تمرین</option>'+
                     ' <option value="dinner"> شام </option>'+
                    ' <option value="before_sleep"> قبل از خواب</option>'+
                         '</select></td>'+
                     '</tr>' +
                    '<tr id="row'+j+'">\n' +
                    '<td><input  style="" type="text" name="offer1[]" placeholder="پیشنهاد اول" class="form-control name_list" required="" /></td></tr>'+
                    '<tr id="row'+j+'">\n' +
                    '<td><input  style="" type="text" name="offer2[]" placeholder="پیشنهاد دوم" class="form-control name_list"  /></td></tr>'+
                    '<tr id="row'+j+'">\n' +
                    '<td><input  style="" type="text" name="offer3[]" placeholder="پیشنهاد سوم" class="form-control name_list"  /></td>'+
                   ' <td><button type="button" name="remove" id="'+j+'" class="btn btn-danger btn_removefood">X</button></td>'+
                 '</tr>');
            });
            $('#addsupplement').click(function(){
                k++;
                $('#dynamic_field').append('<tr id="row'+k+'" class="dynamic-added">' +
                   ' <td><input  style="" type="text" name="supplement_name[]" placeholder="نام مکمل" class="form-control name_list" required="" /></td>'+
                     '</tr>' +
                    '<tr id="row'+k+'">\n' +
                    ' <td><input  style="" type="text" name="time_to_eat[]" placeholder="زمان مصرف" class="form-control name_list" required="" /></td></tr>'+
                    '<tr id="row'+k+'">\n' +
                    ' <td><input  style="" type="text" name="dosage[]" placeholder="مقدار مصرف" class="form-control name_list" required="" /></td></tr>'+
                    '<tr id="row'+k+'">\n' +
                    '<td><select name="day_to_eat[]" id="" required="">\n' +
                    '                                                    <option value="training_day"> روز تمرین</option>\n' +
                    '                                                    <option value="rest_day"> روز استراحت</option>\n' +
                    '                                                    <option value="training_rest_day"> روز استراحت و تمرین</option>\n' +
                    '                                                </select></td>'+
                   ' <td><button type="button" name="remove" id="'+k+'" class="btn btn-danger btn_removesupplement">X</button></td>'+
                 '</tr>');
            });


            $(document).on('click', '.btn_remove', function(){
                j--;
                let button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                $('#row'+button_id+'').remove();
                $('#row'+button_id+'').remove();
            });

            $(document).on('click', '.btn_removefood', function(){
                let button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                $('#row'+button_id+'').remove();
                $('#row'+button_id+'').remove();
                $('#row'+button_id+'').remove();
                $('#row'+button_id+'').remove();
            });
            $(document).on('click', '.btn_removesupplement', function(){
                let button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
                $('#row'+button_id+'').remove();
                $('#row'+button_id+'').remove();
                $('#row'+button_id+'').remove();

            });


            $('#submit').click(function(){
                // $.ajax({
                //     url:postURL,
                //     method:"POST",
                //     data:$('#add_name').serialize(),
                //     type:'json',
                //     success:function(data)
                //     {
                //         i=1;
                //         $('.dynamic-added').remove();
                //         $('#add_name')[0].reset();
                //         alert('Record Inserted Successfully.');
                //     }
                });



        });
    </script>
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
@endsection