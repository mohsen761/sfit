@extends('coach.test')

@section('content')



    <title>  ویرایش برنامه مکملی</title>
    <!-- Main content -->
    <article class="col-xl-12 main-content" role="main" style=";text-align: right;margin-top: 5%">
        <a href="/coach/myorders/" class="btn btn-primary"> بازگشت به عقب</a>
        <div class="container " >
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if(session('success'))
                <div style="width: fit-content; float: right" class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <div class="col-xl-6" style="background-color: #fffacc;box-shadow: 10px 10px 10px #888888;border-width: 2px;border-style: groove;border-color: #3f9ae5;padding: 4%">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form style="width: 100%"  action="/coach/myorders/supplement/update/{{  $supplement->supplement_id }}/update" method="post">
                    <style>
                        .form-group{
                            width: 100%;
                            direction: rtl;
                            font-size: 15px;
                        }
                        .form-control{
                            font-size: 15px;
                        }
                    </style>
                    {{ csrf_field() }}

                    <div class="form-group float-label-control">
                        <label for="">نام مکمل </label>
                        <input value="{{ $supplement->supplement_name }}" name="supplement_name" type="text" class="form-control" placeholder="نام مکمل">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">زمان مصرف </label>
                        <input value="{{ $supplement->time_to_eat }}" name="time_to_eat" type="text" class="form-control" placeholder="زمان مصرف">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">مقدار مصرف </label>
                        <input value="{{ $supplement->dosage }}" name="dosage" type="text" class="form-control" placeholder="مقدار مصرف">
                    </div>
                    <div class="form-group float-label-control">
                        <label for="">روز مصرف؟ </label>
                        <select name="day_to_eat" id="" >
                            <option value="training_day" @if($supplement->day_to_eat=='training_day') selected @endif> روز تمرین</option>
                            <option value="rest_day" @if($supplement->day_to_eat=='rest_day') selected @endif> روز استراحت</option>
                        </select>
                    </div>


                    <button  type="submit" value="اعمال تغییرات" >اعمال تغییرات</button>

                </form>
            </div>
            <br/>



            {{--{{ $orders->links() }}--}}

        </div>

    </article>



@endsection