@extends('coach.test')

@section('content')

    <title>برنامه های غذایی</title>
    <a href="/coach/myorders" class="btn btn-primary" style="width: fit-content"> بازگشت به عقب</a>
    @foreach($programs as $program)
        <table class=" table table-striped table-responsive-sm table-bordered table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>

                <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

                <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->program_date,10)) }}</h5></td>

                @if($program_state =='not_sent')
                    <td>   <h5>عملیات</h5></td>
                @endif
            </tr>
            <tr>
                <td scope="col"><h6>وعده:</h6></td>
                <td scope="col"><h6>پیشنهاد اول:</h6></td>
                <td scope="col"><h6>پیشنهاد دوم:</h6></td>
                <td scope="col"><h6>پیشنهاد سوم:</h6></td>

                {{--@if($program_state =='not_sent')--}}
                {{--<td scope="col">   <h5></h5></td>--}}
                {{--@endif--}}
                <td>
                    {{--<a class="btn btn-outline-warning" href="/coach/myorders/program/edit/{{ $program->program_id }}/edit"> ویرایش برنامه</a>--}}
                    @if($program_state =='not_sent')
                    <a class="btn btn-outline-danger" href="/coach/myorders/program/delete/{{ $program->program_id }}/delete"> حذف برنامه</a></td>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($foods as $item)

                @foreach($item as $food)

                    @if($program->program_id == $food->program_id)
                        <tr>
                            <td scope="row">  <h6>@if($food->meal =='breakfast')
صبحانه
                                    @elseif($food->meal =='snack')ناشتا
                                    @elseif($food->meal =='lunch')نهار
                                    @elseif($food->meal =='before_exercise')قبل از تمرین
                                    @elseif($food->meal =='after_exercise')بعد از تمرین
                                    @elseif($food->meal =='dinner')شام
                                    @elseif($food->meal =='before_sleep')
قبل از خواب

                                @else
                                                      {{ $food->meal }}
                                    @endif
                                </h6></td>

                            <td> <h6>{{ $food->offer1 }} </h6></td>
                            <td>   <h6>{{ $food->offer2 }} </h6></td>
                            <td>   <h6>{{ $food->offer3 }} </h6></td>

                            @if($program_state =='not_sent')
                                <td><a class="btn btn-outline-warning" href="/coach/myorders/food/edit/{{ $food->food_id }}/edit"> ویرایش</a>
                                    <a class="btn btn-outline-danger" href="/coach/myorders/food/delete/{{ $food->food_id }}/delete"> حذف</a></td>
                            @endif
                        </tr>
                    @endif

                @endforeach

            @endforeach


            </tbody>

        </table>
    @endforeach



@endsection