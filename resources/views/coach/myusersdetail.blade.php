@extends('coach.test')

@section('content')
    <title> شاگردان من</title>
    <!-- Main content -->
    <article class="col-md-9 col-sm-9 main-content" role="main" style="background-color: #fffacc; box-shadow: 10px 10px 10px #888888;text-align: right">
        <a href="/coach/myusers" class="btn btn-primary" style="width: fit-content">صفحه قبلی</a>
        <article class="post col">

            <div class="post-content" style="direction: ltr;text-align: right">



                        <div class="post-graphical-image rounded-circle " style="margin-right: 20%" >
                        <img style="width: 28%;border-radius: 25px;border-color: #1d643b;border-width: 10px; " src="
                                {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/student/images/UsersImagesProfile/'. $user->id .'.png'))--}}

                                http://sfit.ir/bodybuilding/student/images/UsersImagesProfile/{{ $user->user_id }}.png
                                {{--@else--}}
                                {{--https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/user-512.png--}}
                                {{--@endif--}}
                                " alt="">
                        </div>




                    <div class="row mt-5">

                        <div class="col-md-5"><lable>:سن ورزشکار </lable>{{ $user->user_age }} <img style="width: 30px" src="https://cdn2.iconfinder.com/data/icons/spa-solid-icons-1/48/25-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5"><lable>نام ورزشکار: </lable>{{ $user->user_name }}   {{ $user->user_family }} <img style="width: 30px" src="https://png.pngtree.com/svg/20170705/24b50e869f.svg" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_height }} <lable>:قد </lable> <img style="width: 30px" src="https://png.pngtree.com/svg/20170717/65935b3a8b.svg" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_weight }} <lable>:وزن </lable><img style="width: 30px" src="https://cdn2.iconfinder.com/data/icons/medical-services-2/256/Weight_Management-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5"><lable>:استان </lable>{{ $user->user_state }} <img style="width: 30px" src="https://cdn2.iconfinder.com/data/icons/pixel-perfect-at-24px-volume-7/24/Untitled-1-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_city }} <lable>:شهر </lable><img style="width: 30px" src="https://cdn0.iconfinder.com/data/icons/good-view/500/View-08-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_address }} <lable>: آدرس</lable><img style="width: 30px" src="https://cdn1.iconfinder.com/data/icons/real-estate-84/64/x-24-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5"><lable>:شغل  </lable> {{ $user->user_job }} <img style="width: 30px" src="https://cdn5.vectorstock.com/i/1000x1000/63/09/job-icon-male-person-worker-avatar-symbol-desk-vector-23636309.jpg" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_waist }} <lable>:دور کمر </lable><img style="width: 30px" src="https://cdn0.iconfinder.com/data/icons/fitness-49/48/6-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_arm }} <lable>:دور بازو </lable><img style="width: 30px" src="https://cdn0.iconfinder.com/data/icons/fitness-49/48/5-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_neck }} <lable>: دور گردن</lable><img style="width: 30px" src="https://cdn2.iconfinder.com/data/icons/body-organ-health-and-medical-elements-vector-ou-1/128/body_healthy_medical_neck_face_shoulder-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_thigh }} <lable>:دور پا </lable><img style="width: 30px" src="https://cdn1.iconfinder.com/data/icons/body-parts/24/Knee-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_leg }} <lable>:دور پا </lable><img style="width: 30px" src="https://cdn0.iconfinder.com/data/icons/clothes-29/100/Artboard_43-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_chest }} <lable>:دور سینه </lable><img style="width: 30px" src="https://img.icons8.com/officel/2x/chest.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">@if( $user->user_taste=='sweet') شیرین@elseif( $user->user_taste=='bitter') تلخ @elseif( $user->user_taste=='sour')ترش @elseif( $user->user_taste=='passion') ترش@else {{ $user->user_taste }} @endif <lable>:چه مزه ای را میپسندد؟ </lable><img style="width: 30px" src="https://cdn1.iconfinder.com/data/icons/cooking-10/500/cooking-kitchen-cook_17-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">@if( $user->user_cigarette== 'yes') بله @elseif($user->user_cigarette== 'no')  خیر @else {{ $user->user_cigarette }} @endif <lable>:سیگاری میکشد؟ </lable><img style="width: 30px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRW8tndKXFOrPE2E_AtJcG7e8txScvzbDsLKmkvb2DB0s9RiQ6O" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_sleep }} <lable>:مقدار خواب  </lable><img style="width: 30px" src="https://cdn3.iconfinder.com/data/icons/miscellaneous-1-glyph/64/inactive_sleep_night_moon-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_sports }}<lable>:رشته ی ورزشی  </lable> <img style="width: 30px" src="https://icon-library.net/images/physical-activity-icon/physical-activity-icon-23.jpg" alt="">  </div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_record }}<lable>:سابقه ی ورزشی  </lable> <img style="width: 30px" src="https://icon-library.net/images/history-icon-png/history-icon-png-29.jpg" alt="">  </div>
                        <hr>
                        <div class="col-md-5">@if( $user->user_stress== 'yes') بله @elseif($user->user_stress== 'no')  خیر @else {{ $user->user_stress }} @endif <lable>:آیا استرس دارد؟ </lable><img style="width: 30px" src="https://cdn.iconscout.com/icon/premium/png-256-thumb/stress-6-825610.png" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_blood }} <lable>:گروه خونی </lable><img style="width: 30px" src="https://cdn5.vectorstock.com/i/1000x1000/88/99/red-blood-drop-icon-circle-shape-vector-13938899.jpg" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_disease }} <lable>:بیماری </lable><img style="width: 30px" src="https://cdn1.iconfinder.com/data/icons/social-issues-and-critical-problems/370/problem-008-512.png" alt=""></div>
                        <hr>
                        <div class="col-md-5"><lable>:اطلاعات دیگر </lable>{{ $user->user_other_info }} <img style="width: 30px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEX///8AAADz8/P39/fj4+PX19f6+vqurq7w8PB7e3vAwMDHx8epqanb29vg4OCUlJSHh4e5ubnExMTOzs6BgYG0tLQeHh5UVFRCQkJiYmKbm5s1NTV0dHSioqI7OztdXV0qKippaWlJSUlPT08QEBAXFxclJSWWlpY2NjaMjIxvb2+E7fKXAAAM8klEQVR4nN1d52LyOgwthBUIM2FDSSgd9P0f8JbS1sdDGURK8t3zr1BiK5a1LT89SaM36A777zN/u9nHqzvi/Wbrz977w267Jz6+KMbL60vcSkf8cl2O657oI5gso00GbYhNtJzUPeUC6O7WBYhTWO/+hcVsDz8fou4Xh6FXNwlpGATnUuT9LGV/UDchbnh9DvJ+iFw2T8aGWzby7vgM6yYJ4R3fmOm7IQ6aspDjgwB5d0RNUCHhS46Z7pNotxwuwvHkhnG4GC53UbLP8ctTt276MtT62Q9GHZrZep1R4GcIqJc6aQyf04i7zjs5n9OZz9Le1LkuGrv0yz9Pw6JSohdOaSpPdezHQULM5nIYPioDvSEptA5t1tnnwDsxk6isGgsj4skBy7zzYvTqnMR2xPL0xcn59Li67eg57ZfXI585Odi5WbUiE6DvXD5uI2vk9MCGzKO40HZp+CivXiiCjkvsbMWXce4Y9Srl0g1cUodnr5NwvNZIUo4PHAPOBMfrrKzhEmlvdWIL1mexMW0Rc64isBLaFvpCZiTfGmguM5CFwBp5KjCKZ1nZkvvBHNzajif2MToXY4iPao3hrikCYmYBNzLf4ZH3+TlwNafAKgOW5guU0PBZ6JpsxGhGmVZihTtQgxlsZpN0Jn8IyeocMHmJyaMyTCc5fZsDps2x43iooQZ9jmeWwImdRIPAaj1tF67MJBoEChv2ubBkJXGmP60OJWEjZGSrqfYobjPiYXR0EpePP0m3eJ+bkij5cht15f+w6tdNtRfOKZaFpye7HrSRJ9pD1rxTLAtPdxofiqO0m0zg01NPI3H/yCP2jSbwi0StUmdb/AGalbvhn2B5eBqTFVaLmhiNmyNFEYMyAlWXMk3Rgyb0WRaTNpq+aYYl44Kmz85FfpmUWP5KoW2mAltRs237cvNjgGY4506/aTu4bn8wC5hqf837I8ynP0vOjgOazjjk+80Rf9PQIjpAt7DMGBT+Rc3AFbnk+QHmXusKGxZDwRljCjSWnhsPtK2Y6Uj18L+bq+p1oOLP9DJQv9QfV8sLTE1lxDQwAiLqUIz70Xa7/dyNeGoAkPPS/xM3rRyP9nZQcZRw5JCGMO9r2j8iQ78zDOyGpnC/sGXwXbCIKU2Hg1//Vn5UAo6CnPL1XBhz+aT/DddaKrrdc1YGl09nYQaQ3l8QouPPk/+AKH0ubzxBVooM2qCylxIzZAFpaZmKDEipfRBwUuZa16LsF+W9NPCjiEXEJZSqVEs58lXai8G352ZBKJhhSa06MLDoUihf3QHJUydHYM5KKnroLE39Qfm8CIbeXCoWGEhM2VM13N8o/3hYRAcJaJGKBYBTT16WHxV3ov0tvN5Uw64UUk8QMQRMQJxatTY93qEIpB7hY+AcsKut+DCESFPMurKwatMAuUOBaYB8lKn1N/RXjFikUJhwDADC2thrIGdEI6QpFPIUqcED9S+m3CMRSFEX7APo3hHwr2iukDZqmIJCY/XEA/G5cJ6CsmoKpcbS8OHmCpBx0tX3hPvEZupDvg3dauU9rrhGImGX/H9pCsZDYeqpoPaASSXK/A3YjMoaUAC7SX0IMY4qjogM9BrRC++BNGBTFeBS5pxchE3DxP8rFFhzaydg0z+l7zk+E0dneDwel12BYILy5P+MF4jh/Av5wizAnvsVYGAH1Do1JoCr/7sDlEHDYv3WDkVhdP8AtmazK0vyQpXkxfcPwKX5V1Ki6QCFe9+I4FfUPDUmgCt49y9UeOj/sQ1xI+6Mv6s/cicDFQ76Du/Dmtbd7YYLoBFvf4K+b2ahbHFA+P4W+1ZdSj7kx+4Nqmg4Bwnhm5GmmFYwjOh1A3/9k4BdbY/SHozKE96CI8qikRI0nZ3VjejyLlpZrdTDTAt2i5wNnbwTeW1JX1vVPa01m43foulM7T4Tf1jJ8arygmNN7jCL0l6Q1cFU7ECxRhQoC9ZBOkkGeTdIlbSAjh/AgvLWsaUlYRSE5A1ELcawKXMWSedEWrM6BamyHTXCAsKXrBUmnoMcF4SkjVKBc8h9s6rDtDwagpdx/qAU4hG4iTWs983862swDMNh5O7udgfnoAoqd3CFWjZW2b16vYKnMqTVoow/o4JrEexJzkhiz5g4nVWTCQ2puIUPFIq21Jk6iPuGjDGsinQToFA0DEUuoox5qoJRJ6BQ9hyl2T5Hdg1VFeIZKJQ94kRZADL7UNmi+6q49Im6I0HGNFWlQ69AoazjTekLmVGhwg0olI20EQRWQKGSAKKZNdJOldkbykG8VEXhxEXd9ysWAVKo9odo/yBX39MbYpnhlOH/BucfRDvYUuVekcxwqC1ULEW0oI0K2QgNihpf+YeSxw17LupuEDIzlF26BU9KMoBpdZb8gVSpp4oS+ZonJYeZi7yW3KkAH6hSbo1g2oI6zyVmZSR/I+zAzxAsDiZ9J6kB1RtdikWENVBVpUJxKD2aCBacXIKUOoUgpaC0iLBoZuYXFJNKOd1gIw6QXDHDNLRp+wZb6bMJiNb20DUVU/mUrhCr/VAK/9ZiQYWHxYrYqfuuxKJ7qu7rlhhR6l+KaTou6lo5W608BGUF3ww1OPEkNJ7ZYUCcZ8AKvklrUBdCwpQ6kyeWAobK/O8giU6wAAgCxY5TW2yphKnMGXUq0SbXbVLZ3XdLVPlPMkVRriMkN8j5o0p237c6GI0ifENFSsVMKDDT7u14YF9KWDVjm7ZvxAJj3WEXPasPJNx89+VUkn3EwIT6+URZNRKNk6gYlFx4VonO3/4Y8Jb5jf22gzh8vfyAbfhr+ILO59eIVChYTleA5PwzfNVH/LEa6opnuZpyFR9Vhm8iyDtU7lcslwdGqQqSgJXDnbyg/Aq5bQiBJ5WngL3Jbe9T21Cu3R0wJBgwINGZx6Pc+yo8JxRmcLKUOQNF3TcrZpTCjsMOisCmzPWQBIFy+h5eaZv4nDUbRAa7pcxukGz6WoGWZO2CRYpSqeIdKE3WrRcsJOAckMqqicXXaTrgiDynShxapAm8RgAoJzMnAi+b02I0rxGT5lKoLbMSd1DHyygFSAplyiLAh4itL+GibcacF1nuLZOshL1mK1z04/hYiJSllsjmGBNHc3wNbg5jRp+ksHUCpT+Yrzl2f6Ke7oqRYMSIbxEp9/CGS3INguB99rlyclVh4BI6ZQkE3/nsYip1aIFBvEGm2X0qPeQd7wdZ59d+wNCPDrsmEv41lITwncynwqUGGAYEHqQ6haKJxRdkoOKlOsqXe+PsyTIdsAgYs6WpTUvvWO3K26lQSE7LZdyJjHHFrKOIPoeziGnYFAZEj5xh1F+QHsYXTjz2GxosaXewoThiLeQjDgVtAq4gO9YkpSqCJN9aF0cvsA5cnAI+wwK5JH1pMOoQs41/R2fur788mNfVfpNES95KE3xzGUlQZKcK+u8xAZsxZpp/+DaqaMDHAeTROPO/0aOTbxPJAu20So6aXJRKTb907Y5TwRlrb0T0hAITtJKrXL/QCnqb399Mu1AiZ5wQq7Qeuoa2SmgslzfBq6Xem97hDA3N/MWOWtJP6q4LHmgFVwUseK2dsVgJIQO0e0gLWShapZboQf1S0JyWYmllPc7Z1Issy9wHrIfj35rZqq7Unc7GFm7kfasl7+U2WgSUv+aGHaXvVn/ytFqfxl0fr9/f9phhokubhpHo6SGDB0t/9QBSoxi1rR9SeVif6YfqnpsjUY0SjxLRSD1EFsv2d8kPo31Bxt2q6TCKtprhSxn5rJJ5OeMoQRNs1ECfUmnPwGd+Xmkc2CdkJFZq9he9DTuBFolxnXa4eQcmUy21mTuqbzMaW5CvyZSZ5JTsvpAC72TMg7HsyCxtEmyQS8OqPmKtUbUygNXnNKy6FebQg9UF6a3aFvVWkeOe3cBqWy3lKtyNbaspg0jzWvs+MdG+SwD7qLRQ1xe7f8dLFWG4kd2uT6xpV8c+DnqQ1v9d+yT4RtLHcRTi+ZI0TkwV2BIX466i30iKxomr64v4pVsD1ymYg4TjGLo6FUi2evqDaRx+Y829+ZfO1lIVmcRtx+ZotV6PfAKg4y4UqzDpThSo81RweX13Teq+WmOY6u3sl+RWb+5kkFYNN6Z1yDu2k/mj7NoJyMvXoypu4DER0gXOm/ew6IwGixnViqjV2tYV5BtRrfRu2M+Geec17vspt7W01nXWZo0+Umb2hZcoCFPo9CaL4yGj1r1W+m4YUedgEc+Jfw2Gi1HYHU+6YThazI8zf0s1wEZsm5Bi76adGimHWTOC7F8mwC7tBotH8VEqH8GOEaXGHsWheRf5tTOvd8qPU1MrBjtHDiJPy6Zk8ZwYzJMy1K2iUXPysDTCaR4NYmN7bIrozIHel5mSZvCYOM/m/xB1Ct3+7Jyl1vfr6bwJWr0EvEF3GLxH/mmzj1eXS+vyuor358Sf7ZaLcVt+1/0H0cmMTajeNQUAAAAASUVORK5CYII=" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_telegram_id }} <lable>:ایدی تلگرام </lable><img style="width: 30px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAsVBMVEVBtOb///8AkcjS0te5ub4wsOU6suUAj8cAjMYAisUAiMQ1seUSl80gntP7/v/Pz9TP6vjj8/tfvulUuujt9/y1uL6g1vGX0vDG5vZ+ye213/T0+v1zxevV6fTKztWx3fOOz+/Z4ujO1t2/3u6izuaRxeFstNmAvd3c5ero7/NNp9Pk7PBSqtQ1n8622OuEtNCavNJiqM24yNWutb2txNS8wsl5r8+hsbyFwN91t9o1lcPrXq8oAAANrklEQVR4nNWd6XbbthaFQcokSFGmqcGSqNnxkMa13fb6urfu+z/YBaiBJOZRovaPrC4nK+WXfQCcgxEE3rWcFPPtdFeuRuPxGACAfh2tytl0Oy8mS///e+DzL58U290IxFEUZVmMBfaq/jvL0M9jMNpti4nPj/BFOJnPRggtq7HYwqgIdDSd+8L0QThZlGMMJ0QjQBHmuFz4oHRNmBczRCcxjmcnopwVueMvckqYz8vMjK5BmZVzp5AOCSs8C7oTZRSXc3ef5YpwOIud4J0gZ0NHX+aGcDGKMmd4e2XRaOHk2xwQTqZuopMUapJTBxmBNeGwdG5frSwrrYPVknC48mJfrThaWTJaEXrnc8FoQTg5B9+B0SLZMSbMZ2fi2zPOjLMAU8JF7K9/YSmLTccOM8LhODorH1Y0NmuORoS7MwZorTjanYmwAOcN0FoZKM5BeBkD9zKxUZdwfTED98rA2i/h9IIG7hVHU4+Ey9H5u1Ba0UgrH9chLKzKd3eKY50OR4Pw8hF6lFakKhPmqy5E6FHRSjmLUyVcji/bh5LKxqqNUZFw2JEmWCuOFZM4NcKiM02wVhyp9TdKhNsuNcFakVK5oULYUUCEuHVDOO0qIEJUGDXkhB0GVEKUEnYaUAVRRtjZNniUtC1KCBddB5T3qGLCKwCUIgoJi2sARIjCoV9EOLwOQJSkihI4AeESdC9VYysGgjRcQDi+FkCEODYhXHWrXBIrW+kTdnykJ8Uf+XmEV9KN1uJ2qBzC5fW0wYPimNPbcAivqJc5Kh7pEM6uLUaxopk64foaAREic8KfRZhfzVDfVgxYU4wswvKaRsKmslKNcH6dMYoVMfbDMQg7NzWqrpiBQ/9od60xipXRK6gU4ZX2o0fR/SlFeIVjfVN0lUESXsW8hUjUnAZBmF+3g1hxLiScdb2biaUxls1EhJNux2icRatiCSV/KpoICMsuBymyb4o+/jEdSP5cySfs8ORaZV/1kc9JKEGMhlzCVVctjKPx9ljgvsJQghiveIQdtRDZV9bjeJ6EoQyxZWKTsJMWYvua/f+6H0oRWyY2CDtoYRxFOyINe6o8lCA2TWwQdq4jjaPRgqpp32AoR2x2pzVhx8bCOIp3rOWI8EgoRGyMiTXhtEvpTBaNHhl4qCmlYaiAmNUTxDVhdyxEQzv3XNdTg1CISBN2pajAQ7vgbN4LDJUQ6xLjRDjqRD9zyMz4GrQI+Yj1/PCRsAtDRZ2ZcTXph6Ea4mnAOBLOLm5hFo3lh/E+klAR8VREHQkv3JHizIxpHzEe/qQI+YhtwsvOkTYT67YeiB+/U4BcxOPc6YHwgvkMPr3NOWHw6wfxgyVtIRfxmNfsCfOLBSmyj87MjgaSgKj6ZRHyEPMG4YWClJeZ7Q3cUIC4+lVHPIQpuFyQMhPr2sDNA/3DV8gmZCIewrQivECQijIzpOWPOwZgwA5SLmJ+Ijz7tgSUWAtvTXi4YwKuyfFejLjfvFARnne4x/YJMzNk4N1vrN944jRDDmI8OxGeca1CklhXBm56bMC6+lVD3K9hYMLz1b7SxLoysHf3O/v3QiEhjVjVwZjwTIVTnGWyxBq3wB4XcChohkzEqoTChGcZK2KVxBob2Nv84vwunXZLEKvxAhP633qB7ONlZk39tukJAINvcZDSiNWiNzhDM+Qn1rSBAkCy+lVAxA0R+E7ZUGJNTnkKDOxt+P8UVPUrR8SJG/A7GooSa4aBIsDgUdoMKURcBgOPMzTCxJploBAw+EeNsImIZ2uAt2lElFg/Kh70PBjY+yH88+8KzZBEzDChl45GklgzDZQAsqtfCSLqaoCHtFshM2t++cFACWBQ8AsLPiJKvoHz2XyFzKypo4EyQG71K0TMpojQaUajZ1/DwDu6oCfErX5FiCirAS4Li0zPvoaBcsBAx8ITIiovQOAKjzvlydXJQBXAtUYzbLoYgKWbZqiYmTV1MrDHLOgJCatfPmK2BC4WLARTnlzVBioBEotOyojRBNgPFjgz07797/eTgbyCnpBK2s1AjAqwsItSjcysoYaBioDS6peDmD2CrQ2heMqTq4aBvQ2noCckr37ZiNkWTI0HC9NLRpsGqgKqVL9sTcHOkFA25clV08DeHb/ebWtgCJh8A6OUBtk3NbsfLm8aKCro25qYBWkYwjdgsNVLMzNrqmWgOiBv0UlB70C3/tVMrFtqGyiud9tSrX5pD2+BblqqmZk11TZQB1Az7W5qAMZ6Dj4b3+xLGKgFmBsHqTYhyJL3J6M+hjCw90Pnn0o77W5Ijw8rTNL3J92WSBoorXfb0qp+rQnBIIRJX89J0kBNQMmik3NCgEdfiJ1UhKQM7MnLwbYki05iQs12eEIMlZ38RRqoUO+2ZZZ22xCCYw6l4GT+QBqoDWhQ/dYaaI+He9V/A4Z85kNSBhoAmlS/DULDOf1mJozC9ZYNSbdAtYKekEUzRDmN6REEItlHbZKGpA00AlRbdOLo3ay2YCDSTtIt0AzQtPrdf9WbcX1IIyIlaQ3JMFBxxoIUY8ulOuG3RY3PQsThevuM590YBhoCMrdcKhP+tJqn4RTeSTp4pvH4eywkWlokpWHybDfXxp1bSBkhql7vtmVe/WLCD8v5Uh5i8qczQKu0O0webee8OX8x/MsZoEX1i5SurdcteC6SgOZv4tiMhmF/ab/2xEZM/uMK0Kb6DSHMHawfMhHhHy1Ai3fGbNLuMLx1sgbMQoTvTcKvB3NCm+oXpTRu1vGZiM3x4uvmy9hFm+oXDYeI0MWmL1YC998G4c3Nzb1hX2pV/aLhEBE6OdNFI8K/G4SfGPHBiPDDpqNBg4WzPVEUIhwQhDdmkWpT/aLBwuG+NgqxMV5sbva6N1gOMFn7rXXrcm8iVS/W48XXgfDm80EXcGIVpPDb6f5SAhG+Uh4iFzeahIpbLjlCHY3TPcKki7SH+pFqU/2iZjh0vM+7jViPF01CzT5VecslSzAMXO/VbyHW40WLUCtSrapfnNE4P2/RmmT8tz1Y1PpUzsPnVoTJU+D+zEwTMTkmbjek/se+MIGWVfVbNUP3554aiPBQ6G/uSQ/D/j9qhFbVb9UMPZxdqxGPhT5J+IlG8eRVJVJzq+Eej4Zezh+eEGHI6mhu7qs0BSYK+zmsqt8w/TgROj5DekI8JG5fDECEmMoj1a76TZcnQtdnu06Lb38whsM60UylkWqVdsPXoCZ0fTDogHgo9L/YgHgWRbIr1a76/WgQOj/qfETcEIT37VIB9p9FgHbV7z5Ivd2psEfcTwx/8gBDSaTaLDrtE5qa0P35tQpxP1588gFxh8uPVOMtl1iHIPV4t0m1Y2PQrJ1YgMJINd1yWSnN24Qejujh78PjxUYIiL/ljT29YbzlEgu+BG1CH3cMDfbjxUYCyI1Uq0WndE0QejmFOKgK/S8ZIP6eJwah8ZZLrEFAEno5sj7AiduXHBCVAYxItal+kyeK0M89SgNU6GPC+39lX5sMyEjNbUbDfk4T+rlzL/wbDxZywJCOVJ0Dh6SSnwFN6OfQevzeu5eG6PHfvR2pNtVvOmQQerpaIULVoYqD1b98K1Itqt9jPkMQ+rmULv5TGTBsRWpukXb310xCT3fQvmgAIsTjQG1T/R7qJprQ082Ct1peJINDE7KoftM1h9DXRcJ6iPAQqebVb8vC89znrYd4iFRoTNhfcwm93VSjiYgj1bz6bXakFKG3i1w0EWH6Yb7olA4FhP7eRtBERDaaBmnyHYgI/b1voYtoCgiTpZDQ461YuoiGSsgZgzO+M3MexAEJRBF6vFD4HIh96vDgWd978o+YvFA8rEeu/N0b5RsRJvRMwZnfXfOMmH4wDKN/5PXtPK+IjBi9wPuHHhEhVH3/0O/ba/4Q+8w11wu8Q+oLMWGvuHLekvX6joAfRHjLRrnIe8A+EGHC2U52mTedPSD2eVt0LvQut3NE/raHS72t7hgxeeVy8Ak93xDtFBEO+BusBYRLv4/KukTk9TISQt8vs7hD7Is2rYgIfd9G7wqR243KCX3f9O0Gsc9aP1YlDLbdRxRvOpISeh4WHSCmEkApYdcR5RscpYTdRlTYwSkn7HJblLVBRcLu9qiSXlSdsKuISoBqhGjo714CB8UDvSZhMOxcjgoTxVtvFQmD5bhbxVQyUD0hpkqI6sUulcTpq/J5VHXCDlX9UPXIjSZhUHhtjOqIMFE9NqVLGEzGXZhHTW61DmlqEeJIvfSEP+z/lH+mBWGwBpddtkmg7vW3uoRBsPNoowwRpi/aZ/r1CYPCo41ixATqdDHmhHjh5iLbGfrf8k9zRBgM/XWqXER6I7hPQlRuxOfdPZUkSoWEQ8Ig9xaqDESYfpvesG1OiMb/0hMjiQj7b2YXwNsSoua48sPYQoT9V7MG6ILQG2ONaMtnTYgYy8hDn3NATNI3Sz4HhKg9TjP3RmLEpP9t0f4cEiItRs6NHPQHTxa3vNVyQ4iCdRY7NBI/f2L+QkFbrgiR5qWbaMVvK5m+n8GQQ0KUBVSQNpRxheckOo9ySoiUF7NxZEaJ6KLxrHCKF7gnxJosShBFmQ5lnEURKBfGqZlAPgixJvPZKMOYMs4Yw2Wj6dwHHZYvwkqTYluOACKIMoQan2ir/86qn4NxuS18wVXySrjXcljMt9NduRqNx/idAvTraFXOptt5MTG/uFVZ/wcXSzKa2W573gAAAABJRU5ErkJggg==" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->email }} <lable>:ایمیل </lable><img style="width: 30px" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxATEBUTExIVFRUXFxYVFhcXFQ8VGBUSFRUXFhUXGBUYHSggGBolHRUVITEhJSkrLjAuFx8zODMtNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQgFBgcEAgP/xABLEAABAgMEBQUKDAQGAwEAAAABAAIDBDEFESFBBgdRYXESE3Ox0hYiUlSBkZOUsvEUIyQlMjM0NUJVobMXQ1PwYmOCksHRCKLhcv/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwDt6X7EOxRuHuQSTkEJyzUUwCU4oJJu4oTcopxSmJqgm+6qX5lRvPuTeUEg5lAVFeCV4daCQb+CX38FFeCbggm/YhOQUbh7kpgEEk5BCfOopxSm8oJJuS+6qimJqm8+5BN+ZQHMqN5SuJogkFAb+CivDrSvDrQSDfwS/YorgE3D3IJJyCE5BRTAJTigknzqb1803lSBdWqCVKhSg+ScgopgFiNLdJpez5Z0xHdcBg1ou5UR5vuY0Zk3HgATkq/W3rotaM8mC9kszJrGQ4jrv8T4jTeeAbwQWYpxSnFVTGtW3PHXejluwn8Vbcv+2u9HLdhBaymJqm8+5VT/AIq25f8AbXejluwh1q254670ct2EFrN5SvBVTOtW3PHXejluwpOtW3PHXejluwgtXXh1pXgqqHWrbnjrvRy3YT+KtueOu9HLdhBavcE3D3Kqg1q254670Ut2EGtW3PHXejluwgtXTAJTiqpjWrbnjrvRy3YQa1bc8dd6OW7CC1lN5SmJqqp/xVty/wC2u9HLdhP4q25f9td6OW7CC1m8+5N5VUzrVtzx13o5bsIdatueOu9HLdhBayuJoleHWqqHWrbnjrvRy3YQ61bc8dd6OW7CC1deHWlcAqqfxVtzx13o5bsINatueOu9FLdhBavcPclMAqqDWrbnjrvRy3YUDWrbnjrvRy3YQWspxSm8qqY1q254670ct2E/irbnjrvRy3YQWspiaqQMyqw2TrktiFEDokVkw3NkSHCGGdzoYaQfPwXeNA9NJa04HOw+9ey4RYRILobjTH8TTcbnZ3GhBCDZ1Ki9Sgrn/wCRFqvfaMOXv+Lgwg67/Miklx/2hg8hXKV0TX0PnqJ0cH2FztAX3zbth8xXwuxal9ZPMubITkT4k97AiuP1Tsobj/TOR/CcKHvQ5Bzbth8xTm3bD5irujbkgx4daCkXNu2HzFRzbth8xV3r7+CE34BBSHm3bD5inNu2HzFXeJyCE5BBSHm3bD5inNu2HzFXevu3lL7t5QUi5t2w+YoYbth8xV3aVQbSgpFzbth8xTm3bD5irujaUGOOSCkXNu2HzFObdsPmKu6MeHWovv4IKR827YfMVHNu2HzFXeJvwCE5BBSLm3bD5io5t2w+Yq7xOQS+7CpQUh5t2w+Ypzbth8xV3r7t5SlUFIubd4J8xXwu+659ZJgh0jKP+NcLo8Rp+qaf5bCPxnM/hGFT3vAkBb1qWtZ8C2IDQe9jcqDEGRDmkt8zw0+daKto1Yj54kunYgt1eiIgrDr6++onRwfYXO10TX199ROjg+wudoNp070Lj2dEZyr3wYrQ6FFureASx2x4v8teGrK4lo2DAnpAS8w2+G6Gy7JzHBo5L2nJw/8AhwJCq1ppopMWbMmBFF4N5hRACGxYd+DhsO0ZHyEh1rUxrJ53kWfOP78ANl4rj9YBSE8+EPwnOlbr+zV4KkLXEEEG4jEEYEHarH6oNZHw1glJl100wd644fCIbRiR/mAVGYxGdwdP3BNw9ybh7kpgECmASnFKcUpvKBTeUpiapTE1TeUDefcm8pvKVxNECuJoleHWleHWleHWgV4daVwCVwCbh7kDcPclMAlMAlOKBTilN5Sm8pTE1QKYmq5nre1jCRYZaXcDNxG4kXES8Mj6R/zDkPKcgcnrQ0/h2ZA5LLnzcQHmmVDG051/+EZD8RGwEisE7NxIsR0SI8ve9xc9xN5c44klB+UR5cSSSSSSSSSSTiSTms5oZorMWjNNgQRdnEiEEthQ78XHbuGZ/Tx6PWJHnJhkvLs5UR54Na0fSc45NGZ/5wVq9BdEYFnSogwsXG50WIRc6LEuqdjRQNyG8kkKm2vKiDMRoTSSIcSIwE1IY8tBO/BZ3ViPniS6dixWk/26a6eN+45ZXVgPniS6diC3SKLlKCsOvr76idHB9hc7XRNfR+eonRwfYXO0F1rNF8GFs5tnl70LFaa6KwLSlnQIouIxhxAL3Q4l2DhtG0ZjyFZWzcYMPZzbPZC9O4IKZ6RWFMSUy+XmG8mIw5Xlr2n6L2HNp/8AhuIIXhlJl8N7YjHFj2EOa5puLXA3gg5XK1usbQeDactyMGx4YJgxfBPgOuxLHZ7K8ar2nZ8aXjPgxmGHEhu5L2moI6xmCMCCCEFmdVesFlpQObfc2bhj4xtBEbTnWDZfdeMidhC3ynFUssi040tHZHgvLIkN3Ka4ZGhB2ggkEZgkK1GrvTaDactzjbmx23CNCvxa7JzdrDcbjxFQg2unFKYmqUxNf7wTefcgbym8pvKVxNECuJoleHWleHWleHWgV4daVwCbgm4e5A3D3JTAJTAJTigU4pTeUpvKUxNUCmJqtX1gaZwbMludfc6M+8QYV+L3DM7GC8XngKkL3aXaSy9nyzpiOaYMYCOVEiEYMbv2nIXlVS0r0ijz80+Yjm9zsGtF/JhwxfyWNGTReeJJNSg8tt2tHmo748d5fEiG9x6gBk0C4AZAL8rOkIseKyDBYXxHkNa1tST1DaTgAL1+MKE5zgxjS5ziGgAElzibgABiSTkrM6pNXbbPg89GaDNxG99Q80w48207fCIqcKDEMlq00EhWbLXG50w8AxogzNeQwn8A/WpyA3GvDrSvDrU37EFMtJ/t0108b9xyyurD74kunYsVpOPl0108b9xyyurH74kunYgtzcpUKUFYdfR+eonRwfYXO10TX199ROjg+wudoLr2b9TDA/ps9kL0bh7l57NPxMID+mzyd6F6KYBApgFoGtfV6y0YPOQgBNw294cBzrRjzbz57iaHcSt/pxSm8oKSTEB7HuY9pa5pLXNcCC1wNxBBoQVkdGNIJiRmmTEB1z21Bv5L2H6THjNp/wCiMQCu7a49Wwm2Gclm/Kmj4xgH17AMhnEAptAu2KurgQbvPxQXC0N0pl7RlWzEI40ewkF0KJm0/wDBzCzm8qoOhGlsezZpsaEeU03CLDJ72LDvxB2OFQ7I7ReDa3R+2oE7LsmIDuVDcMKXtcPpNeMnA5IMjXE0SvDrSvDrSvDrQK8OtK4BK4BNw9yBuHuSmASmASnFApxSm8pTeUpiaoFMTVeC3LYgScu+YmHhrGC87ScmtGbicAF6J6chwIT40Z4YxjS5zjRrQqtazNPItpzGF7JaGSIMP9Ocftef0GAzJDwaeaYR7TmjGid6wXtgw77xDh3/AKuOBLs+AAGuKF3DUrq0+haE4zY6XhOHlEZ4P/qPLsQZjUzq2+CsbOzTPlDhfCY4fUMIqRlEI8wN1b11evDrSvDrSuAQK4BTfkFG4e5TTAIKZaTj5dNdPG/ccsrqx++JLp2LFaTj5dNdPG/ccsrqx++JLp2ILdIoUoKw6+vvqJ0cH2FztdE19ffUTo4PsLnaC69mn4iEB/TZ7IXopxXns03QIW3m2eyF6KcUCm8pTE1SmJqm8oG8+5cU10ateUH2hKM77F0xCaPpDOM0bfCArWt9/a95S6+tEFIFt+rjTqLZkxfi+XeRz0IGo8Nl+AePMaHIjbdcmrb4O509KM+Icb4sMD6lxOL2j+mTl+EnZddyNBdezp+FMQmRoLw+E9oc1wo4Hq2EVyXoOOAVYtVGsR9nReZjEulIh74YkwXH+Y0bPCAqMRiLjZqBGa9rXQ3BzXAOa4EEFpF4IIreEH3uHuSmASmASnFApxSm8pTeUpiaoFMTVfMWI1jS95DQ0EkkgBrQLyScuK+t59yr1rl1lGZc6SlH/ENN0WID9c8H6LT/AEwc/wARGwC8MVra1ivtGKYEAlspDdhkY7x/McPB8FvlOJuHOkXRNU+rl1oxeejgtlIZ744gxnD+W05DwnDgMTeAyupvVp8Kc2dm2fJ2m+FDcPr3g1cM4YP+44UvvsNXgviDCa1oYxoaxoDWhoAAaBcAAKABfdcAgVwCbh7k3D3JuCBuCmmGaimAqppxQUy0n+3TXTxv3HLK6sT88SXTsWK0n+3TPTxv3HLK6sT88SXTsQW5UqFKCsOvr76idHB9hc7XRNfI+eonRwfYXO0F17NwgQ9vIZ7IXopiarz2bhAhn/LZ7IXo3lA3n3JvKbylcTRAriaJXh1pXh1rlmuPWUJNhk5V3ylw794/kMI/cIpsBvzCDD66tZYAfZ0o/HFkzFGQo6C07cnHKlb7uFqScziVmtENGJi0JpsvBGJxe838mHDFXu3bsyQEGEXV9Tmsr4I4Sc0/5M4/FxHHCA9xodkMn/acaErVtYugsay5jkuJiQH4wot11+1jhk8fqMdw1FBd8HZjf/d6U3lcO1K6yQORZ827DBstFcfI2C4nLJp8mxdxpiaoFMTVN5TeVynXHrI+CtdJyr/lLhdEe0/Z2OFAcopH+0G+tyDEa6dZX07PlH44tmIrT9HJ0FpGeTj/AKdq4apJWe0L0UmLRmmwIIuFYkQglsKHm47TsGZ8pAYBdY1N6yPgpbIzb7pdx+KiH+Q9xvuccoZJr+Em+hN2o6wtCo1mTPIde+C+8wYt1we0VB2PF4vHA5rVUF3778BRNw9y4jqW1lX8iz5t9xwbLRXGuQguO3Jp8mxdu3BA3BKYCqUwFUpvKBTeVIwrVRTE1UgZlBTLSf7dM9PG/ccsrqx++JLp2LFaT/bpnp437jlldWP3xJdOxBbm9SovUoKw6+h89ROjg+wudromvr76idHB9hc7QXXsz6iGT/TZ7IXo3leezB8TCJ/ps9kL0VxNECuJoleHWleHWtS1j6cQbNluUbnRngiDD8J3hOuoxue2gqgx2tfWGyzoPNQiHTUQd43A8204c44ee4ZkbAVWKYjve9z3uLnuJc5xN5c4m8knMkr9rUtCLMRnx4zy+JEPKc45n/gAXAAYAABfNnyUWNFZChML4j3BrGtq5x/uqD0WDYsecmGS8BnLiPNwGQGbnHJoGJKtVoHofAsyWEGH30R1zo0UjGI+79Gi8gNy4kk+DVloJDsyXx5L5qIBz0QUaKiGz/AP/Y47AN0pxQY3SKw5eclny0dvKY8buU12T2nJwyP/AAqp6b6JTFmzRgRhe03uhRAO9iw78CNjhQtyO0XE2+pvKwWmWi0vaEq6BHGNYbwO+hRLsHN/5GYQU9VhdTOsgTLWyU2/5Q0XQYjjjGYB9EnOIBnVwG2+/h+k1gTEjMvlo7eS9tD+F7D9F7Tm0/8AYOIIWNhRXNcHMJa5pDmuBIIcDeCCKEHNBZrWxrCbZ8LmoRBm4je8GBEFhw5xw2+CDnuGNZY0VznFznFznEuc5xJLnE3kknEknNftaM/FjxXxYr3RIjzynvcbyT/eF2Vymy7PjTEZkCCwxIsRwa1oqSeoDEknAAElB6dHLCmJ2YZLy7OVEf5GsaPpPecmjM+QXkgK1mhGicCzpVsCFifpRYhFzosTMnY0UAyG+8nw6uNBoNmy3JFz47wDHi+E7wGZhg/WvDbq4BBidKNHpeflny0dt7DiHC7lQ3j6L2HJwv8AMSDgSqp6ZaLTFnTToEYX5w3gd7Fh5OGzeMirhbh7lrunWiEvaUqYEQXPF7oUW68wol1d7TQtzG8AgKhjarEam9ZPwpgk5p3ylouhvJ+vYBQnOIB5wL6grhGkNiTEnMPl5hnJew+Rzfwuac2nIrwQIzmua9ji1zSHNcCQWuBvBBFCDmgu5TilMTVc+1Taw2WhB5qMQJyGO+GA55gw5xo2+EBQ40OHQd5QN5UgZlRvPuUjHEoKZaTn5dNdPG/ccsrqx++JLp2LFaTn5dNdPG/ccsrqxHzxJdOxBbpERBWHX199ROjg+wudromvr76idHB9hc7QXXswXwYV9ObZ7IXorw615rNF8GFs5tnl70Ly6R27Ak5Z8xHdyYbBu5T3ZMYM3H+8L0Hk010rl7PlXR4pv/DDhggOixMmt2Dacgqo6R27MTsw+YmHcp7qAYNY0fRYwZNGzym8kle3TXSyYtGZMeNgBhChAkthQ8mjadrszsFwGvoPuFDc5wa0FznEBrQCS5xNwAAqVZXVJq8FnwhHjtBnIgxoRAhn8APhH8RHAYYnFamdXHwdrZ6bZ8ocL4MNw+oYR9NwNIhGX4RvJu65TigU4pTeUpvKUxNf7wQKYmqbym8pvKDVdYWhMG05YsfcyMy8wYt2LHeC7ax2F48tQqr2xZcaVjPgR2FkRhucD+hBzBqCKgq6VcTRYfSHRaRnmgTUuyJdg1xva8D/AAvaQ4Ddegp7LS74j2w4bS97iGta0ElzjgAAKlWb1U6vGWdB5yKA6aiD4x2B5tpx5ph9oipGwBZ/RzQqzpJxdLSzGOxHLJc99xqA95JA4LYK4BArgE3D3JuHuTcEDcEpgKpTAVSm8oNO1l6CQrTlrhc2ZYCYMQ7a828j8B/Q4jMGrVoyEWBFfBjMMOIwlr2uqCOviMCFdamJquea2tXjbQgmPBAE3DHe0AjMH8tx8LwSc8KG8BWyzZ+LAjMjQXmHEYQ5jhUEdYyIOBBuKtPq105hWnLco8lsxDAEaGMj4bAcSx36U3mqUaE5jixzS1zSWua4EFrgbiCDiCDkvfo9bkeSmGTEu/kxGHi1zc2OGbTmOooLm1xKV4da1/QjSyBacq2PD71wuEWFeC6HEuxB2t2HMeUDYK8OtBTPSf7dNdPG/ccsrqwHzxJdOxYrSf7dNdPG/ccsrqw++JLp2ILdXIouUoKw6+vvqJ0cH2FztdE19H56idHB9hc7QXMdaMKBJiNGeIcKHCa57js5Iw3km4ADEkgBVi1j6cxbTmOViyXYSIMLYPDfdgXn9KDafZrJ1gRJ/kQId7JWEGhrTgYsRrbjEeNleSMhjU4aIgLtepbVvyuRaE2zDB0tDcK5iM4HLwRnXZfiNTurf4Y8Tk0z5M0/FsNI7wcx/TBrtOFL1YsC7AIFOKU3lKbylMTVApiapvKbym8oG8pXE0SuJoleHWgV4daV4daV4daVwCBXAJuHuTcPcm4IG4JTAVSmAqlN5QKbylMTVKYmqbygbym8+5N59yVxKDk+uTVv8KY6dlWfKGi+JDaMY7APpAf1AB5QLqgX14V368OtcQ10atvp2hJs2umIbR5TGYPaHl2oOWaG6UzFnTLY8E3ikSGSQ2LDzadh2HI+ZWu0Z0ggT0syYl3XsdUG7lMePpQ3DJwv6iMCFTRbbq604jWZM8sXvgPIEaF4TfCbfgHjG7bTNBhdJx8umunjfuOWV1YffEl07FhLbmGxJqPEaSWPixHtvwJa55IwywKzerH74kunYgtypUKUFYdfR+eonRwfYXO10/8A8hJBzLVbFIPJiwWFpy5TCWOHEXNP+oLmCAugaqdXj7Sjc7FBbKQz35xBiuGPNtPWRQbyufrMSWldowobYcKdmIcNuDWMjRWNaK4NabgguHLwGQmNhw2hrWgNa1oADWtFwAAoAF+lN5VP+7a1vzCb9Yj9pO7a1vzCb9Yj9pBcCmJqm8qn/dtav5hN+sR+0ndtav5hN+sR+0guBvKVxNFT86bWr+YTfrEftJ3bWr+YTfrEftILgV4daV4dap/3bWr+YTfrEftJ3bWr+YTfrEftILgVwCbh7lT/ALtrV/MJv1iP2kGm1q/mE36xH7SC4G4JTAVVPxptav5hN+sR+0g02tb8wm/WI/aQXApvKUxNVT/u2tX8wm/WI/aTu2tX8wm/WI/aQXA3lN59yp/3bWr+YTfrEftJ3bWr+YTfrEftILgVxKV4dap+dNrV/MJv1iP2k7trV/MJv1iP2kFwK8OtDjhln/0qf921q/mE36xH7Sd21q/mE36xH7SDedcerUSrnTkoz5O43xYbRhAeTUDKGTl+E4UIu5Os5E0xtRzS10/NOa4FpaY8YhzSLiCCbiCFg0BbRqx++JLp2LV1uep+RdGtmVDQbmOMVx2NhtJvPE8kf6ggtcpUKUGsaf6HQLTluZeeQ9p5UKKBeYb7rqfiaaEcMwFXK3tWtrSry10rEityfAa6M0jb3o5TR/8AoBWyJyCinFBTfuWtHxGa9XmOyh0WtHxGa9XmOyrk03lKYmqCmx0WtHxGa9XmOyh0WtHxGa9XmOyrk7yoG0oKcHRa0fEZr1eY7Kdyto+IzXq8x2VcgY45KK8OtBTgaK2j4jNerzHZQaK2j4jNerzHZVx68OtL78AgpwNFbR8RmvV5jsqBoraPiM16vMdlXIJyCE5BBTfuVtHxGa9XmOyp7lrR8RmvV5jsq5FMM0pvKCm3ctaPiM16vMdlDotaPiM16vMdlXJpWqbygpudFrR8RmvV5jsodFrR8RmvV5jsq5A2lQMcSgpx3K2j4jNerzHZTuVtHxGa9XmOyrj14daV4daCnA0VtHxGa9XmOyg0WtHxGa9XmOyrj334BCcggpuNFbR8RmvV5jsp3LWj4jNerzHZVyTsCUwFUFNu5a0fEZr1eY7Kdy1o+IzXq8x2Vcmm8pTigqLZWr+1ph4aySjNxHfRGOhMG8uiXA+S8qwmrDV/DsuCS5wiTMQDnHi/ktAxEOHfjyb86k7LgButMSpAzKCVKIg+SfOopxX0VAF2OaCKYmqbypAzKAZlBG8pXE0U3X1S6/h1oIrw60rw61Jx4IdiCK4BNw9yk7Am4II3BKYCqmlKpddvKCKbylMTVSBdjmgGZQRvKbz7lIGZS6+qCK4lK8OtTdfw60OPDrQRXh1pXAUUnYh2BBG4JuCncEpRBFMBVKbypuu3lALuKCKcUpiVIGZQDMoI3n3KRjiUuvxKV4IJvUoiCEUoghCpRAKIiAoClEEBFKIChSiCEUoggoVKICIiAFAUoghFKIIRSiCFKIggqURBCIiD/9k=" alt=""></div>
                        <hr>
                        <div class="col-md-5">{{ $user->user_mobile }} <lable>:شماره موبایل </lable><img style="width: 30px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANgAAADpCAMAAABx2AnXAAAAwFBMVEX///8AAAAREiTa2tsODyLr6+v7+/vX19f39/fo6Oi/v7/j4+Py8vL29vbLy8sUFBSampqurq5vb28AABomJiaOjo7CwsIAABdQUFC3t7dLS0tAQEBpaWmgoKA1NTV2dnYcHByLi4t/f38RERFbW1s8PDyDg4MAABOmpqaUlJoxMTGdnZ1hYWFtbnY5OUV7e4NMTVcmKDaHiJAhITFYWGMAAB9gYGdRUVyDg4t1dX4xMT4kJTNBQksZGSuQkJmzsrjyWshZAAAMhElEQVR4nO1diXqiMBAuilKPohartVqr9kBaFa+1aq37/m+1IiiomSGQAO1++XftWheQn0wmcyW5uhIQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEPhPkC3mCmlFze+gKulCrphN+o6Yca3ky6371450gs7rfaucV66TvruQyFV67xKK914ll/RdBkSm8dTBSR1b76mRSfpuaVHo39OROuC+X0j6nv2RrTwEY2XjofKzFUq6F4aVjV466bsHUQsogue4ryXNgIhGm42WhXYjaRYXyFfZaVmo5pNmcgK1yYeWhaaaNJsjMi1+tCy0fsjI9siXloXHpDntoHCUQhdNJWle5ShoWSgnSisXSXPZaCZoIOejo2UhMc1/Fy0vSbpLhNY1owFFg/sEnNEcJ1MDRzX2jqaU4uAlSaWY9b4aDy0LsVpYtfh4SVKMzkzEav4csan9mHnFxixc/2o2ao2P3qAZSunE0s+UULweDqdnCo234CNgDLox9xyK2Okzz9a6lHFHB8+Rj2fZkOPyZeQwXQ5iQVejDs+9hOMl9UkXU97oL/ASLa/wdi+5/2f71CIZqUXMoujbrbdyRc3cXFzzlfICESr9DAOvA6qDO/UsWEMbkYwuxhMqLE9C8+M0nt2nOusBuC1mcHUsS2Wvory5pTknom4WbmRG8JL36PAajRaJZpyOIHBT9QQQixRh12YUvCIKtD26rVbxPzqCoBwPjUhEx82wKP5mDX/NyCE+Dxn290cVWfTVuy3evNhjAVbDZGqPLyQl4ao73+fH24Nh1hxu71AeLw3O5lH3+5mPnPUHaku9VJRcoXaHm0Ynfkemf3Fw5fB/XR9mfC0rZJAZHB+2gplG50OQci50b5TMOjx5NcCveT+ReaRo4LLXF55OjziGfX2kkWeeGpSypzP/bwDfz61aOHcW06edrV10Psftq1d+vMAw4kXmsYDekvR+WykgVz4GtHGtzy/QCAVfKpeH+jsAzz3Vk2rInshjyWF2g47U97x4pYEv+CAcS+WAVLuedjtRuKUi/pU2eNXwACqBaATQWl4D9+YK3uq/ttMPUbuxx4kY+eqdCyd/D+r6nNax1bJeHXKQM1SB8OEF6HpgoAwQeHINKS8LpzmuMf+Mj8Ynqw7ItAmSiGkf5dEr7Q3/C3FRH4AGh4zRYO7NcbzwMnPoPoFnkeKvwUHWc7AtGizic3s4zTO0O9cuIqcR468BQY79wlfuQjcz6JKE+uFgjni0jtP5kKGDQ1wYEC04SQB5bnsBy1UunlPVGbqKnbNjr66QKnB2T5qsExGvCJCgoz+W6Z/d78FE9DwR5/KI/mDXi+TRGYvwkX1Sj7LJNk6DBE1HGj1xS8dYg/sr+xhNvi5mh5LDWX3kmEOPcZ2Iqj36I03GyitHviyWhyN3sufTXlE4aQxHADwWoiO5sB3DmgkEzA7slCz5lNJb48SLPpFxRwI8ZostnrDJyNrJAAMYPQcZyVoeGfZWpjpWfdHNAztNBl6JtZORfecqek4XJmZFtY/Gs7c5bs/JPttNBiZCGP3oa3IqHY+BwRESm9pRij48nzpa0/3APgp0yZ/ZCuOADAtODFA4LgaEsM27/YnbZE42DCydYMu8APoWJ4Y79hY6ByPW0x3tFvIM77b5AdpVbJEPIMWC9zGa0gLneXsMNqfTuNqqu/8dlEW2xAvkx+JnYQ7HAU6beVSj7bm6wl+yj4CC67fAl9MBevb4WTRTDEqOcnSHYMd7dPWwrU8+SGdLjAZ+FnpcRfQ0qqqJwcWxdiO60m/LIpQhbrLU6hShwAOezKHLOTla320y27By7aqOfQBwegd/uDhAP58QKg1MzBmJ3GH6+ZyI3YRQ2JzFJQNHpCcOxBy95jEtbQXvqh67TaFOxmIGg7oWN2ho8/D20W4+yXZt3CZ8Qh8TS0AHLu3AHleRtvTPbhHXArMVnSsmth0ABXVYTA9YppC8YoG6pPHl7Mar+153ffzd9uGyQOiDJRsNE4M7GUW1xgGOs+LeuC1drp1ldzpAe7AQg13zDmBcpwOFFW1pcseUvRxk3YiILauAT8hiLCIxB+LzKgac4L1P2Hi8nFfLHPG4YPbIBth1LMQQvU0y1QJI4YGZkj7R5tV+w5t031cCQ2NONH1MkgiyGJyYH9pqJg+VjbMQwxKLpJmvEc5svARLXhPzhUk+WYyTlNgGaNTJJw1lDKtDBAaLSYUmu0jxASD4EwlYjGA8ekHSt/HNLKuSc+CUQIfbd9IZNGEBLmCr6UZKiCSyV3bts7QRNwyYiOHPv0Syq7gXewPAXUI/+BTadEOcwwtsaWi/gYnoE8XTzdhqaPGaJiAknOU2VQQDY0GV3/wFUqGYNxsUGTpsvPyrNogPzqdskQdYZ/D4TtR5JcYtozcaWSfw+Ad1yanFyC0Q1mLua/+vIGeDo2bGvCAGRf0hWT9Fy6zNyuuq6/8lgDkaaT8jmgaBQHN7gIaKUjeyT3G5oVkcAJgjlIksVFBi8llsUM2vApRvlnhyu3d3171lIs1jthXd3GcoI0xIlRzty6xa6b2GM1J4zNyhtI8gY1s9d8LP/O4bpRK88Z5Zkn5HUM71g3KBZzOBSeN5NlO7C7QUBp95f7RqG3SQTkKe8LNW+i+0cslp2h/tcyRa+hZu3KgcXp2RVenEgw8v+onC8F0fFyv0TfXTyAevScP0pfRIrZ1dLgs2qgsKU4zbnGF6X/8FaZF+FcyqBfs2tjCOFwECT6/I07z+oArA+BbPcVxxIMCCMhwWa/P5Np6LzASKFeLFLRTwUVZcl4gINHyy9gHciOM2kXGPYOHdV7bacfzLOK/pQbVCigum4nF0KGMrU7xEUKfxiSEmARVP7cF9zf9uQGad8CKDxTLZQwIXCLzMXjdkpSQWVi/x5bRH8KhTO1yjYXZwJKtjhliDJYzgYL2Z+/ore4B1tAiegz9ipIcxVcsiCLXyWyugGsMqwCNb/S1cRu8uiBLBxjB+Vv0FQu6HQZ9VxSx79qg2jLCx3SrlfjMFbEyJdDsenxlUMN77FNTQdbAj3vmEYcW+Oz+PHq0KjHyJeJYVxVo1JOReQItlohnBvGArCaj2VLJI5vDlMh5i2BPKf9qbD1qVCzVQw0ubGAvCaFHksNT9oNtXC7lMJpdTGne+EZVSRBbHOVClHAFKse27FkMZhxcx7ieXibHNSrHulJThsNkYHdox7wB1E0sh2E7Px6IPTxAwbhUOvGNSVIhsvycXCe38FHklWGJ7x/kvEsuCh5iGZSIi3PcpmR2fjlAjGtFKyW9hGGAVO3q8+X9v9OC/y1/yO/w54DwhjjlzyA9ZjvL49rP2Gc5xmjnW+3l7ehc4tNrbz9zxulhmmkjwXE5yRPZBPuxOPNLLz9pW+BKFcghXrV3+mTJ4hkI50OS499/BykaucUvVcO3bxs9Tgz7IFvJvA8SSLA3e8hfrqf8aZItqpfx0fxJjrd4/lStq8ddyOsdNMZMpxh/BEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD4IUj/p6BerOO34Sr1n0IQ+21wiMnOK+X5N5XStJTs/rZ7J2vurz8cNjF5JKfkybf9fn34v/p8rq9HByrfY1n+mq9/CzObmDZcaPVtXa+n6ro0/dZ0XdN0qfK33x9+SLokyZokrRVJGk03v4uYPNvqM9M0lpJprJbG2jBWG2OcHkvSMDM3FHWySafHtY0y2YxiJabJVlfYvay/mvXD6Q72O+tzWdN2n3zv/k3tfxw6i9PHdCM1HK7qq+FCkqZ/tilptZrrGzVtDo2JKi0qtZE0ySiaJsfKS15q5u5mJ6PU9+7ZT4cjbaJps6X2rX2lZin5a9d9hivTnG83C0M31qvpzJDMT9lLTFvMp3NjMTQ3mj6VP/T6cDneCeOftGmMG9Kooer6pKB8xSyHdXM17xvmvL+ZLo3P5Wdlvlqai/5S2yzHU3O7Mpar3dvP4XyzXo3N2Xy+ML/MT81LTJb75mwrTyaGPFsshouUMf/UPoZbdZRep4e1ldEwN+o4rcdLTN78XRvj7Xj32j7+2XEcDrfG+HE5+jTn06m5XhqrkbFemIu5+TX8u95q5t/Ro3lCbKc+ZtpkurJe+kIaTmfrtfxpLOv6YvzHWOj6TiSH0ireHra7q7W2nsib+noym2ij2fdsJGub2Wykjb7Xs1nq62vz/fk52mEia9+piVyf7bpb6oSY1enkurZ/7f7UrV6r6XXZ6r/67hlouqdjxgdbbVg6wv6xVxnH99YPTT58vtcnxzP/d8vj/4Mg9tvwD7Cv/ERyVcyDAAAAAElFTkSuQmCC" alt=""></div>
                        <hr>
                    </div>
                </div>

                <div style="margin-top: 10%">
                    {{--<header> <h2>{{$program_date_exercise->program_state}}:برنامه تمرینی</h2></header>--}}
                    <header> @if(count($program_date_exercise) )  <h2>برنامه تمرینی:</h2> @endif
                        @foreach($program_date_exercise as $exercise)
{{--                            <a id="exercise"  name="{{ $exercise->order_id }}" href="/coach/myusers/{{ $user->id }}/exercise/{{$exercise->program_date}}">{{$exercise->program_date}}</a>,--}}
                            <a id="exercise"  name="{{ $exercise->exercise_type }}" data-toggle="modal" href="#showexercisetext" >{{$exercise->program_date}}<input type="hidden" value="{{ $exercise }}" ></a>,

                        @endforeach
                    </header>
                </div>
            <div class="modal fade modal-lg center" id="showexercisetext" style="width: 100%">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content modal-lg">
                        <div class="modal-header modal-lg">
                            <h4 class="modal-title"> برنامه</h4>
                            <button id="close1" type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div  class="modal-body modal-lg">
                            <div id="text-modal" style="width:100% " ></div>
                        </div>
                        <div  class="modal-body modal-lg">
                            <div id="image-modal" ></div>
                        </div>
                        <div class="modal-footer modal-lg">
                            <a id="close" role="button" href="#" data-dismiss="modal" class="btn btn-danger">بستن</a>
                        </div>
                    </div>
                </div>

            </div>
            <script>
                $(document).ready(function(){
                    $(document).on('click', 'a', function(){
                        // $('a[]')
                        let exercise_type = this.name;
                        // let program_date = this.childNodes[0];
                        // let order_id=this.childNodes[1].value;
                        let order=this.childNodes[1].value;

                        let text_exercise_program = {!! json_encode($text_exercise_program) !!};
                        let text_exercise = {!! json_encode($text_exercise) !!};
                            console.log(order);
                        if (exercise_type == "text"){
                            // $("#showexercisetext").modal();
                            {{--let jobs = JSON.parse("{{ json_encode($program_date_exercise) }}");--}}
                            for(let i = 0; i < text_exercise_program.length; i++){
                                if (text_exercise_program[i].order_id != order.order_id) {
                                $('#text-modal').append('<table class=" table table-striped table-responsive-sm table-bordered table-hover table-active table-dark " style="text-align: right;;;margin: 2%;">\n' +
                                    '\n' +
                                    '        <thead>\n' +
                                    '        <tr style="">\n' +
                                    '            <td> عنوان برنامه:  <h5>'+text_exercise_program[i].title+'</h5> </td>\n' +


                                    '            <td> طول برنامه: <h5>'+text_exercise_program[i].period+'روز</h5>  </td>\n' +



                                    '            <td>  نوع برنامه: <h5>'+text_exercise_program[i].description+'</h5></td>\n' +
                                    '            <hr>\n' +
                                    '            <td>  تاریخ برنامه: <h5>'+text_exercise_program[i].program_date+'</h5></td>\n' +
                                    '            <hr>\n' +

                                    '        </tr>'+
                              //       ' <tr>'+
                              //     '  <td scope="col"><h6>حرکت:</h6></td>'+
                              //     '  <td scope="col"><h6>روز:</h6></td>'+
                              // '  <td scope="col"><h6> ست:</h6></td>'+
                              //  ' <td scope="col"><h6> تکرار: </h6></td>'+
                              //  ' <td scope="col"><h6> سیستم تمرینی :</h6></td>'+
                              //
                              //  ' </tr> '+
                                    '       </thead>');

                                $('#text-modal').append('<tbody>');
                                for(let j = 0; j < text_exercise[0].length; j++){
                                    if (text_exercise[0][j].days =='first'){ text_exercise[0][j].days="روز اول" ;}
                                    if (text_exercise[0][j].days =='second'){ text_exercise[0][j].days="روز دوم" ;}
                                    if (text_exercise[0][j].days =='third'){ text_exercise[0][j].days="روز سوم" ;}
                                    if (text_exercise[0][j].days =='fourth'){ text_exercise[0][j].days="روز چهارم" ;}
                                    if (text_exercise[0][j].days =='fifth'){ text_exercise[0][j].days="روز پنجم" ;}
                                    if (text_exercise[0][j].days =='sixth'){ text_exercise[0][j].days="روز ششم" ;}
                                    if (text_exercise[0][j].days =='seventh'){ text_exercise[0][j].days="روز هفتم" ;}
                                    if (text_exercise[0][j].exercise_systems == null){ text_exercise[0][j].exercise_systems="" ;}
                                    if (text_exercise[0][j].program_id ==text_exercise_program[i].program_id ){
                                        $('#text-modal').append('<tr>');

                                        $('#text-modal').append('<td ><h6 class="ml-5 mr-5">'+text_exercise[0][j].type_of_movement+'</h6></td>');

                                        // $('#text-modal').append('<div>'+text_exercise[0].length+'</div><br>');
                                        $('#text-modal').append('<td><h6 class="ml-5 mr-5">'+text_exercise[0][j].days+' </h6></td>');
                                        $('#text-modal').append('<td><h6>'+text_exercise[0][j].sets+'*'+'</h6></td>');
                                        $('#text-modal').append('<td><h6>'+text_exercise[0][j].repetitions+'</h6></td>');
                                        $('#text-modal').append('<td><h6 class="ml-5 mr-5">'+text_exercise[0][j].exercise_systems+'</h6></td>');
                                        $('#text-modal').append('</tr>');
                                    }
                                // }
                                    $('#text-modal').append('</tbody></table>');

                                }
                            }
                            }
                        }
                    });
                    $('#close').click(function () {
                        $('#text-modal').html('');
                    });
                    $('#close1').click(function () {
                        $('#text-modal').html('');
                    })
                });
            </script>
                <div style="margin-top: 10%">
                 <header>@if(count($program_date_supplement))    <h2>برنامه مکملی:</h2> @endif
                     @foreach($program_date_supplement as $supplement)

                         <a href="/coach/myusers/{{ $user->id }}/supplement/{{$exercise->program_date}}">{{$supplement->program_date}}</a>,

                        @endforeach
                    </header>
                    <div id="supplement"></div>
                </div>
                <div style="margin-top: 10%">
                 <header>@if(count($program_date_food))   <h2>برنامه غذایی:</h2> @endif
                     @foreach($program_date_food as $food)

                         <a href="/coach/myusers/{{ $user->id }}/food/{{$food->program_date}}">{{$food->program_date}}</a>,

                        @endforeach
                    </header>
                    <div id="supplement"></div>
                </div>

            </div>
            </footer>
        </article>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

    </article>
    <!-- END Main content -->




    <!-- Scripts -->
    {{--<script src="http://labs.infyom.com/laravelgenerator/public/assets/js/theDocs.all.min.js"></script>--}}
    {{--</body>--}}
    {{--</html>--}}

@endsection

