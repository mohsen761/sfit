<!DOCTYPE html>
<html lang="fa">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <!-- Bootstrap core CSS -->
    <link href="/sidebar/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/sidebar/css/simple-sidebar.css" rel="stylesheet">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="/css/style2.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />--}}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
    {{-- dropzone--}}
    <link rel="stylesheet" href="{{ url('/css/dropzone.css') }}">
    <link rel="stylesheet" href="{{ url('/css/custom.css') }}">
    {{-- dropzone--}}
    {{--font--}}
    <link href="https://cdn.rawgit.com/rastikerdar/samim-font/v[1.0.0]/dist/font-face.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.rawgit.com/rastikerdar/sahel-font/v1.0.0/dist/font-face.css" rel="stylesheet" type="text/css" />
    {{--font--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


</head>

<body>
<style>body{direction:rtl!important;
        background-color: #d6e9c6;
        /*background-image:url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYP5JZP8iwiXmQ3LlZms0lHvdqUTS8zzKSp1IGbvzV3UXC5Viw");*/
        /*background-position: center;*/
        /*background-repeat: no-repeat;*/
        /*background-size: cover;*/
        /*font-family: 'Bnazanin', sans-serif;*/
    }
    @font-face {
        font-family: Sahel;
        src: url('Sahel.eot');
        src: url('Sahel.eot?#iefix') format('embedded-opentype'),
        url('Sahel.woff') format('woff'),
        url('Sahel.ttf') format('truetype');
        font-weight: normal;
    }
</style>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        {{--<div class="sidebar-heading">Start Bootstrap </div>--}}

        <div class="list-group list-group-flush">
            <a href="#" class="navbar-brand  d-block
                mx-auto text-center py-3 mb-4 bottom-border">Sfit</a>
            <div class="bottom-border pb-3 text-center">
                <img id="currentPhoto" src="{{ $coach[1]}}" onerror="this.onerror=null; this.src='/images/profile.png'" width="50" style="height: 50px"  class="mr-3 rounded-circle mr-3" alt="">
                {{--<img src="{{ $coach[1]}}" width="50" style="height: 50px"  class="mr-3 rounded-circle mr-3" alt="">--}}
                <a href="#" > {{ $coach[0] }}</a>
            </div>
            <ul class="navbar-nav flex-column mt-4" style="margin-right: -18%">
                <li class="nav-item sidebar-link current " >
                    <a href="/coach/home" class="nav-link   p-3 mb-2" style="float: right;text-align: right">
                        <img style=";width: 15%;height: auto" src="/images/home.png" alt="">
                        خانه
                    </a>
                </li>
                <li class="nav-item sidebar-link">
                    <a href="/coach/myprof" class="nav-link   p-3 mb-2" style="float: right;text-align: right">
                        <img style=";width: 15%;height: auto" src="/images/profile.png" alt="">
                        پروفایل
                    </a>
                </li> <li class="nav-item sidebar-link">
                    <a href="/coach/myorders" class="nav-link   p-3 mb-2" style="text-align: right;float: right">
                        <img style=";width: 15%;height: auto" src="/images/request.png" alt="">
                        درخواست ها
                    </a>
                </li> <li class="nav-item sidebar-link">
                    <a href="/coach/myusers" class="nav-link   p-3 mb-2" style="text-align: right;float: right">
                        <img style=";width: 15%;height: auto" src="/images/student.png" alt="">
                        شاگردان من
                    </a>
                </li>
                <li class="nav-item sidebar-link">
                    <a href="/coach/mygym" class="nav-link   p-3 mb-2 " style="float: right;text-align: right">
                        <img style=";width: 15%;height: auto" src="/images/gym.png" alt="">
                        باشگاه من
                    </a>
                </li> <li class="nav-item sidebar-link">
                    <a href="/coach/getmoney" class="nav-link   p-3 mb-2" style="text-align: right;float: right">
                        <img style=";width: 15%;height: auto" src="/images/cash.png" alt="">
                        تسویه حساب
                    </a>
                </li> <li class="nav-item sidebar-link">
                    <a href="/coach/editprice" class="nav-link   p-3 mb-2" style="text-align: right;float: right">
                        <img style=";width: 15%;height: auto" src="/images/edit.png" alt="">
                        ویرایش قیمت ها
                    </a>
                </li>
                <li class="nav-item sidebar-link">
                    <a href="/coach/changepassword" class="nav-link  p-3 mb-2" style="text-align: right;float: right">
                        <img style=";width: 15%;height: auto" src="/images/change.png" alt="">
                        تغییر رمز
                    </a>
                </li>
                <li class="nav-item sidebar-link">
                    <a href="/coach/logout" class="nav-link  p-3 mb-2" style="text-align: right;float: right">
                        <img style=";width: 15%;height: auto" src="/images/exit.png" alt="">
                        خروج
                    </a>
                </li>

            </ul>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-primary" id="menu-toggle">منو</button>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                {{--<ul class="navbar-nav ml-auto mt-2 mt-lg-0">--}}
                    {{--<li class="nav-item active">--}}
                        {{--<a class="nav-link" href="#"> <span class="sr-only">(current)</span></a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="#">  </a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item dropdown">--}}
                        {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}

                        {{--</a>--}}
                        {{--<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
                            {{--<a class="dropdown-item" href="#">   </a>--}}
                            {{--<a class="dropdown-item" href="#">   </a>--}}
                            {{--<div class="dropdown-divider"></div>--}}
                            {{--<a class="dropdown-item" href="#">   </a>--}}

                        {{--</div>--}}
                    {{--</li>--}}
                {{--</ul>--}}
                <a href="#" class="nav-link" data-toggle="modal" data-target="#sign-out">
                    <img style=";width: 5%;height: auto" src="/images/exit.png" alt="">
                    {{--<h5 class="text-light">خروج</h5>--}}
                </a>
            </div>
        </nav>

        <div class="container-fluid">
            @yield('content')
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

{{--modal--}}

<div class="modal fade" id="sign-out">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"> آیا میخواهید خارج شوید ؟</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                برای خروج دکمه خروج را بزنید
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success">نه میمانم</button>
                <a role="button" href="/coach/logout"  class="btn btn-danger">میخواهم بروم</a>
            </div>
        </div>
    </div>
</div>
{{--end of  modal--}}


<!-- Bootstrap core JavaScript -->
<script src="/sidebar/vendor/jquery/jquery.min.js"></script>
<script src="/sidebar/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>

</body>

</html>
