@extends('coach.test')

@section('content')
    <title>ارسال به صورت گزینه</title>

    @if(session('success'))
        <div style="float: right;text-align: right;" class="alert alert-success alert-dark ">
            {{ session('success') }}
        </div>
        <br>
        <hr>
    @endif
    {{--<a href="/coach/myorders/{{ $program_exercise->order_id }}" class="btn btn-primary" style="width: fit-content"> بازگشت به عقب</a>--}}
    <form  method="post" action="/coach/myorders/{{ $order->order_id }}/exercise/checkbox" style="text-align: right;max-width: 800px;" >
        @csrf
        @if(isset($program_exercise))
            <lable>نام برنامه</lable> <input style="width: 70%" type="text" name="title" value="{{ $program_exercise->title }}" class="form-control name_list" required="" />
            <lable>طول برنامه</lable>  <input  style="width: 70%" type="number" name="period" value="{{ $program_exercise->period }}" class="form-control name_list" required="" />
            <lable>توضیحات برنامه</lable> <input style="width: 70%"  type="text" name="description" value="{{ $program_exercise->description }}" class="form-control name_list" required="" />
            <input type="hidden" name="program_type" value="{{ $type }}" />
            <input type="hidden" name="program_exercise_update" value="{{ $program_exercise->program_id }}" />
            <hr>
        @else
            <input type="text" name="title" placeholder=" عنوان برنامه را وارد کنید" class="form-control name_list" required="" />
            <input type="number" name="period" placeholder=" مدت زمان برنامه را وارد کنید" class="form-control name_list" required="" />
            <input type="text" name="description" placeholder="توضیحات برنامه را وارد کنید" class="form-control name_list" required="" />
            <input type="hidden" name="program_type" value="{{ $type }}" />
            <input type="hidden" name="program_exercise_update" value="no" />
            <hr>
        @endif
        @foreach($movements as $movement)
            <hr>
        <div class="custom-control custom-switch mt-2 mb-2" >
            <input  type="checkbox" class="custom-control-input" value="{{ $movement->movement_name }}" name="name[]" id="{{ $movement->movement_id }}">
            <label class="custom-control-label" for="{{ $movement->movement_id }}">{{ $movement->movement_name }}</label>
            <div id="switch{{ $movement->movement_id }}"></div>

        </div>
            <hr>
        @endforeach
        <input type="hidden" id="items" name="items" value="">
        <script type="text/javascript">
            $(document).on('click', 'input', function(){
                // console.log(e.currentTarget.attributes.id);
               // let m= $('input[type="checkbox"]');
               //  let items = [];

                if ($(this).attr("id") != undefined){
                    let j= $(this).attr("id");

                    // if ($('#switch'+j+'').text() == ""){
                    //     items.push('"'+j+'"');
                    //     $('#items').val(items);
                    // }else {
                    //
                    // }
                    // alert($('#switch'+j+'').text());
                    if ($('#switch'+j+'').text() == ""){
                       $('#switch'+j).append('<select class="form-control name_list"  name="day[]" id="" style="width: 40%;margin-top: 2px;height:35px;">\n' +
                            '                                                               <option value="first"> روز اول</option>\n' +
                            '                                                                <option value="second"> روز دوم</option>\n' +
                            '                                                                <option value="third"> روز سوم</option>\n' +
                            '                                                                <option value="fourth"> روز چهارم</option>\n' +
                            '                                                                <option value="fifth"> روز پنجم</option>\n' +
                            '                                                                <option value="sixth"> روز ششم</option>\n' +
                            '                                                                <option value="seventh"> روز هفتم</option>\n' +
                            '                                                            </select><br>\n' +
                            '              <select   name="set[]"  class="form-control name_list"  style=";width: 40%;margin-top: 2px;">' +
                           '  <option value="1">  1 ست</option>' +
                           '  <option value="2">  2 ست</option>' +
                           '  <option value="3">  3 ست</option>' +
                           '  <option value="4">  4 ست</option>' +
                           '  <option value="5">  5 ست</option>' +

                           '</select>  ' +
                           '<select class="form-control name_list"   name="repetitions[]" id="" style=";width: 40%;margin-top: 2px;">' +
                           '  <option value="1">  1 تکرار</option>' +
                           '  <option value="2">  2 تکرار</option>' +
                           '  <option value="3">  3 تکرار</option>' +
                           '  <option value="4">  4 تکرار</option>' +
                           '  <option value="5">  5 تکرار</option>' +
                           '  <option value="6">  6 تکرار</option>' +
                           '  <option value="7">  7 تکرار</option>' +
                           '  <option value="8">  8 تکرار</option>' +
                           '  <option value="9">  9 تکرار</option>' +
                           '  <option value="10">  10 تکرار</option>' +
                           '  <option value="11">  11 تکرار</option>' +
                           '  <option value="12">  12 تکرار</option>' +
                           '</select>  ' +
                           // '<input style="width: 70px" type="text" name="set[]" placeholder="ست" class="form-control name_list"  />\n' +
                           //  '                <input  style="width: 70px;" type="text" name="repetitions[]" placeholder="تکرار" class="form-control name_list"  />\n' +
                            '                <input  style="width: 150px" type="text" name="exercise_systems[]" placeholder="سیستم تمرینی" class="form-control name_list"  />')
                        // alert('empty');
                    }else{
                        $('#switch'+j+'').html('');
                    }

                    // alert($(this).attr("id"));
                }

                // $('#movement_name'+i+'').val($(this).text());
                // $('#movementList'+i+'').fadeOut();
            });
        </script>
        <button type="submit" class="m-5 btn btn-lg btn-outline-success"> ارسال برنامه</button>
    </form>




@endsection