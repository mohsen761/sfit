@extends('coach.test')

@section('content')
    <title> باشگاه من</title>

    <!-- Main content -->
    <article class="col-xl-10 main-content" role="main" style="background-color: #fffacc;box-shadow: 10px 10px 10px #888888;;text-align: right;margin-top: 5%">
        <a href="/coach/home" class="btn btn-primary"> بازگشت به عقب</a>
        <div class="container col">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if($gymdetails!= 'null' )
            @foreach ($gymdetails as $gymdetail)
                <div class="row" style="border-width: 2px;border-style: groove;border-color: #3f9ae5;width: fit-content;padding: 10%">
                        <article class="">
                            <header>
                                <h1>
                                    <p> نام باشگاه من: {{$gymdetail->gym_name }}</p>
                                </h1>
                            </header>
                            <div class="post-content ">
                                <div class="">
                                    <p>شهر :{{$gymdetail->gym_state }} </p>
                                    <p>   شهریه ماهانه: {{ $gymdetail->gym_monthly_fee }}تومان</p>
                                    <p>  نوع باشگاه:  @if($gymdetail->gym_type =='ladies') مخصوص خانم ها  @elseif ($gymdetail->gym_type =='gentleman')  مخصوص آقایان @else  خانم ها و آقایان @endif   </p>
                                    <p> جایگاه باشگاه:{{ $gymdetail->gym_state }} </p>
                                    <p>  آدرس :    {{ $gymdetail->gym_address }} </p>
                                    <p> امکانات باشگاه: {{ $gymdetail->gym_services }} </p>
                                    <p>  تعداد ورزشکاران باشگاه:{{ $gymdetail->gym_students_number }}</p>
                                </div>
                            </div>
                            <br>
                            <hr>
                            <div class="post-content">
                                <div class="post-graphical-image" style="" >

                                        <h3>عکس های باشگاه</h3>
                                        <img class="img-thumbnail " style="width:70%;height:auto;" src="
                                             {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/coach/images/gymImage/'.$gymdetail->gym_id.'/0.png'))--}}
                                               /bodybuilding/gym/images/gymImage/{{$gymdetail->gym_id}}/0.png"
                                             onerror="this.onerror=null; this.src='http://www.genoaparkdistrict.com/wp-content/uploads/2017/10/GFC-Track-Equipment.jpg'"
                                    {{--@else--}}

                                    {{--@endif--}}
                                                 >
                                    <img class="img-thumbnail " style="width:70%;height:auto;" src="
                                             {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/coach/images/gymImage/'.$gymdetail->gym_id.'/0.png'))--}}
                                                /bodybuilding/gym/images/gymImage/{{$gymdetail->gym_id}}/1.png" onerror="this.onerror=null; this.src='   https://www.nuffieldhealth.com/local/da/18/86413a804a69951e0942862c3524/treadmills-at-our-ilford-gym.jpg'"
                                    {{--@else--}}

{{--@endif--}}
                                                >
                                    <img class="img-thumbnail " style="width:70%;height:auto;" src="
                                             {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/coach/images/gymImage/'.$gymdetail->gym_id.'/0.png'))--}}
                                                /bodybuilding/gym/images/gymImage/{{$gymdetail->gym_id}}/2.png" onerror="this.onerror=null; this.src='https://content3.jdmagicbox.com/comp/surat/k2/0261px261.x261.170619175502.j5k2/catalogue/abs-fitness-gym-bhagal-surat-gyms-pvkmp.jpg'"
                                    {{--@else--}}

{{--@endif--}}
                                                >
                                </div>
                            </div>
                            <a role="button" class="btn btn-warning" style="font-size: 20px;" href="/coach/mygym/{{$gymdetail->gym_id}}/edit"> ویرایش اطلاعات باشگاه</a>
                        </article>

                </div>
                <br/>
            @endforeach

                @else

                    <div class="row" style=";border-width: 2px;border-style: groove;border-color: #3f9ae5;width: fit-content;padding: 7%">
                        @if(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                        @if(session('success'))
                            <div style="width: fit-content; float: right" class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                    @if ($errors->any())1
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="/coach/mygym/store" method="post">
                        <style>
                            .form-group{
                                width: 500px;
                                direction: rtl;
                                font-size: 15px;
                            }
                            .form-control{
                                font-size: 15px;
                            }
                        </style>
                        {{ csrf_field() }}
                        <div class="form-group float-label-control">
                            <label for="">نام باشگاه </label>
                            <input required  name="gym_name" type="text" class="form-control" placeholder="نام باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">مکان باشگاه </label>
                            <input  required  name="gym_state" type="text" class="form-control" placeholder="مکان باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">شهریه باشگاه </label>
                            <input required  name="gym_monthly_fee" type="text" class="form-control" placeholder="شهریه باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">نوع باشگاه </label>
                            <select  required name="gym_type" id="">
                                <option value="gentleman" >مردانه</option>
                                <option value="ladies" >زنانه</option>
                                <option value="ladies_and_gentleman" >مردانه و زنانه</option>
                            </select>

                        </div>
                        <div class="form-group float-label-control">
                            <label for="">شهر باشگاه </label>
                            <input required  name="gym_city" type="text" class="form-control" placeholder="شهر باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">آدرس باشگاه </label>
                            <input  required  name="gym_address" type="text" class="form-control" placeholder="آدرس باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">امکانات باشگاه </label>
                            <input required  name="gym_services" type="text" class="form-control" placeholder="امکانات باشگاه">
                        </div>
                        <div class="form-group float-label-control">
                            <label for="">تعداد ورزشکاران باشگاه </label>
                            <input required  name="gym_students_number" type="text" class="form-control" placeholder="تعداد ورزشکاران باشگاه">
                        </div>

                        <button  type="submit" class="btn btn-lg btn-outline-success" value="اعمال تغییرات" >اعمال تغییرات</button>

                    </form>
                  </div>
                       <br/>

                @endif

        </div>

            {{--{{ $orders->links() }}--}}



    </article>
    <!-- END Main content -->


@endsection
