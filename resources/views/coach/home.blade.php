@extends('coach.test')

@section('content')
    <title> خوش آمدید</title>

        <!-- Main content -->
        <article class="col-md-9 col-sm-9 main-content" role="main" style="">
            <header>
                 <div class="container">

                    @foreach ($posts as $post)

                        <div class="col" style="background-color: #fffacc; box-shadow: 10px 10px 10px #888888;;text-align: right ;direction: rtl;">
                            <div style="font-size: 200%">{{ $post->post_title }} <br></div>
                            <br>
                            <div>
                                <img class="rounded img-fluid" src="/bodybuilding/admin/images/post/{{ $post->post_id }}.png" alt="">
                            </div>
                            <br>
                            {{--<div>{{ $post->post_description }}</div>--}}
                            <div>{{ str_limit($post->post_description, $limit = 200, $end = '...')}} <br> <a href="/coach/post/{{ $post->post_id}}"> ادامه مطلب >>> </a> </div>
                        </div>  <br> <br>

                    @endforeach


                        {{ $posts->onEachSide(1)->links() }}

                     </div>

            </header>
        </article>

<!-- Scripts -->
{{--<script src="http://labs.infyom.com/laravelgenerator/public/assets/js/theDocs.all.min.js"></script>--}}
{{--</body>--}}
{{--</html>--}}

@endsection

