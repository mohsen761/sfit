<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="/css/style2.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />--}}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
    {{-- dropzone--}}
    <link rel="stylesheet" href="{{ url('/css/dropzone.css') }}">
    <link rel="stylesheet" href="{{ url('/css/custom.css') }}">
    {{-- dropzone--}}
    {{--font--}}
    <link href="https://cdn.rawgit.com/rastikerdar/samim-font/v[1.0.0]/dist/font-face.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.rawgit.com/rastikerdar/sahel-font/v1.0.0/dist/font-face.css" rel="stylesheet" type="text/css" />
    {{--font--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<style>body{direction:rtl!important;
        background-color: #d6e9c6;
        /*background-image:url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYP5JZP8iwiXmQ3LlZms0lHvdqUTS8zzKSp1IGbvzV3UXC5Viw");*/
        /*background-position: center;*/
        /*background-repeat: no-repeat;*/
        /*background-size: cover;*/
        /*font-family: 'Bnazanin', sans-serif;*/
    }
    @font-face {
        font-family: Sahel;
        src: url('Sahel.eot');
        src: url('Sahel.eot?#iefix') format('embedded-opentype'),
        url('Sahel.woff') format('woff'),
        url('Sahel.ttf') format('truetype');
        font-weight: normal;
    }
</style>
<!--  navbar  -->
 <nav class="navbar navbar-expand-lg navbar-light">
     <button class="navbar-toggler ml-auto mb-2 bg-light" type="button"
     data-toggle="collapse" data-target="#myNavbar">
         <span class="navbar-toggler-icon"></span>
     </button>
        <div class="collapse navbar-collapse" id="myNavbar">
        <div class="container-fluid ">
        <div class="row">
            <!-- sidebar -->
            <div class="col-xl-2 col-lg-3 col-md-4 col-sm-4 col-6 sidebar fixed-top">
                <a href="#" class="navbar-brand text-white d-block
                mx-auto text-center py-3 mb-4 bottom-border">Sfit</a>
                <div class="bottom-border pb-3 text-center">
                    <img src="{{ $coach[1]}}" width="50" style="height: 50px"  class="mr-3 rounded-circle mr-3" alt="">
                    <a href="#" class="text-white"> {{ $coach[0] }}</a>
                </div>
                <ul class="navbar-nav flex-column mt-4" style="margin-right: -18%">
                    <li class="nav-item sidebar-link current " >
                        <a href="/coach/home" class="nav-link text-white p-3 mb-2" style="float: right;text-align: right">
                            <img style=";width: 15%;height: auto" src="/images/home.png" alt="">
                            خانه
                        </a>
                    </li>
                    <li class="nav-item sidebar-link">
                        <a href="/coach/myprof" class="nav-link text-white p-3 mb-2" style="float: right;text-align: right">
                            <img style=";width: 15%;height: auto" src="/images/profile.png" alt="">
                            پروفایل
                        </a>
                    </li> <li class="nav-item sidebar-link">
                        <a href="/coach/myorders" class="nav-link text-white p-3 mb-2" style="text-align: right;float: right">
                            <img style=";width: 15%;height: auto" src="/images/request.png" alt="">
                            درخواست ها
                        </a>
                    </li> <li class="nav-item sidebar-link">
                        <a href="/coach/myusers" class="nav-link text-white p-3 mb-2" style="text-align: right;float: right">
                            <img style=";width: 15%;height: auto" src="/images/student.png" alt="">
                            شاگردان من
                        </a>
                    </li>
                    <li class="nav-item sidebar-link">
                        <a href="/coach/mygym" class="nav-link text-white p-3 mb-2 " style="float: right;text-align: right">
                            <img style=";width: 15%;height: auto" src="/images/gym.png" alt="">
                            باشگاه من
                        </a>
                    </li> <li class="nav-item sidebar-link">
                        <a href="/coach/getmoney" class="nav-link text-white p-3 mb-2" style="text-align: right;float: right">
                            <img style=";width: 15%;height: auto" src="/images/cash.png" alt="">
                            تسویه حساب
                        </a>
                    </li> <li class="nav-item sidebar-link">
                        <a href="/coach/editprice" class="nav-link text-white p-3 mb-2" style="text-align: right;float: right">
                            <img style=";width: 15%;height: auto" src="/images/edit.png" alt="">
                             ویرایش قیمت ها
                        </a>
                    </li>
                    <li class="nav-item sidebar-link">
                        <a href="/coach/changepassword" class="nav-link text-white p-3 mb-2" style="text-align: right;float: right">
                            <img style=";width: 15%;height: auto" src="/images/change.png" alt="">
                            تغییر رمز
                        </a>
                    </li>

                </ul>
            </div>


            <!--end of sidebar -->

            <!-- top-nav -->
            <div class="col-xl-10 col-lg-9 col-md-8 mr-auto bg-dark fixed-top py-2 top-navbar">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <h4 class="text-light mb-0">sfit ,راهی نو برای داشتن اندامی فیت</h4>
                    </div>
                    <div class="col-md-8">
                        <ul class="navbar-nav">
                            <li class="navbar-item mr-auto">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#sign-out">
                                    <img style=";width: 15%;height: auto" src="/images/exit.png" alt="">
                                    {{--<h5 class="text-light">خروج</h5>--}}
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--end of top-nav -->
        </div>
    </div>

</div>



 </nav>
<!-- end of  navbar  -->

{{--modal--}}

<div class="modal fade" id="sign-out">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"> آیا میخواهید خارج شوید ؟</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                برای خروج دکمه خروج را بزنید
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-success">نه میمانم</button>
                <a role="button" href="/coach/logout"  class="btn btn-danger">میخواهم بروم</a>
            </div>
        </div>
    </div>
</div>
{{--end of  modal--}}

{{--section--}}
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-9 mr-auto">
                <div class="row pt-5 mt-3 mb-5">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</section>

{{-- end of section--}}






<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="/js/script.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="{{ url('/js/jquery.js') }}"></script>
<script src="{{ url('/js/dropzone.js') }}"></script>
<script src="{{ url('/js/dropzone-config.js') }}"></script>
</body>
</html>