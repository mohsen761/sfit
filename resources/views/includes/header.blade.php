<div class="nav-container">
    <img class="logo" alt="سامانه اشتراک گذاری" src="{{url('img/logo.png')}}" />
    <span class="site-name">سامانه اشتراک گذاری</span>
    <a class="logout" href="{{url('/logout')}}">خروج</a>
    <span style="float: left; margin-left: 10px" class="username">{{Auth::user()->name?:Auth::user()->email}}</span>
</div>