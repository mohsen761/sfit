<aside class="sidebar">
    <div id="leftside-navigation" class="nano">
        <ul class="nano-content">
            @foreach($sideMenuItems as $item)
                <li class="sub-menu">
                    <a href="{{$item["url"]??"javascript:void(0);"}}"><i
                                class="{{$item['faIcon']??"fa fa-cogs"}}"></i><span>{{$item['label']}}</span>
                        @if($item['items']??[])
                        <i class="arrow fa fa-angle-left pull-left">
                        @endif

                        </i></a>
                    @if($item['items']??[])
                        <ul>
                            @foreach($item['items']??[] as $subItem)

                                <li>
                                    <a href="{{$subItem['url']??"javascript:void(0);"}}">{{$subItem['label']}}</a>
                                </li>



                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
                {{--<li class="sub-menu">--}}
                {{--<a href="javascript:void(0);"><i class="fa fa-cogs"></i><span>خرید ها</span>--}}
                {{--<i class="arrow fa fa-angle-left pull-left"></i></a>--}}
                {{--<ul>--}}

                {{--<li>--}}
                {{--<a href="{{action("Admin\OwnershipController@index")}}">لیست خرید ها</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="{{action("Admin\GoodController@create")}}">افزودن کالای جدید</a>--}}
                {{--</li>--}}
                {{--</ul>--}}
                {{--</li>--}}
        </ul>
    </div>
</aside>