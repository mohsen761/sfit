@extends('guest.test')

@section('content')
    <title> لطفا وارد سایت شوید.</title>

    <article class="col-md-9 col-sm-9 main-content" role="main"style=";text-align: right;margin-top: 5%">
        {{--<a href="/user/home" class="btn btn-primary"> بازگشت به عقب</a>--}}
        <div class="container col">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
                <h2> {{ $coach->coach_name }} {{ $coach->coach_family }}</h2>

                <div class="col" style="padding: 2%;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;;text-align: right ;direction: rtl;">
                    <div style="font-size: 200%">{{ $coach->post_title }} <br></div>
                    <br>
                    <p>عکس پروفایل مربی:   </p>
                    <div>
                        <img class="rounded img-fluid " style="width: 50%;" src="http://sfit.ir/bodybuilding/coach/images/coachesImagesProfile/{{ $coach->coach_id }}.png" alt="">
                    </div>
                    <br>
                    <p>عکس های بیشتر:   </p>
                    <div class="w3-content w3-display-container">
                        <img  id="currentPhoto" style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $coach->coach_id }}/0.png" onerror="this.onerror=null; this.src='/images/profile.png'" alt="">
                        <img id="currentPhoto" style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $coach->coach_id }}/1.png" onerror="this.onerror=null; this.src='/images/profile.png'" alt="">
                        <img id="currentPhoto" style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $coach->coach_id }}/2.png" onerror="this.onerror=null; this.src='/images/profile.png'" alt="">
                        {{--<img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $user->coach_id }}/1.png" alt="">--}}
                        {{--<img style="width: 100%;" class="mySlides"  src="/bodybuilding/coach/images/coachesImages/{{ $user->coach_id }}/0.png" alt="">--}}
                        {{--<img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/images/coachesImages/{{ $user->coach_id }}/2.png" alt="">--}}
                        <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10094;</button>
                        <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10095;</button>



                    </div>
                    <script>
                        var slideIndex = 1;
                        showDivs(slideIndex);

                        function plusDivs(n) {
                            showDivs(slideIndex += n);
                        }

                        function showDivs(n) {
                            var i;
                            var x = document.getElementsByClassName("mySlides");
                            if (n > x.length) {slideIndex = 1}
                            if (n < 1) {slideIndex = x.length}
                            for (i = 0; i < x.length; i++) {
                                x[i].style.display = "none";
                            }
                            x[slideIndex-1].style.display = "block";
                        }
                    </script>
                    <div class="profile-usertitle">
                        <hr>
                        <p>  افتخارات: </p>
                        <p >  {{ $coach->coach_honors }}  </p> <hr>
                        <p>  سن: </p>
                        <p>  {{ $coach->coach_age }}  </p> <hr>
                        <p>  استان: </p>
                        <p>  {{ $coach->coach_state }}  </p> <hr>
                        {{--<p>  موبایل: </p>--}}
                        {{--<p>  {{ $coach->coach_mobile }}  </p> <hr>--}}
                        <p>  ایمیل: </p>
                        <p>  {{ $coach->email }}  </p> <hr>
                        <p>  اینستاگرام: </p>
                        <p>   <img class="ml-3" style=";width: 5%;height: auto" src="/images/instagram.png" alt=""><a href="https://instagram.com/{{ $coach->coach_instagram }}">{{ $coach->coach_instagram }}</a>  </p> <hr>
                    </div>
                    @if($price)

                   <div>{{ $price->exercise_price }} تومان <lable>قیمت برنامه تمرینی</lable></div>
                    <div>{{ $price->food_price }} تومان <lable>قیمت برنامه غذایی</lable></div>
                    <div>{{ $price->supplement_price }} تومان <lable>قیمت برنامه مکملی</lable></div>
                        @if($price->discount)
                    <div>{{ $price->discount }} درصد <lable>تخفیف</lable></div>

                    <div> به مناسبت <lable>{{ $price->event }}</lable></div>
                    <br>
                            @endif
                    @endif
                    <div> <a class="btn btn-primary btn-outline-success" href="/user/login">  انتخاب این مربی برای من</a></div>
                    {{--<div> <a class="btn btn-primary btn-outline-success" href="/user/coachdetail/select/{{ $coach->coach_id }}">  انتخاب این مربی برای من</a></div>--}}
                    {{--<div>{{ str_limit($post->post_description, $limit = 200, $end = '...')}}</div>--}}
                </div>  <br> <br>

        </div>

        {{--{{ $orders->links() }}--}}



    </article>
@endsection