@extends('guest.test')

@section('content')
    <title> مربی ها </title>

    <article class="col-md-9 col-sm-9 main-content" role="main"style=";text-align: right;margin-top: 5%">

        <div class="container col">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @foreach($coaches  as $item)
                <h2> {{ $item->coach_name }} {{ $item->coach_family }}</h2>

                <div class="col" style="padding: 2%;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;;text-align: right ;direction: rtl;">
                    <br>
                    <div>
                        <img class="rounded img-fluid " style="width: 50%;" src="http://sfit.ir/bodybuilding/coach/images/coachesImagesProfile/{{ $item->coach_id }}.png" alt="">
                    </div>
                    <br>
                    <div>{{ $item->coach_honors }}</div>
                    <br>
                    <div> <a href="/coachdetail/{{ $item->coach_id }}">اطلاعات بیشتر و انتخاب مربی</a></div>
                    {{--<div>{{ str_limit($post->post_description, $limit = 200, $end = '...')}}</div>--}}
                </div>  <br> <br>

            @endforeach



        </div>

        {{--{{ $orders->links() }}--}}



    </article>
@endsection