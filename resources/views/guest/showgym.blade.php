@extends('guest.test')

@section('content')
    <title> باشگاه {{ $gym->gym_name }}</title>

    <!-- Main content -->
    <article class="col-xl-10 main-content" role="main" style="background-color: #fffacc;box-shadow: 10px 10px 10px #888888;;text-align: right;margin-top: 5%">
        <div class="container col">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
           
             
                    <div class="row" style="border-width: 2px;border-style: groove;border-color: #3f9ae5;width: fit-content;padding: 10%">
                        <article class="">
                            <header>
                                <h1>
                                    <p> نام باشگاه : {{$gym->gym_name }}</p>
                                </h1>
                            </header>
                            <div class="post-content ">
                                <div class="">
                                    <p>شهر :{{$gym->gym_state }} </p>
                                    <p>   شهریه ماهانه: {{ $gym->gym_monthly_fee }}تومان</p>
                                    <p>  نوع باشگاه:  @if($gym->gym_type =='ladies') مخصوص خانم ها  @elseif ($gym->gym_type =='gentleman')  مخصوص آقایان @else  خانم ها و آقایان @endif   </p>
                                    <p> جایگاه باشگاه:{{ $gym->gym_state }} </p>
                                    <p>  آدرس :    {{ $gym->gym_address }} </p>
                                    <p> امکانات باشگاه: {{ $gym->gym_services }} </p>
                                    <p>  تعداد ورزشکاران باشگاه:{{ $gym->gym_students_number }}</p>
                                </div>
                            </div>
                            <br>
                            <hr>

                            @if($working !='0')
                        <table class="table table-striped table-responsive-sm  table-hover table-active">
                            <thead>

                            </thead>

                            <tbody>
                                <tr>
                                    <td>روز هفته</td>
                                    <td>وضعیت باشگاه</td>
                                    <td>مردانه یا زنانه</td>
                                    <td>از ساعت</td>
                                    <td>تا ساعت</td>
                                </tr>

                                @foreach($working as $item)
                                    <tr>
                                        @if($item->days == 'sat')
                                   <td> <div  > شنبه</div></td>
                                        @endif
                                       @if($item->days == 'sun')
                                   <td> <div  > یکشنبه</div></td>
                                        @endif
                                       @if($item->days == 'mon')
                                   <td> <div  > دوشنبه</div></td>
                                        @endif
                                       @if($item->days == 'tue')
                                   <td> <div > سه شنبه</div></td>
                                        @endif
                                       @if($item->days == 'wed')
                                   <td> <div  > چهارشنبه</div></td>
                                        @endif
                                       @if($item->days == 'thu')
                                   <td> <div  > پنجشنبه</div></td>
                                        @endif
                                       @if($item->days == 'fri')
                                   <td> <div  > جمعه</div></td>
                                        @endif

                                            @if($item->gym_status == 'open')
                                                <td>  <div class="col-md-2"> باز</div></td>
                                            @endif
                                            @if($item->gym_status == 'close')
                                                <td>  <div class="col-md-2"> بسته</div></td>
                                            @endif
                                            @if($item->sex == 'gentlemen')
                                                <td>  <div > آقایان</div></td>
                                            @endif
                                            @if($item->sex == 'ladies')
                                                <td>  <div > خانم ها</div></td>
                                            @endif


                                  <td>  <div class="col-md-2"> {{ $item->time_from }}</div></td>
                                   <td> <div class="col-md-2"> {{ $item->time_to }}</div></td>
                                     </tr>
                          @endforeach


                                    </tbody>
                                    </table>
                            @endif
                            <div class="post-content">
                                <div class="post-graphical-image" style="" >

                                    <h3>عکس های باشگاه</h3>
                                    <img class="img-thumbnail " style="width:70%;height:auto;" src="
                                             {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/coach/images/gymImage/'.$gym->gym_id.'/0.png'))--}}
                                            /bodybuilding/gym/images/gymImage/{{$gym->gym_id}}/0.png"
                                         onerror="this.onerror=null; this.src='http://www.genoaparkdistrict.com/wp-content/uploads/2017/10/GFC-Track-Equipment.jpg'"
                                            {{--@else--}}

                                            {{--@endif--}}
                                    >
                                    <img class="img-thumbnail " style="width:70%;height:auto;" src="
                                             {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/coach/images/gymImage/'.$gym->gym_id.'/0.png'))--}}
                                            /bodybuilding/gym/images/gymImage/{{$gym->gym_id}}/1.png" onerror="this.onerror=null; this.src='   https://www.nuffieldhealth.com/local/da/18/86413a804a69951e0942862c3524/treadmills-at-our-ilford-gym.jpg'"
                                            {{--@else--}}

                                            {{--@endif--}}
                                    >
                                    <img class="img-thumbnail " style="width:70%;height:auto;" src="
                                             {{--@if(@GetImageSize('http://sfit.ir/bodybuilding/coach/images/gymImage/'.$gym->gym_id.'/0.png'))--}}
                                            /bodybuilding/gym/images/gymImage/{{$gym->gym_id}}/2.png" onerror="this.onerror=null; this.src='https://content3.jdmagicbox.com/comp/surat/k2/0261px261.x261.170619175502.j5k2/catalogue/abs-fitness-gym-bhagal-surat-gyms-pvkmp.jpg'"
                                            {{--@else--}}

                                            {{--@endif--}}
                                    >
                                </div>
                            </div>
                        </article>

                    </div>

        </div>

        {{--{{ $orders->links() }}--}}



    </article>
    <!-- END Main content -->


@endsection
