@extends('guest.test')

@section('content')

    <title>برنامه های غذایی</title>
    {{--<a href="/coach/myorders" class="btn btn-primary" style="width: fit-content"> بازگشت به عقب</a>--}}

        <table class=" table table-striped table-responsive-sm table-bordered table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>

                <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

                <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->created_at,10)) }}</h5></td>

            </tr>
            <tr>
                <td scope="col"><h6>وعده:</h6></td>
                <td scope="col"><h6>پیشنهاد اول:</h6></td>
                <td scope="col"><h6>پیشنهاد دوم:</h6></td>
                <td scope="col"><h6>پیشنهاد سوم:</h6></td>

            </tr>
            </thead>
            <tbody>
            @foreach($foods as $food)
                    @if($program->program_id == $food->program_id)
                        <tr>
                            <td scope="row">  <h6>@if($food->meal == 'breakfast')  صبحانه @elseif($food->meal == 'snack')  میان وعده @elseif($food->meal == 'lunch') نهار @elseif($food->meal == 'before_exercise') قبل از تمرین @elseif($food->meal == 'after_exercise') بعد از تمرین @elseif($food->meal == 'dinner')   شام @elseif($food->meal == 'before_sleep')  قبل از خواب  @endif   </h6></td>
                            <td> <h6>{{ $food->offer1 }} </h6></td>
                            <td>   <h6>{{ $food->offer2 }} </h6></td>
                            <td>   <h6>{{ $food->offer3 }} </h6></td>
                        </tr>
                    @endif

            @endforeach


            </tbody>

        </table>




@endsection