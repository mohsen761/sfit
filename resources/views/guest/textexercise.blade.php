@extends('guest.test')

@section('content')
    <title>برنامه های تمرینی</title>
    {{--<a href="/coach/myorders" class="btn btn-primary" style="padding-top: 1.5%;width: fit-content"> بازگشت به عقب</a>--}}

    <style>

        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons inside the tab */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
        table {
            counter-reset: rowNumber;
        }

        table tr {
            counter-increment: rowNumber;
        }

        table tr td:first-child::before {
            content: counter(rowNumber);
            min-width: 1em;
            margin-right: 0.5em;
        }
    </style>

    <div class="tab" style="text-align: right;float: right">
        <button class="tablinks" onclick="openCity(event, 'seven')">روز هفتم</button>
        <button class="tablinks" onclick="openCity(event, 'six')">روز ششم</button>
        <button class="tablinks" onclick="openCity(event, 'five')">روز پنجم</button>
        <button class="tablinks" onclick="openCity(event, 'four')">روز چهارم</button>
        <button class="tablinks" onclick="openCity(event, 'three')">روز سوم</button>
        <button class="tablinks" onclick="openCity(event, 'two')">روز دوم</button>
        <button class="tablinks" onload="openCity(event, 'one')" onclick="openCity(event, 'one')">روز اول</button>
    </div>
    <br>
    <br>

    <style>
        .table{
            box-shadow: 10px 10px 10px #888888;
        }
    </style>
    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @if(session('success'))
        <div style="width: fit-content; float: right" class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif




    <div id="one"  class="tabcontent active " style="width: 100%" >
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td ></td>
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>
                @if($program->period !='0')
                <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>
                @else
                    <td></td>
                    @endif
                @if($program->program_date !='')
                <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->program_date,10)) }}</h5></td>
                    @else
                    <td></td>
                @endif
                <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            </tr>
            {{--<tr>--}}
                {{--<td ></td>--}}
                {{--<td scope="col"><h6>حرکت:</h6></td>--}}
                {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
                {{--<td scope="col"><h6></h6></td>--}}
                {{--<td scope="col"><h6>  </h6></td>--}}
                {{--<td scope="col"><h6> سیستم تمرینی :</h6></td>--}}
            {{--</tr>--}}
            </thead>
            <tbody></tbody>
        </table>
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">
            <thead>
            @foreach($exercises as $exercise)

                @if($exercise->days =='first')
                    <tr id="{{ $exercise->exercise_id}}" >
                        <td></td>
                        <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                        {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                        <td> <h6>{{ $exercise->sets }} ست</h6></td>
                        <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                        {{--@if($exercise->exercise_systems !='')--}}
                            {{--<td>  <h6> {{ $exercise->exercise_systems }} </h6></td>@else <td></td>@endif--}}
                        @if($exercise->exercise_systems =='basic')
                            <td>  <h6> ساده </h6></td>
                       @elseif($exercise->exercise_systems =='superSet')
                            <td>  <h6> سوپرست </h6></td>
                       @elseif($exercise->exercise_systems =='dropSet')
                            <td>  <h6> کم کردنی </h6></td>
                       @elseif($exercise->exercise_systems =='threeSet')
                            <td>  <h6> تری ست </h6></td>
                       @elseif($exercise->exercise_systems =='maximum')
                            <td>  <h6> حداکثر توان </h6></td>
                         @elseif($exercise->exercise_systems =='pyramid')
                            <td>  <h6> هرمی </h6></td>@else
                            <td>  <h6> {{ $exercise->exercise_systems }} </h6></td>
                            @endif
                        @if($exercise->description !='')
                            <td>  <h6> {{ $exercise->description }} </h6></td>@else <td></td>@endif


                    </tr>
                @endif

            @endforeach
            </thead>

        </table>


    </div>
    <div id="two" class="tabcontent" style="width: 100%" >
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td ></td>
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>

                @if($program->period !='0')
                    <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>
                @else
                    <td></td>
                @endif
                @if($program->program_date !='')
                    <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->program_date,10)) }}</h5></td>
                @else
                    <td></td>
                @endif
                <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            </tr>
            {{--<tr>--}}
                {{--<td ></td>--}}
                {{--<td scope="col"><h6>حرکت:</h6></td>--}}
                {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
                {{--<td scope="col"><h6> ست:</h6></td>--}}
                {{--<td scope="col"><h6> تکرار: </h6></td>--}}
                {{--<td scope="col"><h6> سیستم تمرینی :</h6></td>--}}
            {{--</tr>--}}
            </thead>
            <tbody></tbody>
        </table>
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">
            <thead>
            @foreach($exercises as $exercise)

                @if($exercise->days =='second')
                    <tr id="{{ $exercise->exercise_id}}" >
                        <td></td>
                        <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                        {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                        <td> <h6>{{ $exercise->sets }} ست</h6></td>
                        <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                        @if($exercise->exercise_systems =='basic')
                            <td>  <h6> ساده </h6></td>
                        @elseif($exercise->exercise_systems =='superSet')
                            <td>  <h6> سوپرست </h6></td>
                        @elseif($exercise->exercise_systems =='dropSet')
                            <td>  <h6> کم کردنی </h6></td>
                        @elseif($exercise->exercise_systems =='threeSet')
                            <td>  <h6> تری ست </h6></td>
                        @elseif($exercise->exercise_systems =='maximum')
                            <td>  <h6> حداکثر توان </h6></td>
                        @elseif($exercise->exercise_systems =='pyramid')
                            <td>  <h6> هرمی </h6></td>@else
                            <td>  <h6> {{ $exercise->exercise_systems }} </h6></td>
                        @endif
                        @if($exercise->description !='')
                            <td>  <h6> {{ $exercise->description }} </h6></td>@else <td></td>@endif
                    </tr>
                @endif

            @endforeach
            </thead>

        </table>


    </div>

    <div id="three" class="tabcontent" style="width: 100%" >
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td ></td>
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>

                @if($program->period !='0')
                    <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>
                @else
                    <td></td>
                @endif
                @if($program->program_date !='')
                    <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->program_date,10)) }}</h5></td>
                @else
                    <td></td>
                @endif
                <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            </tr>
            {{--<tr>--}}
                {{--<td ></td>--}}
                {{--<td scope="col"><h6>حرکت:</h6></td>--}}
                {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
                {{--<td scope="col"><h6> ست:</h6></td>--}}
                {{--<td scope="col"><h6> تکرار: </h6></td>--}}
                {{--<td scope="col"><h6> سیستم تمرینی :</h6></td>--}}
            {{--</tr>--}}
            </thead>
            <tbody></tbody>
        </table>
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">
            <thead>
            @foreach($exercises as $exercise)

                @if($exercise->days =='third')
                    <tr id="{{ $exercise->exercise_id}}" >
                        <td></td>
                        <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                        {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                        <td> <h6>{{ $exercise->sets }} ست</h6></td>
                        <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                        @if($exercise->exercise_systems =='basic')
                            <td>  <h6> ساده </h6></td>
                        @elseif($exercise->exercise_systems =='superSet')
                            <td>  <h6> سوپرست </h6></td>
                        @elseif($exercise->exercise_systems =='dropSet')
                            <td>  <h6> کم کردنی </h6></td>
                        @elseif($exercise->exercise_systems =='threeSet')
                            <td>  <h6> تری ست </h6></td>
                        @elseif($exercise->exercise_systems =='maximum')
                            <td>  <h6> حداکثر توان </h6></td>
                        @elseif($exercise->exercise_systems =='pyramid')
                            <td>  <h6> هرمی </h6></td>@else
                            <td>  <h6> {{ $exercise->exercise_systems }} </h6></td>
                        @endif
                        @if($exercise->description !='')
                            <td>  <h6> {{ $exercise->description }} </h6></td>@else <td></td>@endif

                    </tr>
                @endif

            @endforeach
            </thead>

        </table>


    </div>

    <div id="four" class="tabcontent" style="width: 100%" >
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td ></td>
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>

                @if($program->period !='0')
                    <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>
                @else
                    <td></td>
                @endif
                @if($program->program_date !='')
                    <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->program_date,10)) }}</h5></td>
                @else
                    <td></td>
                @endif
                <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            </tr>
            {{--<tr>--}}
                {{--<td ></td>--}}
                {{--<td scope="col"><h6>حرکت:</h6></td>--}}
                {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
                {{--<td scope="col"><h6> ست:</h6></td>--}}
                {{--<td scope="col"><h6> تکرار: </h6></td>--}}
                {{--<td scope="col"><h6> سیستم تمرینی :</h6></td>--}}
            {{--</tr>--}}
            </thead>
            <tbody></tbody>
        </table>
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">
            <thead>
            @foreach($exercises as $exercise)

                @if($exercise->days =='fourth')
                    <tr id="{{ $exercise->exercise_id}}" >
                        <td></td>
                        <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                        {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                        <td> <h6>{{ $exercise->sets }} ست</h6></td>
                        <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                        @if($exercise->exercise_systems =='basic')
                            <td>  <h6> ساده </h6></td>
                        @elseif($exercise->exercise_systems =='superSet')
                            <td>  <h6> سوپرست </h6></td>
                        @elseif($exercise->exercise_systems =='dropSet')
                            <td>  <h6> کم کردنی </h6></td>
                        @elseif($exercise->exercise_systems =='threeSet')
                            <td>  <h6> تری ست </h6></td>
                        @elseif($exercise->exercise_systems =='maximum')
                            <td>  <h6> حداکثر توان </h6></td>
                        @elseif($exercise->exercise_systems =='pyramid')
                            <td>  <h6> هرمی </h6></td>@else
                            <td>  <h6> {{ $exercise->exercise_systems }} </h6></td>
                        @endif
                        @if($exercise->description !='')
                            <td>  <h6> {{ $exercise->description }} </h6></td>@else <td></td>@endif
                    </tr>
                @endif

            @endforeach
            </thead>

        </table>


    </div>
    <div id="five" class="tabcontent" style="width: 100%" >
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td ></td>
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>

                @if($program->period !='0')
                    <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>
                @else
                    <td></td>
                @endif
                @if($program->program_date !='')
                    <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->program_date,10)) }}</h5></td>
                @else
                    <td></td>
                @endif
                <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            </tr>
            {{--<tr>--}}
                {{--<td ></td>--}}
                {{--<td scope="col"><h6>حرکت:</h6></td>--}}
                {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
                {{--<td scope="col"><h6> ست:</h6></td>--}}
                {{--<td scope="col"><h6> تکرار: </h6></td>--}}
                {{--<td scope="col"><h6> سیستم تمرینی :</h6></td>--}}
            {{--</tr>--}}
            </thead>
            <tbody></tbody>
        </table>
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">
            <thead>
            @foreach($exercises as $exercise)

                @if($exercise->days =='fifth')
                    <tr id="{{ $exercise->exercise_id}}" >
                        <td></td>
                        <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                        {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                        <td> <h6>{{ $exercise->sets }} ست</h6></td>
                        <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                        @if($exercise->exercise_systems =='basic')
                            <td>  <h6> ساده </h6></td>
                        @elseif($exercise->exercise_systems =='superSet')
                            <td>  <h6> سوپرست </h6></td>
                        @elseif($exercise->exercise_systems =='dropSet')
                            <td>  <h6> کم کردنی </h6></td>
                        @elseif($exercise->exercise_systems =='threeSet')
                            <td>  <h6> تری ست </h6></td>
                        @elseif($exercise->exercise_systems =='maximum')
                            <td>  <h6> حداکثر توان </h6></td>
                        @elseif($exercise->exercise_systems =='pyramid')
                            <td>  <h6> هرمی </h6></td>@else
                            <td>  <h6> {{ $exercise->exercise_systems }} </h6></td>
                        @endif
                        @if($exercise->description !='')
                            <td>  <h6> {{ $exercise->description }} </h6></td>@else <td></td>@endif

                    </tr>
                @endif

            @endforeach
            </thead>

        </table>


    </div>

    <div id="six" class="tabcontent" style="width: 100%" >
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td ></td>
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>
                @if($program->period !='0')
                    <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>
                @else
                    <td></td>
                @endif
                @if($program->program_date !='')
                    <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->program_date,10)) }}</h5></td>
                @else
                    <td></td>
                @endif
                <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            </tr>
            {{--<tr>--}}
                {{--<td ></td>--}}
                {{--<td scope="col"><h6>حرکت:</h6></td>--}}
                {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
                {{--<td scope="col"><h6> ست:</h6></td>--}}
                {{--<td scope="col"><h6> تکرار: </h6></td>--}}
                {{--<td scope="col"><h6> سیستم تمرینی :</h6></td>--}}
            {{--</tr>--}}
            </thead>
            <tbody></tbody>
        </table>
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">
            <thead>
            @foreach($exercises as $exercise)

                @if($exercise->days =='sixth')
                    <tr id="{{ $exercise->exercise_id}}" >
                        <td></td>
                        <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                        {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                        <td> <h6>{{ $exercise->sets }} ست</h6></td>
                        <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                        @if($exercise->exercise_systems =='basic')
                            <td>  <h6> ساده </h6></td>
                        @elseif($exercise->exercise_systems =='superSet')
                            <td>  <h6> سوپرست </h6></td>
                        @elseif($exercise->exercise_systems =='dropSet')
                            <td>  <h6> کم کردنی </h6></td>
                        @elseif($exercise->exercise_systems =='threeSet')
                            <td>  <h6> تری ست </h6></td>
                        @elseif($exercise->exercise_systems =='maximum')
                            <td>  <h6> حداکثر توان </h6></td>
                        @elseif($exercise->exercise_systems =='pyramid')
                            <td>  <h6> هرمی </h6></td>@else
                            <td>  <h6> {{ $exercise->exercise_systems }} </h6></td>
                        @endif
                        @if($exercise->description !='')
                            <td>  <h6> {{ $exercise->description }} </h6></td>@else <td></td>@endif

                    </tr>
                @endif

            @endforeach
            </thead>

        </table>


    </div>
    <div id="seven" class="tabcontent" style="width: 100%" >
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td ></td>
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>
                @if($program->period !='0')
                    <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>
                @else
                    <td></td>
                @endif
                @if($program->program_date !='')
                    <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->program_date,10)) }}</h5></td>
                @else
                    <td></td>
                @endif
                <td>  نوع برنامه: <h5>برنامه تمرینی</h5></td>

            </tr>
            {{--<tr>--}}
                {{--<td ></td>--}}
                {{--<td scope="col"><h6>حرکت:</h6></td>--}}
                {{--<td scope="col"><h6>روز هفته:</h6></td>--}}
                {{--<td scope="col"><h6> ست:</h6></td>--}}
                {{--<td scope="col"><h6> تکرار: </h6></td>--}}
                {{--<td scope="col"><h6> سیستم تمرینی :</h6></td>--}}
            {{--</tr>--}}
            </thead>
            <tbody></tbody>
        </table>
        <table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">
            <thead>
            @foreach($exercises as $exercise)

                @if($exercise->days =='seventh')
                    <tr id="{{ $exercise->exercise_id}}" >
                        <td></td>
                        <td scope="row">  <h6>{{ $exercise->type_of_movement }}</h6></td>
                        {{--<td>  <h6>@if($exercise->days =='first')  روز اول@endif</h6></td>--}}
                        <td> <h6>{{ $exercise->sets }} ست</h6></td>
                        <td>   <h6>{{ $exercise->repetitions }} تکرار</h6></td>
                        @if($exercise->exercise_systems =='basic')
                            <td>  <h6> ساده </h6></td>
                        @elseif($exercise->exercise_systems =='superSet')
                            <td>  <h6> سوپرست </h6></td>
                        @elseif($exercise->exercise_systems =='dropSet')
                            <td>  <h6> کم کردنی </h6></td>
                        @elseif($exercise->exercise_systems =='threeSet')
                            <td>  <h6> تری ست </h6></td>
                        @elseif($exercise->exercise_systems =='maximum')
                            <td>  <h6> حداکثر توان </h6></td>
                        @elseif($exercise->exercise_systems =='pyramid')
                            <td>  <h6> هرمی </h6></td>@else
                            <td>  <h6> {{ $exercise->exercise_systems }} </h6></td>
                        @endif
                        @if($exercise->description !='')
                            <td>  <h6> {{ $exercise->description }} </h6></td>@else <td></td>@endif
                    </tr>
                @endif

            @endforeach
            </thead>

        </table>


    </div>

    <script>
        function openCity(evt, cityName) {
            let i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>




@endsection