<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="بهترین راه برای داشتن اندامی زیبا زیر نظر با تجربه ترین مربیان بدنسازی">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>سلامتی و تندرستی با اس فیت</title>
        <link rel="icon" type="image/png" href="/images/favicon.ico" />

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="styling.css" rel="stylesheet">
        <link rel="stylesheet" href="/assets/css/main.css" />
        <!--[if lte IE 9]><link rel="stylesheet" href="/assets/css/ie9.css" /><![endif]-->
        <!--[if lte IE 8]><link rel="stylesheet" href="/assets/css/ie8.css" /><![endif]-->
        <link rel="stylesheet" href="/assets/css/rtl.css" />
        <!-- Styles -->
    </head>
    <body data-spy="scroll" data-target="#mynavBar">
    <style>
      body{direction:rtl!important;
          /*background-image: url("/images/login.jpg");  */
          width: auto;background-size: cover
      ; background-repeat: no-repeat;
      }
        @font-face {
            font-family: "Vazir";
            src: url( "../fonts/Vazir.ttf" );
            src: local( "?" ), url( "../fonts/Vazir.ttf" )format( "truetype" );
        }

        *{
            font-family:Vazir !important;
            font-size:16px;
        }
    </style>
    <!--Add video to the body element-->
       {{--<video autoplay loop muted id="bgvideo">--}}
    {{--<source src="images/nature%20video.mp4" type="video/mp4">--}}
    {{--</video>--}}

    <!--Create navigation bar with scrollspy-->
    <nav role="navigation" class="navbar navbar-custom navbar-fixed-top" id="mynavBar">

        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand">گروه نرم افزاری اس فیت</a>
                <button type="button" class="navbar-toggle" data-target="#navbarCollapse" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="navbar-collapse collapse" id="navbarCollapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ route('register') }}">ثبت نام ورزشکار</a></li>
                    <li>   <a href="/coach/register">ثبت نام مربی</a></li>
                    <li> <a href="/coach/login"> ورود مربی </a></li>
                    <li> <a href="{{ route('login') }}">ورود ورزشکار</a></li>
                    {{--<li><a href="#promise">اپلیکیشن ما</a></li>--}}
                      {{--<li><a href="#about">درباره ی ما</a></li>--}}
                    {{--<li class="active"><a href="#home">خانه</a></li>--}}
                </ul>
            </div>
        </div>
    </nav>

    <!--Create the home section with title, goal and joins us button-->
    <div class="jumbotron" id="home" style="margin-top: 10%">
        <div class="container">
            <h1 style="font-size: 300%">اس فیت،راهی نو برای سلامتی و تندرستی</h1>
            <h2>بهترین راه برای داشتن اندامی زیبا زیر نظر با تجربه ترین مربیان بدنسازی</h2>
            <div class="col-md-5 "  >
           {{--<a class="btn" href="https://cafebazaar.ir/app/sfit.ir.bodybuilding_coach/?l=fa"> دانلود نسخه ی مربی</a><br>--}}
            {{--<img class="mt-5" src="images/coachqr.png" alt=""><br>--}}
            </div>
            <div class="col-md-5 ">
           {{--<a class="btn" href="https://cafebazaar.ir/app/sfit.ir.bodybuilding_student/"> دانلود نسخه ی شاگرد</a><br>--}}
            {{--<img class="mt-5" src="images/studentqr.png" alt="">--}}
            </div>
        </div>
    </div>
    <div id="wrapper">
        <!-- Main -->
        <div id="main">

            <!-- Introduction -->
            <section id="intro" class="main" style="margin-top: 20%">
                <div class="spotlight">
                    <div class="content">
                        <header class="major">
                            <h2> نسخه ی شاگرد</h2>
                        </header>
                        <p> بدنسازی یا ورزشکاری؟

                            دوست داری بدن زیبا و متناسبی داشته باشی؟

                            دنبال مربی می گردی تا بتونی برنامه خوب بگیری و ورزش کنی؟

                            آموزش حرکات تمرینی می خوای؟

                            کالری روزانه تو می خوای حساب کنی؟

                            آهنگ های باشگاهی می خوای؟


                            همه ی این امکانات رو می تونی تو این برنامه بهش دسترسی داشته باشی! کافیه همین الان نصبش کنی. </p>
                        <ul class="statistics">
                            <li class="style1">
                                <a class="btn btn-lg  " href="/user/login">
                                    <img src="images/userlogin.png" class="aboutImage"></a>

                            </li>

                            <li class="style2">
                                <a class="btn btn-lg  "  href="http://bit.ly/sfit_student">
                                    <img src="images/bazar.png" class="aboutImage"></a>

                            </li>
                            <li class="style3"> <a class="btn btn-lg  " href="/bodybuilding/student.apk">
                                    <img src="images/direct.png" class="aboutImage"></a>

                            </li>
                            <li class="style4">
                                <a class="btn btn-lg  " href="http://bit.ly/sfit-student">
                                    <img src="images/googleplay.png" class="aboutImage"></a>
                            </li>
                            </li>
                            <li class="style5">
                                <a class="btn btn-lg  " href="/home">
                                    <img src="images/guest.png" class="aboutImage"></a>
                            </li>
                        </ul>
                        {{--<ul class="actions">--}}
                        {{--<li> <a class="btn btn-lg btn-danger" href="http://bit.ly/sfit-student"> دانلود از گوگل پلی نسخه ی شاگرد</a><br></li>یا--}}
                        {{--<li> <a class="btn btn-lg btn-danger" href="http://bit.ly/sfit-student"> دانلود با لینک مستقیم</a><br></li>یا--}}
                        {{--<li> <a class="btn btn-lg btn-danger" href="http://bit.ly/sfit-student"> دانلود از بازار نسخه ی شاگرد</a><br></li>یا--}}
                        {{--<li> <a class="btn btn-lg btn-danger" href="http://bit.ly/sfit-student"> دانلود از مایکت نسخه ی شاگرد</a><br></li>یا--}}
                        {{--<li> <a class="btn btn-lg btn-danger" href="/user/login"> ورود شاگرد</a><br></li>یا--}}
                        {{--<li class="mt-5"> <a class="btn btn-lg btn-danger mt-5 " href="/home"> ورود به عنوان مهمان</a><br></li>--}}
                        {{--</ul>--}}
                    </div>
                    {{--<span class="image"><img src="images/sfit.jpg" alt="" /></span>--}}
                </div>
                <div class="spotlight">
                    <div class="content">
                        <header class="major">
                            <h2> نسخه ی مربی</h2>
                        </header>
                        <p> مربی ورزشی هستید؟

                            می خواهید  کسب درامد بیشتری داشته باشید؟

                            می خواهید برای شاگرداتون به راحتی برنامه بفرستید؟
                            پس وقتو تلف نکنید و برنامه رو همین الان نصب کنید!

                            این برنامه مخصوص مربیان ورزشی طراحی و ساخته شده است که در تعامل با برنامه ی "(نسخه شاگرد) Sfit" می باشد.

                            شما مربیان عزیز بعد از ثبت نام در برنامه و ارسال مدرک بدنسازی خود، حساب کاربری شما فعال شده و به لیست مربیان ما افزوده خواهید شد و بعد از آن ورزشکاران می توانند شما را به عنوان مربی خود انتخاب کرده و از شما درخواست برنامه تمرینی، غذایی و مکملی کنند و سپس شما می توانید برای آن ها برنامه بفرستید.

                             </p>
                        <ul class="statistics">
                            <li class="style1"> <a class="btn btn-lg  " href="/coach/login">
                                    <img src="images/coachlogin.png" class="aboutImage"></a>

                            </li>
                            <li class="style2"> <a class="btn btn-lg  "  href="http://bit.ly/sfit_coach">
                                    <img src="images/bazar.png" class="aboutImage"></a>

                            </li>
                            <li class="style3"> <a class="btn btn-lg  " href="/bodybuilding/coach.apk">
                                    <img src="images/direct.png" class="aboutImage"></a>

                            </li>
                                 <li class="style4">
                                     <a class="btn btn-lg  " href="http://bit.ly/sfit-coach">
                                         <img src="images/googleplay.png" class="aboutImage"></a>
                            </li>
                            <li class="style5">
                                <a class="btn btn-lg  " href="https://myket.ir/app/sfit.ir.bodybuilding_coach?lang=fa">
                                    <img src="images/myket.png" class="aboutImage"></a>
                            </li>


                        </ul>
                        {{--https://myket.ir/app/sfit.ir.bodybuilding_coach?lang=fa--}}
                        {{--<span class="image"><img src="images/sfit.jpg"   alt="" /></span>--}}
                        {{--<ul class="actions">--}}
                            {{--<li> <a class="btn btn-lg btn-success" href="http://bit.ly/sfit-coach"><img style="width: 10%;height: 10%" src="images/sfit.jpg"   alt="" />  دانلود از گوگل پلی نسخه ی مربی</a><br></li> یا--}}
                            {{--<li> <a class="btn btn-lg btn-success" href="http://bit.ly/sfit-coach"> دانلود با لینک مستقیم</a><br></li> یا--}}
                            {{--<li> <a class="btn btn-lg btn-success" href="http://bit.ly/sfit-coach"> دانلود از بازار نسخه ی مربی</a><br></li> یا--}}
                            {{--<li> <a class="btn btn-lg btn-success" href="http://bit.ly/sfit-coach"> دانلود از مایکت نسخه ی مربی</a><br></li> یا--}}
                            {{--<li> <a class="btn btn-lg btn-success" href="/coach/login"> ورود مربی</a><br></li>--}}
                        {{--</ul>--}}
                    </div>

                </div>

            </section>
            {{--<a class="btn btn-lg btn-danger " style="margin-right: 10%" href="/home">ورود به عنوان کاربر مهمان</a>--}}
            <!-- First Section -->
            <section id="first" class="main special">
                <header class="major">
                    <h2>امکانات ما چه چیز هایی هستند؟</h2>
                </header>
                <ul class="features">
                    <li>
                        <div class="col-md-12">
                            <img src="images/exercise.png" class="aboutImage">
                            <h2>برنامه تمرینی</h2>
                            <p>این امکان منحصر به فرد ،برنامه دهی سریع و مطمئن را فراهم میسازد</p>
                        </div>


                    </li>
                    <li>
                        <div class="col-md-12">
                            <img src="images/diet.png" class="aboutImage">
                            <h2>برنامه غذایی/رژیمی</h2>
                            <p>در این سایت میتوان انواع برنامه های رژیمی و تغذیه را جستجو و تهیه کرد.</p>
                        </div>
                    </li>
                    <li>
                        <div class="col-md-12">
                            <img src="images/supplement.png" class="aboutImage">
                            <h2>برنامه مکملی</h2>
                            <p>دیگر نگران میزان و زمان مصرف مکمل های خود نباشید.</p>
                        </div>
                    </li>
                </ul>
                {{--<footer class="major">--}}
                    {{--<ul class="actions">--}}
                        {{--<li><a href="generic.html" class="button">کسب اطلاعات بیشتر</a></li>--}}
                    {{--</ul>--}}
                {{--</footer>--}}
            </section>

            <!-- Second Section -->
            <section id="second" class="main special">
                <header class="major">
                    <h2>چگونه از امکانات عالی ما استفاده کنید؟</h2>
                    <p>راه های بسیار خوبی برای انتخاب ما وجود دارد<br />
                        .جهت دانلود اپلیکیشن ها و ارتباط با ما می توانید از طریق نرم افزار بارکد خوان عکس های زیر را اسکن کنید</p>
                    <p>جهت اسکن کدهای زیر می توانید از این نرم افزار استفاده کنید</p>
                    <a class="btn btn-lg btn-danger" href="/bodybuilding/QR.apk"> دانلود نرم افزار اسکن QR</a>
                </header>
                <ul class="statistics">
                    <li class="style1">
                        <img src="images/qr-code.svg" class="aboutImage">
                    </li>
                    <li class="style2">
                        <img src="images/qr-code2.svg" class="aboutImage">
                    </li>
                    <li class="style3">
                        <img src="images/qr-code3.svg" class="aboutImage">
                    </li>
                    <li class="style4">
                        <img src="images/qr-code4.svg" class="aboutImage">
                    </li>
                    <li class="style5">
                        <img src="images/qr-code5.svg" class="aboutImage">
                    </li>
                </ul>
                {{--<p class="content">کمپانی اپل در سال ۱۹۷۱ با دوستی استیو وزنیاک ۲۱ ساله مهندس کامپیوتر و استیو جابز ۱۶ ساله متولد شد به طوری که با گذشت شش سال از آشنایی این دو نفر در سال ۱۹۷۷ این کمپانی با معرفی کامپیوتر شخصی Apple I که در گاراژ خانه جابز به صورت دستی ساخته شده بود رسماً با نام تجاری Apple Computer Inc به بازار تکنولوژی وارد شد و توانست ظرف مدت کوتاهی با فروش تعدادی از این مدل کامپیوتر شخصی، اعتباری برای خود دست و پا کند. بلافاصله در سال ۱۹۷۷ اپل نوع دیگری از کامپیوتر شخصی یعنی Apple II را وارد بازار کرد که به دلیل مجهز بودنش به فلاپی درایو ۵٫۲۵ اینچی از رقبای دیگر خود در آن دوره نظیر Commodore که از نوار مغناطیسی برای ذخیره‌سازی اطلاعات استفاده می‌کرد، پیشی گرفت.</p>--}}
                {{--<footer class="major">--}}
                    {{--<ul class="actions">--}}
                        {{--<li><a href="generic.html" class="button">هنوز قانع نشده اید؟ روی اینجا کلیک کنید</a></li>--}}
                    {{--</ul>--}}
                {{--</footer>--}}
            </section>

            <!-- Get Started -->
            <section id="cta" class="main special">
                <header class="major">
                    <h2>از کجا شروع کنم؟</h2>
                    {{--<p>فعالیت‌های این گروه، عمدتاً بر ارائه خدمات ورزشی ،سلامت و بهداشت میباشد.<br />--}}
                        هیچوقت برای داشتن اندامی ایده آل دیر نیست!
                        همین حالا ثبت نام کنید و با قدرت شروع کنید💪🏻</p>
                </header>
                <footer class="major">
                    <ul class="actions">
                        <li><a href="/user/login" class="button special">برای ورود کلیک کنید</a></li>
                        <li><a href="/user/register" class="button">درخواست مشاوره</a></li>
                    </ul>
                </footer>
            </section>
            {{--<a style="visibility:hidden;" href="https://mrcode.ir" target="_blank">آموزش طراحی سایت</a>--}}
        </div>

        <!-- Footer -->

    </div>

    <!-- Scripts -->

    <!--Create about section showing course details using grid system-->
    {{--<div id="about">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-4">--}}
                    {{--<img src="images/exercise.png" class="aboutImage">--}}
                    {{--<h2>برنامه تمرینی</h2>--}}
                    {{--<p>این امکان منحصر به فرد ،برنامه دهی سریع و مطمئن را فراهم میسازد</p>--}}
                {{--</div>--}}
                {{--<div class="col-md-4">--}}
                    {{--<img src="images/diet.png" class="aboutImage">--}}
                    {{--<h2>برنامه غذایی/رژیمی</h2>--}}
                    {{--<p>در این سایت میتوان انواع برنامه های رژیمی و تغذیه را جستجو و تهیه کرد.</p>--}}
                {{--</div>--}}
                {{--<div class="col-md-4">--}}
                    {{--<img src="images/supplement.png" class="aboutImage">--}}
                    {{--<h2>برنامه مکملی</h2>--}}
                    {{--<p>دیگر نگران میزان و زمان مصرف مکمل های خود نباشید.</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <!--Create Carousel-->
    {{--<div id="promise" class="container-fluid">--}}
        {{--<div class="carousel slide" id="myCarousel" data-ride="carousel">--}}
            {{--<ol class="carousel-indicators">--}}
                {{--<li data-target="#myCarousel" data-slide-to="0" class="active">--}}
                {{--</li>--}}
                {{--<li data-target="#myCarousel" data-slide-to="1">--}}
                {{--</li>--}}
                {{--<li data-target="#myCarousel" data-slide-to="2">--}}
                {{--</li>--}}
                {{--<li data-target="#myCarousel" data-slide-to="3">--}}
                {{--</li>--}}
                {{--<li data-target="#myCarousel" data-slide-to="4">--}}
                {{--</li>--}}
                {{--<li data-target="#myCarousel" data-slide-to="5">--}}
                {{--</li>--}}
                {{--<li data-target="#myCarousel" data-slide-to="6">--}}
                {{--</li>--}}
            {{--</ol>--}}
            {{--<div class="carousel-inner">--}}
                {{--<div class="item active">--}}
                    {{--<img src="images/screenshot/1.png">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<h2>Crystal Clear and Engaging 100% Hands-on Lectures.</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<img src="images/screenshot/2.png">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<h2>Full curriculum including all major Web Technologies: Front-End, Back end, Mobile Apps and API's.</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<img src="images/screenshot/3.png">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<h2>Start from Zero-Skills! No prior Knowledge Required!</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<img src="images/screenshot/4.png">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<h2>Build over 15 real professional Websites, Games and Mobile apps.</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<img src="images/screenshot/5.png">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<h2>Start Making money from week 1 and Earn over 30000$ by the time you finish the Course.</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<img src="images/skills.jpg">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<h2>Enhance your career and employability skills.</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="item">--}}
                    {{--<img src="images/time.jpg">--}}
                    {{--<div class="carousel-caption">--}}
                        {{--<h2>Lifetime access to all course material.</h2>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<a class="left carousel-control" href="#myCarousel" data-slide="prev">--}}
                {{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
                {{--<img style="background: aliceblue;margin-top: 150%;width: 25%;height: auto" src="/images/left.png" alt="">--}}

                {{--<span class="sr-only">Previous</span>--}}
            {{--</a>--}}
            {{--<a class="right carousel-control" href="#myCarousel" data-slide="next">--}}
                {{--<img style="background: aliceblue;margin-top: 150%;width: 25%;height: auto" src="/images/right.png" alt="">--}}
                {{--<span class="sr-only">Previous</span>--}}
            {{--</a>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!--Add a footer for our website-->
    <footer id="footer">
        {{--<section>--}}
            {{--<h2>درباره اس فیت</h2>--}}
            {{--<p>این دو کارشناس به دنبال محصولات جدید و کارا بودند و می‌خواستند کالاهای مصرفی کاملاً نوینی روانه بازار کنند. برای این کار تکنولوژی مورد نیازشان را خود ابداع کردند. این ایده بعدها یکی از اصولی بود که آنها و همین طور مدیران سال‌های اخیر سونی، از آن پیروی می‌کردند، شرکتی که همواره خواسته همه ...</p>--}}
            {{--<ul class="actions">--}}
                {{--<li><a href="generic.html" class="button">کسب اطلاعات بیشتر</a></li>--}}
            {{--</ul>--}}
        {{--</section>--}}
        <section>
            <h2>ارتباط با ما</h2>
            {{--<dl class="alt">--}}
                {{--<dt>آدرس</dt>--}}
                {{--<dd>بزرگراه اینترنت، خروجی اینترنت شرق، خیابان پهن باند، کوچه سایت شما، پلاک پروتکل امن</dd>--}}
                {{--<dt>تلفن</dt>--}}
                {{--<dd>(000) 000-0000 x 0000</dd>--}}
                {{--<dt>ایمیل</dt>--}}
                {{--<dd><a href="#">information@untitled.tld</a></dd>--}}
            {{--</dl>--}}
            <ul class="icons">
                {{--<li><a href="#" class="icon fa-twitter alt"><span class="label">Twitter</span></a></li>--}}
                {{--<li><a href="#" class="icon fa-facebook alt"><span class="label">Facebook</span></a></li>--}}
                <li><a href="http://bit.ly/sfit-instagram" ><img src="/images/instagram.png" style="width: 10%" alt=""></a>
               <a href="http://bit.ly/sfit-telegram" ><img src="/images/telegram.png" style="width: 10.5%" alt=""></a></li>
                {{--<li><a href="#" class="icon fa-dribbble alt"><span class="label">Dribbble</span></a></li>--}}
            </ul>
        </section>
        <p class="copyright">&copy; کلیه حقوق برای این سایت محفوظ است.</p>
    </footer>
    {{--<div class="footer">--}}
        {{--<div class="container">--}}
            {{--<p>&copy;sfit</p>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script>
        $(function(){
            $('.carousel').carousel({
                interval: 10000
            });
        });
    </script>
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/jquery.scrollex.min.js"></script>
    <script src="/assets/js/jquery.scrolly.min.js"></script>
    <script src="/assets/js/skel.min.js"></script>
    <script src="/assets/js/util.js"></script>
    <!--[if lte IE 8]><script src="/assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="/assets/js/main.js"></script>
    <script src="https://static.pushe.co/pusheweb.js"></script>
    <script>
        Pushe.init("5dn6473xqok05lle");
        Pushe.subscribe();
    </script>
    <script type="text/javascript"> (function(){ var now = new Date(); var version = now.getFullYear().toString() + "0" + now.getMonth() + "0" + now.getDate() + "0" + now.getHours(); var head = document.getElementsByTagName("head")[0]; var link = document.createElement("link"); link.rel = "stylesheet"; link.href = "https://app.najva.com/static/css/local-messaging.css" + "?v=" + version; head.appendChild(link); var script = document.createElement("script"); script.type = "text/javascript"; script.async = true; script.src = "https://app.najva.com/static/js/scripts/sfit-website-7601-7cbe67c5-8339-4eed-95e4-e71bbe007758.js" + "?v=" + version; head.appendChild(script); })() </script>
    </body>

</html>
