<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>

    <link rel="stylesheet" href="{{url('css/login.css')}}">

</head>

<body>
<div class="body"></div>
<div class="grad"></div>
<div class="header">
    <div>SRTTU<span>sharing</span></div>
</div>
<br>
<div class="login" style="font-size: 20px;">
    @if (count($errors))
            @foreach($errors->all() as $error)
                <span style="direction: rtl; margin: 5px; text-align: right;">{{ $error }}</span>
            @endforeach
    @endif
    <form action="/login" method="post" style="direction: ltr;">
        {{csrf_field()}}
        <input type="text" placeholder="ایمیل" name="user_email"><br>
        <input type="password" placeholder="رمز عبور" name="user_password"><br>
        <input type="submit" value="ورود">
    </form>
</div>

</body>
</html>
