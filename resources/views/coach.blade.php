@extends('layouts.app')

@section('content')
    <a href="coach/logout" role="button" class="btn btn-danger" style="margin-left:20%">خروج</a>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Coach Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in,Coach!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
