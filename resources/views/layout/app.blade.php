<!doctype html>
<html>
<head>
    @include('includes.head')

</head>
<body>
<div class="container">

    <header class="row">
        @include('includes.header')
    </header>

    @include('includes.sidebar')
    <div id="main" class="content">
        @if(Session::has('message'))
            <section id="pop" class="content-header clearfix">
                <div class="success" style="padding: 15px; margin-bottom: 20px; border: 1px solid transparent; border-top-color: transparent; border-right-color: transparent;
                border-bottom-color: transparent; border-left-color: transparent; border-radius: 4px; color: #3c763d; background-color: #dff0d8; border-color: #d6e9c6;">
                    <ul style="list-style: none;">
                        <span style="cursor: pointer; float: right; font-size: 21px; font-weight: 700; line-height: 1; color: #000; text-shadow: 0 1px 0 #fff; filter: alpha(opacity=20); opacity: .2;" onclick="closePopUp()">×</span>
                        <li>{{ Session::get('message') }}</li>
                    </ul>

                </div>
            </section>
        @endif
        @yield('content')

    </div>

    <footer class="row">
        {{--@include('includes.footer')--}}
    </footer>

</div>
@include('includes.scripts')
<script>
    function closePopUp() {
        $("#pop").hide();
    }
</script>
</body>
</html>