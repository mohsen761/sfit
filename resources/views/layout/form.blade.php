@extends('layout.app')

@push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('css/form.css')}}">
@endpush

@section('content')

    {!! form($form) !!}
@endsection