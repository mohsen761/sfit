@extends('layout.app')

@section('content')
    @if(isset($filterForm))
    {!! $filterForm !!}
    @endif
    {!! $html->table([],true,false) !!}
    <span class="smallButton" style="float: left;margin:5px;cursor: pointer" onclick="exportExcel('{{$excelOut}}')">خروجی اکسل</span>


    @push('styles')
    <link rel="stylesheet" type="text/css" href="{{url('css/datatable.css')}}">
    @endpush
    @push('scripts')
    <script>
        function exportExcel(route) {
            var data = window.LaravelDataTables.dataTableBuilder.ajax.params();
            var x = JSON.stringify(data, null, 4);
//            console.log($.param(data   ));
            downloadURI(route+"?"+$.param(data));
//            $(this).attr("href", this.href + "?" + $.param(data) + '&o=csv');
        }
        function downloadURI(uri, name)
        {
            var link = document.createElement("a");
//            link.download = name;
            link.href = uri;
            link.click();
        }
        $(document).ready(function() {

                var table = window.LaravelDataTables.dataTableBuilder;
                @if(!isset($noFilter))
                // Setup - add a text input to each footer cell
                $('#dataTableBuilder tfoot th').each(function (count) {
                    var title = $(this).text();
                    console.log(this,table.column(count).dataSrc());
                    if (table.column(count).dataSrc()!= 'actions') {
                        $(this).html('<input class="newInput" type="text" style="font-family: shabnam; float: right;" placeholder="جستجو ' + title + '" />');
                    }
                });

                // DataTable
//                console.log(table);
                // Apply the search
                table.columns().every(function () {
                    var column = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (column.search() !== this.value) {
                            column
                                .search(this.value)
                                .draw();
                        }
                    });
                });
                @endif
        } );
    </script>
    @endpush
    {!! $html->scripts() !!}



@endsection