@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">ثبت نام مربی</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="user_name" class="col-md-4 col-form-label text-md-right">{{ __('نام') }}</label>

                            <div class="col-md-6">
                                <input id="coach_name" type="text" class="form-control{{ $errors->has('coach_name') ? ' is-invalid' : '' }}" name="coach_name" value="{{ old('coach_name') }}" required autofocus>

                                @if ($errors->has('coach_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('coach_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="coach_email" class="col-md-4 col-form-label text-md-right">{{ __('ایمیل') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('coach_email') ? ' is-invalid' : '' }}" name="coach_email" value="{{ old('coach_email') }}" required>

                                @if ($errors->has('coach_email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('coach_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="user_password" class="col-md-4 col-form-label text-md-right">{{ __('رمز عبور') }}</label>

                            <div class="col-md-6">
                                <input id="coach_password" type="password" class="form-control{{ $errors->has('coach_password') ? ' is-invalid' : '' }}" name="coach_password" required>

                                @if ($errors->has('coach_password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('coach_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="coach_password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('تکرار پسورد') }}</label>

                            <div class="col-md-6">
                                <input id="coach_password-confirm" type="password" class="form-control" name="coach_password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
