@extends('user.test')

@section('content')


    <title>برنامه های رایگان</title>

    <article class="col-md-9 col-sm-9 main-content" role="main"style=";text-align: right;margin-top: 5%">


            @foreach($programs as $program)
                @if($program->program_type == 'exercise')
                    <div class="col" style="padding: 2%;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;;text-align: right ;direction: rtl;">
                      <a class="btn" href="/user/freeprograms/exercise/{{ $program->program_id }}">
                           <p>{{ $program->title }}</p>
                      </a>
                    </div>
                @endif
            @endforeach
            {{--<div>{{ str_limit($post->post_description, $limit = 200, $end = '...')}}</div>--}}

                @foreach($programs as $program)
                    @if($program->program_type == 'food')
                        <div class="col" style="padding: 2%;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;;text-align: right ;direction: rtl;">
                            <a class="btn" href="/user/freeprograms/food/{{ $program->program_id }}">
                                <p>{{ $program->title }}</p>
                            </a>
                        </div>
                    @endif
                @endforeach

                @foreach($programs as $program)
                    @if($program->program_type == 'supplement')
                        <div class="col" style="padding: 2%;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;;text-align: right ;direction: rtl;">
                            <a class="btn" href="/user/freeprograms/supplement/{{ $program->program_id }}">
                                <p>{{ $program->title }}</p>
                            </a>
                        </div>
                    @endif
                @endforeach



    </article>

@endsection