@extends('user.test')

@section('content')
<title>تغییر رمز</title>


<div style="background-color: #fffacc;box-shadow: 10px 10px 10px #888888;text-align: center;margin-top: 7%;padding: 17%">
    <form  id="form-change-password" role="form" method="POST" action="/user/changepassword" novalidate class="form-horizontal">
        <div class="col-md-12">

            <div class="col-xl-12">
                <div class="form-group">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="password" class="form-control" id="current-password" name="current-password" placeholder="رمز قبلی">
                </div>
            </div>



            <div class="col-xl-12">
                <div class="form-group">
                    <input type="password" class="form-control" id="password" name="password" placeholder="رمز جدید را وارد کنید">
                </div>
            </div>

            <div class="col-xl-12">
                <div class="form-group">
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="رمز جدید را دوباره وارد کنید">
                </div>
            </div>

        </div>
        <div class="form-group" style="margin-right: 31%;">
            <div class="col-sm-offset-5 col-sm-6">
                <button type="submit" class="btn btn-outline-danger">
                 اعمال تغییرات
                </button>
            </div>
        </div>
        @if(session('success'))
            <div class="alert alert-success alert-dark">

                {{ session('success') }}
            </div>
        @endif
        @if(session('current-password'))
            <div class="alert alert-danger alert-dark">

                {{ session('current-password') }}
            </div>
        @endif
        @if(session('new-password'))
            <div class="alert alert-danger alert-dark">

                {{ session('new-password') }}
            </div>
        @endif
    </form>

</div>
@endsection