@extends('user.test')

@section('content')

    <title>برنامه های غذایی</title>
    {{--<a href="/coach/myorders" class="btn btn-primary" style="width: fit-content"> بازگشت به عقب</a>--}}

        <table class=" table table-striped table-responsive-sm table-bordered table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

            <thead>
            <tr style="">
                <td> عنوان برنامه:  <h5>{{ $program->title }}</h5> </td>

                <td> توضیحات برنامه : <h5>{{ $program->description }}</h5></td>

                <td> طول برنامه: <h5>{{ $program->period }}روز</h5>  </td>

                <td>  تاریخ برنامه: <h5>{{ str_replace("...","",str_limit($program->created_at,10)) }}</h5></td>
            </tr>
            <tr>
                <td scope="col"><h6>نام مکمل:</h6></td>
                <td scope="col"><h6>زمان مصرف:</h6></td>
                <td scope="col"><h6>مقدار:</h6></td>
                <td scope="col"><h6>روز مصرف:</h6></td>

            </tr>
            </thead>
            <tbody>
            @foreach($supplements as $supplement)
                    @if($program->program_id == $supplement->program_id)
                        <tr>
                            <td scope="row">  <h6>{{ $supplement->supplement_name }}</h6></td>
                            <td> <h6>{{ $supplement->time_to_eat }} </h6></td>
                            <td>   <h6>{{ $supplement->dosage }} </h6></td>
                            <td>   <h6>{{ $supplement->day_to_eat }} </h6></td>
                        </tr>
                    @endif

                @endforeach
            </tbody>

        </table>

@endsection