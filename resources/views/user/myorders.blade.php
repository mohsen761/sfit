@extends('user.test')

@section('content')
<title>سفارشات من</title>

@if(session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif
<table class=" table table-striped table-responsive-sm  table-hover table-active table-dark " style=" box-shadow: 10px 10px 10px #888888;text-align: right;;;margin: 2%;">

    <thead>
    <tr style="">
        <td> برنامه تمرینی </td>
        <td> برنامه غذایی </td>
        <td> برنامه مکملی </td>
        <td> مبلغ </td>
        <td> وضعیت پرداخت </td>
        <td> عملیات </td>

    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
            <tr>

                    @if($order->exercise =='yes') <td>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt=""></td>@else <td> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt=""></td>@endif
                    @if($order->food =='yes') <td>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt=""></td>@else <td> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt=""></td>@endif
                    @if($order->supplement =='yes') <td>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt=""></td>@else <td> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt=""></td>@endif
{{--<td>{{ $order->order_date }}</td>--}}
<td>{{ $order->price }}</td>
                        @if($order->user_payment_state =='paid') <td>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt=""></td><td>پرداخت شده</td>@elseif($order->token == null) <td> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt=""></td>   <td>
                            <form action="http://sfit.ir/api/bodybuilding/student/pay" method="POST">  <input type="hidden" name="order_id" value="{{ $order->order_id }}"><input
                                        type="hidden" name="price" value="{{ $order->price }}"> <button type="submit" class="btn btn-outline-primary ">پرداخت</button></form></td>@else<td>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt=""></td><td>پرداخت ناموفق</td> @endif

            </tr>
    @endforeach
    </tbody>

</table>

@endsection