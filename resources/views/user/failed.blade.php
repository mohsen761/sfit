@extends('user.test')

@section('content')


    <title>سفارش ناموفق</title>

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="alert alert-danger" style="text-align: right">
        <h1>پرداخت شما ناموفق بوده است .لطفا بار دیگر درخواست خود را ایجاد نمایید.</h1>
    </div>


@endsection