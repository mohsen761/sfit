@extends('user.test')

@section('content')

    <title>عکس های برنامه مکملی</title>
    {{--<a href="/coach/myorders" class="btn btn-primary" style="width: fit-content"> بازگشت به عقب</a>--}}

    {{--bodybuilding/coach/images/programs/supplement/$id--}}
    {{--<img src="/bodybuilding/coach/images/programs/supplement/{{ $id }}/0.png" alt="">--}}
    <style>
        div.gallery {
            margin: 5px;
            border: 1px solid #ccc;
            float: left;
            width: 100%;
        }

        div.gallery:hover {
            border: 1px solid #777;
        }

        div.gallery img {
            width: 100%;
            height: auto;
        }

        div.desc {
            padding: 15px;
            text-align: center;
        }
    </style>

    <div class="w3-content w3-display-container">
        <img style="width: 100%;" class="mySlides"  src="/bodybuilding/coach/program/images/supplement/{{ $id }}/1.png" alt="">
        <img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/program/images/supplement/{{ $id }}/2.png" alt="">
        <img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/program/images/supplement/{{ $id }}/3.png" alt="">
        <img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/program/images/supplement/{{ $id }}/4.png" alt="">
        <img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/program/images/supplement/{{ $id }}/5.png" alt="">
        <img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/program/images/supplement/{{ $id }}/6.png" alt="">
        <img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/program/images/supplement/{{ $id }}/7.png" alt="">
        <img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/program/images/supplement/{{ $id }}/8.png" alt="">
        <img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/program/images/supplement/{{ $id }}/9.png" alt="">
        <img style="width: 100%;" class="mySlides" src="/bodybuilding/coach/program/images/supplement/{{ $id }}/10.png" alt="">

        <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10094;</button>
        <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10095;</button>



    </div>
    <script>
        var slideIndex = 1;
        showDivs(slideIndex);

        function plusDivs(n) {
            showDivs(slideIndex += n);
        }

        function showDivs(n) {
            var i;
            var x = document.getElementsByClassName("mySlides");
            if (n > x.length) {slideIndex = 1}
            if (n < 1) {slideIndex = x.length}
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            x[slideIndex-1].style.display = "block";
        }
    </script>
    {{--<div class="gallery">--}}
        {{--<a target="_blank" href="/bodybuilding/coach/images/programs/supplement/{{ $id }}/0.png">--}}
            {{--<img src="/bodybuilding/coach/images/programs/supplement/{{ $id }}/0.png"   >--}}
        {{--</a>--}}
    {{--</div>--}}

  {{--<div class="gallery">--}}
        {{--<a target="_blank" href="/bodybuilding/coach/images/programs/supplement/{{ $id }}/1.png">--}}
            {{--<img src="/bodybuilding/coach/images/programs/supplement/{{ $id }}/1.png"   >--}}
        {{--</a>--}}
    {{--</div>--}}

  {{--<div class="gallery">--}}
        {{--<a target="_blank" href="/bodybuilding/coach/images/programs/supplement/{{ $id }}/2.png">--}}
            {{--<img src="/bodybuilding/coach/images/programs/supplement/{{ $id }}/2.png"   >--}}
        {{--</a>--}}
    {{--</div>--}}

  {{--<div class="gallery">--}}
        {{--<a target="_blank" href="/bodybuilding/coach/images/programs/supplement/{{ $id }}/3.png">--}}
            {{--<img src="/bodybuilding/coach/images/programs/supplement/{{ $id }}/3.png"   >--}}
        {{--</a>--}}
    {{--</div>--}}

  {{--<div class="gallery">--}}
        {{--<a target="_blank" href="/bodybuilding/coach/images/programs/supplement/{{ $id }}/4.png">--}}
            {{--<img src="/bodybuilding/coach/images/programs/supplement/{{ $id }}/4.png"   >--}}
        {{--</a>--}}
    {{--</div>--}}

  {{--<div class="gallery">--}}
        {{--<a target="_blank" href="/bodybuilding/coach/images/programs/supplement/{{ $id }}/5.png">--}}
            {{--<img src="/bodybuilding/coach/images/programs/supplement/{{ $id }}/5.png"   >--}}
        {{--</a>--}}
    {{--</div>--}}

  {{--<div class="gallery">--}}
        {{--<a target="_blank" href="/bodybuilding/coach/images/programs/supplement/{{ $id }}/6.png">--}}
            {{--<img src="/bodybuilding/coach/images/programs/supplement/{{ $id }}/6.png"   >--}}
        {{--</a>--}}
    {{--</div>--}}

  {{--<div class="gallery">--}}
        {{--<a target="_blank" href="/bodybuilding/coach/images/programs/supplement/{{ $id }}/7.png">--}}
            {{--<img src="/bodybuilding/coach/images/programs/supplement/{{ $id }}/7.png"   >--}}
        {{--</a>--}}
    {{--</div>--}}



@endsection