@extends('user.test')

@section('content')
    <title>برنامه های من</title>
    <div style="margin-right: 10%;margin-top: 1%;">
    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @if(count($orders) ==0 )
        <div style="width: fit-content;text-align: right" class="alert alert-danger">
           برای شما هنوز هیچ برنامه ای ارسال نشده است.
        </div>
    @endif
    @foreach ($orders as $order)
        <div class="ml-5" style="margin-right: -5%;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;border-width: 2px;border-style: groove;border-color: #3f9ae5;padding: 30px">
            {{--<a  class="mt-5" href="/coach/myorders/{{ $order->order_id }}">--}}
                <article class="post ">
                    <div class="post-content ">
                        <div class="col" style="text-align: right">
                            @if($order->exercise =='yes' && $order->program_state == 'sent')<a href="/user/exercise/{{ $order->order_id }}">   مشاهده ی برنامه تمرینی  </a>@endif
                                <hr>
                            @if($order->food =='yes' && $order->program_state == 'sent')<a href="/user/food/{{ $order->order_id }}">   مشاهده ی برنامه غذایی</a> @endif
                                <hr>
                            @if($order->supplement =='yes' && $order->program_state == 'sent')<a href="/user/supplement/{{ $order->order_id }}">   مشاهده ی برنامه مکملی </a>@endif
                                <hr>
                        </div>
                        <div class="row">
                            <p>  {{ $order->price }} تومان  </p>
                        </div>
                        <div class="row">
                            <p>تاریخ: {{ str_replace("_","/", $order->order_date) }}</p>
                        </div>


                    </div>
                </article>
            {{--</a>--}}
            <div class="row mt-5">
                @if($order->program_state == 'sent') <p>برنامه باموفقیت  از طرف مربی ارسال شده است</p> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt="">
                @else
                    <p>هنوز هیچ برنامه ای برای شما ارسال نشده است</p>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt="">
                @endif
            </div>
            <div class="row">
                @if($order->user_payment_state == 'paid') <p>هزینه ی برنامه پرداخت شده است.</p> <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20170510/418329c59c.png" alt="">
                <br>
                @else
                    <p>شما هنوز هزینه ی آن را پرداخت نکرده اید.</p>  <img style="margin-left: 8px;width: 40px;height: 40px" src="https://png.pngtree.com/svg/20161227/c10042249c.svg" alt="">
                    <br>
                @endif
            </div>
        </div>
        <br/>
    @endforeach
    </div>
@endsection
