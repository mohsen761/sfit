@extends('user.test')

@section('content')


    <title>حرکات تمرینی</title>

    <article class="col-md-9 col-sm-9 main-content" role="main"style=";text-align: right;margin-top: 5%">
        <a href="/user/home" class="btn btn-primary"> بازگشت به عقب</a>
        <div class="container col">
            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            @foreach($movements as $item)
                {{--<title> {{ $item->movement_name }}</title>--}}

                <div class="col" style="padding: 2%;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;;text-align: right ;direction: rtl;">
                    <a class="btn" href="/user/movements/{{ $item->movement_id  }}">
                    <div >{{ $item->movement_name }} <br></div>
                    <div>
                        <img style="width: 40%;" class="rounded img-fluid" src="{{ $item->movement_picture_link }}" alt="">
                    </div>
                    <br>
                    <div>{{ str_limit($item->movement_description, $limit = 200, $end = '...')}}</div>
                    </a>
                    {{--<div>{{ str_limit($post->post_description, $limit = 200, $end = '...')}}</div>--}}
                </div>  <br> <br>

            @endforeach

                {{ $movements->links() }}

        </div>

        {{--{{ $orders->links() }}--}}



    </article>

@endsection