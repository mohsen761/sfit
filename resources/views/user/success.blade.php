@extends('user.test')

@section('content')



    <title>سفارش موفق</title>

    @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="alert alert-success" style="text-align: right">
        <h1>پرداخت شما با موفقیت صورت گرفت.</h1>
    </div>

@endsection