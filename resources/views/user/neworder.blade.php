@extends('user.test')

@section('content')
    <title>درخواست برنامه</title>
    @if(session('success'))
        <div style="float: right;text-align: right;" class="alert alert-success alert-dark ">
            {{ session('success') }}
        </div><br>
    @endif
<div style="text-align: right">

    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.f1') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class=" panel-heading"><h5>ازینجا عکس فیگور جفت بازو از جلو خود را آپلود کنید</h5></div>
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="image" class="form-control" >
            </div>

            <div class="col-md-6">
                <button type="submit" class="btn btn-outline-success">آپلود عکس جفت بازو از جلو</button>
            </div>
        </div>
    </form>
    @if ($message = Session::get('f1'))
        <div class="alert alert-success alert-block" style="text-align: right">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        <img style="width: 200px;height: auto" src="{{ Session::get('f1image') }}">
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.f2') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class=" panel-heading"><h5>ازینجا عکس فیگور جفت بازو از پشت خود را آپلود کنید</h5></div>
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="image" class="form-control" >
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-outline-success">آپلود عکس جفت بازو از پشت</button>
            </div>
        </div>
    </form>
    @if ($message = Session::get('f2'))
        <div class="alert alert-success alert-block" style="text-align: right">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        <img style="width: 200px;height: auto" src="{{ Session::get('f2image') }}">
    @endif


    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.f3') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class=" panel-heading"><h5>ازینجا عکس فیگور زیربغل از جلو خود را آپلود کنید</h5></div>
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="image" class="form-control" >
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-outline-success">آپلود عکس زیربغل از جلو</button>
            </div>
        </div>
    </form>
    @if ($message = Session::get('f3'))
        <div class="alert alert-success alert-block" style="text-align: right">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        <img style="width: 200px;height: auto" src="{{ Session::get('f3image') }}">
    @endif


    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.f4') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class=" panel-heading"><h5>ازینجا عکس فیگور زیربغل از پشت خود را آپلود کنید</h5></div>
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="image" class="form-control" >
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-outline-success">آپلود عکس جفت زیربغل از پشت</button>
            </div>
        </div>
    </form>
    @if ($message = Session::get('f4'))
        <div class="alert alert-success alert-block" style="text-align: right">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        <img style="width: 200px;height: auto" src="{{ Session::get('f4image') }}">
    @endif


    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.f5') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class=" panel-heading"><h5>ازینجا عکس فیگور قفسه سینه خود را آپلود کنید</h5></div>
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="image" class="form-control" >
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-outline-success">آپلود عکس قفسه سینه</button>
            </div>
        </div>
    </form>
    @if ($message = Session::get('f5'))
        <div class="alert alert-success alert-block" style="text-align: right">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        <img style="width: 200px;height: auto" src="{{ Session::get('f5image') }}">
    @endif


    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.f6') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class=" panel-heading"><h5>ازینجا عکس فیگور پشت بازو از بغل خود را آپلود کنید</h5></div>
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="image" class="form-control" >
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-outline-success">پشت بازو از بغل</button>
            </div>
        </div>
    </form>
    @if ($message = Session::get('f6'))
        <div class="alert alert-success alert-block" style="text-align: right">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        <img style="width: 200px;height: auto" src="{{ Session::get('f6image') }}">
    @endif


    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('image.upload.f7') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class=" panel-heading"><h5>ازینجا عکس فیگورشکم و پاها از جلو خود را آپلود کنید</h5></div>
        <div class="row">
            <div class="col-md-6">
                <input type="file" name="image" class="form-control" >
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-outline-success">آپلود عکس شکم و پاها از جلو </button>
            </div>
        </div>
    </form>
    @if ($message = Session::get('f7'))
        <div class="alert alert-success alert-block" style="text-align: right">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        <img style="width: 200px;height: auto" src="{{ Session::get('f7image') }}">
    @endif


    <div style="text-align: center">

        <form action="/user/setgoal" method="POST">
            @csrf
            <div class="mt-5">
                <h4>لطفا هدف خود را انتخاب نمایید</h4>
                <div class="radio selected">
                    <label><input type="radio" name="goal" value="weight_gain" checked>افزایش وزن</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="goal" value="weight_loss" >کاهش وزن</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="goal" value="increased_muscle" >افزایش حجم</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="goal" value="increased_muscle_weight" > افزایش وزن و حجم</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="goal" value="muscle_breakdown" >تفکیک ماهیچه ها</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="goal" value="competition" >آمادگی برای مسابقات</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="goal" value="fitness" >تناسب اندام</label>
                </div>
                    <div class="radio">
                    <label><input type="radio" name="goal" value="depend_on_coach" >طبق نظر مربی</label>
                </div>
                <hr>
                <br>
                <div>
                    <lable>مکان تمرین</lable>
                    <br>
                    <br>
                    <div class="radio selected">
                        <label><input type="radio" name="exercise_place" value="gym" checked>باشگاه</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="exercise_place" value="home" >خانه</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="exercise_place" value="workplace" >محل کار</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="exercise_place" value="outdoor" > بیرون خانه</label>
                    </div>
                </div>
                <hr>
                <br>
                <div class="checkbox">
                    <label><input id="check1" name="exercise_price" type="checkbox" value="{{ $price->exercise_price }}"> برنامه تمرینی {{ $price->exercise_price }} تومن</label>
                </div>
                 <div class="checkbox">
                    <label><input id="check2" name="food_price"  type="checkbox" value="{{ $price->food_price }}"> برنامه غذایی {{ $price->food_price }} تومن</label>
                </div>
                 <div class="checkbox">
                    <label><input id="check3" name="supplement_price" type="checkbox" value="{{ $price->supplement_price }}"> برنامه مکملی {{ $price->supplement_price }} تومن</label>
                </div>
                @if($price->discount)
                    <div>{{ $price->discount }} درصد <lable>تخفیف</lable></div>
                    @if($price->event)
                    <div> به مناسبت <lable>{{ $price->event }}</lable></div>
                    {{--<div>میزان تخفیف پس از ایجاد درخواست توسط شما در سیستم اعمال خواهد شد .لطفا پس از ایجاد درخواست از منو ی سفارشات من آن را مشاهده و پرداخت نمایید.</div>--}}
                    @endif
                @endif
                <div>

                    <h2>قیمت نهایی </h2> <h2 id="semiprice">0 تومن</h2>
                    <h2>قیمت نهایی بعد از تخفیف </h2><input id="check5" name="price" type="hidden" value=""> <h2 id="price">0 تومن</h2>
                </div>
                <script>
                    $(document).ready(function(){
                        let j = 0;
                        let flag1=true;
                        let flag2=true;
                        let flag3=true;
                       let  value = 0 ;
                        let value1 = JSON.parse("{{  $price->exercise_price }}");
                        let value2 = JSON.parse("{{  $price->food_price }}");
                        let value3 = JSON.parse("{{  $price->supplement_price }}");
                        let discount = JSON.parse("{{  $price->discount }}");
                        let finalvalue =0;

                        $('#check1').click(function(){
                            if (flag1){
                                // flag2 = !flag2;
                                // value += value2;
                                // $('#semiprice').html(value+'تومن');
                                // $('#check4').val(value);
                                flag1 = !flag1;
                                value += value1;
                                $('#semiprice').html(value+'تومن');
                                finalvalue = ((100-discount) * value)/100;
                                $('#price').html(finalvalue+'تومن');
                                $('#check5').val(finalvalue);
                            }else{
                                flag1 = !flag1;
                                value -= value1;
                                $('#semiprice').html(value+'تومن');
                                finalvalue = ((100-discount) * value)/100;
                                $('#price').html(finalvalue+'تومن');
                                $('#check5').val(finalvalue);
                            }
                        });
                       $('#check2').click(function(){
                            if (flag2){
                                flag2 = !flag2;
                                value += value2;
                                $('#semiprice').html(value+'تومن');
                                finalvalue = ((100-discount) * value)/100;
                                $('#price').html(finalvalue+'تومن');
                                $('#check5').val(finalvalue);
                            }else{
                                flag2 = !flag2;
                                value -= value2;
                                $('#semiprice').html(value+'تومن');
                                finalvalue = ((100-discount) * value)/100;
                                $('#price').html(finalvalue+'تومن');
                                $('#check5').val(finalvalue);
                            }
                        });
                       $('#check3').click(function(){
                            if (flag3){
                                flag3 = !flag3;
                                value += value3;
                                $('#semiprice').html(value+'تومن');
                                finalvalue = ((100-discount) * value)/100;
                                $('#price').html(finalvalue+'تومن');
                                $('#check5').val(finalvalue);
                            }else{
                                flag3 = !flag3;
                                value -= value3;
                                $('#semiprice').html(value+'تومن');
                                finalvalue = ((100-discount) * value)/100;
                                $('#price').html(finalvalue+'تومن');
                                $('#check5').val(finalvalue);
                            }
                        });

                    });
                </script>

                <button type="submit" class="btn btn-outline-success btn-lg"> ثبت سفارش </button>

            </div>
        </form>


    </div>

</div>






    @endsection