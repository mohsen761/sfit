@extends('user.test')

@section('content')
    <title>  ویرایش اطلاعات من</title>
    <!-- Main content -->
        <a href="/user/myprof" class="btn btn-primary"> بازگشت به عقب</a>
        <div class="container " style="max-width:800px;text-align: right;background-color: #fffacc; box-shadow: 10px 10px 10px #888888;">
            <div class="panel panel-primary" style="padding: 6%">

                <div class="panel-body mt-5" >
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block" style="text-align: right">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        <img style="width: 200px;height: auto" src="{{ Session::get('image') }}">
                    @endif
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">

                            <ul>
                                @foreach ($errors->all() as $error)

                                    <li>{{ $error }}</li>

                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form style="border-style: groove; border-color: #00cde5;padding: 2%    " action="{{ route('upload.user.profile') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class=" panel-heading"><h5>ازینجا عکس پروفایل خود را آپلود کنید(حداکثر 1 مگابایت)</h5></div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="file" name="image" class="form-control"  onchange="submitForm();" id="fileUploadBox" required>
                            </div>
                            <div class="col-md-6">
                                <button id="btn" type="submit" class="btn btn-outline-success">آپلود عکس پروفایل</button>
                            </div>

                                {{--<span id="mio" class="spinner-border spinner-border-sm " hidden ></span>--}}


                        </div>
                    </form>
                        <div id="wait" style="display:none;width:69px;height:89px;border:1px solid black;position:absolute;top:50%;left:50%;padding:2px;"><img src='/images/demo_wait.gif' width="64" height="64" /><br> صبرکنید..</div>
                        {{--<script>--}}
                            {{--$(document).ready(function(){--}}
                               {{----}}
                                {{--$("#btn").click(function(){--}}
                                    {{--$("#wait").css("display", "block");--}}
                                {{--});--}}
                            {{--});--}}
                        {{--</script>--}}


                        @if ($message = Session::get('profile'))
                            <div class="alert alert-success alert-block" style="text-align: right">
                                {{--<button type="button" class="close" data-dismiss="alert">×</button>--}}
                                <strong>{{ $message }}</strong>
                            </div>
                            <img style="width: 200px;height: auto" src="{{ Session::get('profileimage') }}">
                        @endif
                </div>
            </div>
        </div>

        <div class="container " style="max-width:800px;text-align: right">


            <div class="mt-5 "  style="background-color: #fffacc; box-shadow: 10px 10px 10px #888888;padding: 2%;border-width: 2px;border-style: groove;border-color: #3f9ae5;">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <form action="">
                    <div class="" >
                        <div class="col-md-5">
                            <div class="profile-userpic">
                                    <img class="img-thumbnail " style="width:70%;height:auto;" onerror="this.onerror=null; this.src='/images/profile.png'" src="/bodybuilding/student/images/UsersImagesProfile/{{ $User->user_id }}.png " >
                                <a href="{{ route('user.delete.profile') }}" >
                                    <button type="submit" class="close" data-dismiss="modal">&times;</button>حذف
                                </a> <lable> پروفایل</lable>
                                {{--@endif--}}
                            </div>
                        </div>
                    </div>
                    </form>
                    <hr>
                    <br>
                <form action="/user/myprof/{{  $User->user_id }}/update" method="post" style="width: 100%;max-width: 800px">
                    <style>
                        .form-group{
                            width: 500px;
                            direction: rtl;
                            font-size: 15px;
                        }
                        .form-control{
                            font-size: 15px;
                            width: 100%;
                        }
                    </style>
                    {{ csrf_field() }}

                    <div style="width: 80%;" class="mt-5 col-md-12   form-group float-label-control">
                        <div class="col-md-12"><img style="width: 30px" src="https://png.pngtree.com/svg/20170705/24b50e869f.svg" alt=""> <lable>نام ورزشکار: </lable></div>

                        <input value="{{$User->user_name }}" name="user_name" type="text" class="form-control" placeholder="نام " required>
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6  form-group float-label-control">
                        <div class="col-md-5"><img style="width: 30px" src="https://png.pngtree.com/svg/20170705/24b50e869f.svg" alt=""> <lable>نام خانوادگی ورزشکار: </lable></div>

                        <input value="{{$User->user_family }}" name="user_family" type="text" class="form-control" placeholder="نام خانوادگی" required>
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6  form-group float-label-control">
                        <label for="">جنسیت:  </label>
                        <input  value="{{$User->user_sex }}" name="user_sex" type="text" class="form-control" placeholder="مرد یا زن" required>
                    </div>


                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"><img style="width: 30px" src="https://cdn2.iconfinder.com/data/icons/spa-solid-icons-1/48/25-512.png" alt=""><lable>سن ورزشکار: </lable> </div>
                        <input required value="{{ $User->user_age }}" name="user_age" type="number" class="form-control" placeholder="سن">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"><img style="width: 30px" src="https://png.pngtree.com/svg/20170717/65935b3a8b.svg" alt=""> <lable>قد: </lable>   </div>

                        <input  value="{{ $User->user_height }}" name="user_height" type="number" class="form-control" placeholder="قد به سانتی متر">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"><img style="width: 30px" src="https://cdn2.iconfinder.com/data/icons/medical-services-2/256/Weight_Management-512.png" alt=""><lable>وزن: </lable> </div>
                        <input  value="{{ $User->user_weight }}" name="user_weight" type="number" class="form-control" placeholder=" وزن به کیلو گرم">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <label for=""> سابقه تمرینی</label>
                        <input  value="{{ $User->user_record }}" name="user_record" type="text" class="form-control" placeholder="سابقه تمرینی">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                       <div class="col-md-5"> <img style="width: 30px" src="https://cdn0.iconfinder.com/data/icons/good-view/500/View-08-512.png" alt=""><lable>شهر: </lable> </div>
                        <input required value="{{ $User->user_city }}" name="user_city" type="text" class="form-control" placeholder="شهر">
                    </div>

                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                       <div class="col-md-5"> <img style="width: 30px" src="https://cdn5.vectorstock.com/i/1000x1000/63/09/job-icon-male-person-worker-avatar-symbol-desk-vector-23636309.jpg" alt=""><lable>شغل:  </lable> </div>
                        <input  value="{{ $User->user_job }}" name="user_job" type="text" class="form-control" placeholder="شغل">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"><img style="width: 30px" src="https://cdn0.iconfinder.com/data/icons/fitness-49/48/6-512.png" alt=""> <lable>دور کمر: </lable></div>

                        <input  value="{{ $User->user_waist }}" name="user_waist" type="number" class="form-control" placeholder="دور کمر به سانتی متر">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"><img style="width: 30px" src="https://cdn1.iconfinder.com/data/icons/real-estate-84/64/x-24-512.png" alt=""> <lable> آدرس:</lable></div>

                        <input  value="{{ $User->user_address }}" name="user_address" type="text" class="form-control" placeholder="آدرس">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://cdn0.iconfinder.com/data/icons/fitness-49/48/5-512.png" alt=""><lable>دور بازو: </lable></div>

                        <input  value="{{ $User->user_arm }}" name="user_arm" type="number" class="form-control" placeholder="دور بازو به سانتی متر">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://cdn2.iconfinder.com/data/icons/body-organ-health-and-medical-elements-vector-ou-1/128/body_healthy_medical_neck_face_shoulder-512.png" alt=""><lable> دور گردن:</lable></div>

                        <input  value="{{ $User->user_neck }}" name="user_neck" type="number" class="form-control" placeholder="دور گردن به سانتی متر">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://cdn1.iconfinder.com/data/icons/body-parts/24/Knee-512.png" alt=""><lable>دور پا: </lable></div>

                        <input  value="{{ $User->user_thigh }}" name="user_thigh" type="number" class="form-control" placeholder="دور پا به سانتی متر">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://cdn0.iconfinder.com/data/icons/clothes-29/100/Artboard_43-512.png" alt=""><lable>دور پا:</lable></div>

                        <input  value="{{ $User->user_leg }}" name="user_leg" type="number" class="form-control" placeholder="دور پا به سانتی متر">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://img.icons8.com/officel/2x/chest.png" alt=""><lable>اندازه سینه: </lable></div>

                        <input  value="{{ $User->user_chest }}" name="user_chest" type="number" class="form-control" placeholder="اندازه سینه به سانتی متر">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://cdn1.iconfinder.com/data/icons/cooking-10/500/cooking-kitchen-cook_17-512.png" alt=""><lable>ذائقه: </lable></div>

                        <input  value="{{ $User->user_taste }}" name="user_taste" type="text" class="form-control" placeholder="ذائقه">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRW8tndKXFOrPE2E_AtJcG7e8txScvzbDsLKmkvb2DB0s9RiQ6O" alt=""><lable>سیگاری بودن: </lable></div>

                        <input  value="{{ $User->user_cigarette }}" name="user_cigarette" type="text" class="form-control" placeholder="بله یا خیر">
                    </div>
                    {{--<div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">--}}
                        {{--<label for="">اینستاگرام </label>--}}
                        {{--<input  value="{{ $user->user_tea }}" name="user_tea" type="text" class="form-control" placeholder="اینستاگرام">--}}
                    {{--</div>--}}
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://cdn3.iconfinder.com/data/icons/miscellaneous-1-glyph/64/inactive_sleep_night_moon-512.png" alt=""><lable>خواب:  </lable></div>

                        <input  value="{{ $User->user_sleep }}" name="user_sleep" type="number" class="form-control" placeholder="مقدار خواب شبانه به ساعت">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://cdn.iconscout.com/icon/premium/png-256-thumb/stress-6-825610.png" alt=""><lable>استرس: </lable></div>

                        <input  value="{{ $User->user_stress }}" name="user_stress" type="text" class="form-control" placeholder="بله یا خیر">
                    </div>
                    {{--<div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">--}}
                        {{--<label for="">اینستاگرام </label>--}}
                        {{--<input  value="{{ $user->user_energy }}" name="user_energy" type="text" class="form-control" placeholder="اینستاگرام">--}}
                    {{--</div>--}}
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="https://cdn5.vectorstock.com/i/1000x1000/88/99/red-blood-drop-icon-circle-shape-vector-13938899.jpg" alt=""><lable>گروه خونی: </lable></div>

                        <input  value="{{ $User->user_blood }}" name="user_blood" type="text" class="form-control" placeholder="گروه خونی">
                    </div>
                   {{--<div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">--}}
                        {{--<label for="">اینستاگرام </label>--}}
                        {{--<input  value="{{ $user->user_workout_a_week }}" name="user_workout_a_week" type="text" class="form-control" placeholder="اینستاگرام">--}}
                    {{--</div>--}}
                   <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                       <div class="col-md-5"> <img style="width: 30px" src="https://cdn1.iconfinder.com/data/icons/social-issues-and-critical-problems/370/problem-008-512.png" alt=""><lable>بیماری: </lable></div>

                       <input  value="{{ $User->user_disease }}" name="user_disease" type="text" class="form-control" placeholder="بیماری">
                    </div>
                   <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                       <div class="col-md-5"> <img style="width: 30px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAgVBMVEX///8AAADz8/P39/fj4+PX19f6+vqurq7w8PB7e3vAwMDHx8epqanb29vg4OCUlJSHh4e5ubnExMTOzs6BgYG0tLQeHh5UVFRCQkJiYmKbm5s1NTV0dHSioqI7OztdXV0qKippaWlJSUlPT08QEBAXFxclJSWWlpY2NjaMjIxvb2+E7fKXAAAM8klEQVR4nN1d52LyOgwthBUIM2FDSSgd9P0f8JbS1sdDGURK8t3zr1BiK5a1LT89SaM36A777zN/u9nHqzvi/Wbrz977w267Jz6+KMbL60vcSkf8cl2O657oI5gso00GbYhNtJzUPeUC6O7WBYhTWO/+hcVsDz8fou4Xh6FXNwlpGATnUuT9LGV/UDchbnh9DvJ+iFw2T8aGWzby7vgM6yYJ4R3fmOm7IQ6aspDjgwB5d0RNUCHhS46Z7pNotxwuwvHkhnG4GC53UbLP8ctTt276MtT62Q9GHZrZep1R4GcIqJc6aQyf04i7zjs5n9OZz9Le1LkuGrv0yz9Pw6JSohdOaSpPdezHQULM5nIYPioDvSEptA5t1tnnwDsxk6isGgsj4skBy7zzYvTqnMR2xPL0xcn59Li67eg57ZfXI585Odi5WbUiE6DvXD5uI2vk9MCGzKO40HZp+CivXiiCjkvsbMWXce4Y9Srl0g1cUodnr5NwvNZIUo4PHAPOBMfrrKzhEmlvdWIL1mexMW0Rc64isBLaFvpCZiTfGmguM5CFwBp5KjCKZ1nZkvvBHNzajif2MToXY4iPao3hrikCYmYBNzLf4ZH3+TlwNafAKgOW5guU0PBZ6JpsxGhGmVZihTtQgxlsZpN0Jn8IyeocMHmJyaMyTCc5fZsDps2x43iooQZ9jmeWwImdRIPAaj1tF67MJBoEChv2ubBkJXGmP60OJWEjZGSrqfYobjPiYXR0EpePP0m3eJ+bkij5cht15f+w6tdNtRfOKZaFpye7HrSRJ9pD1rxTLAtPdxofiqO0m0zg01NPI3H/yCP2jSbwi0StUmdb/AGalbvhn2B5eBqTFVaLmhiNmyNFEYMyAlWXMk3Rgyb0WRaTNpq+aYYl44Kmz85FfpmUWP5KoW2mAltRs237cvNjgGY4506/aTu4bn8wC5hqf837I8ynP0vOjgOazjjk+80Rf9PQIjpAt7DMGBT+Rc3AFbnk+QHmXusKGxZDwRljCjSWnhsPtK2Y6Uj18L+bq+p1oOLP9DJQv9QfV8sLTE1lxDQwAiLqUIz70Xa7/dyNeGoAkPPS/xM3rRyP9nZQcZRw5JCGMO9r2j8iQ78zDOyGpnC/sGXwXbCIKU2Hg1//Vn5UAo6CnPL1XBhz+aT/DddaKrrdc1YGl09nYQaQ3l8QouPPk/+AKH0ubzxBVooM2qCylxIzZAFpaZmKDEipfRBwUuZa16LsF+W9NPCjiEXEJZSqVEs58lXai8G352ZBKJhhSa06MLDoUihf3QHJUydHYM5KKnroLE39Qfm8CIbeXCoWGEhM2VM13N8o/3hYRAcJaJGKBYBTT16WHxV3ov0tvN5Uw64UUk8QMQRMQJxatTY93qEIpB7hY+AcsKut+DCESFPMurKwatMAuUOBaYB8lKn1N/RXjFikUJhwDADC2thrIGdEI6QpFPIUqcED9S+m3CMRSFEX7APo3hHwr2iukDZqmIJCY/XEA/G5cJ6CsmoKpcbS8OHmCpBx0tX3hPvEZupDvg3dauU9rrhGImGX/H9pCsZDYeqpoPaASSXK/A3YjMoaUAC7SX0IMY4qjogM9BrRC++BNGBTFeBS5pxchE3DxP8rFFhzaydg0z+l7zk+E0dneDwel12BYILy5P+MF4jh/Av5wizAnvsVYGAH1Do1JoCr/7sDlEHDYv3WDkVhdP8AtmazK0vyQpXkxfcPwKX5V1Ki6QCFe9+I4FfUPDUmgCt49y9UeOj/sQ1xI+6Mv6s/cicDFQ76Du/Dmtbd7YYLoBFvf4K+b2ahbHFA+P4W+1ZdSj7kx+4Nqmg4Bwnhm5GmmFYwjOh1A3/9k4BdbY/SHozKE96CI8qikRI0nZ3VjejyLlpZrdTDTAt2i5wNnbwTeW1JX1vVPa01m43foulM7T4Tf1jJ8arygmNN7jCL0l6Q1cFU7ECxRhQoC9ZBOkkGeTdIlbSAjh/AgvLWsaUlYRSE5A1ELcawKXMWSedEWrM6BamyHTXCAsKXrBUmnoMcF4SkjVKBc8h9s6rDtDwagpdx/qAU4hG4iTWs983862swDMNh5O7udgfnoAoqd3CFWjZW2b16vYKnMqTVoow/o4JrEexJzkhiz5g4nVWTCQ2puIUPFIq21Jk6iPuGjDGsinQToFA0DEUuoox5qoJRJ6BQ9hyl2T5Hdg1VFeIZKJQ94kRZADL7UNmi+6q49Im6I0HGNFWlQ69AoazjTekLmVGhwg0olI20EQRWQKGSAKKZNdJOldkbykG8VEXhxEXd9ysWAVKo9odo/yBX39MbYpnhlOH/BucfRDvYUuVekcxwqC1ULEW0oI0K2QgNihpf+YeSxw17LupuEDIzlF26BU9KMoBpdZb8gVSpp4oS+ZonJYeZi7yW3KkAH6hSbo1g2oI6zyVmZSR/I+zAzxAsDiZ9J6kB1RtdikWENVBVpUJxKD2aCBacXIKUOoUgpaC0iLBoZuYXFJNKOd1gIw6QXDHDNLRp+wZb6bMJiNb20DUVU/mUrhCr/VAK/9ZiQYWHxYrYqfuuxKJ7qu7rlhhR6l+KaTou6lo5W608BGUF3ww1OPEkNJ7ZYUCcZ8AKvklrUBdCwpQ6kyeWAobK/O8giU6wAAgCxY5TW2yphKnMGXUq0SbXbVLZ3XdLVPlPMkVRriMkN8j5o0p237c6GI0ifENFSsVMKDDT7u14YF9KWDVjm7ZvxAJj3WEXPasPJNx89+VUkn3EwIT6+URZNRKNk6gYlFx4VonO3/4Y8Jb5jf22gzh8vfyAbfhr+ILO59eIVChYTleA5PwzfNVH/LEa6opnuZpyFR9Vhm8iyDtU7lcslwdGqQqSgJXDnbyg/Aq5bQiBJ5WngL3Jbe9T21Cu3R0wJBgwINGZx6Pc+yo8JxRmcLKUOQNF3TcrZpTCjsMOisCmzPWQBIFy+h5eaZv4nDUbRAa7pcxukGz6WoGWZO2CRYpSqeIdKE3WrRcsJOAckMqqicXXaTrgiDynShxapAm8RgAoJzMnAi+b02I0rxGT5lKoLbMSd1DHyygFSAplyiLAh4itL+GibcacF1nuLZOshL1mK1z04/hYiJSllsjmGBNHc3wNbg5jRp+ksHUCpT+Yrzl2f6Ke7oqRYMSIbxEp9/CGS3INguB99rlyclVh4BI6ZQkE3/nsYip1aIFBvEGm2X0qPeQd7wdZ59d+wNCPDrsmEv41lITwncynwqUGGAYEHqQ6haKJxRdkoOKlOsqXe+PsyTIdsAgYs6WpTUvvWO3K26lQSE7LZdyJjHHFrKOIPoeziGnYFAZEj5xh1F+QHsYXTjz2GxosaXewoThiLeQjDgVtAq4gO9YkpSqCJN9aF0cvsA5cnAI+wwK5JH1pMOoQs41/R2fur788mNfVfpNES95KE3xzGUlQZKcK+u8xAZsxZpp/+DaqaMDHAeTROPO/0aOTbxPJAu20So6aXJRKTb907Y5TwRlrb0T0hAITtJKrXL/QCnqb399Mu1AiZ5wQq7Qeuoa2SmgslzfBq6Xem97hDA3N/MWOWtJP6q4LHmgFVwUseK2dsVgJIQO0e0gLWShapZboQf1S0JyWYmllPc7Z1Issy9wHrIfj35rZqq7Unc7GFm7kfasl7+U2WgSUv+aGHaXvVn/ytFqfxl0fr9/f9phhokubhpHo6SGDB0t/9QBSoxi1rR9SeVif6YfqnpsjUY0SjxLRSD1EFsv2d8kPo31Bxt2q6TCKtprhSxn5rJJ5OeMoQRNs1ECfUmnPwGd+Xmkc2CdkJFZq9he9DTuBFolxnXa4eQcmUy21mTuqbzMaW5CvyZSZ5JTsvpAC72TMg7HsyCxtEmyQS8OqPmKtUbUygNXnNKy6FebQg9UF6a3aFvVWkeOe3cBqWy3lKtyNbaspg0jzWvs+MdG+SwD7qLRQ1xe7f8dLFWG4kd2uT6xpV8c+DnqQ1v9d+yT4RtLHcRTi+ZI0TkwV2BIX466i30iKxomr64v4pVsD1ymYg4TjGLo6FUi2evqDaRx+Y829+ZfO1lIVmcRtx+ZotV6PfAKg4y4UqzDpThSo81RweX13Teq+WmOY6u3sl+RWb+5kkFYNN6Z1yDu2k/mj7NoJyMvXoypu4DER0gXOm/ew6IwGixnViqjV2tYV5BtRrfRu2M+Geec17vspt7W01nXWZo0+Umb2hZcoCFPo9CaL4yGj1r1W+m4YUedgEc+Jfw2Gi1HYHU+6YThazI8zf0s1wEZsm5Bi76adGimHWTOC7F8mwC7tBotH8VEqH8GOEaXGHsWheRf5tTOvd8qPU1MrBjtHDiJPy6Zk8ZwYzJMy1K2iUXPysDTCaR4NYmN7bIrozIHel5mSZvCYOM/m/xB1Ct3+7Jyl1vfr6bwJWr0EvEF3GLxH/mmzj1eXS+vyuor358Sf7ZaLcVt+1/0H0cmMTajeNQUAAAAASUVORK5CYII=" alt=""><lable>اطلاعات دیگر: </lable> </div>

                       <input  value="{{ $User->user_other_info }}" name="user_other_info" type="text" class="form-control" placeholder="اطلاعات دیگر">
                    </div>
                   <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                       <div class="col-md-5"> <img style="width: 30px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANgAAADpCAMAAABx2AnXAAAAwFBMVEX///8AAAAREiTa2tsODyLr6+v7+/vX19f39/fo6Oi/v7/j4+Py8vL29vbLy8sUFBSampqurq5vb28AABomJiaOjo7CwsIAABdQUFC3t7dLS0tAQEBpaWmgoKA1NTV2dnYcHByLi4t/f38RERFbW1s8PDyDg4MAABOmpqaUlJoxMTGdnZ1hYWFtbnY5OUV7e4NMTVcmKDaHiJAhITFYWGMAAB9gYGdRUVyDg4t1dX4xMT4kJTNBQksZGSuQkJmzsrjyWshZAAAMhElEQVR4nO1diXqiMBAuilKPohartVqr9kBaFa+1aq37/m+1IiiomSGQAO1++XftWheQn0wmcyW5uhIQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEPhPkC3mCmlFze+gKulCrphN+o6Yca3ky6371450gs7rfaucV66TvruQyFV67xKK914ll/RdBkSm8dTBSR1b76mRSfpuaVHo39OROuC+X0j6nv2RrTwEY2XjofKzFUq6F4aVjV466bsHUQsogue4ryXNgIhGm42WhXYjaRYXyFfZaVmo5pNmcgK1yYeWhaaaNJsjMi1+tCy0fsjI9siXloXHpDntoHCUQhdNJWle5ShoWSgnSisXSXPZaCZoIOejo2UhMc1/Fy0vSbpLhNY1owFFg/sEnNEcJ1MDRzX2jqaU4uAlSaWY9b4aDy0LsVpYtfh4SVKMzkzEav4csan9mHnFxixc/2o2ao2P3qAZSunE0s+UULweDqdnCo234CNgDLox9xyK2Okzz9a6lHFHB8+Rj2fZkOPyZeQwXQ5iQVejDs+9hOMl9UkXU97oL/ASLa/wdi+5/2f71CIZqUXMoujbrbdyRc3cXFzzlfICESr9DAOvA6qDO/UsWEMbkYwuxhMqLE9C8+M0nt2nOusBuC1mcHUsS2Wvory5pTknom4WbmRG8JL36PAajRaJZpyOIHBT9QQQixRh12YUvCIKtD26rVbxPzqCoBwPjUhEx82wKP5mDX/NyCE+Dxn290cVWfTVuy3evNhjAVbDZGqPLyQl4ao73+fH24Nh1hxu71AeLw3O5lH3+5mPnPUHaku9VJRcoXaHm0Ynfkemf3Fw5fB/XR9mfC0rZJAZHB+2gplG50OQci50b5TMOjx5NcCveT+ReaRo4LLXF55OjziGfX2kkWeeGpSypzP/bwDfz61aOHcW06edrV10Psftq1d+vMAw4kXmsYDekvR+WykgVz4GtHGtzy/QCAVfKpeH+jsAzz3Vk2rInshjyWF2g47U97x4pYEv+CAcS+WAVLuedjtRuKUi/pU2eNXwACqBaATQWl4D9+YK3uq/ttMPUbuxx4kY+eqdCyd/D+r6nNax1bJeHXKQM1SB8OEF6HpgoAwQeHINKS8LpzmuMf+Mj8Ynqw7ItAmSiGkf5dEr7Q3/C3FRH4AGh4zRYO7NcbzwMnPoPoFnkeKvwUHWc7AtGizic3s4zTO0O9cuIqcR468BQY79wlfuQjcz6JKE+uFgjni0jtP5kKGDQ1wYEC04SQB5bnsBy1UunlPVGbqKnbNjr66QKnB2T5qsExGvCJCgoz+W6Z/d78FE9DwR5/KI/mDXi+TRGYvwkX1Sj7LJNk6DBE1HGj1xS8dYg/sr+xhNvi5mh5LDWX3kmEOPcZ2Iqj36I03GyitHviyWhyN3sufTXlE4aQxHADwWoiO5sB3DmgkEzA7slCz5lNJb48SLPpFxRwI8ZostnrDJyNrJAAMYPQcZyVoeGfZWpjpWfdHNAztNBl6JtZORfecqek4XJmZFtY/Gs7c5bs/JPttNBiZCGP3oa3IqHY+BwRESm9pRij48nzpa0/3APgp0yZ/ZCuOADAtODFA4LgaEsM27/YnbZE42DCydYMu8APoWJ4Y79hY6ByPW0x3tFvIM77b5AdpVbJEPIMWC9zGa0gLneXsMNqfTuNqqu/8dlEW2xAvkx+JnYQ7HAU6beVSj7bm6wl+yj4CC67fAl9MBevb4WTRTDEqOcnSHYMd7dPWwrU8+SGdLjAZ+FnpcRfQ0qqqJwcWxdiO60m/LIpQhbrLU6hShwAOezKHLOTla320y27By7aqOfQBwegd/uDhAP58QKg1MzBmJ3GH6+ZyI3YRQ2JzFJQNHpCcOxBy95jEtbQXvqh67TaFOxmIGg7oWN2ho8/D20W4+yXZt3CZ8Qh8TS0AHLu3AHleRtvTPbhHXArMVnSsmth0ABXVYTA9YppC8YoG6pPHl7Mar+153ffzd9uGyQOiDJRsNE4M7GUW1xgGOs+LeuC1drp1ldzpAe7AQg13zDmBcpwOFFW1pcseUvRxk3YiILauAT8hiLCIxB+LzKgac4L1P2Hi8nFfLHPG4YPbIBth1LMQQvU0y1QJI4YGZkj7R5tV+w5t031cCQ2NONH1MkgiyGJyYH9pqJg+VjbMQwxKLpJmvEc5svARLXhPzhUk+WYyTlNgGaNTJJw1lDKtDBAaLSYUmu0jxASD4EwlYjGA8ekHSt/HNLKuSc+CUQIfbd9IZNGEBLmCr6UZKiCSyV3bts7QRNwyYiOHPv0Syq7gXewPAXUI/+BTadEOcwwtsaWi/gYnoE8XTzdhqaPGaJiAknOU2VQQDY0GV3/wFUqGYNxsUGTpsvPyrNogPzqdskQdYZ/D4TtR5JcYtozcaWSfw+Ad1yanFyC0Q1mLua/+vIGeDo2bGvCAGRf0hWT9Fy6zNyuuq6/8lgDkaaT8jmgaBQHN7gIaKUjeyT3G5oVkcAJgjlIksVFBi8llsUM2vApRvlnhyu3d3171lIs1jthXd3GcoI0xIlRzty6xa6b2GM1J4zNyhtI8gY1s9d8LP/O4bpRK88Z5Zkn5HUM71g3KBZzOBSeN5NlO7C7QUBp95f7RqG3SQTkKe8LNW+i+0cslp2h/tcyRa+hZu3KgcXp2RVenEgw8v+onC8F0fFyv0TfXTyAevScP0pfRIrZ1dLgs2qgsKU4zbnGF6X/8FaZF+FcyqBfs2tjCOFwECT6/I07z+oArA+BbPcVxxIMCCMhwWa/P5Np6LzASKFeLFLRTwUVZcl4gINHyy9gHciOM2kXGPYOHdV7bacfzLOK/pQbVCigum4nF0KGMrU7xEUKfxiSEmARVP7cF9zf9uQGad8CKDxTLZQwIXCLzMXjdkpSQWVi/x5bRH8KhTO1yjYXZwJKtjhliDJYzgYL2Z+/ore4B1tAiegz9ipIcxVcsiCLXyWyugGsMqwCNb/S1cRu8uiBLBxjB+Vv0FQu6HQZ9VxSx79qg2jLCx3SrlfjMFbEyJdDsenxlUMN77FNTQdbAj3vmEYcW+Oz+PHq0KjHyJeJYVxVo1JOReQItlohnBvGArCaj2VLJI5vDlMh5i2BPKf9qbD1qVCzVQw0ubGAvCaFHksNT9oNtXC7lMJpdTGne+EZVSRBbHOVClHAFKse27FkMZhxcx7ieXibHNSrHulJThsNkYHdox7wB1E0sh2E7Px6IPTxAwbhUOvGNSVIhsvycXCe38FHklWGJ7x/kvEsuCh5iGZSIi3PcpmR2fjlAjGtFKyW9hGGAVO3q8+X9v9OC/y1/yO/w54DwhjjlzyA9ZjvL49rP2Gc5xmjnW+3l7ehc4tNrbz9zxulhmmkjwXE5yRPZBPuxOPNLLz9pW+BKFcghXrV3+mTJ4hkI50OS499/BykaucUvVcO3bxs9Tgz7IFvJvA8SSLA3e8hfrqf8aZItqpfx0fxJjrd4/lStq8ddyOsdNMZMpxh/BEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD4IUj/p6BerOO34Sr1n0IQ+21wiMnOK+X5N5XStJTs/rZ7J2vurz8cNjF5JKfkybf9fn34v/p8rq9HByrfY1n+mq9/CzObmDZcaPVtXa+n6ro0/dZ0XdN0qfK33x9+SLokyZokrRVJGk03v4uYPNvqM9M0lpJprJbG2jBWG2OcHkvSMDM3FHWySafHtY0y2YxiJabJVlfYvay/mvXD6Q72O+tzWdN2n3zv/k3tfxw6i9PHdCM1HK7qq+FCkqZ/tilptZrrGzVtDo2JKi0qtZE0ySiaJsfKS15q5u5mJ6PU9+7ZT4cjbaJps6X2rX2lZin5a9d9hivTnG83C0M31qvpzJDMT9lLTFvMp3NjMTQ3mj6VP/T6cDneCeOftGmMG9Kooer6pKB8xSyHdXM17xvmvL+ZLo3P5Wdlvlqai/5S2yzHU3O7Mpar3dvP4XyzXo3N2Xy+ML/MT81LTJb75mwrTyaGPFsshouUMf/UPoZbdZRep4e1ldEwN+o4rcdLTN78XRvj7Xj32j7+2XEcDrfG+HE5+jTn06m5XhqrkbFemIu5+TX8u95q5t/Ro3lCbKc+ZtpkurJe+kIaTmfrtfxpLOv6YvzHWOj6TiSH0ireHra7q7W2nsib+noym2ij2fdsJGub2Wykjb7Xs1nq62vz/fk52mEia9+piVyf7bpb6oSY1enkurZ/7f7UrV6r6XXZ6r/67hlouqdjxgdbbVg6wv6xVxnH99YPTT58vtcnxzP/d8vj/4Mg9tvwD7Cv/ERyVcyDAAAAAElFTkSuQmCC" alt=""><lable>شماره موبایل: </lable> </div>

                       <input required value="{{ $User->user_mobile }}" name="user_mobile" type="number" class="form-control" placeholder="09xxxxxxxxx">
                    </div>
                    <div style="width: 80%" class="col-md-12 col-sm-6 form-group float-label-control">
                        <div class="col-md-5"> <img style="width: 30px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAsVBMVEVBtOb///8AkcjS0te5ub4wsOU6suUAj8cAjMYAisUAiMQ1seUSl80gntP7/v/Pz9TP6vjj8/tfvulUuujt9/y1uL6g1vGX0vDG5vZ+ye213/T0+v1zxevV6fTKztWx3fOOz+/Z4ujO1t2/3u6izuaRxeFstNmAvd3c5ero7/NNp9Pk7PBSqtQ1n8622OuEtNCavNJiqM24yNWutb2txNS8wsl5r8+hsbyFwN91t9o1lcPrXq8oAAANrklEQVR4nNWd6XbbthaFQcokSFGmqcGSqNnxkMa13fb6urfu+z/YBaiBJOZRovaPrC4nK+WXfQCcgxEE3rWcFPPtdFeuRuPxGACAfh2tytl0Oy8mS///e+DzL58U290IxFEUZVmMBfaq/jvL0M9jMNpti4nPj/BFOJnPRggtq7HYwqgIdDSd+8L0QThZlGMMJ0QjQBHmuFz4oHRNmBczRCcxjmcnopwVueMvckqYz8vMjK5BmZVzp5AOCSs8C7oTZRSXc3ef5YpwOIud4J0gZ0NHX+aGcDGKMmd4e2XRaOHk2xwQTqZuopMUapJTBxmBNeGwdG5frSwrrYPVknC48mJfrThaWTJaEXrnc8FoQTg5B9+B0SLZMSbMZ2fi2zPOjLMAU8JF7K9/YSmLTccOM8LhODorH1Y0NmuORoS7MwZorTjanYmwAOcN0FoZKM5BeBkD9zKxUZdwfTED98rA2i/h9IIG7hVHU4+Ey9H5u1Ba0UgrH9chLKzKd3eKY50OR4Pw8hF6lFakKhPmqy5E6FHRSjmLUyVcji/bh5LKxqqNUZFw2JEmWCuOFZM4NcKiM02wVhyp9TdKhNsuNcFakVK5oULYUUCEuHVDOO0qIEJUGDXkhB0GVEKUEnYaUAVRRtjZNniUtC1KCBddB5T3qGLCKwCUIgoJi2sARIjCoV9EOLwOQJSkihI4AeESdC9VYysGgjRcQDi+FkCEODYhXHWrXBIrW+kTdnykJ8Uf+XmEV9KN1uJ2qBzC5fW0wYPimNPbcAivqJc5Kh7pEM6uLUaxopk64foaAREic8KfRZhfzVDfVgxYU4wswvKaRsKmslKNcH6dMYoVMfbDMQg7NzWqrpiBQ/9od60xipXRK6gU4ZX2o0fR/SlFeIVjfVN0lUESXsW8hUjUnAZBmF+3g1hxLiScdb2biaUxls1EhJNux2icRatiCSV/KpoICMsuBymyb4o+/jEdSP5cySfs8ORaZV/1kc9JKEGMhlzCVVctjKPx9ljgvsJQghiveIQdtRDZV9bjeJ6EoQyxZWKTsJMWYvua/f+6H0oRWyY2CDtoYRxFOyINe6o8lCA2TWwQdq4jjaPRgqpp32AoR2x2pzVhx8bCOIp3rOWI8EgoRGyMiTXhtEvpTBaNHhl4qCmlYaiAmNUTxDVhdyxEQzv3XNdTg1CISBN2pajAQ7vgbN4LDJUQ6xLjRDjqRD9zyMz4GrQI+Yj1/PCRsAtDRZ2ZcTXph6Ea4mnAOBLOLm5hFo3lh/E+klAR8VREHQkv3JHizIxpHzEe/qQI+YhtwsvOkTYT67YeiB+/U4BcxOPc6YHwgvkMPr3NOWHw6wfxgyVtIRfxmNfsCfOLBSmyj87MjgaSgKj6ZRHyEPMG4YWClJeZ7Q3cUIC4+lVHPIQpuFyQMhPr2sDNA/3DV8gmZCIewrQivECQijIzpOWPOwZgwA5SLmJ+Ijz7tgSUWAtvTXi4YwKuyfFejLjfvFARnne4x/YJMzNk4N1vrN944jRDDmI8OxGeca1CklhXBm56bMC6+lVD3K9hYMLz1b7SxLoysHf3O/v3QiEhjVjVwZjwTIVTnGWyxBq3wB4XcChohkzEqoTChGcZK2KVxBob2Nv84vwunXZLEKvxAhP633qB7ONlZk39tukJAINvcZDSiNWiNzhDM+Qn1rSBAkCy+lVAxA0R+E7ZUGJNTnkKDOxt+P8UVPUrR8SJG/A7GooSa4aBIsDgUdoMKURcBgOPMzTCxJploBAw+EeNsImIZ2uAt2lElFg/Kh70PBjY+yH88+8KzZBEzDChl45GklgzDZQAsqtfCSLqaoCHtFshM2t++cFACWBQ8AsLPiJKvoHz2XyFzKypo4EyQG71K0TMpojQaUajZ1/DwDu6oCfErX5FiCirAS4Li0zPvoaBcsBAx8ITIiovQOAKjzvlydXJQBXAtUYzbLoYgKWbZqiYmTV1MrDHLOgJCatfPmK2BC4WLARTnlzVBioBEotOyojRBNgPFjgz07797/eTgbyCnpBK2s1AjAqwsItSjcysoYaBioDS6peDmD2CrQ2heMqTq4aBvQ2noCckr37ZiNkWTI0HC9NLRpsGqgKqVL9sTcHOkFA25clV08DeHb/ebWtgCJh8A6OUBtk3NbsfLm8aKCro25qYBWkYwjdgsNVLMzNrqmWgOiBv0UlB70C3/tVMrFtqGyiud9tSrX5pD2+BblqqmZk11TZQB1Az7W5qAMZ6Dj4b3+xLGKgFmBsHqTYhyJL3J6M+hjCw90Pnn0o77W5Ijw8rTNL3J92WSBoorXfb0qp+rQnBIIRJX89J0kBNQMmik3NCgEdfiJ1UhKQM7MnLwbYki05iQs12eEIMlZ38RRqoUO+2ZZZ22xCCYw6l4GT+QBqoDWhQ/dYaaI+He9V/A4Z85kNSBhoAmlS/DULDOf1mJozC9ZYNSbdAtYKekEUzRDmN6REEItlHbZKGpA00AlRbdOLo3ay2YCDSTtIt0AzQtPrdf9WbcX1IIyIlaQ3JMFBxxoIUY8ulOuG3RY3PQsThevuM590YBhoCMrdcKhP+tJqn4RTeSTp4pvH4eywkWlokpWHybDfXxp1bSBkhql7vtmVe/WLCD8v5Uh5i8qczQKu0O0webee8OX8x/MsZoEX1i5SurdcteC6SgOZv4tiMhmF/ab/2xEZM/uMK0Kb6DSHMHawfMhHhHy1Ai3fGbNLuMLx1sgbMQoTvTcKvB3NCm+oXpTRu1vGZiM3x4uvmy9hFm+oXDYeI0MWmL1YC998G4c3Nzb1hX2pV/aLhEBE6OdNFI8K/G4SfGPHBiPDDpqNBg4WzPVEUIhwQhDdmkWpT/aLBwuG+NgqxMV5sbva6N1gOMFn7rXXrcm8iVS/W48XXgfDm80EXcGIVpPDb6f5SAhG+Uh4iFzeahIpbLjlCHY3TPcKki7SH+pFqU/2iZjh0vM+7jViPF01CzT5VecslSzAMXO/VbyHW40WLUCtSrapfnNE4P2/RmmT8tz1Y1PpUzsPnVoTJU+D+zEwTMTkmbjek/se+MIGWVfVbNUP3554aiPBQ6G/uSQ/D/j9qhFbVb9UMPZxdqxGPhT5J+IlG8eRVJVJzq+Eej4Zezh+eEGHI6mhu7qs0BSYK+zmsqt8w/TgROj5DekI8JG5fDECEmMoj1a76TZcnQtdnu06Lb38whsM60UylkWqVdsPXoCZ0fTDogHgo9L/YgHgWRbIr1a76/WgQOj/qfETcEIT37VIB9p9FgHbV7z5Ivd2psEfcTwx/8gBDSaTaLDrtE5qa0P35tQpxP1588gFxh8uPVOMtl1iHIPV4t0m1Y2PQrJ1YgMJINd1yWSnN24Qejujh78PjxUYIiL/ljT29YbzlEgu+BG1CH3cMDfbjxUYCyI1Uq0WndE0QejmFOKgK/S8ZIP6eJwah8ZZLrEFAEno5sj7AiduXHBCVAYxItal+kyeK0M89SgNU6GPC+39lX5sMyEjNbUbDfk4T+rlzL/wbDxZywJCOVJ0Dh6SSnwFN6OfQevzeu5eG6PHfvR2pNtVvOmQQerpaIULVoYqD1b98K1Itqt9jPkMQ+rmULv5TGTBsRWpukXb310xCT3fQvmgAIsTjQG1T/R7qJprQ082Ct1peJINDE7KoftM1h9DXRcJ6iPAQqebVb8vC89znrYd4iFRoTNhfcwm93VSjiYgj1bz6bXakFKG3i1w0EWH6Yb7olA4FhP7eRtBERDaaBmnyHYgI/b1voYtoCgiTpZDQ461YuoiGSsgZgzO+M3MexAEJRBF6vFD4HIh96vDgWd978o+YvFA8rEeu/N0b5RsRJvRMwZnfXfOMmH4wDKN/5PXtPK+IjBi9wPuHHhEhVH3/0O/ba/4Q+8w11wu8Q+oLMWGvuHLekvX6joAfRHjLRrnIe8A+EGHC2U52mTedPSD2eVt0LvQut3NE/raHS72t7hgxeeVy8Ak93xDtFBEO+BusBYRLv4/KukTk9TISQt8vs7hD7Is2rYgIfd9G7wqR243KCX3f9O0Gsc9aP1YlDLbdRxRvOpISeh4WHSCmEkApYdcR5RscpYTdRlTYwSkn7HJblLVBRcLu9qiSXlSdsKuISoBqhGjo714CB8UDvSZhMOxcjgoTxVtvFQmD5bhbxVQyUD0hpkqI6sUulcTpq/J5VHXCDlX9UPXIjSZhUHhtjOqIMFE9NqVLGEzGXZhHTW61DmlqEeJIvfSEP+z/lH+mBWGwBpddtkmg7vW3uoRBsPNoowwRpi/aZ/r1CYPCo41ixATqdDHmhHjh5iLbGfrf8k9zRBgM/XWqXER6I7hPQlRuxOfdPZUkSoWEQ8Ig9xaqDESYfpvesG1OiMb/0hMjiQj7b2YXwNsSoua48sPYQoT9V7MG6ILQG2ONaMtnTYgYy8hDn3NATNI3Sz4HhKg9TjP3RmLEpP9t0f4cEiItRs6NHPQHTxa3vNVyQ4iCdRY7NBI/f2L+QkFbrgiR5qWbaMVvK5m+n8GQQ0KUBVSQNpRxheckOo9ySoiUF7NxZEaJ6KLxrHCKF7gnxJosShBFmQ5lnEURKBfGqZlAPgixJvPZKMOYMs4Yw2Wj6dwHHZYvwkqTYluOACKIMoQan2ir/86qn4NxuS18wVXySrjXcljMt9NduRqNx/idAvTraFXOptt5MTG/uFVZ/wcXSzKa2W573gAAAABJRU5ErkJggg==" alt=""><lable>ایدی تلگرام: </lable></div>

                        <input  value="{{ $User->user_telegram_id }}" name="user_telegram_id" type="text" class="form-control" placeholder="ایدی تلگرام">
                    </div>
                    <button class="btn btn-outline-success btn-lg mr-5 mb-5 mt-5" style="alignment: center"  type="submit" value="اعمال تغییرات" >اعمال تغییرات</button>

                </form>
            </div>
            <br/>



            {{--{{ $orders->links() }}--}}

        </div>

    </article>
    <!-- END Main content -->



@endsection
