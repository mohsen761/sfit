@extends('coach.layout.auth')

@section('content')
    <title>ثبت نام ورزشکار</title>
    <div class="d-flex justify-content-center " style="margin-top: 7%;text-align: right;font-size: 15px">
        <div class="card">
            <div class="card-header">
                <h3>ثبت نام ورزشکار</h3>

            </div>
            <div class="card-body">
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/register') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }}">
                        <label for="name" style="text-align: right" class="text-light mr-5 control-label">نام ورزشکار</label>

                        <div class="col-xl-12">
                            <input id="name" type="text" class="form-control" name="user_name" value="{{ old('user_name') }}" autofocus>

                            @if ($errors->has('user_name'))
                                <span class="help-block text-white">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('user_family') ? ' has-error' : '' }}">
                        <label for="name" style="text-align: right" class="text-light mr-5 control-label">نام خانوادگی</label>

                        <div class="col-xl-12">
                            <input id="name" type="text" class="form-control" name="user_family" value="{{ old('user_family') }}" autofocus>

                            @if ($errors->has('user_family'))
                                <span class="help-block text-white">
                                        <strong>{{ $errors->first('user_family') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('user_email') ? ' has-error' : '' }}">
                        <label for="email"  class="text-light mr-5 control-label"> آدرس ایمیل یا آیدی</label>

                        <div class="col-xl-12">
                            <input id="email" type="text" class="form-control" name="user_email" value="{{ old('user_email') }}">

                            @if ($errors->has('user_email'))
                                <span class="help-block text-white">
                                        <strong>{{ $errors->first('user_email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="text-light mr-5 control-label">رمز عبور</label>

                        <div class="col-xl-12">
                            <input id="password" type="password" class="form-control" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block text-white">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm" class="text-light mr-5 control-label">تکرار رمز عبور</label>

                        <div class="col-xl-12">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block text-white">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary btn-lg ">
                                ثبت نام
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-center links text-light">
                    ثبت نام کرده اید؟<a href="/user/login" class="text-light">ورود ورزشکار</a>
                </div>
                <div class="d-flex justify-content-center">
                    <a href="/user/forgetpassword" class="text-light">رمز خود را فراموش کرده اید ؟</a>
                </div>
            </div>
        </div>
    </div>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Numans');

        html,body{
            /*background-image: url('http://getwallpapers.com/wallpaper/full/a/5/d/544750.jpg');*/
            background-size: cover;
            background-image: url("/images/register.jpg");
            background-repeat: no-repeat;
            height: 100%;
            font-family: 'Numans', sans-serif;
        }

        .container{
            height: 100%;
            align-content: center;
        }

        .card{
            height: 50%;
            margin-top: auto;
            margin-bottom: auto;
            width: 400px;
            background-color: rgba(0,0,0,0.5) !important;
        }

        .social_icon span{
            font-size: 60px;
            margin-left: 10px;
            color: #FFC312;
        }

        .social_icon span:hover{
            color: white;
            cursor: pointer;
        }

        .card-header h3{
            color: white;
        }

        .social_icon{
            position: absolute;
            right: 20px;
            top: -45px;
        }

        .input-group-prepend span{
            width: 50px;
            background-color: #FFC312;
            color: black;
            border:0 !important;
        }

        input:focus{
            outline: 0 0 0 0  !important;
            box-shadow: 0 0 0 0 !important;

        }

        .remember{
            color: white;
        }

        .remember input
        {
            width: 20px;
            height: 20px;
            margin-left: 15px;
            margin-right: 5px;
        }

        .login_btn{
            color: black;
            background-color: #FFC312;
            width: 100px;
        }

        .login_btn:hover{
            color: black;
            background-color: white;
        }

        .links{
            color: white;
        }

        .links a{
            margin-left: 4px;
        }
    </style>
@endsection
