<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplement extends Model
{
    protected $primaryKey='supplement_id';
    protected $table = 'supplement';
}
