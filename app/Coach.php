<?php

namespace App;

use App\Notifications\CoachResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\Coach as Authenticatable;

class Coach extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'coach_name', 'coach_email', 'password',
    ];

    protected $primaryKey = 'coach_id';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CoachResetPassword($token));
    }
}
