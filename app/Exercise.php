<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    protected $primaryKey='exercise_id';
    protected $table = 'exercises';
}
