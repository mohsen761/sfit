<?php
/**
 * Created by PhpStorm.
 * User: saeidraei
 * Date: 2/13/2018 AD
 * Time: 10:57 AM
 */
namespace App;

use Illuminate\Database\Eloquent\Model;
use Kris\LaravelFormBuilder\FormBuilder;

class BaseModel extends Model{

    protected $guarded = [];

    public function superSave($params)
    {

        $params = ary($params);
        if(!$params->form or !class_exists($params->form)){
            throw new \Exception("form is not set or is not valid");
        }
//        if(!$params->only){
//            throw new \Exception("please specify the only params");
//        }
        if(!$params->data){
            throw new \Exception("data is null");
        }
//        dd($params->all());
        /** @var FormBuilder $formBuilder */
        $formBuilder = app('laravel-form-builder');
        $form = $formBuilder->create($params->form);
        if(!$form->isValid()){
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $fields = $form->getFields();
        $only=[];
        foreach ($fields as $field){
            if($name = $field->getName()){
               $only[]= $name;
            }
        }

        $data = ary($params->data);
        $this->fill($data->only($only)->toArray());

        $this->save();

        return true;

    }

}