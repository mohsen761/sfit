<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gym extends Model
{
    protected $primaryKey='gym_id';
    protected $table = 'gym';
}
