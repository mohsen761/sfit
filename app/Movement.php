<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    protected $primaryKey='movement_id';
    protected $table = 'movements';
}
