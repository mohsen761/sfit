<?php

namespace App\Http\Controllers;

use App\coach;
use App\Gym;
use App\Movement;
use App\Working;
use Morilog\Jalali\Jalalian;
use App\Exercise;
use App\Food;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;

class GuestController extends Controller
{
    public function share()
    {
        $user=['' , ''
        ];
        return View::share('user' ,$user);
    }

    public function showgym(Gym $id)
    {
        $this->share();
        $gym = $id;
        $working = Working::where('gym_id' , $gym->gym_id)->get();
//        dd($working);
        if(sizeof($working) <1){
            $working='0';
        }
        return view('guest.showgym', compact('gym','working'));
    }
    public function gyms()
    {
        $this->share();
        $gyms =Gym::all();
        return view('guest.gyms', compact('gyms'));
    }

    public function coachdetail($id)
    {
        $this->share();
        $coach =\App\Coach::where('coach_id' , $id)->first();
        $price = Price::where('coach_id' , $id)->first();
        return view('guest.coachdetail',compact('coach','price'));
    }


    public function coaches()
    {
        $this->share();
        $coaches = \App\Coach::where('coach_visibility','show')->get();
        return view('guest.selectcoach',compact('coaches'));
    }
    public function freeprogramsshow($type , $id)
    {
        $this->share();
        if ($type =='exercise'){
            $program = Program::where('program_id' , $id )->first();
            $exercises = Exercise::where('program_id' , $id)->get();
            return view('guest.textexercise',compact('program','exercises'));
//            return view('user.freeprogramexercise');
        }
        if ($type =='food'){
            $program = Program::where('program_id' , $id )->first();
            $foods = Food::where('program_id' , $id)->get();
            return view('guest.textfood',compact('program','foods'));
        }
        if ($type == 'supplement'){
            $program = Program::where('program_id' , $id )->first();
            $supplements = Supplement::where('program_id' , $id)->get();
            return view('guest.textsupplement',compact('program','supplements'));
        }
    }

    public function freeprogramstype($type)
    {
        $this->share();
        $programs = Program::all();


        if ($type =='exercise'){
            $programs = Program::where('program_type' , 'exercise')->where('order_id' , null)->get();
        }
        if ($type =='food'){
            $programs = Program::where('program_type' , 'food')->get();
        }
        if ($type == 'supplement'){
            $programs = Program::where('program_type' , 'supplement')->get();
        }
        return view('guest.freeprogramtype',compact('programs'));
    }

    public function freeprograms()
    {
        $this->share();
        return view('guest.freeprograms');
    }

    public function showmovement($id)
    {
        $this->share();
        $item = Movement::find($id);
        return view('guest.showmovement' , compact('item'));
    }
    public function movements()
    {
        $this->share();
        $movements = Movement::paginate(10);

        return view('guest.movements',compact('movements'));
    }

    public function showpost($id)
    {
        $this->share();
        $post = \App\Post::where('post_id' , $id)->get();
        return view('guest.post',compact('post'));
    }
    public function posts()
    {
        $this->share();
        $posts = \App\Post::orderBy('post_id', 'desc')->paginate(3);
        return view('guest.home',compact('posts'));
    }
}
