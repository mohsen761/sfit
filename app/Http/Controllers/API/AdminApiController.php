<?php

namespace App\Http\Controllers\API;

use App\coach;
use App\Exercise;
use App\Food;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;

class AdminApiController
{

    public function coachCheckout(Request $request)
    {
        require_once('dbConnect.php');
        $coach_id = $_POST['coach_id'];
        $checkout_state = $_POST['checkout_state'];

        //تمام سفارش هایی را که مربی به آنها پاسخ داده را در حالت paid برده.
        //یعنی پولش را پرداخت کرده ایم و دیگر نمیتواند درخواست تسویه حساب برای سفارش های قبلی را دهد.
        $sql = "UPDATE orders SET payment_state = 'paid' WHERE coach_id='$coach_id' AND payment_state = 'unpaid' AND program_state = 'sent'";

        if (mysqli_query($con, $sql)) {

            //در این قسمت هم وضعیت پرداخت به مربی را به حالت پرداخت شده بروزرسانی میکنیم.
            $sql_update = "UPDATE checkout SET checkout_state = 'paid' WHERE coach_id='$coach_id' AND checkout_state = 'unpaid'";

            if (mysqli_query($con, $sql)) {
                echo "grant";
            } else {
                echo "error";
            }
        }
    }

      public function coaches(Request $request)
    {
        require_once('dbConnect.php');
        $sql = "SELECT * FROM coaches";
        $result = $con->query($sql);
        $coaches = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['coach_id'] = $row['coach_id'];
                $coachId = $row['coach_id'];
                $name = $row['coach_name'];
                $family = $row['coach_family'];
                $temp['full_name'] = $name . " " . $family;
                $temp['coach_profile'] = "http://sfit.ir/bodybuilding/coach/images/coachesImagesProfile/" . $coachId . ".png";
                $temp['device_id'] = $row['device_id'];
                $temp['coach_access'] = $row['coach_access'];
                $temp['coach_visibility'] = $row['coach_visibility'];

                array_push($coaches, $temp);
            }
        }
        echo json_encode($coaches);
        mysqli_close($con);
    }

      public function coachInformation(Request $request)
    {
        require_once('dbConnect.php');
        $coach_id = $_POST['coach_id'];
        $checkout_state = $_POST['checkout_state'];

        //تمام سفارش هایی را که مربی به آنها پاسخ داده را در حالت paid برده.
        //یعنی پولش را پرداخت کرده ایم و دیگر نمیتواند درخواست تسویه حساب برای سفارش های قبلی را دهد.
        $sql = "UPDATE orders SET payment_state = 'paid' WHERE coach_id='$coach_id' AND payment_state = 'unpaid' AND program_state = 'sent'";

        if (mysqli_query($con, $sql)) {

            //در این قسمت هم وضعیت پرداخت به مربی را به حالت پرداخت شده بروزرسانی میکنیم.
            $sql_update = "UPDATE checkout SET checkout_state = 'paid' WHERE coach_id='$coach_id' AND checkout_state = 'unpaid'";

            if (mysqli_query($con, $sql)) {
                echo "grant";
            } else {
                echo "error";
            }
        }
    }

      public function delete_file(Request $request)
    {
        require_once('dbConnect.php');
        $directory = "images/post/1.png";
        if(unlink($directory.$_GET['file']))
            echo "File Deleted";
    }

      public function get_post_comments(Request $request)
    {
        require_once('dbConnect.php');
        $post_id = $_GET['post_id'];

        //نظرهای مربوط به پست مورد نظر را از جدول post_like_comment به دست می آورد.
        $req = "SELECT * FROM post_like_comment WHERE post_id = '$post_id'";
        $findComments = mysqli_query($con, $req);
        $comments = array();
        if (mysqli_num_rows($findComments) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($findComments)) {

                if ($row['comment'] != null){
                    array_push($comments, array(
                            "post_comment" => $row['comment']
                        )
                    );
                }
            }
        }
        echo json_encode($comments);

        mysqli_close($con);
    }

      public function getCheckouts()
    {
        require_once('dbConnect.php');
        $checkout = array();
        $coachesHaveCheckout = array();
        $coachNames = array();
        $coachFamilies = array();
        $coachDeviceId = array();

        //آیدی همه ی مربی هایی که درخواست تسویه حساب داده اند را در میاوریم.
        $sql = "SELECT * FROM checkout";
        $findCoachHaveCheckout = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_assoc($findCoachHaveCheckout)) {

            array_push($coachesHaveCheckout, $row['coach_id']);
        }

        //اسم و فامیلی و شناسه ی دستگاه مربی هایی را که درخواست تسویه حساب داده اند را از جول coaches به دست آوریم.
        for ($i = 0; $i < sizeof($coachesHaveCheckout); $i++) {

            $query = "SELECT * FROM coaches WHERE coach_id = ($coachesHaveCheckout[$i])";
            $findCoachFullName = mysqli_query($con, $query);

            if ($findCoachFullName->num_rows > 0) {
                while ($row = mysqli_fetch_assoc($findCoachFullName)) {

                    array_push($coachNames, $row['coach_name']);
                    array_push($coachFamilies, $row['coach_family']);
                    array_push($coachDeviceId, $row['device_id']);

                }
            }
        }

        //لیستی از همه ی مربی هایی را که درخواست تسویه حساب داده اند را به همراه اسم و فامیلی و شناسه دستگاه آن ها به ادمین نمایش میدهیم.
        $fetch = "SELECT * FROM checkout";
        $fetchCheckout = mysqli_query($con, $fetch);

        $i = 0;

        while ($row = mysqli_fetch_assoc($fetchCheckout)) {

            array_push($checkout, array(
//            "checkout_id" => $row['checkout_id'],
                "coach_id" => $row['coach_id'],
                "amount" => $row['amount'],
                "checkout_date" => $row['checkout_date'],
                "number_of_orders" => $row['number_of_orders'],
                "checkout_state" => $row['checkout_state'],
                "device_id" => $coachDeviceId[$i],
                "coach_full_name" => $coachNames[$i] . " " . $coachFamilies[$i]
            ));
            $i++;
        }

        echo json_encode($checkout);

        mysqli_close($con);
    }

      public function getPosts()
    {
        require_once('dbConnect.php');

        $key_post_id = "post_id";
        $key_post_title = "post_title";
        $key_post_description = "post_description";
        $key_post_image = "post_image";
        $key_post_like = "post_like";
        $key_post_comment = "post_comment";
        $key_post_link = "post_link";

        if ($con->connect_error) {
            die("Connection failed: " . $con->connect_error);
        }

        $posts = array();
//همه ی پست ها را از جدول post استخراج کرده و به همراه عکس آن ها به صورت لیست میفرستیم.
          $sql = "SELECT * FROM post";
          $execute = mysqli_query($con, $sql);

          if ($execute->num_rows > 0) {
              $i = 0;
              while ($row = mysqli_fetch_assoc($execute)) {

                  array_push($posts, array(
                      $key_post_id => $row[$key_post_id],
                      $key_post_title => $row[$key_post_title],
                      $key_post_description => $row[$key_post_description],
                      $key_post_image => "http://sfit.ir/bodybuilding/admin/images/post/$row[$key_post_id].png",
                      $key_post_like => $row[$key_post_like],
                      $key_post_comment => $row[$key_post_comment],
                      $key_post_link => $row[$key_post_link],
                  ));

                  $i++;

              }
              echo json_encode($posts);

              mysqli_close($con);
          }
    }

      public function getSubscriptionInformation(Request $request)
    {
        require_once('dbConnect.php');
        $coach_id = $_GET['coach_id'];

        //این کدها برای نمایش اشتراک ماهیانه و سالیانه مربیانی است که اشتراک برنامه را خریداری کرده اند.
        //این ویژگی فعلا پیاده سازی نشده!
        //get coach information from table subscription
        $get = "SELECT * FROM subscription WHERE coach_id = '$coach_id'";
        $getInformation = mysqli_query($con, $get);
        $subscription = array();
        if (mysqli_num_rows($getInformation) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($getInformation)) {

                array_push($subscription,array(
                    "start_date" => $row['start_date'],
                    "end_date" => $row['end_date'],
                    "subscribe_type" => $row['subscribe_type'],
                    "subscribe_price" => $row['subscribe_price'],
                ));
            }
        }
        echo json_encode($subscription);

        mysqli_close($con);
    }

    public function getUserInformation(Request $request)
    {
        require_once('dbConnect.php');
        $user_id = $_GET['user_id'];

        //تمام اطلاعات مربوط به شاگرد مورد نظر را از جدول users در می آورد.
        $get = "SELECT * FROM users WHERE user_id = '$user_id'";
        $getInformation = mysqli_query($con, $get);
        $UserInformation = array();
        if (mysqli_num_rows($getInformation) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($getInformation)) {

                array_push($UserInformation, array(

                        "user_id" => $row['user_id'],
                        "device_id" => $row['device_id'],
                        "user_name" => $row['user_name'],
                        "user_family" => $row['user_family'],
                        "user_sex" => $row['user_sex'],
                        "user_rel" => $row['user_rel'],
                        "user_age" => $row['user_age'],
                        "user_height" => $row['user_height'],
                        "user_weight" => $row['user_weight'],
                        "user_record" => $row['user_record'],
                        "user_level" => $row['user_level'],
                        "user_state" => $row['user_state'],
                        "user_city" => $row['user_city'],
                        "user_address" => $row['user_address'],
                        "user_job" => $row['user_job'],
                        "user_cigarette" => $row['user_cigarette'],
                        "user_tea" => $row['user_tea'],
                        "user_sleep" => $row['user_sleep'],
                        "user_stress" => $row['user_stress'],
                        "user_energy" => $row['user_energy'],
                        "user_blood" => $row['user_blood'],
                        "user_taste" => $row['user_taste'],
                        "user_waist" => $row['user_waist'],
                        "user_arm" => $row['user_arm'],
                        "user_neck" => $row['user_neck'],
                        "user_thigh" => $row['user_thigh'],
                        "user_leg" => $row['user_leg'],
                        "user_chest" => $row['user_chest'],
                        "user_workout" => $row['user_workout_a_week'],
                        "user_disease" => $row['user_disease'],
                        "user_other" => $row['user_other_info'],
                        "user_mobile" => $row['user_mobile'],
                        "user_telegram_id" => $row['user_telegram_id'],
                        "email" => $row['user_email'],
                        "password" => $row['password'],
                        "user_visibility" => $row['user_visibility'],
                        "user_score" => $row['user_score']
                    )
                );
            }
        }
        echo json_encode($UserInformation);

        mysqli_close($con);
    }

    public function getUsersInformation(Request $request)
    {
        require_once('dbConnect.php');
        $users = array();

        //لیستی از اطلاعات همه ی شاگردان را به همراه عکس پروفایل آنها در می آوریم.
        $id = "SELECT * FROM users";
        $findUsersInformation = mysqli_query($con, $id);

        if ($findUsersInformation->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($findUsersInformation)) {

                $userName = $row['user_name'];
                $userFamily = $row['user_family'];

                array_push($users, array(
                    "user_full_name" => $userName . " " . $userFamily,
                    "user_id" => $row['user_id'],
                    "user_profile_photo" => "http://sfit.ir/bodybuilding/student/images/UsersImagesProfile/" . $row['user_id'] . ".png",
                    "user_telegram_id" => $row['user_telegram_id'],
                    "user_push_id" => $row['device_id']
                ));

            }
            echo json_encode($users);

            mysqli_close($con);
        }
    }

    public function gyms(Request $request)
    {
        require_once('dbConnect.php');

        $sql = "SELECT * FROM gym";
        $result = $con->query($sql);
        $gyms = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $gymId = $row['gym_id'];
                $temp['gym_id'] = $row['gym_id'];
                $temp['gym_name'] = $row['gym_name'];
                $temp['gym_type'] = $row['gym_type'];
                $temp['gym_state'] = $row['gym_state'];
                $temp['gym_city'] = $row['gym_city'];
                $temp['gym_address'] = $row['gym_address'];
                $temp['gym_monthly_fee'] = $row['gym_monthly_fee'];
                $temp['gym_services'] = $row['gym_services'];
                $temp['gym_students_number'] = $row['gym_students_number'];
                $temp['gym_score'] = $row['gym_score'];
                $temp['gym_visibility'] = $row['gym_visibility'];
                $temp['gym_image'] = "http://192.168.1.4/bodybuilding/gym/images/gymImage/$gymId/" . 0 . ".png";

                array_push($gyms, $temp);

            }

        }
        echo json_encode($gyms);
    }

    public function keys(Request $request)
    {
        require_once('dbConnect.php');
        $key_post_id = "post_id";
        $key_post_title = "post_title";
        $key_post_description = "post_description";
        $key_post_image = "post_image";
        $key_post_like = "post_like";
        $key_post_comment = "post_comment";
        $key_post_link = "post_link";
    }

    public function order(Request $request)
    {
        require_once('dbConnect.php');
        //لیستی از همه ی سفارش های ثبت شده از سوی شاگردان
        $reg = "SELECT * FROM orders";
        $findOrders = mysqli_query($con, $reg);
        $request = array();
        if (mysqli_num_rows($findOrders) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($findOrders)) {

                array_push($request, array(

                        "order_id" => $row['order_id'],
                        "user_id" => $row['user_id'],
                        "coach_id" => $row['coach_id'],
                        "exercise" => $row['exercise'],
                        "food" => $row['food'],
                        "supplement" => $row['supplement'],
                        "goal" => $row['goal'],
                        "price" => $row['price'],
                        "order_date" => $row['order_date'],
                        "user_payment_state" => $row['user_payment_state'],
                        "token" => $row['token']
                    )
                );
            }
        }
        echo json_encode($request);

        mysqli_close($con);
    }

    public function set_like_comment(Request $request)
    {
        require_once('dbConnect.php');
        $user_id = $_POST['user_id'];
        $post_id = $_POST['post_id'];
        $like_state = $_POST['post_like'];
        $comment = $_POST['post_comment'];

        //اگر کاربر لایک کرده بود
        if ($like_state != null) {
            // چک کن ببین قبلا لایک کرده اون پست مورد نظر رو یا نه
            $check = "SELECT post_id FROM post_like_comment WHERE user_id='$user_id' AND post_id='$post_id'";
            //executing query
            $result = mysqli_query($con, $check);
            //اگه قبلا لایک کرده بود لایکشو بردار اگه لایک نکرده بود لایک بشه
            if (mysqli_num_rows($result) > 0) {
                $sql = "UPDATE post_like_comment SET like_state = '$like_state' WHERE post_id='$post_id' and user_id='$user_id'";
                if (mysqli_query($con, $sql)) {
                    //اگه کاربر لایک کرده بود
                    if ($like_state == "liked") {
                        //یکی به تعداد کل لایک های اون پست در جدول post اضافه بشه
                        $getLikeNumber = $con->query(
                            "SELECT post_like postLike " .
                            "FROM post WHERE post_id ='$post_id'");
                        $fetchLike = $getLikeNumber->fetch_assoc();
                        $postLikeNumber = $fetchLike['postLike'];

                        $postLikeNumber += 1;
                        $sql_update = "UPDATE post SET post_like = $postLikeNumber WHERE post_id='$post_id'";

                        if (mysqli_query($con, $sql_update)) {
                            echo "liked";
                        } else {
                            echo "error";
                        }
                        //اگه کاربر لایکشو میخواست برداره
                    } else if ($like_state == "unlike") {
                        //یکی از تعداد کل لایک های اون پست در جدول post کم بشه
                        $getLikeNumber = $con->query(
                            "SELECT post_like postLike " .
                            "FROM post WHERE post_id ='$post_id'");
                        $fetchLike = $getLikeNumber->fetch_assoc();
                        $postLikeNumber = $fetchLike['postLike'];

                        $postLikeNumber -= 1;
                        $sql_update = "UPDATE post SET post_like = $postLikeNumber WHERE post_id='$post_id'";

                        if (mysqli_query($con, $sql_update)) {
                            echo "unlike";
                        } else {
                            echo "error";
                        }
                    }
                } else {
                    echo "error";
                }
            } else {
                //اگه برای اولین بار بود که میخواست لایک کنه بیا یه ردیف جدید ایجاد کن
                $sql = "INSERT INTO post_like_comment (post_id,user_id,like_state)
                                VALUES ('$post_id','$user_id','$like_state')";
                if (mysqli_query($con, $sql)) {
                    //یکی به تعداد کل لایک های اون پست در جدول post اضافه بشه
                    $getLikeNumber = $con->query(
                        "SELECT post_like postLike " .
                        "FROM post WHERE post_id ='$post_id'");
                    $fetchLike = $getLikeNumber->fetch_assoc();
                    $postLikeNumber = $fetchLike['postLike'];

                    $postLikeNumber += 1;
                    $sql_update = "UPDATE post SET post_like = $postLikeNumber WHERE post_id='$post_id'";

                    if (mysqli_query($con, $sql_update)) {
                        echo "liked";
                    } else {
                        echo "error";
                    }
                } else {
                    echo "error";
                }
            }
            //اگه کاربر نظر میخواست بفرسته
        } else if ($comment != null) {
            //چک کن ببین قبلا راجع به اون پست نظر داده یا نه
            $check = "SELECT post_id FROM post_like_comment WHERE user_id='$user_id' AND post_id='$post_id'";
            //executing query
            $result = mysqli_query($con, $check);
            //اگه قبلا نظر داده بود
            if (mysqli_num_rows($result) > 0) {
                //نظر جدید رو جایگزین نظر قبلیش کن
                $sql = "UPDATE post_like_comment SET comment = '$comment' WHERE post_id='$post_id' and user_id='$user_id'";
                if (mysqli_query($con, $sql)) {
                    echo "successful";
                } else {
                    echo "error";
                }
                //اگه اولین بار بود که میخواست نظر بده
            } else {
                //بیا یه نظر جدید رو تو جدول post_like_comment ثبت کن
                $sql = "INSERT INTO post_like_comment (post_id,user_id,comment)
                                VALUES ('$post_id','$user_id','$comment')";
                if (mysqli_query($con, $sql)) {
                    //یکی هم به تعداد کل نظرهای داده شده راجع به اون پست اضافه کن
                    $getCommentNumber = $con->query(
                        "SELECT post_comment postComment " .
                        "FROM post WHERE post_id ='$post_id'");
                    $fetchComment = $getCommentNumber->fetch_assoc();
                    $postCommentNumber = $fetchComment['postComment'];

                    $postCommentNumber += 1;
                    $sql_update = "UPDATE post SET post_comment = $postCommentNumber WHERE post_id='$post_id'";

                    if (mysqli_query($con, $sql_update)) {
                        echo "successful";
                    } else {
                        echo "error";
                    }
                } else {
                    echo "error";
                }
            }
        }
    }

    public function setCoachAccess(Request $request)
    {
        require_once('dbConnect.php');
        $coach_id = $_POST['coach_id'];
        $coach_access = $_POST['coach_access'];

        // دادن یا ندادن دسترسی به مربی برای استفاده از برنامه
        $sql = "UPDATE coaches SET coach_access = '$coach_access' WHERE coach_id='$coach_id'";
        mysqli_query($con, $sql);

        if (mysqli_query($con, $sql)) {
            if ($coach_access == "yes"){
                echo "grant";
            }else if ($coach_access == "no"){
                echo "ban";
            }
        } else {
            echo "error";
        }
    }

    public function setCoachVisibility(Request $request)
    {
        require_once('dbConnect.php');
        $coach_id = $_POST['coach_id'];
        $coach_visibility = $_POST['coach_visibility'];

        //نمایش دادن یا پنهان کردن مربی از لیست مربیان
        $sql = "UPDATE coaches SET coach_visibility = '$coach_visibility' WHERE coach_id='$coach_id'";
        mysqli_query($con, $sql);

        if (mysqli_query($con, $sql)) {
            if ($coach_visibility == "show"){
                echo "show";
            }else if ($coach_visibility == "hide"){
                echo "hide";
            }
        } else {
            echo "error";
        }
    }

    public function setGymVisibility(Request $request)
    {
        require_once('dbConnect.php');
        $gym_id = $_POST['gym_id'];
        $gym_visibility = $_POST['gym_visibility'];

        //نمایش دادن یا پنهان کردن باشگاه از لیست باشگاه ها
        $sql = "UPDATE gym SET gym_visibility = '$gym_visibility' WHERE gym_id='$gym_id'";
        mysqli_query($con, $sql);

        if (mysqli_query($con, $sql)) {
            if ($gym_visibility == "show"){
                echo "show";
            }else if ($gym_visibility == "hide"){
                echo "hide";
            }
        } else {
            echo "error";
        }
    }

    public function uploadPost(Request $request)
    {
        require_once('dbConnect.php');
        $objFile = &$_FILES["image"];
        $post_title = $_POST['post_title'];
        $post_description = $_POST['post_description'];

        //آخرین post_id را از جدول post درآورده و یکی به آن اضافه میکنیم.
        $sql = "SELECT post_id FROM post ORDER BY post_id DESC LIMIT 1";
        $result = $con->query($sql);

        $fetchId = $result->fetch_assoc();
        $postId = $fetchId['post_id'];
        $postId++;

        $path = "images/post/$postId.png";

        //عکس پست مورد نظر را در مسیر بالایی قرار میدهیم.
        if (move_uploaded_file($objFile["tmp_name"], $path)) {
            print "عکس فرستاده شد. ";
        } else {
            print "عکس فرستاده نشد ";
        }

        //پست مورد نظر را در جدول ثبت میکنیم.
        $sql = "INSERT INTO post (post_title,post_description)
            VALUES ('$post_title','$post_description')";

        if (mysqli_query($con, $sql)) {
            echo "پست فرستاده شد";
        } else {
            echo "پست فرستاده نشد. دوباره امتحان کن";
        }
    }


}