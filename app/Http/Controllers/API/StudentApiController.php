<?php

namespace App\Http\Controllers\API;

use App\coach;
use App\Exercise;
use App\Food;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;
use SoapClient;

class StudentApiController
{

    public function check_user_have_order()
    {
        require_once('dbConnect.php');
        $userId = $_POST['user_id'];

        //چک کن ببین شاگرد تا الان درخواستی رو ثبت کرده یا نه
        $checkOrder = "SELECT order_id FROM orders WHERE user_id='$userId'";
        //executing query
        $result = mysqli_query($con, $checkOrder);
        //fetching result
        $check = mysqli_fetch_array($result);
        //if we got some result
        if (isset($check)) {
            echo "successful";
        } else {
            echo "failed";
        }
        mysqli_close($con);

    }

    public function check_user_order_image()
    {

        require_once('dbConnect.php');


        $userId = $_GET['user_id'];
        $date = $_GET['date'];

        $images = array();

        //عکس های فیگوری فرستاده شده توسط شاگرد در سفارش مربوطه را پیدا کن و بفرست
        if (file_exists("images/joft_bazu_az_jolo/" . $userId . "/" . $date . ".png")) {
            array_push($images, "http://sfit.ir/bodybuilding/student/images/joft_bazu_az_jolo/" . $userId . "/" . $date . ".png");
        }
        if (file_exists("images/joft_bazu_az_posht/" . $userId . "/" . $date . ".png")) {
            array_push($images, "http://sfit.ir/bodybuilding/student/images/joft_bazu_az_posht/" . $userId . "/" . $date . ".png");
        }
        if (file_exists("images/zir_baghal_az_jolo/" . $userId . "/" . $date . ".png")) {
            array_push($images, "http://sfit.ir/bodybuilding/student/images/zir_baghal_az_jolo/" . $userId . "/" . $date . ".png");
        }
        if (file_exists("images/zir_baghal_az_posht/" . $userId . "/" . $date . ".png")) {
            array_push($images, "http://sfit.ir/bodybuilding/student/images/zir_baghal_az_posht/" . $userId . "/" . $date . ".png");
        }
        if (file_exists("images/posht_bazu_az_baghal/" . $userId . "/" . $date . ".png")) {
            array_push($images, "http://sfit.ir/bodybuilding/student/images/posht_bazu_az_baghal/" . $userId . "/" . $date . ".png");
        }
        if (file_exists("images/ghafase_sine/" . $userId . "/" . $date . ".png")) {
            array_push($images, "http://sfit.ir/bodybuilding/student/images/ghafase_sine/" . $userId . "/" . $date . ".png");
        }
        if (file_exists("images/shekam_va_paha_az_jolo/" . $userId . "/" . $date . ".png")) {
            array_push($images, "http://sfit.ir/bodybuilding/student/images/shekam_va_paha_az_jolo/" . $userId . "/" . $date . ".png");
        }
        if (file_exists("images/nimrokh/" . $userId . "/" . $date . ".png")) {
            array_push($images, "http://sfit.ir/bodybuilding/student/images/nimrokh/" . $userId . "/" . $date . ".png");
        }
        echo json_encode($images);
        mysqli_close($con);

    }

    public function chooseCoach()
    {

        require_once('dbConnect.php');

        $email = $_POST['email'];
        $coachId = $_POST['coach_id'];

        //انتخاب مربی توسط شاگرد که در جدول users مشخص میشود
        $sql = "UPDATE users SET coach_id = '$coachId' WHERE user_email='$email'";

        if (mysqli_query($con, $sql)) {
            echo "successful";
        } else {
            echo "failed";
        }
        mysqli_close($con);
    }

    public function coaches()
    {
        require_once('dbConnect.php');


        $sql = "SELECT * FROM coaches WHERE coach_visibility='show'";
        $result = $con->query($sql);
        $coaches = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['coach_id'] = $row['coach_id'];
                $coachId = $row['coach_id'];
                $name = $row['coach_name'];
                $family = $row['coach_family'];
                $temp['full_name'] = $name . " " . $family;
                $temp['coach_profile'] = "http://sfit.ir/bodybuilding/coach/images/coachesImagesProfile/" . $coachId . ".png";
                $temp['coach_email'] = $row['coach_email'];
                $temp['coach_level'] = $row['coach_level'];
                $temp['have_story'] = $row['have_story'];

                array_push($coaches, $temp);

            }

        }
        echo json_encode($coaches);
        mysqli_close($con);
    }

    public function coachesInformation()
    {
        require_once('dbConnect.php');

        $coachId = $_GET['coach_id'];

        //قیمت های برنامه تمرینی، غذایی و مکملی مربی را پیدا کن
        $sql_price = "SELECT * FROM price WHERE coach_id = '$coachId'";
        $result = $con->query($sql_price);
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $exercisePrice = $row['exercise_price'];
                $foodPrice = $row['food_price'];
                $supplementPrice = $row['supplement_price'];
                $discount = $row['discount'];
                $event = $row['event'];
            }

        }

        //قیمت های پیدا شده را به همراه مشخصات مربی همه رو باهم بفرست.
        $sql = "SELECT * FROM coaches WHERE coach_id = '$coachId'";
        $result = $con->query($sql);
        $coaches = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $coachName = $row['coach_name'];
                $coachFamily = $row['coach_family'];
                $temp['coach_full_name'] = $coachName . " " . $coachFamily;
                $temp['coach_score'] = $row['coach_score'];
                $temp['coach_honors'] = $row['coach_honors'];
                $temp['coach_age'] = $row['coach_age'];
                $temp['coach_record'] = $row['coach_record'];
                $temp['coach_state'] = $row['coach_state'];
                $temp['exercise_price'] = $exercisePrice;
                $temp['food_price'] = $foodPrice;
                $temp['supplement_price'] = $supplementPrice;
                $temp['discount'] = $discount;
                $temp['event'] = $event;
                $temp['coach_profile'] = "http://sfit.ir/bodybuilding/coach/images/coachesImagesProfile/" . $coachId . ".png";
                $temp['coach_id'] = $row['coach_id'];
                $temp['coach_instagram'] = $row['coach_instagram'];
                $temp['coach_level'] = $row['coach_level'];
                array_push($coaches, $temp);
            }

        }
        echo json_encode($coaches);

        mysqli_close($con);
    }

    public function coachImages()
    {
        require_once('dbConnect.php');

        $coachId = $_GET['coach_id'];

        //سه تا عکس مربی را از مسیر مشخص شده پیدا کن و بفرست
        $result = array();

        for ($i = 0; $i <= 2; $i++) {
            array_push($result, "http://sfit.ir/bodybuilding/coach/images/coachesImages/$coachId/" . $i . ".png");
        }

        echo json_encode($result);

        mysqli_close($con);
    }

    public function confirm(Request $request)
    {

            require_once('dbConnect.php');

            $PIN = '7QossyDU6p0s70mfibeS';
            $wsdl_url = "https://pec.shaparak.ir/NewIPGServices/Confirm/ConfirmService.asmx?WSDL";

            $Token = $request->Token;
            $status = $request->status;
            $OrderId = $request->OrderId;
            $TerminalNo = $request->TerminalNo;
            $Amount = $request->Amount;
            $RRN = $request->RRN;
            // این اطلاعات را بر اساس order id ذخیره کنید و بعدا توسط یک فایل php دیگه در اون اکتویتی pay res به کاربر نمایش بدید

//            echo  '<pre>';
//            print_r($_POST);// چاپ اطلاعات برگشتی از درگاه پرداخت
//            echo '</pre>';

//            $sql_update = "UPDATE orders SET token = '$Token' WHERE order_id='$OrderId'";

//            if (mysqli_query($con, $sql_update)) {
//                header('Location: http://sfit.ir/pay/failed/');
//            }
            //header('Location: app://sfit.ir/pay/res/');// این خط باعث میشود کاربر از مرورگر مستقیما به اپ شما هدایت شود.

            if ($RRN > 0 && $status == 0) {

                $params = array(
                    "LoginAccount" => $PIN,
                    "Token" => $Token
                );

                $client = new SoapClient ($wsdl_url);

                try {
                    $result = $client->ConfirmPayment(
                        array(
                            "requestData" => $params
                        )
                    );
                    if ($result->ConfirmPaymentResult->Status != '0') {
                        $msg = "(<strong> کد خطا : " . $result->ConfirmPaymentResult->Status . "</strong>) " .
                            $result->ConfirmPaymentResult->Message;


                        $sql_update = "UPDATE orders SET token = '$Token' WHERE order_id='$OrderId'";

                        if (mysqli_query($con, $sql_update)) {
//                            header('Location: http://sfit.ir/user/pay/failed/');
                            return redirect()->route('failed')->with('message' ,$msg );
                        }

                    } else {
                        $msg = 'SUCCESS<br/>Token:' . $Token . '<br/>Status:' . $status . '<br/>OrderID:' . $OrderId . '<br/>Terminal No:' . $TerminalNo . '<br/>Amount:' . $Amount . '<br/>RNN:' . $RRN . '<br/>Confirm Result:<pre>' . print_r($result->ConfirmPaymentResult) . '</pre>';

            //            echo '<pre>';
            //            print_r($result->ConfirmPaymentResult);// چاپ اطلاعات تایید شده درگاه
            //            echo '</pre>';
                        //todo: save data to db

                        $sql_update = "UPDATE orders SET user_payment_state = 'paid',token = '$Token' WHERE order_id='$OrderId'";

                        if (mysqli_query($con, $sql_update)) {
                            return redirect()->route('success')->with('message' ,$msg );
//                            header('Location: http://sfit.ir/pay/res/',true,);
                        }

                    }
                } catch (Exception $ex) {
                    $msg = $ex->getMessage();
                    //todo: save data to db

                    $sql_update = "UPDATE orders SET token = '$Token' WHERE order_id='$OrderId'";

                    if (mysqli_query($con, $sql_update)) {
//                        header('Location: http://sfit.ir/pay/failed/');
                        return redirect()->route('failed')->with('message' ,$msg );
                    }

                }
            } elseif ($status) {
                $msg = "کد خطای ارسال شده از طرف بانک $status " . "";
                //todo: save data to db

                $sql_update = "UPDATE orders SET token = '$Token' WHERE order_id='$OrderId'";

                if (mysqli_query($con, $sql_update)) {
//                    header('Location: http://sfit.ir/pay/failed/');
                    return redirect()->route('failed')->with('message' ,$msg );
                }

            } else {
                $msg = "پاسخی از سمت بانک ارسال نشد ";
                //todo: save data to db

                $sql_update = "UPDATE orders SET token = '$Token' WHERE order_id='$OrderId'";

                if (mysqli_query($con, $sql_update)) {
//                    header('Location: http://sfit.ir/pay/failed/');
                    return redirect()->route('success')->with('message' ,$msg );
                }
            }

    }

    public function figure_exercise_images()
    {

        require_once('dbConnect.php');

        //    $image = $_POST['image'];
            $objFile = &$_FILES["image"];
            $userId = $_POST['user_id'];
            $imageName = $_POST['img_name'];
            $date = $_POST['date'];

            //عکس های فیگوری فرستاده شده از طرف شاگرد را بگیر رو در مسیر مشخص شده قرار بده.
            $dir = "bodybuilding/student/images/$imageName/$userId";
        if (!file_exists($dir)){
            mkdir($dir);
        }

            $path = "$dir/$date.png";

            if (move_uploaded_file($objFile["tmp_name"], $path)) {
                print "successful";
            } else {
                print "failed";
            }

        //    $success = file_put_contents($path, base64_decode($image));
        //
        //    if(!$success){
        //        echo "Error";
        //    }
        //    else{
        //        echo "Successfully Uploaded";
        //    }
            mysqli_close($con);

    }

    public function findMyCoachImageProfile()
    {
        require_once('dbConnect.php');

        $email = $_GET['email'];
        $coachId = "";

        //آیدی مربی شاگرد را پیدا کن
        $id = "SELECT * FROM users WHERE user_email ='$email'";
        $findId = mysqli_query($con, $id);
        while ($row = mysqli_fetch_assoc($findId)) {
            $coachId = $row['coach_id'];
        }

        if ($coachId == null) {
            echo 'error';
            //اگه مربی رو انتخاب کرده بود اطلاعاتش رو بفرست
        } else {
            $result = array();
            $cid = "SELECT * FROM coaches WHERE coach_id ='$coachId'";
            $findCid = mysqli_query($con, $cid);
            while ($row = mysqli_fetch_assoc($findCid)) {

                array_push($result, array(
                        "coach_id" => $coachId,
                        "coach_name" => $row['coach_name'],
                        "coach_family" => $row['coach_family'],
                        "path" => "http://sfit.ir/bodybuilding/coach/images/coachesImagesProfile/$coachId.png"
                    )
                );
            }
            echo json_encode($result);
        }
        mysqli_close($con);

    }

    public function findUserImageProfile()
    {

        require_once('dbConnect.php');

        $userId = $_GET['user_id'];

        //عکس پروفایل شاگرد را از مسیر زیر پیدا کن و بفرست
        $path = "http://sfit.ir/bodybuilding/student/images/UsersImagesProfile/$userId.png";

        echo $path;

        mysqli_close($con);


    }

    public function forceUpdate()
    {
        require_once('dbConnect.php');

        $update = array();

        //اگه پیغامی نخوایم نشون داده بشه مقدارش رو خالی می گذاریم.
        array_push($update, array(

                "force_update" => "yes",
                "latestVersion" => "1.5.9",
//            "messages" => "سرور در حال بروزرسانی است.\nاز صبر و شکیبایی شما سپاسگذاریم."
                "messages" => ""
            )
        );
        echo json_encode($update);
    }

    public function forget_password()
    {
        require_once('dbConnect.php');
        $email = $_POST['email'];
        $device_id = $_POST['device_id'];

        //چک کن ببین همچین ایمیل یا آیدیی وجود داره یا نه
        $checkEmail = "SELECT * FROM users WHERE user_email='$email'";
        $result = mysqli_query($con, $checkEmail);
        $check = mysqli_fetch_array($result);
        if (isset($check)) {

            if (mysqli_num_rows($result) > 0) {
                while ($row = mysqli_fetch_assoc($result)) {
                    $dId = $row['device_id'];
                }
            }
            if ($dId == $device_id) {
                echo "successful";
            } else {
                echo "error_device_id";
            }
        } else {
            echo "همچین ایمیل یا آیدیی وجود ندارد.";
        }
        mysqli_close($con);
    }

    public function get_items_count()
    {
        require_once('dbConnect.php');
        $user_id = $_GET['user_id'];

        //تعداد سفارش های شاگرد را پیدا کن و بفرست
        $counter = $con->query(
            "SELECT COUNT(order_id) orders_count " .
            "FROM orders WHERE user_id ='$user_id'");
        $fetchOrder = $counter->fetch_assoc();
        $orders_count = $fetchOrder['orders_count'];

        //تعداد باشگاه های وارد شده توسط شاگرد را پیدا کن و بفرست
        $counter = $con->query(
            "SELECT COUNT(gym_id) my_gyms_count " .
            "FROM gym WHERE user_id ='$user_id'");
        $fetchMyGyms = $counter->fetch_assoc();
        $my_gyms_count = $fetchMyGyms['my_gyms_count'];

        //تعداد کل باشگاه ها را پیدا کن و بفرست
        $counter = $con->query(
            "SELECT COUNT(gym_id) gyms_count " .
            "FROM gym WHERE gym_visibility ='show'");
        $fetchGyms = $counter->fetch_assoc();
        $gyms_count = $fetchGyms['gyms_count'];

        //تعداد کل مربی ها را پیدا کن و بفرست
        $counter = $con->query(
            "SELECT COUNT(coach_id) coaches_count " .
            "FROM coaches WHERE coach_visibility ='show'");
        $fetchCoaches = $counter->fetch_assoc();
        $coaches_count = $fetchCoaches['coaches_count'];

        //تعداد کل حرکت ها را پیدا کن و بفرست
        $counter = $con->query(
            "SELECT COUNT(movement_id) movement_count " .
            "FROM movements");
        $fetchMovements = $counter->fetch_assoc();
        $workout_count = $fetchMovements['movement_count'];

        //تعداد برنامه های فرستاده شده از سوی مربیان را پیدا کن و بفرست
        $counter = $con->query(
            "SELECT COUNT(order_id) order_count " .
            "FROM orders WHERE user_id ='$user_id' AND new_order ='yes' AND program_state ='sent'");
        $fetchCoachProgram = $counter->fetch_assoc();
        $coach_program_count = $fetchCoachProgram['order_count'];

        $array = array("orders_count" => $orders_count,
            "my_gyms_count" => $my_gyms_count,
            "gyms_count" => $gyms_count,
            "coaches_count" => $coaches_count,
            "workout_count" => $workout_count,
            "coach_program_count" => $coach_program_count
        );
        echo json_encode($array);
        mysqli_close($con);
    }

    public function get_nutrition()
    {
        require_once('dbConnect.php');

        $category = $_GET['category'];
        $nutrition = array();

        if ($category == "") {
            //لیستی از همه ی غذاها با کالریشون را پیدا کن و بفرست.
            $sql = "SELECT * FROM nutrition";
            $result = $con->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
                while ($row = mysqli_fetch_assoc($result)) {

                    array_push($nutrition, array(

                        "nutrition_id" => $row['nutrition_id'],
                        "nutrition_name" => $row['nutrition_name'],
                        "category" => $row['category'],
                        "measurement" => $row['measurement'],
                        "calorie" => $row["calorie"]
//                    "protein" => $row["protein"],
//                    "fat" => $row["fat"],
//                    "carbohydrate" => $row["carbohydrate"]
                    ));
                }
            }
            echo json_encode($nutrition);
        } else {
            //لیستی از دسته بندی غذاهای مشخص شده را پیدا کن و بفرست.
            $sql = "SELECT * FROM nutrition WHERE category='$category'";
            $result = $con->query($sql);

            if ($result->num_rows > 0) {
                // output data of each row
                while ($row = mysqli_fetch_assoc($result)) {

                    array_push($nutrition, array(

                        "nutrition_id" => $row['nutrition_id'],
                        "nutrition_name" => $row['nutrition_name'],
                        "category" => $row['category'],
                        "measurement" => $row['measurement'],
                        "calorie" => $row["calorie"]
//                    "protein" => $row["protein"],
//                    "fat" => $row["fat"],
//                    "carbohydrate" => $row["carbohydrate"]
                    ));
                }
            }
            echo json_encode($nutrition);
        }

        mysqli_close($con);
    }

    public function getCoachDeviceId()
    {
        require_once('dbConnect.php');

        $email = $_GET['email'];

        //اسم و فامیلی شاگرد و آیدی مربی اش را پیدا کن
        $id = "SELECT * FROM users WHERE user_email ='$email'";
        $findId = mysqli_query($con, $id);
        while ($row = mysqli_fetch_assoc($findId)) {
            $coachId = $row['coach_id'];
            $userId = $row['user_id'];
            $userName = $row['user_name'];
            $userFamily = $row['user_family'];
        }

        //شناسه ی گوشی مربی را پیدا کن(برای فرستادن نوتیفیکیشن به او)
        $sql = "SELECT device_id FROM coaches WHERE coach_id ='$coachId'";
        $findDeviceId = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_assoc($findDeviceId)) {
            $deviceId = $row['device_id'];
        }

        $result = array();
        array_push($result, array(
                "device_id" => $deviceId,
                "user_id" => $userId,
                "user_name" => $userName,
                "user_family" => $userFamily,
            )
        );

        echo json_encode($result);
        mysqli_close($con);
    }

    public function getPrices()
    {
        require_once('dbConnect.php');

        $user_id = $_GET['user_id'];

        //مربی شاگرد را پیدا کن
        $id = "SELECT coach_id FROM users WHERE user_id ='$user_id'";
        $findCoachId = mysqli_query($con, $id);

        if ($findCoachId->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($findCoachId)) {
                $coach_id = $row['coach_id'];
            }
        }

        //قیمت هایی را که مربی تنظیم کرده از جدول  پیدا کن و موقعی که شاگرد میخواد ثبت سفارش کنه بهش قیمت ها رو نشون بده.
        $sql = "SELECT * FROM price WHERE coach_id = '$coach_id'";
        $result = $con->query($sql);
        $price = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['exercise_price'] = $row['exercise_price'];
                $temp['food_price'] = $row['food_price'];
                $temp['supplement_price'] = $row['supplement_price'];
                $temp['discount'] = $row['discount'];
                $temp['event'] = $row['event'];

                array_push($price, $temp);
            }
        }
        echo json_encode($price);
        mysqli_close($con);
    }

    public function getUserInformation()
    {
        require_once('dbConnect.php');

        $user_email = $_GET['email'];

        //تمام اطلاعات شاگرد را پیدا کن و بفرست.
        $get = "SELECT * FROM users WHERE user_email = '$user_email'";
        $getInformation = mysqli_query($con, $get);
        $UserInformation = array();
        if (mysqli_num_rows($getInformation) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($getInformation)) {

                array_push($UserInformation, array(

                        "user_id" => $row['user_id'],
                        "device_id" => $row['device_id'],
                        "user_name" => $row['user_name'],
                        "user_family" => $row['user_family'],
                        "user_sex" => $row['user_sex'],
                        "user_rel" => $row['user_rel'],
                        "user_age" => $row['user_age'],
                        "user_height" => $row['user_height'],
                        "user_weight" => $row['user_weight'],
                        "user_record" => $row['user_record'],
                        "user_level" => $row['user_level'],
                        "user_sports" => $row['user_sports'],
                        "user_state" => $row['user_state'],
                        "user_city" => $row['user_city'],
                        "user_address" => $row['user_address'],
                        "user_job" => $row['user_job'],
                        "user_cigarette" => $row['user_cigarette'],
                        "user_tea" => $row['user_tea'],
                        "user_sleep" => $row['user_sleep'],
                        "user_stress" => $row['user_stress'],
                        "user_energy" => $row['user_energy'],
                        "user_blood" => $row['user_blood'],
                        "user_taste" => $row['user_taste'],
                        "user_waist" => $row['user_waist'],
                        "user_arm" => $row['user_arm'],
                        "user_neck" => $row['user_neck'],
                        "user_thigh" => $row['user_thigh'],
                        "user_leg" => $row['user_leg'],
                        "user_chest" => $row['user_chest'],
                        "user_workout" => $row['user_workout_a_week'],
                        "user_disease" => $row['user_disease'],
                        "user_other" => $row['user_other_info'],
                        "user_telegram_id" => $row['user_telegram_id'],
                        "user_visibility" => $row['user_visibility'],
                        "user_score" => $row['user_score']
                    )
                );
            }
        }
        echo json_encode($UserInformation);
        mysqli_close($con);
    }

    public function loginUsers(Request $request)
    {


        //required headers
        header("Access-Control-Allow-Origin:*");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        //importing dbConnect.php script
        require_once('dbConnect.php');

            //Getting values
//            $email = $_POST['email'];
//            $password = $_POST['password'];
            $device_id = $_POST['device_id'];
        $email = $request->email;
        $password = $request->password;
        $device_id = $request->device_id;
//        $device_id = $_POST['device_id'];
        //Creating sql query check coach email and password is correct or not
        $validate_admin = \App\User::where('user_email', $email)
            ->first();

        if ($validate_admin && Hash::check($password, $validate_admin->password)) {
            echo "successful";
        }else {
            echo "not_access";
        }

    }

    public function order()
    {
        require_once('dbConnect.php');
            //get user_id address from android
        $userId = $_GET['user_id'];

        //جزئیات همه ی سفارش های شاگرد رو پیدا کن و بفرست.
        $reg = "SELECT * FROM orders WHERE user_id = '$userId'";
        $findOrders = mysqli_query($con, $reg);
        $request = array();
        if (mysqli_num_rows($findOrders) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($findOrders)) {

                array_push($request, array(

                        "order_id" => $row['order_id'],
                        "exercise" => $row['exercise'],
                        "food" => $row['food'],
                        "supplement" => $row['supplement'],
                        "goal" => $row['goal'],
                        "exercise_place" => $row['exercise_place'],
                        "price" => $row['price'],
                        "order_date" => $row['order_date'],
                        "user_payment_state" => $row['user_payment_state'],
                        "token" => $row['token']
                    )
                );
            }
        }
        echo json_encode($request);

        mysqli_close($con);

    }

    public function pay()
    {

//        include("nusoap.php");
        require_once('nusoap.php');
        ini_set("soap.wsdl_cache_enabled", "0");

        $PIN = '7QossyDU6p0s70mfibeS';
        $wsdl_url = "https://pec.shaparak.ir/NewIPGServices/Sale/SaleService.asmx?WSDL";
        $site_call_back_url = "http://sfit.ir/api/bodybuilding/student/confirm";// location oc confirm php file

        $order_id = $_POST['order_id'];// Set amount for pay | get it from user by $_POST
        $amount = $_POST['price'];// Set order unique id | get it from user by $_POST
        $amount = $amount .'0';
        $params = array(
            "LoginAccount" => $PIN,
            "Amount" => $amount,
            "OrderId" => $order_id,
            "CallBackUrl" => $site_call_back_url
        );

        $client = new SoapClient ($wsdl_url);

        try {
            $result = $client->SalePaymentRequest(
                array(
                    "requestData" => $params
                )
            );
            if ($result->SalePaymentRequestResult->Token && $result->SalePaymentRequestResult->Status === 0) {
                //todo: save data to db
                $token = $result->SalePaymentRequestResult->Token;
//                echo $token;
//                exit(0);
                // این توکن را به اپ بازگردانید و سپس در اپ دستور باز کردن مرورگر را به ادرس زیر بدهید
                // 'https://pec.shaparak.ir/NewIPG/?Token=' + $token
                // این ادرس کاربر را به درگاه پرداخت شما منتقل خواهد کرد.
                header("Location: https://pec.shaparak.ir/NewIPG/?Token=" .$token ); /* Redirect browser */
//        exit ();
            } elseif ($result->SalePaymentRequestResult->Status != '0') {
                //todo: save data to db
                $err_msg = "(<strong> کد خطا : " . $result->SalePaymentRequestResult->Status . "</strong>) " .
                    $result->SalePaymentRequestResult->Message;
            }
        } catch (Exception $ex) {
            $err_msg = $ex->getMessage();
            //todo: save data to db
        }

//        echo 'ERROR : ' . $err_msg;


//function calc_price($orders)
//{
//    // محاسبه قیمت
//    return 1000;
//}
    }

    public function set_coach_score()
    {

require_once('dbConnect.php');


        $userId = $_POST['user_id'];
        $coachId = $_POST['coach_id'];
        $coachScore = $_POST['coach_score'];

        //چک کن ببین این شاگرد قبلا به این مربی امتیاز داده یا نه
        $checkScore = "SELECT user_id FROM coach_score WHERE user_id='$userId' AND coach_id = '$coachId'";
        //executing query
        $result = mysqli_query($con, $checkScore);
        //fetching result
        $check = mysqli_fetch_array($result);
        //اگه امتیاز داده بود، امتیاز جدید رو جایگزین قبلی کن
        if (isset($check)) {
            $sql = "UPDATE coach_score SET score = '$coachScore' WHERE user_id='$userId' AND coach_id = '$coachId'";
            $result2 = mysqli_query($con, $sql);
            if ($result2) {
                echo "امتیاز ثبت شد";
            } else {
                echo "امتیاز ثبت نشد.دوباره امتحان کنید";
            }
        } else {
            //وگرنه امتیازی که شاگرد به مربی خودش داده رو ثبت کن
            $sql2 = "INSERT INTO coach_score (user_id,coach_id,score)
            VALUES ('$userId','$coachId','$coachScore')";
            $result3 = mysqli_query($con, $sql2);
            if ($result3) {
                echo "امتیاز ثبت شد";
            } else {
                echo "امتیاز ثبت نشد";
            }
        }

        //میانگین امتیازات مربی رو حساب کن و در جدول coaches امتیاز مربی رو ثبت کن.
        $ratings = $con->query(
            "SELECT AVG(score) avg_score " .
            "FROM coach_score WHERE coach_id='$coachId'");
        $data = $ratings->fetch_assoc();
        $avg_rating = $data['avg_score'];

        $average = round($avg_rating, 1);

        $update = $con->query("UPDATE coaches SET coach_score = '$average' WHERE coach_id='$coachId'");


        mysqli_close($con);

    }

    public function setOrderHadSeen()
    {
        require_once('dbConnect.php');
        $order_id = $_POST['order_id'];
        $new = $_POST['new_order'];

        //وقتی شاگرد برای اولین بار روی سفارش جدیدی که مربی بهش فرستاده لمس کرد، اون سفارش از حالت جدید در میاد.
        $sql_update = "UPDATE orders SET new_order = '$new' WHERE order_id='$order_id'";

        if (mysqli_query($con, $sql_update)) {
            echo "order successfully has been set not new";
        } else {
            echo "error";
        }
        mysqli_close($con);

    }

    public function signUpUsers()
    {
        require_once('dbConnect.php');


            $device_id = $_POST['device_id'];
            $name = $_POST['user_name'];
            $family = $_POST['user_family'];
            $sex = $_POST['user_sex'];
            $rel = $_POST['user_rel'];
            $mobile = $_POST['user_mobile'];
            $email = $_POST['email'];
            $password = bcrypt( $_POST['password']);
            $user_reagent_code = $_POST['user_reagent_code'];

            $checkEmail = "SELECT user_email FROM users WHERE user_email='$email'";
            //executing query
            $result = mysqli_query($con, $checkEmail);
            //fetching result
            $check = mysqli_fetch_array($result);
            //if we got some result
            if (isset($check)) {
                echo "این ایمیل قبلا ثبت شده است";
            } else {

//        $checkDeviceId = "SELECT device_id FROM users WHERE device_id='$device_id'";
//        //executing query
//        $result = mysqli_query($con, $checkDeviceId);
//        //fetching result
//        $check = mysqli_fetch_array($result);
//        //if we got some result
//        if (isset($check)) {
//            echo "شما قبلا با این دستگاه حساب کاربری ساخته اید و بیشتر از یک حساب کاربری نمی توانید ایجاد کنید.";
//        } else {

                //اگر کاربر کد معرف را وارد کرده بود، به او و طرف مقابلش 1000 تومان به موجودی حسابشان اضافه کن.
                if ($user_reagent_code != null) {

                    $getScore = "SELECT user_score FROM users WHERE user_email='$user_reagent_code'";
                    //executing query
                    $result = mysqli_query($con, $getScore);
                    //fetching result
                    $check = mysqli_fetch_array($result);
                    //if we got some result
                    if (isset($check)) {

                        $getScore = $con->query(
                            "SELECT user_score new_score " .
                            "FROM users WHERE user_email ='$user_reagent_code'");
                        $fetchNewScore = $getScore->fetch_assoc();
                        $userScore = $fetchNewScore['new_score'];
                        $userScore += 1000;
                        $sql_update = "UPDATE users SET user_score = $userScore WHERE user_email='$user_reagent_code'";

                        if (mysqli_query($con, $sql_update)) {

                            //بعد از اضافه کردن موجودی حساب طرف مقابل، ثبت نام کاربر جدید را انجام بده.
                            $sql = "INSERT INTO users (device_id,user_name,user_family,user_sex,user_rel,user_mobile,user_email,password,user_score)
            VALUES ('$device_id','$name','$family','$sex','$rel','$mobile','$email','$password','1000')";
                            if (mysqli_query($con, $sql)) {
                                echo "successful";
                            } else {
                                echo "error";
                            }
                        } else {
                            echo "error";
                        }
                    } else {
                        echo "کد معرف وارد شده اشتباه است.";
                    }
                } else {
                    //اگر کد معرف را وارد نکرده بود و خالی بود ثبت نام را انجام بده؛ اما بدون اضافه کردن موجودی حساب!
                    $sql = "INSERT INTO users (device_id,user_name,user_family,user_sex,user_rel,user_mobile,user_email,password)
            VALUES ('$device_id','$name','$family','$sex','$rel','$mobile','$email','$password')";
                    if (mysqli_query($con, $sql)) {
                        echo "successful";
                    } else {
                        echo "error";
                    }
                }
//        }
            }

    }

    public function submit_student_order()
    {
        require_once('dbConnect.php');

        $userId = $_POST['user_id'];
        $exercise = $_POST['exercise'];
        $food = $_POST['food'];
        $supplement = $_POST['supplement'];
        $goal = $_POST['goal'];
        $exercise_place = $_POST['exercise_place'];
        $price = $_POST['price'];
        $order_date = $_POST['order_date'];

        //ابتدا مربی شاگرد مورد نظر را پیدا کن
        $select_coach_id = "SELECT coach_id FROM users WHERE user_id ='$userId'";
        $findCoachId = mysqli_query($con, $select_coach_id);
        while ($row = mysqli_fetch_assoc($findCoachId)) {
            $coach_id = $row['coach_id'];
        }

        //جزئیات سفارش مورد نظر را در جدول orders ثبت کن
        $sql = "INSERT INTO orders (user_id,coach_id,exercise,food,supplement,goal,exercise_place,price,order_date)
            VALUES ('$userId','$coach_id','$exercise','$food','$supplement','$goal','$exercise_place','$price','$order_date')";

        if (mysqli_query($con, $sql)) {

            $last_id = $con->insert_id;
            echo json_encode($last_id);

        } else {
            echo "error";
        }


    }

    public function submit_student_order_with_balance()
    {
        require_once('dbConnect.php');
        $userId = $_POST['user_id'];
        $exercise = $_POST['exercise'];
        $food = $_POST['food'];
        $supplement = $_POST['supplement'];
        $goal = $_POST['goal'];
        $exercise_place = $_POST['exercise_place'];
        $price = $_POST['price'];
        $order_date = $_POST['order_date'];

        $getBalance = $con->query(
            "SELECT user_score balance " .
            "FROM users WHERE user_id ='$userId'");
        $fetchBalance = $getBalance->fetch_assoc();
        $balance = $fetchBalance['balance'];

        if ($balance >= $price) {
            //ابتدا مربی شاگرد مورد نظر را پیدا کن
            $getCoachId = $con->query("SELECT coach_id c_id " . "FROM users WHERE user_id ='$userId'");
            $fetchCId = $getCoachId->fetch_assoc();
            $coach_id = $fetchCId['c_id'];

            //جزئیات سفارش مورد نظر را در جدول orders ثبت کن
            $sql = "INSERT INTO orders (user_id,coach_id,exercise,food,supplement,goal,exercise_place,price,order_date,user_payment_state,token)
            VALUES ('$userId','$coach_id','$exercise','$food','$supplement','$goal','$exercise_place','$price','$order_date','paid','1')";

            if (mysqli_query($con, $sql)) {

                $balance = $balance - $price;
                $update = $con->query("UPDATE users SET user_score = '$balance' WHERE user_id='$userId'");

                if ($update) {
                    echo "successful";
                } else {
                    echo "سفارش شما ثبت شد. اما از موجودی حساب شما کم نشد!!!";
                }
            } else {
                echo "در ثبت سفارش شما مشکلی پیش آمده. لطفا بعد از چند دقیقه دوباره امتحان کنید.";
            }
        } else {
            echo "موجودی حساب شما کمتر از مبلغ قابل پرداخت است!";
        }
        mysqli_close($con);
    }

    public function top_students()
    {
        require_once('dbConnect.php');


        $dir = __DIR__ . "/images/top_students";

        $arrayOfPictures = array();
        $topStudentsArray = array();

//همه ی عکس های شاگردان برتر را از مسیر زیر پیدا کن و تو آرایه عکس ها بریز.
        $path = "http://sfit.ir/bodybuilding/student/images/top_students/";
// Open a directory, and read its contents
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file != "." and $file != "..") {
                        array_push($arrayOfPictures, $path . $file);
                    }
                }
                closedir($dh);
            }
        }

//بعد بیا اسم مربی و توضیحات لازم رو از جدول top_students پیدا کن و با عکس ها بفرست.
        $i = 0;
        $sql = "SELECT * FROM top_students";
        $result = $con->query($sql);

        if ($result->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['coach_name'] = $row['coach_name'];
                $temp['description'] = $row['description'];
                $temp['top_student_image'] = $arrayOfPictures[$i];

                array_push($topStudentsArray, $temp);

                $i++;
            }
        }

        echo json_encode($topStudentsArray);

        mysqli_close($con);
    }

    public function update_password()
    {
        require_once('dbConnect.php');


        $email = $_POST['email'];
        $password = $_POST['password'];
        $password = bcrypt($password);
        $sql_update = "UPDATE users SET password = '$password' WHERE user_email='$email'";

        if (mysqli_query($con, $sql_update)) {
            echo 'successful';
        }
        mysqli_close($con);

    }

    public function updateUserInformation (Request $request)
    {
        require_once('dbConnect.php');

        $name = $request->user_name;
        $family = $request->user_family;
        $sex = $request->user_sex;
        $rel = $request->user_rel;
        $age = $request->user_age;
        $height = $request->user_height;
        $weight = $request->user_weight;
        $record = $request->user_record;
        $level = $request->user_level;
        $sports = $request->user_sports;
        $state = $request->user_state;
        $city = $request->user_city;
        $address = $request->user_address;
        $job = $request->user_job;
        $cigarette = $request->user_cigarette;
        $tea = $request->user_tea;
        $sleep = $request->user_sleep;
        $stress = $request->user_stress;
        $energy = $request->user_energy;
        $blood = $request->user_blood;
        $taste = $request->user_taste;
        $waist = $request->user_waist;
        $arm = $request->user_arm;
        $neck = $request->user_neck;
        $thigh = $request->user_thigh;
        $leg = $request->user_leg;
        $chest = $request->user_chest;
        $workout = $request->user_workout;
        $disease = $request->user_disease;
        $other = $request->user_other;
        $email = $request->email;
        $telegramId = $request->user_telegram_id;

        //همه ی اطلاعات شاگرد را بگیر و در جدول بروزرسانی کن.
        $sql_update = "UPDATE users SET user_name = '$name',
                                user_family = '$family',
                                user_sex = '$sex',
                                user_rel = '$rel',
                                user_age = '$age',
                                user_height = '$height',
                                user_weight = '$weight',
                                user_record = '$record',
                                user_level = '$level',
                                user_sports = '$sports',
                                user_state = '$state',
                                user_city = '$city',
                                user_address = '$address',
                                user_job = '$job',
                                user_cigarette = '$cigarette',
                                user_tea = '$tea',
                                user_sleep = '$sleep',
                                user_stress = '$stress',
                                user_energy = '$energy',
                                user_blood = '$blood',
                                user_taste = '$taste',
                                user_waist = '$waist',
                                user_arm = '$arm',
                                user_neck = '$neck',
                                user_thigh = '$thigh',
                                user_leg = '$leg',
                                user_chest = '$chest',
                                user_workout_a_week = '$workout',
                                user_disease = '$disease',
                                user_other_info = '$other',
                                user_telegram_id = '$telegramId'
                   WHERE user_email='$email'";

        if (mysqli_query($con, $sql_update)) {
            echo "successful";
        } else {
            echo "failed";
        }

    }

    public function updateUserScore()
    {
        require_once('dbConnect.php');

        $user_id = $_POST['user_id'];
        $score = $_POST['user_score'];

        //از گردونه ی شانس مقدار امتیاز را گرفته و به امتیاز قبلی شاگرد اضافه میکند.
        $getScore = "SELECT user_score FROM users WHERE user_id='$user_id'";
        //executing query
        $result = mysqli_query($con, $getScore);
        //fetching result
        $check = mysqli_fetch_array($result);
        //if we got some result
        $getScore = $con->query(
            "SELECT user_score new_score " .
            "FROM users WHERE user_id ='$user_id'");
        $fetchNewScore = $getScore->fetch_assoc();
        $userScore = $fetchNewScore['new_score'];

        $userScore += (int)$score;
        $sql_update = "UPDATE users SET user_score = '$userScore' WHERE user_id='$user_id'";

        if (mysqli_query($con, $sql_update)) {
            echo 'successful';
        }


    }

    public function uploadUserImageProfile()
    {
        require_once('dbConnect.php');

        $objFile = &$_FILES["image"];
        $email = $_POST['email'];

        //آیدی شاگرد را پیدا کرده
        $id = "SELECT user_id FROM users WHERE user_email ='$email'";
        $findId = mysqli_query($con, $id);
        while ($row = mysqli_fetch_assoc($findId)) {
            $userId = $row['user_id'];
        }

        //براساس آیدی عکس پروفایل شاگرد را در مسیر زیر قرار میدهد.
        $path = "images/UsersImagesProfile/$userId.png";

        if (  request()->image->move($path, $objFile["tmp_name"])) {
//        if (move_uploaded_file($objFile["tmp_name"], $path)) {
            print "Successfully Uploaded";
        } else {
            print "There was an error uploading the file, please try again!";
        }

    }

}
