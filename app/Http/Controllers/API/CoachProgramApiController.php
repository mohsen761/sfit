<?php

namespace App\Http\Controllers\API;

use App\coach;
use App\Exercise;
use App\Food;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;

class CoachProgramApiController
{

    public function deleteExercise(Request $request)
    {
        require_once('dbConnect.php');
        $exercise_id = $_GET['exercise_id'];

        //حرکت تمرینی مورد نظر را از جدول exercises پاک می کند.
        $sql = "DELETE FROM exercises WHERE exercise_id = '$exercise_id'";
        $result = mysqli_query($con, $sql);

        if (mysqli_affected_rows($con) > 0) {
            echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function deleteProgram(Request $request)
    {
        require_once('dbConnect.php');
        $programId = $_GET['program_id'];
        $programType = $_GET['program_type'];

        //برنامه تمرینی، غذایی یا مکملی را از جدول program و جدول مربوط به آن پاک میکند.
        switch ($programType) {
            case "exercise" :
                //اول ببین تو جدول exercises حرکتی مربوط به این برنامه ثبت شده یا نه
                $checking = "SELECT program_id FROM exercises WHERE program_id = '$programId'";
                $result = mysqli_query($con, $checking);
                $check = mysqli_fetch_array($result);
                //اگه بود اول حرکت ها رو از جدول exercises پاک کن، بعد برنامه رو از جدول program پاک کن.
                if (isset($check)) {

                    $sql = "DELETE FROM exercises WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {

                        $sql = "DELETE FROM program WHERE program_id = '$programId'";
                        $result = mysqli_query($con, $sql);

                        if (mysqli_affected_rows($con) > 0) {
                            echo "successful";
                        }
                        else {
                            echo "error";
                        }
                    } else {
                        echo "error";
                    }
                } else {
                    $sql = "DELETE FROM program WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {
                        echo "successful";
                    }
                    else {
                        echo "error";
                    }
                }
                break;
            case "food" :
                $checking = "SELECT program_id FROM foods WHERE program_id = '$programId'";
                //executing query
                $result = mysqli_query($con, $checking);
                //fetching result
                $check = mysqli_fetch_array($result);
                //if we got some result
                if (isset($check)) {

                    $sql = "DELETE FROM foods WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {

                        $sql = "DELETE FROM program WHERE program_id = '$programId'";
                        $result = mysqli_query($con, $sql);

                        if (mysqli_affected_rows($con) > 0) {
                            echo "successful";
                        }
                        else {
                            echo "error";
                        }
                    } else {
                        echo "error";
                    }
                } else {
                    $sql = "DELETE FROM program WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {
                        echo "successful";
                    }
                    else {
                        echo "error";
                    }
                }
                break;
            case "supplement" :

                $checking = "SELECT program_id FROM supplement WHERE program_id = '$programId'";
                //executing query
                $result = mysqli_query($con, $checking);
                //fetching result
                $check = mysqli_fetch_array($result);
                //if we got some result
                if (isset($check)) {

                    $sql = "DELETE FROM supplement WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {

                        $sql = "DELETE FROM program WHERE program_id = '$programId'";
                        $result = mysqli_query($con, $sql);

                        if (mysqli_affected_rows($con) > 0) {
                            echo "successful";
                        }
                        else {
                            echo "error";
                        }
                    } else {
                        echo "error";
                    }
                } else {
                    $sql = "DELETE FROM program WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {
                        echo "successful";
                    }
                    else {
                        echo "error";
                    }
                }
                break;
        }
        mysqli_close($con);
    }

     public function deleteSupplement(Request $request)
    {
        require_once('dbConnect.php');
        $supplement_id = $_GET['supplement_id'];

        //برنامه مکملی مورد نظر را از جدول supplement پاک می کند.
        $sql = "DELETE FROM supplement WHERE supplement_id = '$supplement_id'";
        $result = mysqli_query($con, $sql);

        if (mysqli_affected_rows($con) > 0) {
            echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function exercise(Request $request)
    {
        require_once('dbConnect.php');
        $order_id = $_GET['order_id'];
        $day = $_GET['day'];

        //آیدی برنامه تمرینی مربوط به سفارش مورد نظر را از جدول program پیدا کن
        $id = $con->query(
            "SELECT program_id p_id " .
            "FROM program WHERE order_id='$order_id' AND program_type='exercise' ORDER BY program_id DESC LIMIT 1");
        $fetchProgramId = $id->fetch_assoc();
        $program_id = $fetchProgramId['p_id'];

        //حرکات مورد نظر را از جدول exercises پیدا کن و بفرست
        $reg = "SELECT * FROM exercises WHERE program_id = '$program_id' AND days ='$day'";
        $findOrders = mysqli_query($con, $reg);
        $request = array();
        if (mysqli_num_rows($findOrders) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($findOrders)) {

                array_push($request, array(
                        "type_of_movement" => $row['type_of_movement'],
                        "sets" => $row['sets'],
                        "repetitions" => $row['repetitions'],
                        "exercise_systems" => $row['exercise_systems'],
                        "exercise_description" => $row['description']
                    )
                );
            }
        }
        echo json_encode($request);

        mysqli_close($con);
    }

     public function food(Request $request)
    {
        require_once('dbConnect.php');
        $order_id = $_GET['order_id'];
        $meal = $_GET['meal'];

        //آیدی برنامه غذایی مربوط به سفارش مورد نظر را از جدول program پیدا کن
        $id = $con->query(
            "SELECT program_id p_id " .
            "FROM program WHERE order_id='$order_id' AND program_type='food' ORDER BY program_id DESC LIMIT 1");
        $fetchProgramId = $id->fetch_assoc();
        $program_id = $fetchProgramId['p_id'];

        //وعده های غذایی مورد نظر را از جدول foods پیدا کن و بفرست
        $reg = "SELECT * FROM foods WHERE program_id = '$program_id' AND meal ='$meal'";
        $findOrders = mysqli_query($con, $reg);
        $request = array();
        if (mysqli_num_rows($findOrders) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($findOrders)) {

                array_push($request, array(

                        "offer1" => $row['offer1'],
                        "offer2" => $row['offer2'],
                        "offer3" => $row['offer3'],
                    )
                );
            }
        }
        echo json_encode($request);

        mysqli_close($con);
    }

     public function getMyProgramDate(Request $request)
    {
        require_once('dbConnect.php');
        $userId = $_GET['user_id'];
        $programType = $_GET['program_type'];

        //اطلاعات کلی برنامه تمرینی، غذایی یا مکملی شاگرد مورد نظر را از جدول program پیدا کن و بفرست
        $sql = "SELECT * FROM program WHERE user_id = '$userId' AND program_type = '$programType' AND program_date IS NOT NULL";
        $result = $con->query($sql);
        $dates = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['program_id'] = $row['program_id'];
                $temp['program_type'] = $row['program_type'];
                $temp['program_date'] = $row['program_date'];
                $temp['title'] = $row['title'];
                $temp['description'] = $row['description'];
                $temp['period'] = $row['period'];

                array_push($dates, $temp);

            }
        }
        echo json_encode($dates);
        mysqli_close($con);
    }

     public function getProgramDate(Request $request)
    {
        require_once('dbConnect.php');
        $userId = $_GET['user_id'];
        $coach_id = $_GET['coach_id'];
        $type_of_program = $_GET['type_of_program'];

        //از جدول orders تاریخ فرستادن برنامه توسط مربی، نوع برنامه تمرینی، غذایی و مکملی را پیدا کن و بفرست
        //از اونجایی که تاریخ برای شاگرد اون مربی باشه!
        $sql = "SELECT * FROM orders WHERE user_id = '$userId' AND coach_id = '$coach_id' AND $type_of_program = 'yes' AND program_date IS NOT NULL";
        $result = $con->query($sql);
        $dates = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['order_id'] = $row['order_id'];
                $temp['program_date'] = $row['program_date'];
                $temp['exercise_type'] = $row['exercise_type'];
                $temp['food_type'] = $row['food_type'];
                $temp['supplement_type'] = $row['supplement_type'];

                array_push($dates, $temp);
            }
        }
        echo json_encode($dates);
        mysqli_close($con);
    }

     public function getProgramDetails(Request $request)
    {
        require_once('dbConnect.php');
        $order_id = $_GET['order_id'];
        $program_type = $_GET['type_of_program'];

        //جزئیات برنامه مورد نظر را از جدول program پیدا کن و بفرست.
        $sql = "SELECT * FROM program WHERE order_id = '$order_id' AND program_type = '$program_type'";
        $result = $con->query($sql);
        $dates = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
//            $temp['program_id'] = $row['program_id'];
//            $temp['program_type'] = $row['program_type'];
                $temp['program_date'] = $row['program_date'];
                $temp['program_title'] = $row['title'];
                $temp['program_description'] = $row['description'];
                $temp['period'] = $row['period'];

                array_push($dates, $temp);

            }

        }
        echo json_encode($dates);
        mysqli_close($con);
    }

     public function insert_program(Request $request)
    {
        require_once('dbConnect.php');
        $user_id = $request->user_id;
        $coach_id = $request->coach_id;
        $order_id = $request->order_id;
        $program_date = $request->program_date;
        $program_title = $request->program_title;
        $program_description = $request->program_description;
        $program_period = $request->period;
        $program_type = $request->program_type;

        //برنامه ای که مربی میخواهد برای شاگرد خودش بفرستد در جدول program ثبت میشود و آیدی آن را برمیگرداند.
        $sql = "INSERT INTO program (user_id,coach_id,order_id,program_type,program_date,title,description,period)
            VALUES ('$user_id','$coach_id','$order_id','$program_type','$program_date','$program_title','$program_description','$program_period')";
        if (mysqli_query($con, $sql)) {
            $id = mysqli_insert_id($con);
            echo $id;
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function send_program(Request $request)
    {
        require_once('dbConnect.php');
        $objFile = &$_FILES["image"];
        $imageNumber = $_POST['image_number'];
        $typeOfProgram = $_POST['type_of_program'];
        $orderId = $_POST['order_id'];
//    $date = $_POST['date'];

         //عکس برنامه ی فرستاده شده از سوی مربی را در مسیر مشخص خودش قرار بده
         $dir = "images/programs/$typeOfProgram/$orderId";
         if (!file_exists($dir)){
             mkdir($dir);
         }
//    $dir2 = "$dir/$date";
//    mkdir($dir2);
         $path = "$dir/$imageNumber.png";

         if (move_uploaded_file($objFile["tmp_name"], $path)) {
             print "successful";
         } else {
             print "error";
         }
    }

     public function sendExercise(Request $request)
    {
        require_once('dbConnect.php');
        $program_id = $_POST['program_id'];
        $type_of_movement = $_POST['type_of_movement'];
        $days = $_POST['days'];
        $sets = $_POST['sets'];
        $repetitions = $_POST['repetitions'];
        $exercise_systems = $_POST['exercise_systems'];
        $description = $_POST['exercise_description'];

        //حرکات تمرینی توسط مربی برای شاگردش در جدول exercises ثبت می شود.
        $sql = "INSERT INTO exercises (program_id,type_of_movement,days,sets,repetitions,exercise_systems,description)
            VALUES ('$program_id','$type_of_movement','$days','$sets','$repetitions','$exercise_systems','$description')";

        if (mysqli_query($con, $sql)) {
            echo mysqli_insert_id($con);
//        echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function sendFood(Request $request)
    {
        require_once('dbConnect.php');
        $userId = $_POST['user_id'];
        $program_id = $_POST['program_id'];
        $meal = $_POST['meal'];
        $offer1 = $_POST['offer1'];
        $offer2 = $_POST['offer2'];
        $offer3 = $_POST['offer3'];

        //وعده های غذایی توسط مربی برای شاگردش در جدول foods ثبت می شود.
        $sql = "INSERT INTO foods (program_id,meal,offer1,offer2,offer3)
            VALUES ('$program_id','$meal','$offer1','$offer2','$offer3')";

        if (mysqli_query($con, $sql)) {
            echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function sendSupplement(Request $request)
    {
        require_once('dbConnect.php');
        $program_id = $_POST['program_id'];
        $supplementName = $_POST['supplement_name'];
        $timeToEat = $_POST['time_to_eat'];
        $dosage = $_POST['dosage'];
        $dayToEat = $_POST['day_to_eat'];

        //برنامه مکملی توسط مربی برای شاگردش در جدول supplement ثبت می شود.
        $sql = "INSERT INTO supplement (program_id,supplement_name,time_to_eat,dosage,day_to_eat)
            VALUES ('$program_id','$supplementName','$timeToEat','$dosage','$dayToEat')";

        if (mysqli_query($con, $sql)) {
            echo mysqli_insert_id($con);
//        echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function setProgramState(Request $request)
    {
        require_once('dbConnect.php');
        $order_id = $_POST['order_id'];
        $date = $_POST['date'];

        //وقتی مربی برنامه هاش رو برای شاگردش فرستاده بود، تایید نهایی میکنه و وضعیت سفارش به حالت فرستاده شده تغییر پیدا میکنه
        $sql = "UPDATE orders SET program_state = 'sent', program_date = '$date' WHERE order_id = '$order_id'";

        if (mysqli_query($con, $sql)) {
            echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function setProgramType(Request $request)
    {
        require_once('dbConnect.php');
        $order_id = $_POST['order_id'];
        $programType = $_POST['program_type'];
        $type = $_POST['type'];

        //نوع برنامه فرستاده شده توسط مربی(متنی یا عکسی) برای هر برنامه تمرینی و غذایی و مکملی در جدول orders مشخص می شود.
        switch ($programType) {
            case "exercise" :

                $sql_update = "UPDATE orders SET exercise_type = '$type' WHERE order_id='$order_id'";
                if (mysqli_query($con, $sql_update)) {
                    echo 'successful';
                } else {
                    echo "error";
                }

                break;
            case "food" :
                $sql_update = "UPDATE orders SET food_type = '$type' WHERE order_id='$order_id'";
                if (mysqli_query($con, $sql_update)) {
                    echo 'successful';
                } else {
                    echo "error";
                }

                break;
            case "supplement" :

                $sql_update = "UPDATE orders SET supplement_type = '$type' WHERE order_id='$order_id'";
                if (mysqli_query($con, $sql_update)) {
                    echo 'successful';
                } else {
                    echo "error";
                }

                break;
        }
        mysqli_close($con);
    }

     public function supplement(Request $request)
    {
        require_once('dbConnect.php');
        $order_id = $_GET['order_id'];

        //آیدی برنامه مکملی مربوط به سفارش مورد نظر را از جدول program پیدا کن
        $id = $con->query(
            "SELECT program_id p_id " .
            "FROM program WHERE order_id='$order_id' AND program_type='supplement' ORDER BY program_id DESC LIMIT 1");
        $fetchProgramId = $id->fetch_assoc();
        $program_id = $fetchProgramId['p_id'];

        //برنامه مکملی مورد نظر را از جدول supplement پیدا کن و بفرست
        $reg = "SELECT * FROM supplement WHERE program_id = '$program_id'";
        $findOrders = mysqli_query($con, $reg);
        $request = array();
        if (mysqli_num_rows($findOrders) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($findOrders)) {

                array_push($request, array(
                        "supplement_name" => $row['supplement_name'],
                        "time_to_eat" => $row['time_to_eat'],
                        "dosage" => $row['dosage'],
                        "day_to_eat" => $row['day_to_eat'],
                    )
                );
            }
        }
        echo json_encode($request);

        mysqli_close($con);
    }


}