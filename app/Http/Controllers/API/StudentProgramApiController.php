<?php

namespace App\Http\Controllers\API;

use App\coach;
use App\Exercise;
use App\Food;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;
use SoapClient;

class StudentProgramApiController
{
    public function deleteExercise ()
    {
        require_once('dbConnect.php');

        $exercise_id = $_GET['exercise_id'];
//    $program_id = $_GET['program_id'];
//    $type_of_movement = $_GET['type_of_movement'];
//    $sets = $_GET['sets'];
//    $repetitions = $_GET['repetitions'];
//    $exercise_systems = $_GET['exercise_systems'];

        //حرکت تمرینی مورد نظر را از جدول exercises پاک می کند.
        $sql = "DELETE FROM exercises WHERE exercise_id = '$exercise_id'";
        $result = mysqli_query($con, $sql);

        if (mysqli_affected_rows($con) > 0) {
            echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function deleteProgram()
    {
        require_once('dbConnect.php');  $programId = $_GET['program_id'];
        $programType = $_GET['program_type'];

        //برنامه تمرینی، غذایی یا مکملی را از جدول program و جدول مربوط به آن پاک میکند.
        switch ($programType) {
            case "exercise" :
//اول ببین تو جدول exercises حرکتی مربوط به این برنامه ثبت شده یا نه
                $checking = "SELECT program_id FROM exercises WHERE program_id = '$programId'";
                //executing query
                $result = mysqli_query($con, $checking);
                //fetching result
                $check = mysqli_fetch_array($result);
                //اگه بود اول حرکت ها رو از جدول exercises پاک کن، بعد برنامه رو از جدول program پاک کن.
                if (isset($check)) {

                    $sql = "DELETE FROM exercises WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {

                        $sql = "DELETE FROM program WHERE program_id = '$programId'";
                        $result = mysqli_query($con, $sql);

                        if (mysqli_affected_rows($con) > 0) {
                            echo "successful";
                        } else {
                            echo "error";
                        }
                    } else {
                        echo "error";
                    }
                } else {
                    $sql = "DELETE FROM program WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {
                        echo "successful";
                    } else {
                        echo "error";
                    }
                }
                break;
            case "food" :
                $checking = "SELECT program_id FROM foods WHERE program_id = '$programId'";
                //executing query
                $result = mysqli_query($con, $checking);
                //fetching result
                $check = mysqli_fetch_array($result);
                //if we got some result
                if (isset($check)) {

                    $sql = "DELETE FROM foods WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {

                        $sql = "DELETE FROM program WHERE program_id = '$programId'";
                        $result = mysqli_query($con, $sql);

                        if (mysqli_affected_rows($con) > 0) {
                            echo "successful";
                        } else {
                            echo "error";
                        }
                    } else {
                        echo "error";
                    }
                } else {
                    $sql = "DELETE FROM program WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {
                        echo "successful";
                    } else {
                        echo "error";
                    }
                }
                break;
            case "supplement" :

                $checking = "SELECT program_id FROM supplement WHERE program_id = '$programId'";
                //executing query
                $result = mysqli_query($con, $checking);
                //fetching result
                $check = mysqli_fetch_array($result);
                //if we got some result
                if (isset($check)) {

                    $sql = "DELETE FROM supplement WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {

                        $sql = "DELETE FROM program WHERE program_id = '$programId'";
                        $result = mysqli_query($con, $sql);

                        if (mysqli_affected_rows($con) > 0) {
                            echo "successful";
                        } else {
                            echo "error";
                        }
                    } else {
                        echo "error";
                    }
                } else {
                    $sql = "DELETE FROM program WHERE program_id = '$programId'";
                    $result = mysqli_query($con, $sql);

                    if (mysqli_affected_rows($con) > 0) {
                        echo "successful";
                    } else {
                        echo "error";
                    }
                }
                break;
        }
        mysqli_close($con);
    }

     public function deleteSupplement()
    {
        require_once('dbConnect.php'); $supplement_id = $_GET['supplement_id'];
//    $program_id = $_GET['program_id'];
//    $date = $_GET['date'];
//    $supplement_name = $_GET['supplement_name'];
//    $time_to_eat = $_GET['time_to_eat'];
//    $dosage = $_GET['dosage'];
//    $day_to_eat = $_GET['day_to_eat'];

         //برنامه مکملی مورد نظر را از جدول supplement پاک می کند.
         $sql = "DELETE FROM supplement WHERE supplement_id = '$supplement_id'";
         $result = mysqli_query($con, $sql);

         if (mysqli_affected_rows($con) > 0) {
             echo "successful";
         } else {
             echo "error";
         }
         mysqli_close($con);
    }

     public function exercise()
    {
        require_once('dbConnect.php');$day = $_GET['day'];
        $program_id = $_GET['program_id'];

        //حرکات مورد نظر را از جدول exercises پیدا کن و بفرست
        $reg = "SELECT * FROM exercises WHERE program_id = '$program_id' AND days ='$day'";
        $findOrders = mysqli_query($con, $reg);
        $request = array();
        if (mysqli_num_rows($findOrders) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($findOrders)) {

                array_push($request, array(
                        "type_of_movement" => $row['type_of_movement'],
                        "sets" => $row['sets'],
                        "repetitions" => $row['repetitions'],
                        "exercise_systems" => $row['exercise_systems'],
                        "exercise_description" => $row['description']
                    )
                );
            }
        }
        echo json_encode($request);

        mysqli_close($con);
    }

     public function food()
    {
        require_once('dbConnect.php');  $meal = $_GET['meal'];
        $program_id = $_GET['program_id'];

        //وعده های غذایی مورد نظر را از جدول foods پیدا کن و بفرست
        $reg = "SELECT * FROM foods WHERE program_id = '$program_id' AND meal ='$meal'";
        $findOrders = mysqli_query($con, $reg);
        $request = array();
        if (mysqli_num_rows($findOrders) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($findOrders)) {

                array_push($request, array(

                        "offer1" => $row['offer1'],
                        "offer2" => $row['offer2'],
                        "offer3" => $row['offer3'],
                    )
                );
            }
        }
        echo json_encode($request);

        mysqli_close($con);
    }

     public function get_free_program_id()
    {
        require_once('dbConnect.php'); $programType = $_GET['program_type'];

        //اطلاعات کلی برنامه تمرینی، غذایی یا مکملی شاگرد مورد نظر را از جدول program پیدا کن و بفرست
        $sql = "SELECT * FROM program WHERE program_type = '$programType' AND user_id IS NULL AND coach_id IS NULL AND order_id IS NULL";
        $result = $con->query($sql);
        $dates = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['program_id'] = $row['program_id'];
                $temp['program_type'] = $row['program_type'];
                $temp['program_date'] = $row['program_date'];
                $temp['title'] = $row['title'];
                $temp['description'] = $row['description'];
                $temp['period'] = $row['period'];

                array_push($dates, $temp);

            }

        }
        echo json_encode($dates);
        mysqli_close($con);
    }

     public function getMyProgramDate()
    {
        require_once('dbConnect.php');$userId = $_GET['user_id'];
        $programType = $_GET['program_type'];

        //اطلاعات کلی برنامه تمرینی، غذایی یا مکملی شاگرد مورد نظر را از جدول program پیدا کن و بفرست
        $sql = "SELECT * FROM program WHERE user_id = '$userId' AND program_type = '$programType' AND coach_id IS NULL AND order_id IS NULL";
        $result = $con->query($sql);
        $dates = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['program_id'] = $row['program_id'];
                $temp['program_type'] = $row['program_type'];
                $temp['program_date'] = $row['program_date'];
                $temp['title'] = $row['title'];
                $temp['description'] = $row['description'];
                $temp['period'] = $row['period'];

                array_push($dates, $temp);

            }

        }
        echo json_encode($dates);
        mysqli_close($con);
    }

     public function getProgramDate()
    {
        require_once('dbConnect.php');$userId = $_GET['user_id'];
        $type_of_program = $_GET['type_of_program'];

        //تاریخ برنامه هایی که از سوی مربی فرستاده شده است را پیدا کن و بفرست.
        $sql = "SELECT * FROM orders WHERE user_id = '$userId' AND $type_of_program = 'yes' AND program_date IS NOT NULL";
        $result = $con->query($sql);
        $dates = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['order_id'] = $row['order_id'];
                $temp['program_date'] = $row['program_date'];
                $temp['exercise_type'] = $row['exercise_type'];
                $temp['food_type'] = $row['food_type'];
                $temp['supplement_type'] = $row['supplement_type'];
                $temp['new_order'] = $row['new_order'];
                $temp['coach_id'] = $row['coach_id'];

                array_push($dates, $temp);
            }
        }
        echo json_encode($dates);
        mysqli_close($con);
    }

     public function getProgramDetails()
    {
        require_once('dbConnect.php');  $program_id = $_GET['program_id'];
        $program_type = $_GET['type_of_program'];

        //جزئیات برنامه مورد نظر را از جدول program پیدا کن و بفرست.
        $sql = "SELECT * FROM program WHERE program_id = '$program_id' AND program_type = '$program_type'";
        $result = $con->query($sql);
        $dates = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['program_date'] = $row['program_date'];
                $temp['program_title'] = $row['title'];
                $temp['program_description'] = $row['description'];
                $temp['period'] = $row['period'];

                array_push($dates, $temp);
            }
        }
        echo json_encode($dates);
        mysqli_close($con);
    }

     public function getProgramId()
    {
        require_once('dbConnect.php'); $order_id = $_GET['order_id'];
        $type_of_program = $_GET['type_of_program'];

        //آیدی مربوط به برنامه فرستاده شده از سوی مربی را از جدول program پیدا کن
        $id = $con->query(
            "SELECT program_id p_id " .
            "FROM program WHERE order_id='$order_id' AND program_type='$type_of_program' ORDER BY program_id DESC LIMIT 1");
        $fetchProgramId = $id->fetch_assoc();
        $program_id = $fetchProgramId['p_id'];
        echo $program_id;

        mysqli_close($con);
    }

     public function getProgramImages()
    {
        require_once('dbConnect.php'); $order_id = $_GET['order_id'];
        $typeOfProgram = $_GET['type_of_program'];

        $dir = array();

        function checkRemoteFile($url)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            // don't download content
            curl_setopt($ch, CURLOPT_NOBODY, 1);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            if (curl_exec($ch) !== FALSE) {
                return true;
            } else {
                return false;
            }
        }

//    $date = str_replace('/', '_', $date);

         //عکس هایی را که مربی برای شاگرد فرستاده است را براساس آیدی سفارش و نوع برنامه مورد نظر پیدا کن و بفرست.
         for ($i = 1; $i <= 10; $i++) {

             if (checkRemoteFile("http://sfit.ir/bodybuilding/coach/images/programs/$typeOfProgram/$order_id/$i.png")) {

                 array_push($dir, array(
                     "program_image_url" => "http://sfit.ir/bodybuilding/coach/images/programs/$typeOfProgram/$order_id/$i.png"
                 ));

             } else {
                 $i = 10;
             }
         }
         echo json_encode($dir);
         mysqli_close($con);
    }

     public function insert_program()
    {
        require_once('dbConnect.php');  $user_id = $_POST['user_id'];
        $program_date = $_POST['program_date'];
        $program_title = $_POST['program_title'];
        $program_description = $_POST['program_description'];
        $program_period = $_POST['period'];
        $program_type = $_POST['program_type'];

        //تاریخی را که شاگرد در آن روز برنامه برای خود تنظیم کرده چک کن ببین هست یا نه.
        //چون در هر تاریخی یک نوع برنامه تمرینی می تواند تنظیم کند.
        $checkDate = "SELECT program_date FROM program WHERE user_id='$user_id' AND program_date='$program_date' AND program_type='$program_type' AND coach_id IS NULL AND order_id IS NULL";
        //executing query
        $result = mysqli_query($con, $checkDate);
        //fetching result
        $check = mysqli_fetch_array($result);
        //if we got some result
        if (isset($check)) {
            echo "can not add program";
        } else {
            $sql = "INSERT INTO program (user_id,program_type,program_date,title,description,period)
            VALUES ('$user_id','$program_type','$program_date','$program_title','$program_description','$program_period')";
            if (mysqli_query($con, $sql)) {
                echo mysqli_insert_id($con);
            } else {
                echo "error";
            }
        }
        mysqli_close($con);
    }

     public function send_exercise_with_array()
    {
        require_once('dbConnect.php');  $program_id = $_POST['program_id'];
        $days = $_POST['days'];
        $array = $_POST['array'];

        $array_of_exercise_id = array();
        $workouts = json_decode($array);

        $i = 0;
        while ($i < count($workouts)) {
            $type_of_movement = $workouts[$i]->movement_name;
            $sets = $workouts[$i]->sets;
            $repetitions = $workouts[$i]->repetitions;
            $exercise_systems = $workouts[$i]->exercise_systems;
            $description = $workouts[$i]->description;

            $sql = "INSERT INTO exercises (program_id,type_of_movement,days,sets,repetitions,exercise_systems,description)
            VALUES ('$program_id','$type_of_movement','$days','$sets','$repetitions','$exercise_systems','$description')";

            if (mysqli_query($con, $sql)) {
                array_push($array_of_exercise_id, mysqli_insert_id($con));
                $i++;
            } else {
                break;
            }
        }
        if ($i == count($workouts)) {
//        echo "successful";
            echo json_encode($array_of_exercise_id);
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function sendExercise()
    {
        require_once('dbConnect.php'); $program_id = $_POST['program_id'];
        $type_of_movement = $_POST['type_of_movement'];
        $days = $_POST['days'];
        $sets = $_POST['sets'];
        $repetitions = $_POST['repetitions'];
        $exercise_systems = $_POST['exercise_systems'];
        $description = $_POST['exercise_description'];

        //حرکات تمرینی مربوط به برنامه مورد نظر را از جدول exercises پیدا کن و بفرست.
        $sql = "INSERT INTO exercises (program_id,type_of_movement,days,sets,repetitions,exercise_systems,description)
            VALUES ('$program_id','$type_of_movement','$days','$sets','$repetitions','$exercise_systems','$description')";

        if (mysqli_query($con, $sql)) {
            echo mysqli_insert_id($con);
//        echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function sendFood()
    {
        require_once('dbConnect.php'); $program_id = $_POST['program_id'];
        $meal = $_POST['meal'];
        $offer1 = $_POST['offer1'];
        $offer2 = $_POST['offer2'];
        $offer3 = $_POST['offer3'];

        //اول چک کن ببین با همچین آیدیی برنامه غذایی فرستاده شده یا نه
        $checkFood = "SELECT program_id FROM foods WHERE program_id='$program_id' AND meal='$meal'";
        //executing query
        $result = mysqli_query($con, $checkFood);
        //fetching result
        $check = mysqli_fetch_array($result);
        //اگه بود، یعنی اگه شاگرد تو برنامه به عقب برگشته بود و دوباره میخواست وعده غذاییش رو تنظیم کنه، اون وعده رو بروزرسانی کن
        if (isset($check)) {
            $sql_update = "UPDATE foods SET offer1='$offer1', offer2='$offer2', offer3='$offer3' WHERE program_id='$program_id' AND meal='$meal'";
            if (mysqli_query($con, $sql_update)) {
                echo 'successful';
            } else {
                echo "error";
            }
        } else {
            //وگرنه اطلاعات اون وعده غذایی رو ثبت کن
            $sql = "INSERT INTO foods (program_id,meal,offer1,offer2,offer3)
            VALUES ('$program_id','$meal','$offer1','$offer2','$offer3')";

            if (mysqli_query($con, $sql)) {
                echo "successful";
            } else {
                echo "error";
            }
        }
        mysqli_close($con);
    }

     public function sendSupplement()
    {
        require_once('dbConnect.php');$program_id = $_POST['program_id'];
        $supplementName = $_POST['supplement_name'];
        $timeToEat = $_POST['time_to_eat'];
        $dosage = $_POST['dosage'];
        $dayToEat = $_POST['day_to_eat'];

        //برنامه مکملی مورد نظر را در جدول supplement ثبت کن.
        $sql = "INSERT INTO supplement (program_id,supplement_name,time_to_eat,dosage,day_to_eat)
            VALUES ('$program_id','$supplementName','$timeToEat','$dosage','$dayToEat')";

        if (mysqli_query($con, $sql)) {
            echo mysqli_insert_id($con);
//        echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

     public function supplement()
    {
        require_once('dbConnect.php');$program_id = $_GET['program_id'];

        //برنامه مکملی را از جدول supplement پیدا کن و بفرست.
        $reg = "SELECT * FROM supplement WHERE program_id = '$program_id'";
        $findOrders = mysqli_query($con, $reg);
        $request = array();
        if (mysqli_num_rows($findOrders) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($findOrders)) {

                array_push($request, array(
                        "supplement_name" => $row['supplement_name'],
                        "time_to_eat" => $row['time_to_eat'],
                        "dosage" => $row['dosage'],
                        "day_to_eat" => $row['day_to_eat'],
                    )
                );
            }
        }
        echo json_encode($request);

        mysqli_close($con);
    }

     public function swipe_exercise()
    {
        require_once('dbConnect.php'); $first = $_GET['first'];
        $second = $_GET['second'];

        $type_of_movement1 = "";
        $sets1 = "";
        $repetitions1 = "";
        $exercise_systems1 = "";
        $description1 = "";

        $type_of_movement2 = "";
        $sets2 = "";
        $repetitions2 = "";
        $exercise_systems2 = "";
        $description2 = "";

        $select_first_row = "SELECT * FROM exercises WHERE exercise_id = '$first'";

        $result = $con->query($select_first_row);

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $type_of_movement1 = $row['type_of_movement'];
                $sets1 = $row['sets'];
                $repetitions1 = $row['repetitions'];
                $exercise_systems1 = $row['exercise_systems'];
                $description1 = $row['description'];
            }
        }

        $select_second_row = "SELECT * FROM exercises WHERE exercise_id = '$second'";

        $result = $con->query($select_second_row);

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $type_of_movement2 = $row['type_of_movement'];
                $sets2 = $row['sets'];
                $repetitions2 = $row['repetitions'];
                $exercise_systems2 = $row['exercise_systems'];
                $description2 = $row['description'];
            }
        }

        $sql_update = "UPDATE exercises
 SET exercise_id = '$second',
 type_of_movement = '$type_of_movement2',
 sets = '$sets2',
 repetitions = '$repetitions2',
 exercise_systems = '$exercise_systems2',
 description = '$description2'
 WHERE exercise_id='$first'";

        if (mysqli_query($con, $sql_update)) {

            $sql_update2 = "UPDATE exercises
 SET exercise_id = '$first', 
 type_of_movement = '$type_of_movement1', 
 sets = '$sets1',
 repetitions = '$repetitions1',
 exercise_systems = '$exercise_systems1',
 description = '$description1'
 WHERE exercise_id='$second'";

            if (mysqli_query($con, $sql_update2)) {
                echo 'successful';
            } else {
                echo 'error';
            }
        }

        mysqli_close($con);
    }

     public function update_exercise()
    {
        require_once('dbConnect.php');$exercise_id = $_POST['exercise_id'];
        $type_of_movement = $_POST['type_of_movement'];
        $sets = $_POST['sets'];
        $repetitions = $_POST['repetitions'];
        $exercise_systems = $_POST['exercise_systems'];
        $description = $_POST['exercise_description'];

        $sql = "UPDATE exercises SET 
type_of_movement = '$type_of_movement',
sets = '$sets',
repetitions = '$repetitions',
exercise_systems = '$exercise_systems',
description = '$description'
WHERE exercise_id='$exercise_id'";

        if (mysqli_query($con, $sql)) {
            echo 'successful';
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

       public function update_supplement()
        {
        require_once('dbConnect.php');
        $supplement_id = $_POST['supplement_id'];
        $supplement_name = $_POST['supplement_name'];
        $time_to_eat = $_POST['time_to_eat'];
        $dosage = $_POST['dosage'];
        $day_to_eat = $_POST['day_to_eat'];

                    $sql = "UPDATE supplement SET 
            supplement_name = '$supplement_name',
            time_to_eat = '$time_to_eat',
            dosage = '$dosage',
            day_to_eat = '$day_to_eat'
            WHERE supplement_id='$supplement_id'";

                    if (mysqli_query($con, $sql)) {
                        echo 'successful';
                    } else {
                        echo "error";
                    }
                    mysqli_close($con);
    }


}