<?php

namespace App\Http\Controllers\API;

use App\coach;
use App\Exercise;
use App\Food;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;

class CoachApiController
{
    public function test()
    {
        return response()->json('ok');
    }

    public function swapexercises(Request $request)
    {

        $exercise1=Exercise::where('exercise_id',$request->first)->first();
        $exercise2=Exercise::where('exercise_id',$request->second)->first();

        $exercise = new Exercise();
        $exercise->type_of_movement = $exercise1->type_of_movement;
        $exercise->days =             $exercise1->days;
        $exercise->sets =             $exercise1->sets;
        $exercise->repetitions =      $exercise1->repetitions;
        $exercise->exercise_systems = $exercise1->exercise_systems;
        $exercise->save();
//        $exercise1 = $exercise2;
        $exercise1->type_of_movement = $exercise2->type_of_movement;
        $exercise1->days =             $exercise2->days;
        $exercise1->sets =             $exercise2->sets;
        $exercise1->repetitions =      $exercise2->repetitions;
        $exercise1->exercise_systems = $exercise2->exercise_systems;
        $exercise1->save();
//        $exercise2= $exercise;
        $exercise2->type_of_movement = $exercise->type_of_movement;
        $exercise2->days =             $exercise->days;
        $exercise2->sets =             $exercise->sets;
        $exercise2->repetitions =      $exercise->repetitions;
        $exercise2->exercise_systems = $exercise->exercise_systems;
        $exercise2->save();
        $exercise->delete();

//                dd($exercise1);
        return back()->with(['success' =>'با موفقیت عوض شد.']);

    }

    public function loginCoaches(Request $request)
    {
        require_once('dbConnect.php');

            $email = $request->coach_email;
            $password = $request->coach_password;
        $device_id = $_POST['device_id'];
        //Creating sql query check coach email and password is correct or not
        $validate_admin = \App\Coach::where('coach_email', $email)
            ->first();

        if ($validate_admin && Hash::check($password, $validate_admin->password)) {
            echo "successful";
        }else {
            echo "not_access";
        }

        
    }

    public function coachEvidence(Request $request)
    {
        require_once('dbConnect.php');
        $objFile = &$_FILES["image"];
        $evidenceName = $_POST['evidence_name'];
        $coachId = $_POST['coach_id'];

        //گرفتن عکس مدارک از مربی و قراردادن آن در مسیر مشخص شده ی زیر
        $dir = "images/evidence/$coachId";
        if (!file_exists($dir)){
            mkdir($dir);
        }
        $path = "$dir/$evidenceName.png";

        if (move_uploaded_file($objFile["tmp_name"], $path)) {
            print "successful";
        } else {
            print "error";
        }
    }

    public function findCoachImageProfile(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $_GET['coach_id'];

        //پیدا کردن عکس پروفایل مربی با آیدی از مسیر مشخص شده ی زیر
        $path = "http://sfit.ir/bodybuilding/coach/images/coachesImagesProfile/$coachId.png";
        echo json_encode(array($path));

        mysqli_close($con);
//            echo 'error';

    }

    public function forceUpdate()
    {
        require_once('dbConnect.php');
        $update = array();

        //اگه پیغامی نخوایم نشون داده بشه مقدارش رو خالی می گذاریم.
        array_push($update, array(

                "force_update" => "yes",
                "latestVersion" => "2.6.2",
//            "messages" => "سرور در حال بروزرسانی است.\nاز صبر و شکیبایی شما سپاسگذاریم."
                "messages" => ""
            )
        );
        echo json_encode($update);
    }

    public function getBalance(Request $request)
    {
        require_once('dbConnect.php');

            $coachId = $request->coach_id;

            //درصد سودی که میخواهیم از مربیان برداریم
        $coachId = $_GET['coach_id'];

        //درصد سودی که میخواهیم از مربیان برداریم
        $percent = 0.2;

        //تعداد همه ی سفارش های مربوط به مربی مورد نظر را که هنوز با او تسفیه حساب نشده است را پیدا میکنیم.
        $count = $con->query(
            "SELECT COUNT(order_id) number_of_orders " .
            "FROM orders WHERE coach_id='$coachId' AND payment_state = 'unpaid'");
        $data = $count->fetch_assoc();
        $number_of_orders = $data['number_of_orders'];

        //مجموع همه ی قیمت های سفارش هایی که هنوز تسفیه حساب نشده اند
        //و مربی به آن سفارش ها پاسخ داده و برنامه را برای شاگرد فرستاده را حساب میکنیم.
        $sum = $con->query(
            "SELECT SUM(price) sum_price " .
            "FROM orders WHERE coach_id='$coachId' AND payment_state = 'unpaid' AND program_state = 'sent'");
        $data = $sum->fetch_assoc();
        $sum_price = $data['sum_price'];
        //مبلغ قابل پرداخت به مربی را با کسر درصد مورد نظر حساب میکنیم.
        $sum_price = $sum_price - ($sum_price * $percent);

        echo json_encode(array("number_of_orders" => $number_of_orders, "sum_price" => $sum_price));

        mysqli_close($con);
    }

    public function getCoachImages(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $_GET['coach_id'];
        $coachImages = array();
        //سه تا از عکس های مربی را از مسیر مشخص پیدا کرده و میفرستیم.
        for ($i = 0; $i <= 2; $i++) {
            array_push($coachImages, "http://sfit.ir/bodybuilding/coach/images/coachesImages/$coachId/" . $i . ".png");
        }
        echo json_encode($coachImages);
        mysqli_close($con);
    }

    public function getCoachInformation(Request $request)
    {
        require_once('dbConnect.php');

            $coachId = $request->coach_id;
        $coach_email = $request->coach_email;

//        $coach_email = $_GET['coach_email'];

        //get coach information from table coaches
        $get = "SELECT * FROM coaches WHERE coach_email = '$coach_email'";
        $getInformation = mysqli_query($con, $get);
        $coachInformation = array();
        if (mysqli_num_rows($getInformation) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($getInformation)) {

                array_push($coachInformation, array(

                        "coach_id" => $row['coach_id'],
                        "device_id" => $row['device_id'],
                        "coach_name" => $row['coach_name'],
                        "coach_family" => $row['coach_family'],
                        "coach_honors" => $row['coach_honors'],
                        "coach_sex" => $row['coach_sex'],
                        "coach_age" => $row['coach_age'],
                        "coach_record" => $row['coach_record'],
                        "coach_state" => $row['coach_state'],
                        "coach_city" => $row['coach_city'],
                        "coach_access" => $row['coach_access'],
                        "coach_card_number" => $row['coach_card_number'],
                        "coach_shaba_number" => $row['coach_shaba_number'],
                        "coach_visibility" => $row['coach_visibility'],
                        "coach_level" => $row['coach_level'],
                        "coach_instagram" => $row['coach_instagram']
                    )
                );
            }
        }
        echo json_encode($coachInformation);

        mysqli_close($con);

    }

    public function getCoachStory(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $_GET['coach_id'];

        $coachStory = "images/story/$coachId.png";

        //اگر مربی اِستوری گذاشته بود عکسش رو بفرست
        //اگر نه بگو عکسی وجود نداشت
        if (file_exists($coachStory)){
            echo "http://sfit.ir/bodybuilding/coach/" . $coachStory;
        }else{
            echo "does not exist";
        }

        mysqli_close($con);
    }

    public function getMyGymImages(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $request->coach_id;

        $gym = array();

        //آیدی باشگاه مربوط به مربی را پیدا کن
        $checkGym = "SELECT gym_id FROM gym WHERE coach_id='$coachId'";
        $result = mysqli_query($con, $checkGym);
        $check = mysqli_fetch_array($result);
        //اگر مربی باشگاه خودش رو وارد کرده بود قبلا
        if (isset($check)) {

            //آیدی باشگاه رو پیدا کن
            $id = "SELECT gym_id FROM gym WHERE coach_id ='$coachId'";
            $findInfo = mysqli_query($con, $id);

            if ($findInfo->num_rows > 0) {
                while ($row = mysqli_fetch_assoc($findInfo)) {
                    $gymId = $row['gym_id'];
                }
            }

            //عکس های مربوط به باشگاه رو بفرست
            for ($i = 0; $i <= 2; $i++) {
                array_push($gym, "http://sfit.ir/bodybuilding/gym/images/gymImage/$gymId/" . $i . ".png");
            }
            echo json_encode($gym);
        }else{
            echo "gym not found";
        }

    }

    public function getMyGymInformation(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $request->coach_id;

        //آیدی باشگاه مربوط به مربی را پیدا کن
        $id = "SELECT gym_id FROM gym WHERE coach_id ='$coachId'";
        $findInfo = mysqli_query($con, $id);

        if ($findInfo->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($findInfo)) {
                $gymId = $row['gym_id'];
            }
        }

        //اطلاعات باشگاه مورد نظر را بفرست
        $sql = "SELECT * FROM gym WHERE gym_id = '$gymId'";
        $result = $con->query($sql);
        $gym = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['gym_name'] = $row['gym_name'];
                $temp['gym_type'] = $row['gym_type'];
                $temp['gym_state'] = $row['gym_state'];
                $temp['gym_city'] = $row['gym_city'];
                $temp['gym_address'] = $row['gym_address'];
                $temp['gym_monthly_fee'] = $row['gym_monthly_fee'];
                $temp['gym_services'] = $row['gym_services'];
                $temp['gym_students_number'] = $row['gym_students_number'];

                array_push($gym, $temp);
            }
        }
        echo json_encode($gym);

        mysqli_close($con);

    }

    public function getPrices(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $_GET['coach_id'];

        //قیمت های مربی را پیدا کن و بفرست
        $sql = "SELECT * FROM price WHERE coach_id = '$coachId'";
        $result = $con->query($sql);
        $price = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['exercise_price'] = $row['exercise_price'];
                $temp['food_price'] = $row['food_price'];
                $temp['supplement_price'] = $row['supplement_price'];
                $temp['discount'] = $row['discount'];
                $temp['event'] = $row['event'];

                array_push($price, $temp);
            }
        }
        echo json_encode($price);

        mysqli_close($con);
    }

    public function getStudentFigureImages(Request $request)
    {
        require_once('dbConnect.php');

//        $coachId = $request->coach_id;

        $userId = $request->user_id;
        $figureImageDate = $request->date;

//        $userId = $_GET['user_id'];
//        $figureImageDate = $_GET['date'];

        $figures = array();

        //عکس های فیگوری فرستاده شده از شاگرد در تاریخ ثبت سفارش را پیدا کن
        $figure1 = "http://sfit.ir/bodybuilding/student/images/joft_bazu_az_jolo/$userId/$figureImageDate.png";
        $figure2 = "http://sfit.ir/bodybuilding/student/images/joft_bazu_az_posht/$userId/$figureImageDate.png";
        $figure3 = "http://sfit.ir/bodybuilding/student/images/posht_bazu_az_baghal/$userId/$figureImageDate.png";
        $figure4 = "http://sfit.ir/bodybuilding/student/images/shekam_va_paha_az_jolo/$userId/$figureImageDate.png";
        $figure5 = "http://sfit.ir/bodybuilding/student/images/zir_baghal_az_jolo/$userId/$figureImageDate.png";
        $figure6 = "http://sfit.ir/bodybuilding/student/images/zir_baghal_az_posht/$userId/$figureImageDate.png";
        $figure7 = "http://sfit.ir/bodybuilding/student/images/ghafase_sine/$userId/$figureImageDate.png";
        $figure8 = "http://sfit.ir/bodybuilding/student/images/nimrokh/$userId/$figureImageDate.png";

        //این تابع چک میکنه که عکس مورد نظر تو اون مسیر وجود داره یا نه
        function checkRemoteFile($url)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            // don't download content
            curl_setopt($ch, CURLOPT_NOBODY, 1);
            curl_setopt($ch, CURLOPT_FAILONERROR, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            if (curl_exec($ch) !== FALSE) {
                return true;
            } else {
                return false;
            }
        }

        if (checkRemoteFile($figure1)) {
            array_push($figures, array(
//            "figure_name" => "جفت بازو از جلو",
                "figure_url" => $figure1
            ));
        }

        if (checkRemoteFile($figure2)) {
            array_push($figures, array(
//            "figure_name" => "جفت بازو از پشت",
                "figure_url" => $figure2
            ));
        }

        if (checkRemoteFile($figure3)) {
            array_push($figures, array(
//            "figure_name" => "پشت بازو از بغل",
                "figure_url" => $figure3
            ));
        }

        if (checkRemoteFile($figure4)) {
            array_push($figures, array(
//            "figure_name" => "شکم و پاها از جلو",
                "figure_url" => $figure4
            ));
        }

        if (checkRemoteFile($figure5)) {
            array_push($figures, array(
//            "figure_name" => "زیر بغل از جلو",
                "figure_url" => $figure5
            ));
        }

        if (checkRemoteFile($figure6)) {
            array_push($figures, array(
//            "figure_name" => "زیر بغل از پشت",
                "figure_url" => $figure6
            ));
        }

        if (checkRemoteFile($figure7)) {
            array_push($figures, array(
//            "figure_name" => "قفسه سینه",
                "figure_url" => $figure7
            ));
        }

        if (checkRemoteFile($figure8)) {
            array_push($figures, array(
//            "figure_name" => "نیمرخ",
                "figure_url" => $figure8
            ));
        }

        echo json_encode($figures);
    }

    public function getStudentFigureImagesDate(Request $request)
    {
        require_once('dbConnect.php');

//        $coachId = $request->coach_id;

        $userId = $request->user_id;
        $coach_id = $request->coach_id;

        //تاریخ های سفارش دادن شاگرد مربوط به مربی را پیدا کن و بفرست.
//        $userId = $_GET['user_id'];
//        $coach_id = $_GET['coach_id'];

        //تاریخ های سفارش دادن شاگرد مربوط به مربی را پیدا کن و بفرست.
        $sql = "SELECT * FROM orders WHERE user_id = '$userId' AND coach_id = '$coach_id' AND user_payment_state = 'paid'";
        $result = $con->query($sql);
        $dates = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['date'] = $row['order_date'];

                array_push($dates, $temp);
            }
        }
        echo json_encode($dates);
        mysqli_close($con);
    }

    public function getSubscriptionDate(Request $request)
    {
        require_once('dbConnect.php');

        $coach_id = $_GET['coach_id'];

        //آخرین تاریخ پایان مهلت اشتراک مربی را پیدا کن و بفرست
        $get = "SELECT end_date FROM subscription WHERE coach_id = '$coach_id' ORDER BY end_date DESC LIMIT 1";
        $getInformation = mysqli_query($con, $get);
        $endDate = array();
        if (mysqli_num_rows($getInformation) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($getInformation)) {

                $endDate = $row['end_date'];
            }
        }
        echo json_encode(array("end_date" => $endDate));

        mysqli_close($con);
    }

    public function getUserDeviceId(Request $request)
    {
        require_once('dbConnect.php');

        $user_id = $_GET['user_id'];
        $result = array();
        //شناسه ی گوشی شاگرد را پیدا کن و بفرست
        $id = "SELECT * FROM users WHERE user_id ='$user_id'";
        $findId = mysqli_query($con, $id);
        while ($row = mysqli_fetch_assoc($findId)) {
            array_push($result, array(
                    "device_id" => $row['device_id']
                )
            );
        }
        echo json_encode($result);
        mysqli_close($con);
    }

    public function getUserInformation(Request $request)
    {
        require_once('dbConnect.php');

        $user_id = $_GET['user_id'];

        //get user information from table users
        $get = "SELECT * FROM users WHERE user_id = '$user_id'";
        $getInformation = mysqli_query($con, $get);
        $UserInformation = array();
        if (mysqli_num_rows($getInformation) > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($getInformation)) {

                array_push($UserInformation, array(

                        "user_id" => $row['user_id'],
                        "device_id" => $row['device_id'],
                        "user_name" => $row['user_name'],
                        "user_family" => $row['user_family'],
                        "user_age" => $row['user_age'],
                        "user_height" => $row['user_height'],
                        "user_weight" => $row['user_weight'],
                        "user_record" => $row['user_record'],
                        "user_level" => $row['user_level'],
                        "user_sports" => $row['user_sports'],
                        "user_state" => $row['user_state'],
                        "user_city" => $row['user_city'],
                        "user_address" => $row['user_address'],
                        "user_job" => $row['user_job'],
                        "user_cigarette" => $row['user_cigarette'],
                        "user_tea" => $row['user_tea'],
                        "user_sleep" => $row['user_sleep'],
                        "user_stress" => $row['user_stress'],
                        "user_energy" => $row['user_energy'],
                        "user_blood" => $row['user_blood'],
                        "user_taste" => $row['user_taste'],
                        "user_waist" => $row['user_waist'],
                        "user_arm" => $row['user_arm'],
                        "user_neck" => $row['user_neck'],
                        "user_leg" => $row['user_leg'],
                        "user_thigh" => $row['user_thigh'],
                        "user_chest" => $row['user_chest'],
                        "user_workout" => $row['user_workout_a_week'],
                        "user_disease" => $row['user_disease'],
                        "user_other" => $row['user_other_info'],
                        "user_mobile" => $row['user_mobile']
                    )
                );
            }
        }
        echo json_encode($UserInformation);
        mysqli_close($con);
    }

    public function getUsersInformation(Request $request)
    {
        require_once('dbConnect.php');

        $coachId =$request->coach_id;

//        $coachId = $_GET['coach_id'];

        $users = array();
        $userIds = array();

        //لیستی از شاگردای مربی مورد نظر را به همراه عکس پروفایلشون پیدا کن و بفرست
        $id = "SELECT * FROM users WHERE coach_id ='$coachId'";
        $findUsersInformation = mysqli_query($con, $id);

        if ($findUsersInformation->num_rows > 0) {
//        $i = 0;
            while ($row = mysqli_fetch_assoc($findUsersInformation)) {

                $userName = $row['user_name'];
                $userFamily = $row['user_family'];

                array_push($users, array(
                    "user_full_name" => $userName . " " . $userFamily,
                    "user_id" => $row['user_id'],
                    "user_profile_photo" => "http://sfit.ir/bodybuilding/student/images/UsersImagesProfile/" . $row['user_id'] . ".png",
                    "user_telegram_id" => $row['user_telegram_id'],
                    "user_push_id" => $row['device_id'],
                    "user_email" => $row['user_email']

                ));
            }
            echo json_encode($users);

            mysqli_close($con);
        }
    }

    public function requestCheckout(Request $request)
    {
        require_once('dbConnect.php');

        $coach_id = $request->coach_id;
        $date = $request->date;
        $amount = $request->amount;
        $number_of_orders = $request->number_of_orders;

        //اول چک میکنیم ببینیم درخواست تسویه حساب قبلی مربی تسویه شده یا نه
        $checkout = "SELECT checkout_state FROM checkout WHERE coach_id = '$coach_id' AND checkout_state='unpaid'";
        $result = mysqli_query($con, $checkout);
        $check = mysqli_fetch_array($result);
        //اگر هنوز درخواست مربی درحال بررسی بود و تسویه نشده بود بگو منتظر باشه
        if (isset($check)) {
            echo "exist";
            //اگر نه بیا درخواستش رو ثبت کن
        } else {
            $sql = "INSERT INTO checkout (coach_id,amount,number_of_orders,checkout_date)
            VALUES ('$coach_id','$amount','$number_of_orders','$date')";

            if (mysqli_query($con, $sql)) {
                echo "successful";
            } else {
                echo "متاسفانه درخواست شما ثبت نشد.لطفا دوباره امتحان کنید";
            }
        }
        mysqli_close($con);
    }

    public function RequestsForCoach(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $_GET['coach_id'];

        $userIds = array();
        $userNames = array();
        $userFamilies = array();
        $userDeviceId = array();
        $order = array();
        $usersHaveOrder = array();
        $newOrdersCount = 0;
        $myStudentsCount = 0;

//    $id = "SELECT * FROM users WHERE coach_id ='$coachId'";
//    $findInfo = mysqli_query($con, $id);
//
//    if ($findInfo->num_rows > 0) {
//        while ($row = mysqli_fetch_assoc($findInfo)) {
//
//            array_push($userIds, $row['user_id']);
//
//        }
//    }
//
//    $userIds = implode(',', $userIds);


//    $sql = "SELECT user_id FROM orders WHERE user_id IN ($userIds)";
//    $findUsersHaveOrder = mysqli_query($con, $sql);
//    while ($row = mysqli_fetch_assoc($findUsersHaveOrder)) {
//
//        array_push($usersHaveOrder, $row['user_id']);
//
//    }

        //آیدی همه ی شاگردای مربی مورد نظر را پیدا کن که وضعیت پرداختشون پرداخت شده باشه
        $sql = "SELECT user_id FROM orders WHERE coach_id ='$coachId' AND user_payment_state='paid'";
        $findUsersHaveOrder = mysqli_query($con, $sql);
        while ($row = mysqli_fetch_assoc($findUsersHaveOrder)) {

            array_push($usersHaveOrder, $row['user_id']);

        }

        //اون شاگردایی که سفارش دادن، اسم و فامیلی و شناسه گوشیشون رو پیدا کن
        for ($i = 0; $i < sizeof($usersHaveOrder); $i++) {

            $query = "SELECT * FROM users WHERE user_id IN ($usersHaveOrder[$i])";
            $findUserFullName = mysqli_query($con, $query);

            if ($findUserFullName->num_rows > 0) {
                while ($row = mysqli_fetch_assoc($findUserFullName)) {

                    array_push($userNames, $row['user_name']);
                    array_push($userFamilies, $row['user_family']);
                    array_push($userDeviceId, $row['device_id']);

                }
            }
        }

        //اون اطلاعاتی که از جدول users پیدا کردیو به همراه بقیه اطلاعات بفرست
        $fetch = "SELECT * FROM orders WHERE coach_id ='$coachId' AND user_payment_state='paid'";
        $fetchOrder = mysqli_query($con, $fetch);
        $i = 0;
        while ($row = mysqli_fetch_assoc($fetchOrder)) {

            array_push($order, array(
                "order_id" => $row['order_id'],
                "user_id" => $row['user_id'],
                "exercise" => $row['exercise'],
                "food" => $row['food'],
                "supplement" => $row['supplement'],
                "goal" => $row['goal'],
                "exercise_place" => $row['exercise_place'],
                "price" => $row['price'],
                "order_date" => $row['order_date'],
                "new_order" => $row['new_order'],
                "payment_state" => $row['payment_state'],
                "program_state" => $row['program_state'],
                "user_full_name" => $userNames[$i] . " " . $userFamilies[$i],
                "device_id" => $userDeviceId[$i],
                "user_profile_photo" => "http://sfit.ir/bodybuilding/student/images/UsersImagesProfile/$usersHaveOrder[$i].png"
            ));

            $i++;
        }

        //تعداد سفارش های جدید آمده را پیدا کن و بفرست
        $counter = $con->query(
            "SELECT COUNT(new_order) new_order " .
            "FROM orders WHERE coach_id ='$coachId' AND program_state ='not_sent' AND user_payment_state ='paid'");
        $fetchNewOrder = $counter->fetch_assoc();
        $newOrdersCount = $fetchNewOrder['new_order'];

        //تعداد کل شاگردای مربی مورد نظر را پیدا کن و بفرست
        $count = $con->query(
            "SELECT COUNT(user_id) my_student_count " .
            "FROM users WHERE coach_id ='$coachId'");
        $fetchMyStudent = $count->fetch_assoc();
        $myStudentsCount = $fetchMyStudent['my_student_count'];

        echo json_encode(array("order" => $order, "new_orders_count" => $newOrdersCount, "my_student_count" => $myStudentsCount));

        mysqli_close($con);
    }

    public function send_program(Request $request)
    {
        require_once('dbConnect.php');

        $objFile = &$_FILES["image"];
        $imageNumber = $_POST['image_number'];
        $typeOfProgram = $_POST['type_of_program'];
        $orderId = $_POST['order_id'];

        //عکس برنامه ی فرستاده شده از سوی مربی را در مسیر مشخص خودش قرار بده
        $dir = "bodybuilding/coach/images/programs/$typeOfProgram/$orderId";
        if (!file_exists($dir)){
            mkdir($dir);
        }

        $path = "$dir/$imageNumber.png";

        if (move_uploaded_file($objFile["tmp_name"], $path)) {
            print "successful";
        }
    }

    public function sendGymInformation(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $request->coach_id;
        $gymName = $request->gym_name;
        $gymType = $request->gym_type;
        $gymState = $request->gym_state;
        $gymCity = $request->gym_city;
        $gymAddress = $request->gym_address;
        $gymMonthlyFee = $request->gym_monthly_fee;
        $gymServices = $request->gym_services;
        $gymStudentsNumber = $request->gym_students_number;

        //چک کن ببین قبلا مربی اطلاعات باشگاه خودش رو ثبت کرده یا نه
        $checkGym = "SELECT coach_id FROM gym WHERE coach_id='$coachId'";
        $result = mysqli_query($con, $checkGym);
        $check = mysqli_fetch_array($result);
        //اگه ثبت کرده بود اطلاعاتش رو بروزرسانی کن وگرنه باشگاه رو ثبت کن
        if (isset($check)) {

            $sql_update = "UPDATE gym SET gym_name = '$gymName',
                               gym_type = '$gymType',
                                gym_state = '$gymState',
                                     gym_city = '$gymCity',
                               gym_address = '$gymAddress',
                                gym_monthly_fee = '$gymMonthlyFee',
                               gym_services = '$gymServices',
                               gym_students_number = '$gymStudentsNumber'
                   WHERE coach_id='$coachId'";

            if (mysqli_query($con, $sql_update)) {
                echo "اطلاعات باشگاه شما بروزرسانی شد";
            } else {
                echo "error";
            }
        } else {
            $sql = "INSERT INTO gym (coach_id,gym_name,gym_type,gym_state,gym_city,gym_address,gym_monthly_fee,gym_services,gym_students_number)
            VALUES ('$coachId','$gymName','$gymType','$gymState','$gymCity','$gymAddress','$gymMonthlyFee','$gymServices','$gymStudentsNumber')";

            if (mysqli_query($con, $sql)) {
                echo "اطلاعات باشگاه شما با موفقیت ثبت شد";
            } else {
                echo "error";
            }
        }
        mysqli_close($con);
    }

    public function sendGymTimeAndDay(Request $request)
{
    require_once('dbConnect.php');

    $coachId = $request->coach_id;

    $gymStatus = $request->gym_status;
    $gymDay = $request->day;
    $gymSex = $request->sex;
    $gymTimeFrom = $request->time_from;
    $gymTimeTo = $request->time_to;

    $gymId = "";

    //آیدی باشگاه مربی مورد نظر را پیدا کن
    $id = "SELECT gym_id FROM gym WHERE coach_id='$coachId'";
    $findInfo = mysqli_query($con, $id);

    if ($findInfo->num_rows > 0) {
        while ($row = mysqli_fetch_assoc($findInfo)) {
            $gymId = $row['gym_id'];
        }
    }

    //چک کن ببین قبلا ساعات و روزهای کاری باشگاه رو ثبت کرده یا نه
    $checkGym = "SELECT gym_id FROM working_days_times WHERE gym_id='$gymId' AND days='$gymDay' AND sex='$gymSex'";
    $result = mysqli_query($con, $checkGym);
    $check = mysqli_fetch_array($result);
    //اگه ثبت کرده بود بروزرسانی کن، اگه نه یه ردیف جدید به جدول working_days_times اضافه کن
    if (isset($check)) {

        $sql_update = "UPDATE working_days_times SET gym_status = '$gymStatus',
                                time_from = '$gymTimeFrom',
                                     time_to = '$gymTimeTo'
                   WHERE gym_id='$gymId' AND days='$gymDay' AND sex='$gymSex'";

        mysqli_query($con, $sql_update);

        if (mysqli_query($con, $sql_update)) {
            echo "successful";
        } else {
            echo "error";
        }

    } else {
        $sql = "INSERT INTO working_days_times (gym_id,days,gym_status,sex,time_from,time_to)
            VALUES ('$gymId','$gymDay','$gymStatus','$gymSex','$gymTimeFrom','$gymTimeTo')";

        if (mysqli_query($con, $sql)) {
            echo "successful";
        } else {
            echo "error";
        }
    }
    mysqli_close($con);

}

    public function setCoachStory(Request $request)
    {
        require_once('dbConnect.php');

        $objFile = &$_FILES["image"];
        $coachId = $_POST['coach_id'];
        $dir = "bodybuilding/coach/images/story/$coachId.png";

        if (move_uploaded_file($objFile["tmp_name"], $dir)) {
            print "successful";
        }
    }

    public function setGymLocation(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $request->coach_id;

        $latitude = $request->latitude;
        $longitude = $request->longitude;

        //باشگاه مربی مورد نظر رو پیدا کن و طول و عرض جغرافیایی باشگاه رو در جدول بروزرسانی کن
        $findGym = "SELECT gym_id FROM gym WHERE coach_id = '$coachId'";
        //executing query
        $result = mysqli_query($con, $findGym);
        //fetching result
        $check = mysqli_fetch_array($result);
        //if we got some result
        if (isset($check)) {

            $sql_update = "UPDATE gym SET latitude = '$latitude', longitude = '$longitude' WHERE coach_id='$coachId'";

            if (mysqli_query($con, $sql_update)) {
                echo "successful";
            } else {
                echo "error";
            }
        }else {
            echo "gym_not_found";
        }
        mysqli_close($con);
    }

    public function setOrderHadSeen(Request $request)
    {
        require_once('dbConnect.php');

//        $coachId = $request->coach_id;

        $order_id = $_POST['order_id'];
        $new = $_POST['new_order'];

        //وقتی مربی برای اولین بار روی سفارش جدیدی که بهش رفته لمس کرد، اون سفارش از حالت جدید دربیاد.
        $sql_update = "UPDATE orders SET new_order = '$new' WHERE order_id='$order_id'";

        if (mysqli_query($con, $sql_update)) {
            echo "order successfully has been set not new";
        } else {
            echo "error";
        }
        mysqli_close($con);

    }

    public function setPrices(Request $request)
    {
        require_once('dbConnect.php');

//        $coachId = $request->coach_id;

        $coach_id = $_POST['coach_id'];
        $discount = $_POST['discount'];
        $exercise_price = $_POST['exercise_price'];
        $food_price = $_POST['food_price'];
        $supplement_price = $_POST['supplement_price'];
        $event = $_POST['event'];

        $checkPrice = "SELECT coach_id FROM price WHERE coach_id='$coach_id'";
        //executing query
        $result = mysqli_query($con, $checkPrice);
        //fetching result
        $check = mysqli_fetch_array($result);
        //اگه مربی قبلا قیمت های مورد نظر خودش رو ثبت کرده بود بیا قیمت های جدید رو جایگزین قبلیا کن، وگرنه یه ردیف جدید تو جدول اضافه کن
        if (isset($check)) {

            $sql_update = "UPDATE price SET discount = '$discount',
                                exercise_price = '$exercise_price',
                                food_price = '$food_price',
                                supplement_price = '$supplement_price',
                                event = '$event'
                   WHERE coach_id='$coach_id'";

            if (mysqli_query($con, $sql_update)) {
                echo "successful";
            } else {
                echo "error";
            }

        } else {
            $sql = "INSERT INTO price (coach_id,exercise_price,food_price,supplement_price,discount,event)
            VALUES ('$coach_id','$exercise_price','$food_price','$supplement_price','$discount','$event')";

            if (mysqli_query($con, $sql)) {
                echo "اطلاعات با موفقیت ثبت شد";
            } else {
                echo "متاسفانه اطلاعات مورد نظر ثبت نشد. دوباره امتحان کنید.";
            }
        }
        mysqli_close($con);
    }

    public function setSubscriptionInformation(Request $request)
    {
        require_once('dbConnect.php');

//        $coachId = $request->coach_id;

        $coach_id = $request->coach_id;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $subscribe_type = $request->subscribe_type;
        $subscribe_price = $request->subscribe_price;


        //اگه مربی اشتراک خرید اطلاعاتش تو جدول ثبت میشه
        $sql = "INSERT INTO subscription (coach_id,start_date,end_date,subscribe_type,subscribe_price)
            VALUES ('$coach_id','$start_date','$end_date','$subscribe_type','$subscribe_price')";

        if (mysqli_query($con, $sql)) {
            echo "Successfully send";
        } else {
            echo "Could not send";
        }
        mysqli_close($con);
        //اگه مربی اشتراک خرید اطلاعاتش تو جدول ثبت میشه
        $sql = "INSERT INTO subscription (coach_id,start_date,end_date,subscribe_type,subscribe_price)
            VALUES ('$coach_id','$start_date','$end_date','$subscribe_type','$subscribe_price')";

        if (mysqli_query($con, $sql)) {
            echo "Successfully send";
        } else {
            echo "Could not send";
        }
        mysqli_close($con);
    }

    public function signUpCoaches(Request $request)
    {
        require_once('dbConnect.php');

        $device_id = $request->device_id;
        $name = $request->coach_name;
        $family = $request->coach_family;
        $sex = $request->coach_sex;
        $mobile = $request->coach_mobile;
        $email = $request->coach_email;
        $password = bcrypt($request->coach_password);

        //چک کن ببین قبلا کسی با این ایمیل ثبت نام کرده یا نه
        $checkEmail = "SELECT coach_email FROM coaches WHERE coach_email='$email'";
        //executing query
        $result = mysqli_query($con, $checkEmail);
        //fetching result
        $check = mysqli_fetch_array($result);
        //if we got some result
        if (isset($check)) {
            echo "این ایمیل قبلا ثبت شده است";
        } else {
            $sql = "INSERT INTO coaches (device_id,coach_name,coach_family,coach_sex,coach_mobile,coach_email,password)
            VALUES ('$device_id','$name','$family','$sex','$mobile','$email','$password')";

            if (mysqli_query($con, $sql)) {
                echo "successful";
            } else {
                echo "error";
            }
        }
        mysqli_close($con);

    }

    public function updateCoachInformation(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $request->coach_id;
        $name = $request->coach_name;
        $family = $request->coach_family;
        $honors = $request->coach_honors;
        $age = $request->coach_age;
        $record = $request->coach_record;
        $state = $request->coach_state;
        $city = $request->coach_city;
        $coach_card_number = $request->coach_card_number;
        $coach_shaba_number = $request->coach_shaba_number;
        $coach_instagram = $request->coach_instagram;

        //اطلاعات مربی رو بگیر و در جدول coaches بروزرسانی کن
        $sql_update = "UPDATE coaches SET coach_name = '$name', coach_family = '$family', coach_honors = '$honors',
                    coach_age = '$age',coach_record = '$record', coach_state = '$state', coach_city = '$city',
                    coach_card_number = '$coach_card_number',coach_shaba_number = '$coach_shaba_number',
                    coach_instagram = '$coach_instagram'WHERE coach_id='$coachId'";
        if (mysqli_query($con, $sql_update)) {
            echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

    public function uploadCoachImageProfile(Request $request)
    {
        require_once('dbConnect.php');

        $objFile = &$_FILES["image"];
        $coachId = $_POST['coach_id'];

        //عکس و آیدی مربی رو بگیر و عکس پروفایل مربی رو تغییر بده
        $path = "images/coachesImagesProfile/$coachId.png";

        if (move_uploaded_file($objFile["tmp_name"], $path)) {
            print "successful";
        } else {
            print "error";
        }
    }

    public function uploadCoachImages(Request $request)
    {
        require_once('dbConnect.php');

        $objFile = &$_FILES["image"];
        $coachId = $_POST['coach_id'];
        $ImageName = $_POST['image_name'];

        //اون سه تا عکسی که مربی میتونه واسه پروفایلش تنظیم کنه در مسیر زیر قرار میگیرن
        $dir = "bodybuilding/coach/images/coachesImages/$coachId";
        if (!file_exists($dir)){
              mkdir($dir);
        }
        $path = "$dir/$ImageName.png";

        if (move_uploaded_file($objFile["tmp_name"], $path)) {
            print "successful";
        }

    }

    public function uploadGymImages(Request $request)
    {
        require_once('dbConnect.php');

        $objFile = &$_FILES["image"];
        $coachId = $_POST['coach_id'];
        $gymImageName = $_POST['gym_image_name'];

        //آیدی باشگاه مربی مورد نظر رو پیدا کن و عکس های باشگاه رو در مسیر مورد نظر قرار بده
        $id = "SELECT gym_id FROM gym WHERE coach_id='$coachId'";
        $findId = mysqli_query($con, $id);
        while ($row = mysqli_fetch_assoc($findId)) {
            $gymId = $row['gym_id'];
        }

        $dir = "bodybuilding/coach/images/gymImage/$gymId";
        if (!file_exists($dir)){
            mkdir($dir);
        }

        $path = "$dir/$gymImageName.png";

        if (move_uploaded_file($objFile["tmp_name"], $path)) {
            print "successful";
        }
    }
}