<?php

namespace App\Http\Controllers\API;

use App\coach;
use App\Exercise;
use App\Food;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;

class CoachGymApiController
{
    public function getMyGymImages(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $_GET['coach_id'];
        $gym = array();

        //آیدی باشگاه مربوط به مربی را پیدا کن
        $checkGym = "SELECT gym_id FROM gym WHERE coach_id='$coachId'";
        $result = mysqli_query($con, $checkGym);
        $check = mysqli_fetch_array($result);
        //اگر مربی باشگاه خودش رو وارد کرده بود قبلا
        if (isset($check)) {

            //آیدی باشگاه رو پیدا کن
            $id = "SELECT gym_id FROM gym WHERE coach_id ='$coachId'";
            $findInfo = mysqli_query($con, $id);

            if ($findInfo->num_rows > 0) {
                while ($row = mysqli_fetch_assoc($findInfo)) {
                    $gymId = $row['gym_id'];
                }
            }

            //عکس های مربوط به باشگاه رو بفرست
            for ($i = 0; $i <= 2; $i++) {
                array_push($gym, "http://sfit.ir/bodybuilding/gym/images/gymImage/$gymId/" . $i . ".png");
            }
            echo json_encode($gym);
        }else{
            echo "gym not found";
        }


    }

 public function getMyGymInformation(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $_GET['coach_id'];

        //آیدی باشگاه مربوط به مربی را پیدا کن
        $id = "SELECT gym_id FROM gym WHERE coach_id ='$coachId'";
        $findInfo = mysqli_query($con, $id);

        if ($findInfo->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($findInfo)) {
                $gymId = $row['gym_id'];
            }
        }

        //اطلاعات باشگاه مورد نظر را بفرست
        $sql = "SELECT * FROM gym WHERE gym_id = '$gymId'";
        $result = $con->query($sql);
        $gym = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['gym_name'] = $row['gym_name'];
                $temp['gym_type'] = $row['gym_type'];
                $temp['gym_state'] = $row['gym_state'];
                $temp['gym_city'] = $row['gym_city'];
                $temp['gym_address'] = $row['gym_address'];
                $temp['gym_monthly_fee'] = $row['gym_monthly_fee'];
                $temp['gym_services'] = $row['gym_services'];
                $temp['gym_students_number'] = $row['gym_students_number'];

                array_push($gym, $temp);
            }
        }
        echo json_encode($gym);

        mysqli_close($con);


    }

 public function my_gyms(Request $request)
    {
        require_once('dbConnect.php');

        $coach_id = $_GET['coach_id'];

        //لیستی از باشگاه های ثبت شده توسط شاگرد مورد نظر را پیدا کن و بفرست.
        $sql = "SELECT * FROM gym WHERE coach_id='$coach_id'";
        $result = $con->query($sql);
        $gyms = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $gymId = $row['gym_id'];
                $temp['gym_id'] = $row['gym_id'];
                $temp['gym_name'] = $row['gym_name'];
                $temp['gym_image'] = "http://sfit.ir/bodybuilding/gym/images/gymImage/$gymId/" . 0 . ".png";

                array_push($gyms, $temp);
            }
        }


    }

 public function send_gym_time_and_day_with_array(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $_POST['coach_id'];
        $array = $_POST['array'];

        //آیدی باشگاه مربی مورد نظر را پیدا کن
        $id = $con->query("SELECT gym_id g_id FROM gym WHERE coach_id='$coachId'");
        $fetchGymId = $id->fetch_assoc();
        $gym_id = $fetchGymId['g_id'];

        $timeAndDate = json_decode($array);

        $i = 0;
        while ($i < count($timeAndDate)) {

            $gym_status = $timeAndDate[$i]->gym_status;
            $day = $timeAndDate[$i]->day;
            $sex = $timeAndDate[$i]->sex;
            $time_from = $timeAndDate[$i]->time_from;
            $time_to = $timeAndDate[$i]->time_to;

            //ساعات و روزهای کاری باشگاه را بروزرسانی یا ثبت کن.
            $checkGym = "SELECT gym_id FROM working_days_times WHERE gym_id='$gym_id' AND days='$day' AND sex='$sex'";
            //executing query
            $result = mysqli_query($con, $checkGym);
            //fetching result
            $check = mysqli_fetch_array($result);
            //if we got some result
            if (isset($check)) {

                $sql_update = "UPDATE working_days_times SET gym_status = '$gym_status',
                                time_from = '$time_from',
                                     time_to = '$time_to'
                   WHERE gym_id='$gym_id' AND days='$day' AND sex='$sex'";
                if (mysqli_query($con, $sql_update)) {
                    $i++;
                } else {
                    break;
                }
            } else if ($sex == "" && $time_from == "" && $time_to == "") {
                $sql_update = "UPDATE working_days_times SET gym_status = '$gym_status',
                                time_from = '$time_from',
                                     time_to = '$time_to'
                   WHERE gym_id='$gym_id' AND days='$day'";
                if (mysqli_query($con, $sql_update)) {
                    $i++;
                } else {
                    break;
                }
            } else {
                $sql = "INSERT INTO working_days_times (gym_id,days,gym_status,sex,time_from,time_to)
            VALUES ('$gym_id','$day','$gym_status','$sex','$time_from','$time_to')";
                if (mysqli_query($con, $sql)) {
                    $i++;
                } else {
                    break;
                }
            }
        }
        if ($i == count($timeAndDate)) {
            echo "successful";
        } else {
            echo "error";
        }

        mysqli_close($con);


    }

 public function sendGymInformation(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $request->coach_id;
        $gymId = $request->gym_id;
        $gymName = $request->gym_name;
        $gymType = $request->gym_type;
        $gymState = $request->gym_state;
        $gymCity = $request->gym_city;
        $gymAddress = $request->gym_address;
        $gymMonthlyFee = $request->gym_monthly_fee;
        $gymServices = $request->gym_services;
        $gymStudentsNumber = $request->gym_students_number;
        $doing = $request->doing;

        //اگر عمل افزودن باشگاه میخواست انجام بشه، یه باشگاه جدید اضافه کن.
        if ($doing == "add") {
            $sql = "INSERT INTO gym (coach_id,gym_name,gym_type,gym_state,gym_city,gym_address,gym_monthly_fee,gym_services,gym_students_number,gym_visibility)
            VALUES ('$coachId','$gymName','$gymType','$gymState','$gymCity','$gymAddress','$gymMonthlyFee','$gymServices','$gymStudentsNumber','hide')";

            if (mysqli_query($con, $sql)) {
                echo "اطلاعات باشگاه شما با موفقیت ثبت شد";
            } else {
                echo "error";
            }
            //اگر عمل ویرایش باشگاه میخواست انجام بشه، اطلاعات باشگاه را بروزرسانی کن.
        } else if ($doing == "edit") {
            $sql_update = "UPDATE gym SET gym_name = '$gymName',
                                gym_type = '$gymType',
                                gym_state = '$gymState',
                                     gym_city = '$gymCity',
                               gym_address = '$gymAddress',
                                gym_monthly_fee = '$gymMonthlyFee',
                               gym_services = '$gymServices',
                               gym_students_number = '$gymStudentsNumber'
                   WHERE gym_id='$gymId'";

            if (mysqli_query($con, $sql_update)) {
                echo "اطلاعات باشگاه بروزرسانی شد";
            } else {
                echo "error";
            }
        }
        mysqli_close($con);


    }

 public function sendGymTimeAndDay(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $request->coach_id;
        $gymStatus = $request->gym_status;
        $gymDay = $request->day;
        $gymSex = $request->sex;
        $gymTimeFrom = $request->time_from;
        $gymTimeTo = $request->time_to;

        $gymId = "";

        //آیدی باشگاه مربی مورد نظر را پیدا کن
        $id = "SELECT gym_id FROM gym WHERE coach_id='$coachId'";
        $findInfo = mysqli_query($con, $id);

        if ($findInfo->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($findInfo)) {
                $gymId = $row['gym_id'];
            }
        }

        //چک کن ببین قبلا ساعات و روزهای کاری باشگاه رو ثبت کرده یا نه
        $checkGym = "SELECT gym_id FROM working_days_times WHERE gym_id='$gymId' AND days='$gymDay' AND sex='$gymSex'";
        $result = mysqli_query($con, $checkGym);
        $check = mysqli_fetch_array($result);
        //اگه ثبت کرده بود بروزرسانی کن، اگه نه یه ردیف جدید به جدول working_days_times اضافه کن
        if (isset($check)) {

            $sql_update = "UPDATE working_days_times SET gym_status = '$gymStatus',
                                time_from = '$gymTimeFrom',
                                     time_to = '$gymTimeTo'
                   WHERE gym_id='$gymId' AND days='$gymDay' AND sex='$gymSex'";

            mysqli_query($con, $sql_update);

            if (mysqli_query($con, $sql_update)) {
                echo "successful";
            } else {
                echo "error";
            }

        } else if ($gymSex == "" && $gymTimeFrom == "" && $gymTimeTo == "") {
            $sql_update = "UPDATE working_days_times SET gym_status = '$gymStatus',
                                time_from = '$gymTimeFrom',
                                     time_to = '$gymTimeTo'
                   WHERE gym_id='$gymId' AND days='$gymDay'";
            if (mysqli_query($con, $sql_update)) {
                echo "successful";
            } else {
                echo "error";
            }
        } else {
            $sql = "INSERT INTO working_days_times (gym_id,days,gym_status,sex,time_from,time_to)
            VALUES ('$gymId','$gymDay','$gymStatus','$gymSex','$gymTimeFrom','$gymTimeTo')";

            if (mysqli_query($con, $sql)) {
                echo "successful";
            } else {
                echo "error";
            }
        }
        mysqli_close($con);


    }

 public function setGymLocation(Request $request)
    {
        require_once('dbConnect.php');

        $coachId = $_POST['coach_id'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];

        //باشگاه مربی مورد نظر رو پیدا کن و طول و عرض جغرافیایی باشگاه رو در جدول بروزرسانی کن
        $findGym = "SELECT gym_id FROM gym WHERE coach_id = '$coachId'";
        //executing query
        $result = mysqli_query($con, $findGym);
        //fetching result
        $check = mysqli_fetch_array($result);
        //if we got some result
        if (isset($check)) {

            $sql_update = "UPDATE gym SET latitude = '$latitude', longitude = '$longitude' WHERE coach_id='$coachId'";

            if (mysqli_query($con, $sql_update)) {
                echo "successful";
            } else {
                echo "error";
            }
        }else {
            echo "gym_not_found";
        }
        mysqli_close($con);

    }


}