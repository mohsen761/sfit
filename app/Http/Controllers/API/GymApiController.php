<?php

namespace App\Http\Controllers\API;

use App\coach;
use App\Exercise;
use App\Food;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;

class GymApiController
{
    public function delete_gym(Request $request)
    {
        require_once('dbConnect.php');
        $gym_id = $_GET['gym_id'];

        $checking = "SELECT gym_id FROM working_days_times WHERE gym_id = '$gym_id'";
        //executing query
        $result = mysqli_query($con, $checking);
        //fetching result
        $check = mysqli_fetch_array($result);
        //اگه بود اول حرکت ها رو از جدول exercises پاک کن، بعد برنامه رو از جدول program پاک کن.
        if (isset($check)) {

            $sql = "DELETE FROM working_days_times WHERE gym_id = '$gym_id'";
            $result = mysqli_query($con, $sql);

            if (mysqli_affected_rows($con) > 0) {

                $sql = "DELETE FROM gym WHERE gym_id = '$gym_id'";
                $result = mysqli_query($con, $sql);

                if (mysqli_affected_rows($con) > 0) {
                    $dir = "images/gymImage/$gym_id";
                    removeDirectory($dir);
                    echo "successful";
                } else {
                    echo "error";
                }
            } else {
                echo "error";
            }
        } else {
            $sql = "DELETE FROM gym WHERE gym_id = '$gym_id'";
            $result = mysqli_query($con, $sql);

            if (mysqli_affected_rows($con) > 0) {
                $dir = "images/gymImage/$gym_id";
                removeDirectory($dir);
                echo "successful";
            } else {
                echo "error";
            }
        }

        mysqli_close($con);
    }

    public function getGymDaysAndTimes(Request $request)
    {
        require_once('dbConnect.php');
        $gymId = $_GET['gym_id'];

        //ساعات ها و روزهای کاری باشگاه مورد نظر را از جدول working_days_times پیدا کن و بفرست.
        $sql = "SELECT * FROM working_days_times WHERE gym_id = '$gymId'";
        $result = $con->query($sql);
        $gym = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['days'] = $row['days'];
                $temp['gym_status'] = $row['gym_status'];
                $temp['sex'] = $row['sex'];
                $temp['time_from'] = $row['time_from'];
                $temp['time_to'] = $row['time_to'];

                array_push($gym, $temp);
            }

        }
        echo json_encode($gym);

        mysqli_close($con);
    }

    public function getGymImages(Request $request)
    {
        require_once('dbConnect.php');
        $gymId = $_GET['gym_id'];

        $gym = array();

        //سه عکس مربوط به باشگاه مورد نظر را از مسیر مشخص شده زیر پیدا کن و بفرست.
        for ($i = 0; $i <= 2; $i++) {
            array_push($gym, "http://sfit.ir/bodybuilding/gym/images/gymImage/$gymId/" . $i . ".png");
        }

        echo json_encode($gym);

        mysqli_close($con);
    }

    public function gymInformation(Request $request)
    {
        require_once('dbConnect.php');
        $gymId = $_GET['gym_id'];

        //اطلاعات مربوط به باشگاه مورد نظر را پیدا کن و بفرست.
        $sql = "SELECT * FROM gym WHERE gym_id = '$gymId'";
        $result = $con->query($sql);
        $gym = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $temp['gym_name'] = $row['gym_name'];
                $temp['gym_type'] = $row['gym_type'];
                $temp['gym_state'] = $row['gym_state'];
                $temp['gym_city'] = $row['gym_city'];
                $temp['gym_address'] = $row['gym_address'];
                $temp['gym_monthly_fee'] = $row['gym_monthly_fee'];
                $temp['gym_services'] = $row['gym_services'];
                $temp['gym_students_number'] = $row['gym_students_number'];
                $temp['gym_score'] = $row['gym_score'];
                $temp['latitude'] = $row['latitude'];
                $temp['longitude'] = $row['longitude'];

                array_push($gym, $temp);
            }

        }
        echo json_encode($gym);

        mysqli_close($con);
    }

    public function gyms(Request $request)
    {
        require_once('dbConnect.php');

        $sql = "SELECT * FROM gym WHERE gym_visibility='show' ORDER BY gym_score DESC";
        $result = $con->query($sql);
        $gyms = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $gymId = $row['gym_id'];
                $temp['gym_id'] = $row['gym_id'];
                $temp['gym_name'] = $row['gym_name'];
                $temp['gym_image'] = "http://sfit.ir/bodybuilding/gym/images/gymImage/$gymId/" . 0 . ".png";

                array_push($gyms, $temp);

            }

        }
        echo json_encode($gyms);
        mysqli_close($con);
    }

    public function my_gyms(Request $request)
    {
        require_once('dbConnect.php');
        $userId = $_GET['user_id'];

        //لیستی از باشگاه های ثبت شده توسط شاگرد مورد نظر را پیدا کن و بفرست.
        $sql = "SELECT * FROM gym WHERE user_id='$userId'";
        $result = $con->query($sql);
        $gyms = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                $temp = array();
                $gymId = $row['gym_id'];
                $temp['gym_id'] = $row['gym_id'];
                $temp['gym_name'] = $row['gym_name'];
                $temp['gym_image'] = "http://sfit.ir/bodybuilding/gym/images/gymImage/$gymId/" . 0 . ".png";

                array_push($gyms, $temp);
            }
        }
        echo json_encode($gyms);
        mysqli_close($con);
    }

    public function send_gym_time_and_day_with_array(Request $request)
    {
        require_once('dbConnect.php');
        $gym_id = $_POST['gym_id'];
        $array = $_POST['array'];

        $timeAndDate = json_decode($array);

        $i = 0;
        while ($i < count($timeAndDate)) {

            $gym_status = $timeAndDate[$i]->gym_status;
            $day = $timeAndDate[$i]->day;
            $sex = $timeAndDate[$i]->sex;
            $time_from = $timeAndDate[$i]->time_from;
            $time_to = $timeAndDate[$i]->time_to;

            //ساعات و روزهای کاری باشگاه را بروزرسانی یا ثبت کن.
            $checkGym = "SELECT gym_id FROM working_days_times WHERE gym_id='$gym_id' AND days='$day' AND sex='$sex'";
            //executing query
            $result = mysqli_query($con, $checkGym);
            //fetching result
            $check = mysqli_fetch_array($result);
            //if we got some result
            if (isset($check)) {

                $sql_update = "UPDATE working_days_times SET gym_status = '$gym_status',
                                time_from = '$time_from',
                                     time_to = '$time_to'
                   WHERE gym_id='$gym_id' AND days='$day' AND sex='$sex'";
                if (mysqli_query($con, $sql_update)) {
                    $i++;
                } else {
                    break;
                }
            } else {
                $sql = "INSERT INTO working_days_times (gym_id,days,gym_status,sex,time_from,time_to)
            VALUES ('$gym_id','$day','$gym_status','$sex','$time_from','$time_to')";
                if (mysqli_query($con, $sql)) {
                    $i++;
                } else {
                    break;
                }
            }
        }
        if ($i == count($timeAndDate)) {
            echo "successful";
        } else {
            echo "error";
        }

        mysqli_close($con);
    }

    public function sendGymInformation(Request $request)
    {
        require_once('dbConnect.php');
        $userId = $_POST['user_id'];
        $gymId = $_POST['gym_id'];
        $gymName = $_POST['gym_name'];
        $gymType = $_POST['gym_type'];
        $gymState = $_POST['gym_state'];
        $gymCity = $_POST['gym_city'];
        $gymAddress = $_POST['gym_address'];
        $gymMonthlyFee = $_POST['gym_monthly_fee'];
        $gymServices = $_POST['gym_services'];
        $gymStudentsNumber = $_POST['gym_students_number'];
        $doing = $_POST['doing'];

        //اگر عمل افزودن باشگاه میخواست انجام بشه، یه باشگاه جدید اضافه کن.
        if ($doing == "add") {
            $sql = "INSERT INTO gym (user_id,gym_name,gym_type,gym_state,gym_city,gym_address,gym_monthly_fee,gym_services,gym_students_number,gym_visibility)
            VALUES ('$userId','$gymName','$gymType','$gymState','$gymCity','$gymAddress','$gymMonthlyFee','$gymServices','$gymStudentsNumber','hide')";

            if (mysqli_query($con, $sql)) {
                echo "اطلاعات باشگاه شما با موفقیت ثبت شد";
            } else {
                echo "error";
            }
            //اگر عمل ویرایش باشگاه میخواست انجام بشه، اطلاعات باشگاه را بروزرسانی کن.
        } else if ($doing == "edit") {
            $sql_update = "UPDATE gym SET gym_name = '$gymName',
                                gym_type = '$gymType',
                                gym_state = '$gymState',
                                     gym_city = '$gymCity',
                               gym_address = '$gymAddress',
                                gym_monthly_fee = '$gymMonthlyFee',
                               gym_services = '$gymServices',
                               gym_students_number = '$gymStudentsNumber'
                   WHERE gym_id='$gymId'";

            if (mysqli_query($con, $sql_update)) {
                echo "اطلاعات باشگاه بروزرسانی شد";
            } else {
                echo "error";
            }
        }
        mysqli_close($con);
    }

    public function sendGymTimeAndDay(Request $request)
    {
        require_once('dbConnect.php');
        $gym_id = $_POST['gym_id'];
        $gymStatus = $_POST['gym_status'];
        $gymDay = $_POST['day'];
        $gymSex = $_POST['sex'];
        $gymTimeFrom = $_POST['time_from'];
        $gymTimeTo = $_POST['time_to'];

        //ساعات و روزهای کاری باشگاه را بروزرسانی یا ثبت کن.
        $checkGym = "SELECT gym_id FROM working_days_times WHERE gym_id='$gym_id' AND days='$gymDay' AND sex='$gymSex'";
        //executing query
        $result = mysqli_query($con, $checkGym);
        //fetching result
        $check = mysqli_fetch_array($result);
        //if we got some result
        if (isset($check)) {

            $sql_update = "UPDATE working_days_times SET gym_status = '$gymStatus',
                                time_from = '$gymTimeFrom',
                                     time_to = '$gymTimeTo'
                   WHERE gym_id='$gym_id' AND days='$gymDay' AND sex='$gymSex'";
            if (mysqli_query($con, $sql_update)) {
                echo "successful";
            } else {
                echo "error";
            }
        } else {
            $sql = "INSERT INTO working_days_times (gym_id,days,gym_status,sex,time_from,time_to)
            VALUES ('$gym_id','$gymDay','$gymStatus','$gymSex','$gymTimeFrom','$gymTimeTo')";
            if (mysqli_query($con, $sql)) {
                echo "successful";
            } else {
                echo "error";
            }
        }
        mysqli_close($con);
    }

    public function setGymLocation(Request $request)
    {
        require_once('dbConnect.php');
        $userId = $_POST['user_id'];
        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];

        //باشگاه شاگرد مورد نظر رو پیدا کن و طول و عرض جغرافیایی باشگاه رو در جدول بروزرسانی کن
        $findGym = "SELECT gym_id FROM gym WHERE user_id = '$userId'";
        $result = mysqli_query($con, $findGym);

        $check = mysqli_fetch_array($result);

        if (isset($check)) {

            $sql_update = "UPDATE gym SET latitude = '$latitude', longitude = '$longitude' WHERE user_id='$userId'";

            if (mysqli_query($con, $sql_update)) {
                echo "successful";
            } else {
                echo "error";
            }
        }else {
            echo "gym_not_found";
        }
        mysqli_close($con);
    }

     public function uploadGymImages(Request $request)
    {
        require_once('dbConnect.php');
        $objFile = &$_FILES["image"];
        $gymId = $_POST['gym_id'];
        $gymImageName = $_POST['gym_image_name'];

        //عکس های باشگاه را بگیر و در محل مورد نظر قرار بده
        $dir = "images/gymImage/$gymId";
        mkdir($dir);

        $path = "$dir/$gymImageName.png";

        if (move_uploaded_file($objFile["tmp_name"], $path)) {
            print "successful";
        } else {
            print "error";
        }
    }

  public function find_movement(Request $request)
    {
        require_once('dbConnect.php');
        $movement_name = $_GET['movement_name'];

        //لینک ویدئو و توضیحات حرکت را پیدا کن و بفرست
        $sql = "SELECT * FROM movements WHERE movement_name = '$movement_name'";
        $result = $con->query($sql);
        $movements = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                array_push($movements, array(

                    "movement_id" => $row['movement_id'],
                    "movement_name" => $row['movement_name'],
                    "movement_type" => $row['movement_type'],
                    "movement_description" => $row['movement_description'],
                    "movement_video_link" => $row["movement_video_link"],
                    "movement_picture_link" => $row["movement_picture_link"]
                ));
            }

        }
        echo json_encode($movements);

        mysqli_close($con);
    }

  public function get_workouts(Request $request)
    {
        require_once('dbConnect.php');
        $movement_type = $_GET['movement_type'];

        if ($movement_type == "all") {
            //لیستی از همه ی حرکات تمرینی را پیدا کن و بفرست.
            $sql = "SELECT * FROM movements";
            $result = $con->query($sql);
            $movements = array();
            if ($result->num_rows > 0) {
                // output data of each row
                while ($row = mysqli_fetch_assoc($result)) {

//        $movement_id = $row['movement_id'];

                    array_push($movements, array(

                        "movement_id" => $row['movement_id'],
                        "movement_name" => $row['movement_name'],
                        "movement_type" => $row['movement_type'],
                        "movement_description" => $row['movement_description'],
//            "movement_video_link" => "http://192.168.1.7/bodybuilding/gym/workouts/video/$movement_id" . ".mp4",
//            "movement_picture_link" => "http://192.168.1.7/bodybuilding/gym/workouts/picture/$movement_id" . ".png"
                        "movement_video_link" => $row["movement_video_link"],
                        "movement_picture_link" => $row["movement_picture_link"]
                    ));
                }
            }
            echo json_encode($movements);
        } else {
            //لیستی از حرکات تمرینی مشخص شده را پیدا کن و بفرست.
            $sql = "SELECT * FROM movements WHERE movement_type='$movement_type'";
            $result = $con->query($sql);
            $movements = array();
            if ($result->num_rows > 0) {
                // output data of each row
                while ($row = mysqli_fetch_assoc($result)) {

//        $movement_id = $row['movement_id'];

                    array_push($movements, array(

                        "movement_id" => $row['movement_id'],
                        "movement_name" => $row['movement_name'],
                        "movement_type" => $row['movement_type'],
                        "movement_description" => $row['movement_description'],
                        "movement_video_link" => $row["movement_video_link"],
                        "movement_picture_link" => $row["movement_picture_link"]
                    ));
                }
            }
            echo json_encode($movements);
        }

        mysqli_close($con);
    }

  public function send_movement(Request $request)
    {
        require_once('dbConnect.php');
        $movement_type = $_POST['movement_type'];
        $array = $_POST['array'];

        $array_of_movement = array();
        $movements = json_decode($array);

        $i = 0;
        while ($i < count($movements)) {
            $movement_name = $movements[$i];

            $sql = "INSERT INTO movements (movement_name,movement_type)
            VALUES ('$movement_name','$movement_type')";

            if (mysqli_query($con, $sql)) {
                $i++;
            } else {
                break;
            }
        }
        if ($i == count($movements)) {
            echo "successful";
        } else {
            echo "error";
        }
        mysqli_close($con);
    }

 public function listOfMusic(Request $request)
    {
        require_once('dbConnect.php');

        $sql = "SELECT * FROM gym_music";
        $result = $con->query($sql);
        $songs = array();
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = mysqli_fetch_assoc($result)) {

                array_push($songs, array(

                        "music_id" => $row['music_id'],
                        "music_link" => $row['music_link']
                    )
                );
            }

        }
        echo json_encode($songs);
        mysqli_close($con);
    }



}