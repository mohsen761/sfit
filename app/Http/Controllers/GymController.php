<?php

namespace App\Http\Controllers;

use App\Gym;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use View;
class GymController extends Controller
{
    public function share()
    {
        $coach=[ Auth::guard('coach')->user()->coach_name.'  '.Auth::guard('coach')->user()->coach_family,
            '/bodybuilding/coach/images/coachesImagesProfile/'.Auth::guard('coach')->user()->coach_id.'.png'
        ];
        return View::share('coach' ,$coach);
    }
    public function mygym()
    {
        $this->share();
        $gymdetails= Gym::where('coach_id' ,Auth::guard('coach')->user()->coach_id)->get();

        if (count($gymdetails) == 0){
            $gymdetails='null';
        }
        return view('coach.mygym',compact('gymdetails'));
    }

    public function edit(Gym $gym)
    {
        $this->share();
        $gymdetail= $gym;
        return view('coach.editgym',compact('gymdetail'));
    }

    public function update(Request $request,  $id)
    {
        $this->share();

        $validatedData = $request->validate([
            'gym_name' => 'required',
            'gym_state' => 'required',
            'gym_city' => 'required',
            'gym_address' => 'required',
            'gym_monthly_fee' => 'required',
            'gym_services' => 'required',
        ]);

            $gym = Gym::find($id);
            $gym->gym_name                    = $request->gym_name;
            $gym->gym_type                    = $request->gym_type;
            $gym->gym_state                   = $request->gym_state;
            $gym->gym_city                    = $request->gym_city;
            $gym->gym_address                 = $request->gym_address;
            $gym->gym_monthly_fee             = $request->gym_monthly_fee;
            $gym->gym_services                 = $request->gym_services;
            $gym->gym_students_number         = $request->gym_students_number;
            $gym->latitude                    = $request->latitude;
            $gym->longitude                   = $request->longitude;

            $gym->save();

            // redirect
//            \Session::flash('message', 'Successfully updated nerd!');
            return redirect()->back()->with([
                'success' =>'با موفقیت تغییرات ثبت شد']);


    }

    public function store(Request $request)
    {
        $this->share();
        $gym = new Gym;
//        dd(Auth::guard('coach')->user()->id);
        $gym->coach_id = Auth::guard('coach')->user()->coach_id;
        $gym->gym_name                    = $request->gym_name;
        $gym->gym_type                    = $request->gym_type;
        $gym->gym_state                   = $request->gym_state;
        $gym->gym_city                    = $request->gym_city;
        $gym->gym_address                 = $request->gym_address;
        $gym->gym_monthly_fee             = $request->gym_monthly_fee;
        $gym->gym_services                 = $request->gym_services;
        $gym->gym_students_number         = $request->gym_students_number;
        $gym->latitude                    = $request->latitude;
        $gym->longitude                   = $request->longitude;
        $gym->save();
        return redirect()->back()->with([
            'success' =>'باشگاه شما با موفقیت اضافه شد']);
    }
}
