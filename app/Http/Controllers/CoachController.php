<?php

namespace App\Http\Controllers;

use App\coach;
use Morilog\Jalali\Jalalian;
use App\Exercise;
use App\Food;
use App\Movement;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;
class CoachController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
{
    $this->middleware('coach');
}

    public function deleteimage($coach , $id)
    {
        if (is_file(public_path().'/bodybuilding/coach/images/coachesImages/'.Auth::guard('coach')->user()->coach_id.'/'.$id.'.png'))
        {
            unlink(public_path().'/bodybuilding/coach/images/coachesImages/'.Auth::guard('coach')->user()->coach_id.'/'.$id.'.png');
            return back() ->with(['mio' => ' پروفایل با موفقیت حذف گردید .)']);
        }
        return back();
    }

    public function change()
    {
        $users = Coach::all();
//        dd($users->all());
        foreach ($users as $user){
            if (  strlen ($user->password) <25)
//                echo $user->password;
           $user->password = bcrypt( $user->password);
            $user->save();
        }
    }

    public function share()
    {
        $coach=[ Auth::guard('coach')->user()->coach_name.'  '.Auth::guard('coach')->user()->coach_family,
                 '/bodybuilding/coach/images/coachesImagesProfile/'.Auth::guard('coach')->user()->coach_id.'.png'
            ];
       return View::share('coach' ,$coach);
    }

    public function swapexercises(Request $request)
    {

        $exercise1=Exercise::where('exercise_id',$request->first)->first();
        $exercise2=Exercise::where('exercise_id',$request->second)->first();

        $exercise = new Exercise();
        $exercise->type_of_movement = $exercise1->type_of_movement;
        $exercise->days =             $exercise1->days;
        $exercise->sets =             $exercise1->sets;
        $exercise->repetitions =      $exercise1->repetitions;
        $exercise->exercise_systems = $exercise1->exercise_systems;
        $exercise->save();
//        $exercise1 = $exercise2;
        $exercise1->type_of_movement = $exercise2->type_of_movement;
        $exercise1->days =             $exercise2->days;
        $exercise1->sets =             $exercise2->sets;
        $exercise1->repetitions =      $exercise2->repetitions;
        $exercise1->exercise_systems = $exercise2->exercise_systems;
        $exercise1->save();
//        $exercise2= $exercise;
        $exercise2->type_of_movement = $exercise->type_of_movement;
        $exercise2->days =             $exercise->days;
        $exercise2->sets =             $exercise->sets;
        $exercise2->repetitions =      $exercise->repetitions;
        $exercise2->exercise_systems = $exercise->exercise_systems;
        $exercise2->save();
        $exercise->delete();

//                dd($exercise1);
        return back()->with(['success' =>'با موفقیت عوض شد.']);

    }

    public function deleteprogram(Program $program)
    {
        $exercises = Exercise::where('program_id' , $program->program_id)->get();
        foreach ($exercises as $exercise){
//            $exercise= Exercise::find($exercise)->first();
            $exercise->delete();
        }
        $foods = Food::where('program_id' , $program->program_id)->get();
        foreach ($foods as $food){
//            $food= Exercise::find($food)->first();
            $food->delete();
        }
        $supplements = Supplement::where('program_id' , $program->program_id)->get();
        foreach ($supplements as $supplement){
//            $supplement= Exercise::find($supplement)->first();
            $supplement->delete();
        }
        $program->delete();
        return back()->with(['success' =>'با موفقیت حذف شد.']);
    }
    public function deleteexercise(Exercise $exercise)
    {
//        $programs = Program::where('program_id' , $exercise->program_id)->get();
//        dd(count($programs));
        $exercise->delete();
        return back()->with(['success' =>'با موفقیت حذف شد.']);
    }
    public function updateexercise(Request $request ,Exercise $exercise)
    {
        $exercise->type_of_movement=$request->type_of_movement;
        $exercise->days=$request->days;
        $exercise->sets =$request->sets;
        $exercise->repetitions =$request->repetitions;
        $exercise->exercise_systems=$request->exercise_systems;
        $exercise->save();
        return back()->with(['success' =>'با موفقیت تغییرات ثبت شد']);
    }
    public function editexercise(Exercise $exercise)
    {
        $this->share();
        return view('coach.editexercise',compact('exercise'));
    }
    public function deletefood(Food $food)
    {
//        $programs = Program::where('program_id' , $exercise->program_id)->get();
//        dd(count($programs));
        $food->delete();
        return back()->with(['success' =>'با موفقیت حذف شد.']);
    }
    public function updatefood(Request $request ,Food $food)
    {
        $food->meal=$request->meal;
        $food->offer1=$request->offer1;
        $food->offer2=$request->offer2;
        $food->offer3=$request->offer3;
        $food->save();
        return back()->with(['success' =>'با موفقیت تغییرات ثبت شد']);
    }
    public function editfood(Food $food)
    {
        $this->share();
        return view('coach.editfood',compact('food'));
    }

    public function deletesupplement(Supplement $supplement)
    {
//        $programs = Program::where('program_id' , $exercise->program_id)->get();
//        dd(count($programs));
        $supplement->delete();
        return back()->with(['success' =>'با موفقیت حذف شد.']);
    }
    public function updatesupplement(Request $request ,Supplement $supplement)
    {
        $supplement->supplement_name =$request->supplement_name ;
        $supplement->time_to_eat=$request->time_to_eat;
        $supplement->dosage=$request->dosage;
        $supplement->day_to_eat=$request->day_to_eat;
        $supplement->save();
        return back()->with(['success' =>'با موفقیت تغییرات ثبت شد']);
    }
    public function editsupplement(Supplement $supplement)
    {
        $this->share();
        return view('coach.editsupplement',compact('supplement'));
    }

    public function food(Order $order)
    {
        $this->share();
        if ($order->food == 'yes'){
            $programs = Program::where('order_id' , $order->order_id)
                ->where('coach_id' , Auth::guard('coach')->user()->coach_id)->
                where('program_type','food')->get();
            $foods=[];
            foreach ($programs as $program){
                $foods = Arr::prepend($foods, Food::where('program_id' , $program->program_id)->get());
            }
//          return dd($programs ,$exercises);
            $program_state='not_sent';
            if ($order->program_state != 'not_sent'){
                $program_state='sent';
            }
            return view('coach.showfood',compact(['programs','foods','program_state']));
        }else{
            return back();
        }
    }
    public function exercise(Order $order )
    {
//        dd($order->order_id);
        $this->share();
        if ($order->exercise == 'yes'){
            $id = $order->order_id;
            if ($order->exercise_type == 'text') {
                $programs = Program::where('order_id', $order->order_id)
                    ->where('coach_id', Auth::guard('coach')->user()->coach_id)->
                    where('program_type', 'exercise')->get();
                $exercises = [];
                foreach ($programs as $program) {
                    $exercises = Arr::prepend($exercises, Exercise::where('program_id', $program->program_id)->get());
                }
//          return dd($programs ,$exercises);
                $program_state = 'not_sent';
                if ($order->program_state != 'not_sent') {
                    $program_state = 'sent';
                }
                return view('coach.showexercise', compact(['programs', 'exercises', 'program_state']));
            }else{
                return view('coach.imageexercise',compact('id'));
            }
        }else{
            return back();
        }
    }
    public function supplement(Order $order )
    {
//        dd($order->order_id);
        $this->share();
        if ($order->supplement == 'yes'){
           $programs = Program::where('order_id' , $order->order_id)
                                ->where('coach_id' , Auth::guard('coach')->user()->coach_id)->
                                    where('program_type','supplement')->get();
            $supplements=[];
           foreach ($programs as $program){
               $supplements = Arr::prepend($supplements, Supplement::where('program_id' , $program->program_id)->get());
           }
//          return dd($programs ,$exercises);
            $program_state='not_sent';
            if ($order->program_state != 'not_sent'){
                $program_state='sent';
            }
           return view('coach.showsupplement',compact(['programs','supplements','program_state']));
        }else{
            return back();
        }
    }
    public function sendprogram(Order $order)
    {
        $order= Order::where('order_id' ,$order->order_id)->first();
        if($order->exercise == 'yes'){
            $program = Program::where('order_id', $order->order_id)->where('program_type' , 'exercise')->first();
            if (isset($program) ) {
            $exercise = Exercise::where('program_id' ,$program->program_id)->get();
            if (isset($exercise) ){
                if (count($exercise) >1){
                    $order->exercise_type = 'text';
                }
            }
          }else{$order->exercise_type = 'image';}

        }
        if($order->food == 'yes'){
            $program = Program::where('order_id', $order->order_id)->where('program_type' , 'food')->first();
            if (isset($program) ) {
            $foods = Food::where('program_id' ,$program->program_id)->get();
            if (isset($foods) ) {
                if (count($foods) > 1) {
                    $order->food_type = 'text';
                }
            }
            }else{$order->food_type = 'image';}

        }
        if($order->supplement == 'yes'){
            $program = Program::where('order_id', $order->order_id)->where('program_type' , 'supplement')->first();
            if (isset($program)) {
                $supplements = Supplement::where('program_id', $program->program_id)->get();
                if (isset($supplements)) {
                    if (count($supplements) > 1) {
                        $order->supplement_type = 'text';
                    }
                }
            }else{
            $order->supplement_type = 'image';
            }
        }
        $order->program_state ='sent';
        $date=Str::limit(today(),10);
        $order->program_date =str_replace("...","", $date) ;
        $order->save();
        return back()->with(['success' => 'برنامه با موفقیت تایید نهایی و ارسال شد.)']);

    }
    public function changepassword()
    {
        $this->share();
        return view('coach.changepassword');
    }
    public function postchangepassword(Request $request)
    {
        $this->share();
        $request_data = $request->All();
        $validator = $this->admin_credential_rules($request_data);
        if($validator->fails())
        {
                return redirect()->back()->with([
                    'new-password'=>'رمز جدید را به صورت درست وارد کنید'
                ]);
        }
        else
        {
            $current_password = Auth::guard('coach')->user()->password;
            if(Hash::check($request_data['current-password'], $current_password))
            {
                $user_id = Auth::guard('coach')->user()->coach_id;
                $obj_user = Coach::find($user_id);
                $obj_user->password = Hash::make($request_data['password']);;
                $obj_user->save();

                    return redirect()->back()->with([
                        'success' =>'رمز با موفقیت تغییر یافت'
                    ]);
            }
            else
            {
                    return redirect()->back()->with([
                        'current-password'=>'رمز فعلی را به صورت درست وارد کنید'
                    ]);
            }
        }

    }
    public function admin_credential_rules(array $data)
    {
        $messages = [
            'current-password.required' => 'لطفا رمز فعلی را وارد کنید',
            'password.required' => 'رمز را وارد کنید',
        ];

        $validator = Validator::make($data, [
            'current-password' => 'required',
            'password' => 'required|same:password',
            'password_confirmation' => 'required|same:password',
        ], $messages);

        return $validator;
    }
    public function changeprice(Request $request)
    {
        $this->share();
        $coachPrices=Price::where('coach_id',Auth::guard('coach')->user()->coach_id)->first();
        if (!isset($coachPrices)){
            $price = new Price();
            $price->coach_id = Auth::guard('coach')->user()->coach_id;
            $price->exercise_price = $request->exercise_price;
            $price->food_price = $request->food_price;
            $price->supplement_price = $request->supplement_price;
            $price->discount = $request->discount;
            $price->save();
            return redirect()->back()->with([
                'success' =>'قیمت با موفقیت ثبت شد'
            ]);
        }
        $price = Price::where('coach_id',Auth::guard('coach')->user()->coach_id)->first();
        $price->exercise_price = $request->exercise_price;
        $price->food_price = $request->food_price;
        $price->supplement_price = $request->supplement_price;
        $price->discount = $request->discount;
        $price->save();
        return redirect()->back()->with([
            'success' =>'قیمت با موفقیت تغییر یافت'
        ]);

    }
    public function editprice()
    {
        $this->share();
        $coachPrices=Price::where('coach_id',Auth::guard('coach')->user()->coach_id)->first();
        return view('coach.editprice',compact('coachPrices'));

    }
    public function registergetmoney()
    {
        $this->share();
//
        return redirect()->back()->with([
                'success' =>'درخواست با موفقیت ارسال شد :)'
            ]);
    }
    public function getmoney()
    {
        $value = Order::where('coach_id',Auth::guard('coach')->user()->coach_id)
                ->where('payment_state','unpaid')
                ->where('program_state','sent')->sum('price');

        $this->share();
        return view('coach.getmoney',compact('value'));
    }
    public function ordersend(Order $order , $type , $ordertype)
    {
        $this->share();
        if ($ordertype == 'checkbox'){
            $movements = Movement::all();
            $program_exercise =Program::where('order_id',$order->order_id)->where('program_type' , 'exercise')->first();
            return view('coach.ordercheckbox', compact(['movements','type','order','program_exercise']));
        }
        if ($ordertype == 'image'){
            $images=[];
            if (file_exists(public_path().'/bodybuilding/coach/images/programs/'.$type.'/'.$order->order_id.'/'.$order->order_date)){
                $images = array_diff(scandir(public_path().'/images/programs/'.$type.'/'.$order->order_id.'/'.$order->order_date), array('..', '.'));
            }

//            dd($images);
            return view('coach.orderimage', compact(['order','type','images']));
        }else{
//برای نشان دادن برنامه
            $program_exercise =Program::where('order_id',$order->order_id)->where('program_type' , 'exercise')->first();
            $program_food =Program::where('order_id',$order->order_id)->where('program_type' , 'food')->first();
            $program_supplement =Program::where('order_id',$order->order_id)->where('program_type' , 'supplement')->first();

//            $exercises = Exercise::where('program_id' , $program_exercise->program_id)->get();
//            $foods = Food::where('program_id' , $program_food->program_id)->get();
//            $supplements = Supplement::where('program_id' , $program_supplement->program_id)->get();
            return view('coach.ordertext', compact(['order','type','program_exercise','program_food', 'program_supplement']));

        }
    }
    public function postordersend(Request $request , Order $order , $type , $ordertype)
    {
        $this->share();
        $program='';
        if($request->program_exercise_update == 'no' || $request->program_food_update =='no' || $request->program_supplement_update =='no' ){
            $program = new Program();
            $program->user_id = $order->user_id;
            $program->order_id = $order->order_id;
            $program->coach_id = Auth::guard('coach')->user()->coach_id;
            $program->program_type = $type;
            $Day = Jalalian::now()->getDay();
            if (Jalalian::now()->getDay()<10){
                $Day='0'.Jalalian::now()->getDay();
            }
            $Month=Jalalian::now()->getMonth();
            if (Jalalian::now()->getMonth()<10){
                $Month='0'.Jalalian::now()->getMonth();
            }
            $program->program_date =  Jalalian::now()->getYear().'/'.$Month.'/'.$Day;
            $program->title = $request->title;
            $program->period = $request->period;
            $program->description = $request->description;
            $program->save();
        }else{
            if (isset($request->program_exercise_update)){
                $program = Program::where('program_id',$request->program_exercise_update)->first();
                $program->title = $request->title;
                $program->period = $request->period;
                $program->description = $request->description;
                $program->save();
            }
            if (isset($request->program_food_update)){
                $program = Program::where('program_id',$request->program_food_update)->first();
                $program->title = $request->title;
                $program->period = $request->period;
                $program->description = $request->description;
                $program->save();
            }
            if (isset($request->program_supplement_update)){
                $program = Program::where('program_id',$request->program_supplement_update)->first();
                $program->title = $request->title;
                $program->period = $request->period;
                $program->description = $request->description;
                $program->save();
            }
        }


           if ($type =='exercise'){
//            if ($ordertype == 'checkbox'){
////                dd($request->all());
//                for ($i = 0 ; $i < count($request->name) ;$i ++){
//                    $exercise = new Exercise();
//                    $exercise->program_id = $program->program_id;
//                    $exercise->type_of_movement = $request->name[$i];
//                    $exercise->days =             $request->day[$i];
//                    $exercise->sets =             $request->set[$i];
//                    $exercise->repetitions =      $request->repetitions[$i];
//                    $exercise->exercise_systems = $request->exercise_systems[$i];
//                    $exercise->save();
//                }
//                return redirect()->back()->with([
//                    'success' =>'برنامه با موفقیت ارسال شد'
//                ]);
//            }
//               dd($request->all());
               if (isset($request->name)){
//                   dd($request->all());
                   for ($i = 0 ; $i < count($request->name) ;$i ++){
                        $exercise = new Exercise();
                        $exercise->program_id = $program->program_id;
                        $exercise->type_of_movement = $request->name[$i];
                        $exercise->days =             $request->day[$i];
                        $exercise->sets =             $request->set[$i];
                        $exercise->repetitions =      $request->repetitions[$i];
                        $exercise->exercise_systems = $request->exercise_systems[$i];
                        $exercise->save();
                   }
           }
//           foreach ($request->movement_name as $item){
//               echo $item;
//           }

           return redirect()->back()->with([
               'success' =>'برنامه با موفقیت ارسال شد'
           ]);
           }
           if ($type=='food'){
               if (isset($request->meal)){
               for ($i = 0 ; $i < count($request->meal) ;$i ++){
                   $food = new Food();
                   $food->program_id = $program->program_id;
                   $food->meal = $request->meal[$i];
                   $food->offer1 = $request->offer1[$i];
                   $food->offer2 = $request->offer2[$i];
                   $food->offer3 = $request->offer3[$i];
                   $food->save();
               }
               return redirect()->back()->with([
                   'success' =>'برنامه با موفقیت ارسال شد'
               ]);
           }}
           if ($type=='supplement'){
               if (isset($request->supplement_name)){
               for ($i = 0 ; $i < count($request->supplement_name) ;$i ++){
                   $supplement = new Supplement();
                   $supplement->program_id = $program->program_id;
                   $supplement->supplement_name = $request->supplement_name[$i];
                   $supplement->time_to_eat = $request->time_to_eat[$i];
                   $supplement->dosage = $request->dosage[$i];
                   $supplement->day_to_eat = $request->day_to_eat[$i];

                   $supplement->save();
               }
               return redirect()->back()->with([
                   'success' =>'برنامه با موفقیت ارسال شد'
               ]);
           }}
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->share();
        $posts = \App\Post::orderBy('post_id', 'desc')->paginate(3);
        return view('coach.home',compact('posts'));
    }

    public function test()
    {
        $this->share();
        return view('coach.test');
    }

    public function myusers()
    {
        $users = \App\User::where('coach_id' , Auth::guard('coach')->user()->coach_id)->paginate(4);
        $this->share();
//        View::share('coach' , Auth::guard('coach')->user()->coach_name.'  '.Auth::guard('coach')->user()->coach_family);
//       $users=[];
        return view('coach.myusers',compact('users'));
    }

    public function myusersdetail(User $user)
    {
        $this->share();
        $user_id= $user->user_id;
        $coach_id = Auth::guard('coach')->user()->coach_id;

        $program_date_exercise =Order::where(
            'user_id' , $user_id)->where(
            'coach_id' ,$coach_id)->where(
            'exercise' ,'yes')->where(
            'user_payment_state','paid')->get();
       $program_date_food =Order::where(
            'user_id' , $user_id)->where(
            'coach_id' ,$coach_id)->where(
            'food' ,'yes')->where(
           'user_payment_state','paid')->get();
       $program_date_supplement =Order::where(
            'user_id' , $user_id)->where(
            'coach_id' ,$coach_id)->where(
            'supplement' ,'yes')-> where(
            'user_payment_state','paid')->get();
             $text_exercise=[];
             $text_exercise_program=[];
        if (count($program_date_exercise)){
         foreach ($program_date_exercise as $item){
            if ($item->exercise_type == 'text'){
            $program = Program::where('coach_id' , Auth::guard('coach')->user()->coach_id)->
//
            where('user_id' ,$item->user_id)->
                                where('program_type' , 'exercise')->first();
//            dd($program);
            if (is_array($program) && count($program)){
                $exercise=Exercise::where('program_id',$program->program_id)->get();
                $text_exercise =Arr::prepend($text_exercise ,$exercise );
            }
//dd($text_exercise);
            $text_exercise_program =Arr::prepend($text_exercise_program ,$program );
           }
             }
        }
        return view('coach.myusersdetail',compact(['user',
            'text_exercise',
            'text_exercise_program',
            'program_date_food',
            'program_date_exercise',
            'program_date_supplement'
       ]));
    }

    public function myusersdetailfood(User $user , $y,$m ,$d)
    {
        $this->share();
        $programs = Program::where('user_id' ,$user->id)->
            where('program_type' ,'food')->where('program_date',$y.'/'.$m.'/'.$d)->get();
        return view('coach.myusersdetailprogram' ,compact('programs'));
    }

    public function myusersdetailexercise(User $user , $y,$m ,$d)
    {
        $this->share();
        $programs = Program::where('user_id' ,$user->id)->
            where('program_type' ,'exercise')->where('program_date',$y.'/'.$m.'/'.$d)->get();
        return view('coach.myusersdetailprogram' ,compact('programs'));
    }
    public function myusersdetailsupplement(User $user , $y,$m ,$d)
    {
        $this->share();
        $programs = Program::where('user_id' ,$user->id)->
            where('program_type' ,'supplement')->where('program_date',$y.'/'.$m.'/'.$d)->get();
        return view('coach.myusersdetailprogram' ,compact('programs'));
    }

    public function myorders()
    {
        $this->share();
        $orders = Order::where('coach_id' ,Auth::guard('coach')->user()->coach_id)->
            where('user_payment_state' , 'paid') ->orderBy('order_id', 'desc')->get();
        $array_user_name = [];
        $array_user_last_name = [];
//        dd(Auth::guard('coach')->user()->id);
        foreach ($orders as $order){
            $user = User::where('user_id',$order->user_id)->get();
           foreach ($user as $item){
//               $array_user_name = Arr::prepend($array_user_name, $item->user_name);
//               $array_user_last_name = Arr::prepend($array_user_last_name, $item->user_family);
               $order['firstname'] = $item->user_name;
               $order['lastname'] = $item->user_family;
               $order['user_id'] = $item->user_id;
           }
        }
        return view('coach.myorders',compact(['orders']));
    }

    public function order(Order $order)
    {
        $this->share();
//        if ($order->program_state == 'sent'){
//            return redirect()->back()->with([
//                'error' =>'برنامه قبلا ارسال شده است.'
//            ]);
//        }
        $order->order_date =  str_replace("/","_", $order->order_date);
        $order->save();
        $program = Program::where('order_id' , $order->order_id)->first();
//        $program_exist= 'not_exist';
//        if (count($program)>0){
//            $program_exist = 'exist';
//        }
//        $user = User::where('id',$order->user_id)->first();
        return view('coach.order',compact(['order','program']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function post($id)
    {
        $this->share();
        $post = Post::where('post_id',$id)->get();
//        dd($post);
        return view('coach.post',compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function myprof()
    {
        $this->share();
        $user = Coach::where('coach_id' , Auth::guard('coach')->user()->coach_id)->first();
        return view('coach.prof',compact('user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\coach  $coache
     * @return \Illuminate\Http\Response
     */
    public function show(coach $coache)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\coach  $coache
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->share();
        $Coach= Coach::where('coach_id' , $id)->first();
        return view('coach.editprof',compact('Coach'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\coach  $coache
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'coach_name' => 'required',
            'coach_family' => 'required',
            'coach_honors' => 'required',
            'coach_age' => 'required',
            'coach_state' => 'required',
//            'coach_card_number' => 'required',
//            'coach_shaba_number' => 'required',
            'coach_mobile' => 'required',
//            'email' => 'required',
//            'coach_instagram' => 'required',
        ]);
        $coach = Coach::find($id);
        $coach->coach_name                    = $request->coach_name;
        $coach->coach_family                    = $request->coach_family;
        $coach->coach_honors                   = $request->coach_honors;
        $coach->coach_age                    = $request->coach_age;
        $coach->coach_state                 = $request->coach_state;
        $coach->coach_card_number             = $request->coach_card_number;
        $coach->coach_shaba_number                 = $request->coach_shaba_number;
        $coach->coach_mobile         = $request->coach_mobile;
//        $coach->email                    = $request->email;
        $coach->coach_instagram                   = $request->coach_instagram;

        $coach->save();

        // redirect
//            \Session::flash('message', 'Successfully updated nerd!');
        return redirect()->back()->with([
            'success' =>'با موفقیت تغییرات ثبت شد']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\coach  $coache
     * @return \Illuminate\Http\Response
     */
    public function destroy(coach $coache)
    {
        //
    }
}
