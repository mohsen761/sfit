<?php

namespace App\Http\Controllers;

use App\coach;
use App\Movement;
use Morilog\Jalali\Jalalian;
use App\Exercise;
use App\Food;
use App\Post;
use App\Price;
use App\Program;
use App\Supplement;
use App\User;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use View;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Hekmatinasser\Verta\Verta;

class StudentController extends Controller{

    public function __construct()
    {
        $this->middleware('user');
    }

    public function freeprogramsshow($type , $id)
    {
        $this->share();
        if ($type =='exercise'){
            $program = Program::where('program_id' , $id )->first();
            $exercises = Exercise::where('program_id' , $id)->get();
            return view('user.textexercise',compact('program','exercises'));
//            return view('user.freeprogramexercise');
        }
        if ($type =='food'){
            $program = Program::where('program_id' , $id )->first();
            $foods = Food::where('program_id' , $id)->get();
            return view('user.textfood',compact('program','foods'));
        }
        if ($type == 'supplement'){
            $program = Program::where('program_id' , $id )->first();
            $supplements = Supplement::where('program_id' , $id)->get();
            return view('user.textsupplement',compact('program','supplements'));
        }
    }

    public function freeprogramstype($type)
    {
        $this->share();
        $programs = Program::all();


        if ($type =='exercise'){
            $programs = Program::where('program_type' , 'exercise')->where('order_id' , null)->get();
        }
        if ($type =='food'){
            $programs = Program::where('program_type' , 'food')->get();
        }
        if ($type == 'supplement'){
            $programs = Program::where('program_type' , 'supplement')->get();
        }
         return view('user.freeprogramtype',compact('programs'));
    }

    public function freeprograms()
    {
        $this->share();
        return view('user.freeprograms');
    }

    public function showmovement($id)
    {
        $this->share();
        $item = Movement::find($id);
         return view('user.showmovement' , compact('item'));
    }
    public function movements()
    {
        $this->share();
        $movements = Movement::paginate(10);

        return view('user.movements',compact('movements'));
    }

    public function success()
    {
        $this->share();
        return view('user.success');
    }

    public function failed()
    {
        $this->share();
        return view('user.failed');
    }

    public function exercise($id)
    {
        $this->share();
        $order = Order::where('order_id' ,$id )->first();
        if ($order->exercise_type == 'text'){
            $program = Program::where('order_id' , $order->order_id)->where('program_type' , 'exercise')->first();
            $exercises = Exercise::where('program_id' , $program->program_id)->get();
            return view('user.textexercise',compact('program','exercises'));
        }else{
//            bodybuilding/coach/images/programs/exercise/$id
            return view('user.imageexercise',compact('id'));
        }
    }

   public function food($id)
    {
        $this->share();
        $order = Order::where('order_id' ,$id )->first();
        if ($order->food_type == 'text'){
            $program = Program::where('order_id' , $order->order_id)->where('program_type' , 'food')->first();
            $foods = Food::where('program_id' , $program->program_id)->get();
            return view('user.textfood',compact('program','foods'));
        }else{
            return view('user.imagefood',compact('id'));
        }
    }

   public function supplement($id)
    {
        $this->share();
        $order = Order::where('order_id' ,$id )->first();
        if ($order->supplement_type == 'text'){
            $program = Program::where('order_id' , $order->order_id)->where('program_type' , 'supplement')->first();
            $supplements = Supplement::where('program_id' , $program->program_id)->get();
            return view('user.textsupplement',compact('program','supplements'));
        }else{
            return view('user.imagesupplement',compact('id'));
        }
    }

    public function myprograms()
    {
        $this->share();
        $orders = Order::where('user_id' ,Auth::guard('user')->user()->user_id )->where('program_state','sent')->orderBy('order_id', 'desc')->get();
        return view('user.myprograms' , compact('orders'));
    }

    public function share()
    {
        $user=[ Auth::guard('user')->user()->user_name.'  '.Auth::guard('user')->user()->user_family,
            '/bodybuilding/student/images/UsersImagesProfile/'.Auth::guard('user')->user()->user_id.'.png'
        ];
        return View::share('user' ,$user);
    }

    public function myorders()
    {
        $this->share();
        $orders = Order::where('user_id' ,Auth::guard('user')->user()->user_id )->get();
        return view('user.myorders' ,compact('orders'));
    }

    public function setgoal(Request $request)
    {
//        dd($request->all());
        $user = User::where('user_id' , Auth::guard('user')->user()->user_id)->first();
        $order = new Order();
        $order->user_id= Auth::guard('user')->user()->user_id;
        $order->coach_id= $user->coach_id;
        if (isset($request->exercise_price)){
            $order->exercise = 'yes';
        }
        if (isset($request->food_price)){
            $order->food = 'yes';
        }
        if (isset($request->supplement_price)){
            $order->supplement = 'yes';
        }
        $order->goal = $request->goal;
        $order->price = $request->price;
        $order->exercise_place = $request->exercise_place;
        $order->payment_state = 'unpaid';
        $order->program_state = 'not_sent';
        $order->user_payment_state = 'unpaid';
        $order->new_order ='yes';
        $Day = Jalalian::now()->getDay();
        if (Jalalian::now()->getDay()<10){
            $Day='0'.Jalalian::now()->getDay();
        }
        $Month=Jalalian::now()->getMonth();
        if (Jalalian::now()->getMonth()<10){
            $Month='0'.Jalalian::now()->getMonth();
        }
        $order->order_date = Jalalian::now()->getYear().'/'.$Month.'/'.$Day;
//        dd($order->order_date );
        $order->save();
        return redirect()->back()->with([
            'success' =>'سفارش شما با موفقیت ثبت شد لطفا برای پرداخت به صفحه ی سفارشات من مراجعه نمایید.'
        ]);
    }

    public function neworder()
    {
        $this->share();
        $user_id =Auth::guard('user')->user()->user_id;
        $user = User::where('user_id' , $user_id)->first();
        if (!isset($user->coach_id)) return redirect()->back()->with([
        'error' =>'لطفا ابتدا از منوی انتخاب مربی ،مربی مورد نظر خود را انتخاب نمایید .'
            ]);
        if (!isset($user->user_family)) return redirect()->back()->with([
            'error' =>'لطفا ابتدا از منوی پروفایل،مشخصات خودرا تکمیل نمایید .'
        ]);
        $price=Price::where('coach_id' , $user->coach_id)->first();

        return view('user.neworder',compact('price'));

    }

    public function selectthiscoach($id )
    {
        $user_id =Auth::guard('user')->user()->user_id;
        $user = User::where('user_id' , $user_id)->first();
        $user->coach_id = $id ;
        $user->save();
        return redirect()->back()->with([
            'success' =>'مربی مورد نظر با موفقیت برای شما انتخاب شد .میتوانید برای درخواست برنامه از منوی ایجاد درخواست استفاده نمایید.'
        ]);
    }
    public function coachdetail($id)
    {
        $this->share();
        $coach =\App\Coach::where('coach_id' , $id)->first();
        $price = Price::where('coach_id' , $id)->first();
        return view('user.coachdetail',compact('coach','price'));
    }
    public function selectcoach()
    {
        $this->share();
        $coaches = \App\Coach::where('coach_visibility','show')->get();
        return view('user.selectcoach',compact('coaches'));
    }
    public function edit($id)
    {
        $this->share();
        $User= User::where('user_id' , $id)->first();

        return view('user.editprof',compact('User'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\coach  $coache
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'user_name' => 'required',
            'user_family' => 'required',
            'user_sex' => 'required',
            'user_age' => 'required',
            'user_city' => 'required',
//            'coach_card_number' => 'required',
//            'coach_shaba_number' => 'required',
            'user_mobile' => 'required',
//            'email' => 'required',
//            'coach_instagram' => 'required',
        ]);
        $coach = User::find($id);
        $coach->user_name                    = $request->user_name;
        $coach->user_family                    = $request->user_family;
        $coach->user_sex                   = $request->user_sex;
        $coach->user_age                    = $request->user_age;
        $coach->user_height                 = $request->user_height;
        $coach->user_weight             = $request->user_weight;
        $coach->user_record                 = $request->user_record;
        $coach->user_city         = $request->user_city;
//        $coach->email                    = $request->email;
        $coach->user_job                   = $request->user_job;
        $coach->user_waist                   = $request->user_waist;
        $coach->user_address                   = $request->user_address;
        $coach->user_arm                   = $request->user_arm;
        $coach->user_neck                   = $request->user_neck;
        $coach->user_thigh                   = $request->user_thigh;
        $coach->user_leg                   = $request->user_leg;
        $coach->user_chest                   = $request->user_chest;
        $coach->user_taste                   = $request->user_taste;
        $coach->user_cigarette                   = $request->user_cigarette;
        $coach->user_sleep                   = $request->user_sleep;
        $coach->user_stress                   = $request->user_stress;
        $coach->user_blood                   = $request->user_blood;
        $coach->user_disease                   = $request->user_disease;
        $coach->user_other_info                   = $request->user_other_info;
        $coach->user_mobile                   = $request->user_mobile;
        $coach->user_telegram_id                   = $request->user_telegram_id;

        $coach->save();

        // redirect
//            \Session::flash('message', 'Successfully updated nerd!');
        return redirect()->back()->with([
            'success' =>'با موفقیت تغییرات ثبت شد']);
    }

    public function myprof()
    {
        $this->share();
        $User = User::where('user_id' , Auth::guard('user')->user()->user_id)->first();

        return view('user.prof',compact('User'));
    }

    public function index()
    {
        $this->share();
        $posts = \App\Post::orderBy('post_id', 'desc')->paginate(3);
        return view('user.home',compact('posts'));
    }
    public function post($id)
    {
        $this->share();
        $post = Post::where('post_id',$id)->get();
//        dd($post);
        return view('user.post',compact('post'));
    }
    public function changepassword()
    {
        $this->share();
        return view('user.changepassword');
    }
    public function postchangepassword(Request $request)
    {
        $this->share();
        $request_data = $request->All();
        $validator = $this->admin_credential_rules($request_data);
        if($validator->fails())
        {
            return redirect()->back()->with([
                'new-password'=>'رمز جدید را به صورت درست وارد کنید'
            ]);
        }
        else
        {
            $current_password = Auth::guard('user')->user()->password;
            if(Hash::check($request_data['current-password'], $current_password))
            {
                $user_id = Auth::guard('user')->user()->user_id;
                $obj_user = User::find($user_id);
                $obj_user->password = Hash::make($request_data['password']);;
                $obj_user->save();

                return redirect()->back()->with([
                    'success' =>'رمز با موفقیت تغییر یافت'
                ]);
            }
            else
            {
                return redirect()->back()->with([
                    'current-password'=>'رمز فعلی را به صورت درست وارد کنید'
                ]);
            }
        }

    }
    public function admin_credential_rules(array $data)
    {
        $messages = [
            'current-password.required' => 'لطفا رمز فعلی را وارد کنید',
            'password.required' => 'رمز را وارد کنید',
        ];

        $validator = Validator::make($data, [
            'current-password' => 'required',
            'password' => 'required|same:password',
            'password_confirmation' => 'required|same:password',
        ], $messages);

        return $validator;
    }
}