<?php

namespace App\Http\Controllers\AuthCoach;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('coach')->except('logout');

    }
    public function showLoginForm()
    {
        return view('authCoach.login');
    }

    public function login(Request $request)
    {
        $this->validate($request,[
            'coach_email' =>'required|email',
            'coach_password' =>'required'
        ]);
        $credential =[
            'email' =>$request->coach_email ,
            'password' =>$request->coach_password
        ];
//            $coach=\App\Coach::all()->count();
            $coach=\App\Coach::Where('coach_email' ,$request->coach_email)->first();
           if (count($coach) >0){
               if($coach->coach_password == $request->coach_password){
                   Auth::guard('coach')->login($coach);
//                  dd( Auth::guard('coach')->attempt($credential,false));
                   return redirect()->route('coach.home');
               }
           }

//         if(Auth::guard('coach')->check($coach)){
//             return redirect()->intended(route('coach.home'));
//             return redirect()->route('coach.home');
//         }
         return redirect()->back()->withInput($request->only('user_email','remember'));
    }
}
