<?php

namespace App\Http\Controllers;

use App\Order;
use Morilog\Jalali\Jalalian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use DB;
use App\Movement;
use Hekmatinasser\Verta\Verta;

class ImageUploadController extends Controller

{
    public function uploaddocimage(Request $request)
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $coach_id= Auth::guard('coach')->user()->coach_id;
        $path = '/bodybuilding/coach/images/evidence/'.$coach_id;
        $imageName= time().'.png';
        request()->image->move($path, $imageName);
        return back()
            ->with('doc','عکس شما با موفقیت آپلود شد');

    }
    public function uploadcoachimage(Request $request)
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $coach_id= Auth::guard('coach')->user()->coach_id;
        $path = public_path().'/bodybuilding/coach/images/coachesImages/'.$coach_id;
//        $imageName= time().'.png';
        $imageNumber = 0;
        for ($i = 0 ;$i <3 ; $i++){
            if (!file_exists($path.'/'.$i.'.png')){
                $imageNumber = $i;
                break;
            }else{
                $imageNumber = $i;
            }
        }
        $imageName= $imageNumber.'.png';
        request()->image->move($path, $imageName);
        return back()
            ->with('coachimage','عکس شما با موفقیت آپلود شد');

    }
    public function imageuploadf(Request $request)
    {
        $Day = Jalalian::now()->getDay();
        if (Jalalian::now()->getDay()<10){
            $Day='0'.Jalalian::now()->getDay();
        }
        $Month=Jalalian::now()->getMonth();
        if (Jalalian::now()->getMonth()<10){
            $Month='0'.Jalalian::now()->getMonth();
        }
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if(\Request::route()->getName() == "image.upload.f1"){
           $user_id= Auth::guard('user')->user()->user_id;
            $path = 'bodybuilding/student/images/joft_bazu_az_jolo/'.$user_id;
            $imageName= Jalalian::now()->getYear().'_'.$Month.'_'.$Day.'.png';
            request()->image->move($path, $imageName);
            return back()
                ->with('f1','عکس شما با موفقیت آپلود شد')
                ->with('f1image','/'.$path.'/'.$imageName);
        };
      if(\Request::route()->getName() == "image.upload.f2"){
          $Day = Jalalian::now()->getDay();
          if (Jalalian::now()->getDay()<10){
              $Day='0'.Jalalian::now()->getDay();
          }
          $Month=Jalalian::now()->getMonth();
          if (Jalalian::now()->getMonth()<10){
              $Month='0'.Jalalian::now()->getMonth();
          }
           $user_id= Auth::guard('user')->user()->user_id;
            $path = 'bodybuilding/student/images/joft_bazu_az_posht/'.$user_id;
            $imageName= Jalalian::now()->getYear().'_'.$Month.'_'.$Day.'.png';
            request()->image->move($path, $imageName);
            return back()
                ->with('f2','عکس شما با موفقیت آپلود شد')
                ->with('f2image','/'.$path.'/'.$imageName);
        };
      if(\Request::route()->getName() == "image.upload.f3"){
          $Day = Jalalian::now()->getDay();
          if (Jalalian::now()->getDay()<10){
              $Day='0'.Jalalian::now()->getDay();
          }
          $Month=Jalalian::now()->getMonth();
          if (Jalalian::now()->getMonth()<10){
              $Month='0'.Jalalian::now()->getMonth();
          }
           $user_id= Auth::guard('user')->user()->user_id;
            $path = 'bodybuilding/student/images/zir_baghal_az_posht/'.$user_id;
            $imageName= Jalalian::now()->getYear().'_'.$Month.'_'.$Day.'.png';
            request()->image->move($path, $imageName);
            return back()
                ->with('f3','عکس شما با موفقیت آپلود شد')
                ->with('f3image','/'.$path.'/'.$imageName);
        };
        if(\Request::route()->getName() == "image.upload.f4"){
            $Day = Jalalian::now()->getDay();
            if (Jalalian::now()->getDay()<10){
                $Day='0'.Jalalian::now()->getDay();
            }
            $Month=Jalalian::now()->getMonth();
            if (Jalalian::now()->getMonth()<10){
                $Month='0'.Jalalian::now()->getMonth();
            }
           $user_id= Auth::guard('user')->user()->user_id;
            $path = 'bodybuilding/student/images/posht_bazu_az_baghal/'.$user_id;
            $imageName= Jalalian::now()->getYear().'_'.$Month.'_'.$Day.'.png';
            request()->image->move($path, $imageName);
            return back()
                ->with('f4','عکس شما با موفقیت آپلود شد')
                ->with('f4image','/'.$path.'/'.$imageName);
        };
        if(\Request::route()->getName() == "image.upload.f5"){
            $Day = Jalalian::now()->getDay();
            if (Jalalian::now()->getDay()<10){
                $Day='0'.Jalalian::now()->getDay();
            }
            $Month=Jalalian::now()->getMonth();
            if (Jalalian::now()->getMonth()<10){
                $Month='0'.Jalalian::now()->getMonth();
            }
           $user_id= Auth::guard('user')->user()->user_id;
            $path = 'bodybuilding/student/images/ghafase_sine/'.$user_id;
            $imageName=Jalalian::now()->getYear().'_'.$Month.'_'.$Day.'.png';
            request()->image->move($path, $imageName);
            return back()
                ->with('f5','عکس شما با موفقیت آپلود شد')
                ->with('f5image','/'.$path.'/'.$imageName);
        };
        if(\Request::route()->getName() == "image.upload.f6"){
            $Day = Jalalian::now()->getDay();
            if (Jalalian::now()->getDay()<10){
                $Day='0'.Jalalian::now()->getDay();
            }
            $Month=Jalalian::now()->getMonth();
            if (Jalalian::now()->getMonth()<10){
                $Month='0'.Jalalian::now()->getMonth();
            }
           $user_id= Auth::guard('user')->user()->user_id;
            $path = 'bodybuilding/student/images/shekam_va_paha_az_jolo/'.$user_id;
            $imageName= Jalalian::now()->getYear().'_'.$Month.'_'.$Day.'.png';
            request()->image->move($path, $imageName);
            return back()
                ->with('f6','عکس شما با موفقیت آپلود شد')
                ->with('f6image','/'.$path.'/'.$imageName);
        };
        if(\Request::route()->getName() == "image.upload.f7"){
            $Day = Jalalian::now()->getDay();
            if (Jalalian::now()->getDay()<10){
                $Day='0'.Jalalian::now()->getDay();
            }
            $Month=Jalalian::now()->getMonth();
            if (Jalalian::now()->getMonth()<10){
                $Month='0'.Jalalian::now()->getMonth();
            }
           $user_id= Auth::guard('user')->user()->user_id;
            $path = 'bodybuilding/student/images/nimrokh/'.$user_id;
            $imageName= Jalalian::now()->getYear().'_'.$Month.'_'.$Day.'.png';
            request()->image->move($path, $imageName);
            return back()
                ->with('f7','عکس شما با موفقیت آپلود شد')
                ->with('f7image','/'.$path.'/'.$imageName);
        };

    }

    public function figureimage(Request $request)
    {
//        print_r( $request->id);
      $orders=  Order::where('coach_id',Auth::guard('coach')->user()->coach_id)->
            where('user_id',$request->id)->get();
        $output = '<ul class="dropdown-menu" style="box-shadow: 5px 10px #888888;display:grid; position:relative;">';
        $text="مشاهده عکس های فیگوری در تاریخ :";
        foreach($orders as $row)
        {
            $output .= '
                      <li style=";text-align: right"><a id="modalfigure" data-toggle="modal" class="btn" href="#modal_detail" name="'.$row->order_date.'">'.$text.str_replace("_","/", $row->order_date).' </a></li>
                       ';
//            $output .= '
//                      <li style="text-align: right"><a role="button" class="btn" href="#">'.$text.str_replace("_","/", $row->order_date).' </a></li>
//                       ';
        }
        $output .= '</ul>';

        return $output;
    }
    public function uploadgymimage(Request $request)
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $gymid=$request->gymid;

        $path = public_path().'/bodybuilding/gym/images/gymImage/'.$gymid;
        for ($i = 0 ;$i <3 ; $i++){
            if (!file_exists($path.'/'.$i.'.png')){
                $imageNumber = $i;
                break;
            }else{
                $imageNumber = $i;
            }
        }
        $imageName = $imageNumber.'.png';
//        $imageName = $coachId.'.'.request()->image->getClientOriginalExtension();
        request()->image->move($path, $imageName);
        return back()
            ->with('gym','عکس شما با موفقیت آپلود شد')
            ->with('gymimage','/bodybuilding/gym/images/gymImage/'.$gymid.'/'.$imageName);
    }
    public function fetch(Request $request)
    {
        if($request->get('query'))
        {
            $query = $request->get('query');
            $data = Movement::where('movement_name', 'LIKE', "%{$query}%")
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach($data as $row)
            {
                $output .= '
                      <li style="text-align: right"><a role="button" class="btn btn-lg" href="#">'.$row->movement_name.'</a></li>
                       ';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    public function imagedelete($type ,Order $order , $id , $image )
    {
        if (is_file(public_path().'/bodybuilding/images/programs/'.$type.'/'.$order->order_id.'/'.$order->order_date.'/'.$image))
        {
            unlink(public_path().'/images/programs/'.$type.'/'.$order->order_id.'/'.$order->order_date.'/'.$image);
            return back() ->with(['success' => ' برنامه با موفقیت حذف گردید .)']);
        }
        return back();
    }
    public function imageUpload()
    {
        return view('imageUpload');
    }

    public function deletecoachprofileimage()
    {
        if (is_file(public_path().'/bodybuilding/coach/images/coachesImagesProfile/'.Auth::guard('coach')->user()->coach_id.'.png'))
        {
            unlink(public_path().'/bodybuilding/coach/images/coachesImagesProfile/'.Auth::guard('coach')->user()->coach_id.'.png');
            return back() ->with(['profile' => ' پروفایل با موفقیت حذف گردید .)']);
        }
        return back();
    }
    public function uploadcoachprofileimage(Request $request)
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $coachId=Auth::guard('coach')->user()->coach_id;
        $path = public_path().'/bodybuilding/coach/images/coachesImagesProfile/';
        $imageName = $coachId.'.png';
//        $imageName = $coachId.'.'.request()->image->getClientOriginalExtension();
        request()->image->move($path, $imageName);
        return back()
            ->with('profile','عکس شما با موفقیت آپلود شد')
            ->with('profileimage','/bodybuilding/coach/images/coachesImagesProfile/'.$imageName);

    }
    public function deleteuserprofileimage()
    {
        if (is_file(public_path().'/bodybuilding/student/images/UsersImagesProfile/'.Auth::guard('user')->user()->user_id.'.png'))
        {
            unlink(public_path().'/bodybuilding/student/images/UsersImagesProfile/'.Auth::guard('user')->user()->user_id.'.png');
            return back() ->with(['profile' => ' پروفایل با موفقیت حذف گردید .)']);
        }
        return back();
    }
    public function uploaduserprofileimage(Request $request)
    {


        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $coachId=Auth::guard('user')->user()->user_id;
        $path = public_path().'/bodybuilding/student/images/UsersImagesProfile/';
        $imageName = $coachId.'.png';
//        $imageName = $coachId.'.'.request()->image->getClientOriginalExtension();
        request()->image->move($path, $imageName);
//        $output = array(
//            'profile' => 'عکس شما با موفقیت آپلود شد',
//            'profileimage'  => '/bodybuilding/student/images/UsersImagesProfile/'.$imageName
//        );
//
//        return response()->json($output);
        return back()
            ->with('profile','عکس شما با موفقیت آپلود شد')
            ->with('profileimage','/bodybuilding/student/images/UsersImagesProfile/'.$imageName);

    }
    public function imageUploadProgram(Request $request)
    {
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000',
        ]);
//        dd($request->all());
        $objFile = &$_FILES["image"];
        $imageNumber = $_POST['image_number'];
        $typeOfProgram = $_POST['type_of_program'];
        $orderId = $_POST['order_id'];
        $date = $_POST['date'];

//        $dir = "images/programs/$typeOfProgram/$orderId";
//        mkdir($dir);
//        $path = public_path().'/images/programs/'.$typeOfProgram.'/'.$orderId;
//        \File::makeDirectory($path, $mode = 0777, true, true);
//        $dir2 = "$dir/$date";
//
//        $path = public_path().'/images/programs/'.$typeOfProgram.'/'.$orderId.'/'.$date;
//        \File::makeDirectory($path, $mode = 0777, true, true);

        $path =public_path(). '/bodybuilding/coach/images/programs/'.$typeOfProgram.'/'.$orderId;
//        \File::makeDirectory($path, $mode = 0777, true, true);

        for ($i = 0 ;$i <9 ; $i++){
            if (!file_exists($path.'/'.$i.'.png')){
                $imageNumber = $i;
                break;
            }else{
                 $imageNumber = $i;
             }
        }
        $imageName = $imageNumber.'.png';
//        $imageName = $imageNumber.'.'.request()->image->getClientOriginalExtension();
        request()->image->move($path, $imageName);
        return back()
            ->with('success','برنامه با موفقیت آپلود شد')
            ->with('image','/bodybuilding/coach/images/programs/'.$typeOfProgram.'/'.$orderId.'/'.$imageName);
    }
}