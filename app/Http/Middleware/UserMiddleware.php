<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Illuminate\Support\Facades\Auth;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if(Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug
//            =='user') {
//            return $next($request);
//        } else{
//            return redirect('/');
//        }
        if (Auth::guard('user')->check()){
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
