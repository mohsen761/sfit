<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Sentinel;

class CoachMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if(Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug
//        =='coach') {
//            return $next($request);
//        } else{
//            return redirect('/');
//        }
//        dd(Auth::user());
        if (Auth::guard('coach')->check()){
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
