<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Working extends Model
{
    protected $table = 'working_days_times';
    protected $primaryKey = 'working_id';
}
